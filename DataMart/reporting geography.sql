SELECT distinct 
       Cty.StateAbbrev
     , RptRegionName = 'Code Me Level 1'
     , RptCounty = cty.CountyName + ' County'
	 , Area = 'Code Me using current Region info - Level 2'
	 , MLS.MLSCode
	 , Neighborhood = 'MLS Code Description'
	 , Luxury = ''
	 , Waterfront = ''

FROM BI_Reporting_STG.mls.Listing list
  LEFT JOIN (select CountyID, CountyName, StateAbbrev
             from BI_Reporting_STG.mls.County c
               left join BI_Reporting_STG.mls.[State] s
                 on c.StateID = s.StateID ) cty
    on list.CountyID = cty.CountyID
  LEFT JOIN (select MLSListingID, MLSCode, ma.MLSID, MLSAbbreviation
             from BI_Reporting_STG.mls.MLSAreas ma
               left join BI_Reporting_STG.mls.MLS m
                 on ma.mlsid = m.mlsid) mls
    on mls.MLSListingID = list.MLSListingID and mls.MLSID = list.MLSID


/*
SELECT 
	t1.ListingId,
	t1.MLSID,
	CASE 
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode IN ('500','510','520','530','540','550','560','600','948') THEN 'Eastside King'
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode IN ('170') THEN 'Bainbridge Island'
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode IN ('705','390','380','385','701','720','710','700','715','800','140') THEN 'Seattle'
		WHEN t1.CountyID=48 AND t4.StateName='Oregon' AND t1.City='Bend' THEN 'Bend'
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode IN ('100','110','120','130','360') THEN 'SW King County'
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode IN ('310','320','300','330','340','350') THEN 'SE King County'
		WHEN t2.MLSCode in('141','142','143','144','145','146','147','148','149','150','161','162','163','164','165','166','167','168') THEN 'Kitsap wo Bainbridge'
		WHEN t2.MLSCode in('1','2','3','4','5','6','7','8','9') THEN 'Gig Harbor'
		WHEN t1.StateID=43 AND t1.CountyID IN (31,115) THEN 'East Portland'
		WHEN t1.StateID=43  AND t1.CountyID IN (37,167) THEN 'West Portland'
		WHEN t2.MLSID=127 AND StateName='Idaho' AND t1.CountyID = 1 THEN 'Boise'
		WHEN t2.MLSID=91 AND StateName='Oregon' AND MLSCode = 147 THEN 'Lake Oswego'
		ELSE t3.CountyName + ' County'
	END AS Region,
	CASE
		WHEN t3.CountyID IN (1,2,4,5,6,9,10,11,12,14,15) THEN 'Luxury and Standard'
		WHEN t3.CountyID IN (4,7,8,13,16,17,19) THEN 'Standard'
		ELSE 'Luxury'
	END AS UpdateType,
	NULL AS Waterfront,
	t5.PropertyType
FROM 
	BI_Reporting_STG.mls.Listing t1
	LEFT JOIN [BI_Reporting_STG].[mls].[MLSAreas] t2
	ON t1.MLSID = t2.MLSID AND t1.MLSListingID = t2.MLSListingID
		LEFT JOIN [BI_Reporting_STG].[mls].County t3
		ON t1.CountyID = t3.CountyID
			LEFT JOIN [BI_Reporting_STG].[mls].[State] t4
			ON t3.StateID = t4.StateID
				LEFT JOIN BI_SL_STG..NewConstruction t5
				ON t1.ListingID = t5.ListingID
WHERE 
	(t4.StateName = 'Oregon' AND t3.CountyID IN(37,167,76,31,115,48)) OR
	(t4.StateName = 'Idaho' AND t3.CountyID IN(1,89)) OR
	(t4.StateName = 'Washington' AND t3.CountyID IN(77,84,85,127,146,147,153,168)) OR
	t2.MLSCode in('1','2','3','4','5','6','7','8','9'
					,'100','110','120','130','140','510','170','140','141','142','143','144','145','146','147','148','149','150','161','162','163','164','165','166','167','168','170'
					,'300','310','320','330','340','350','360','380','385','390'
					,'500','510','520','530','540','550','560','600'
					,'700','701','705','710','715','720','800','948')
UNION ALL

SELECT 
	t1.ListingId,
	t1.MLSID,
	CASE 
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode='520' THEN 'West Bellevue'
		WHEN t2.MLSID=9AND t4.StateName='Washington' AND MLSCode='510' THEN 'Mercer Island'
	END AS Region,
	'Luxury' AS UpdateType,
	NULL AS Waterfront,
	t5.PropertyType
FROM 
	BI_Reporting_STG.mls.Listing t1
	LEFT JOIN [BI_Reporting_STG].[mls].[MLSAreas] t2
	ON t1.MLSID = t2.MLSID AND t1.MLSListingID = t2.MLSListingID
		LEFT JOIN [BI_Reporting_STG].[mls].County t3
		ON t1.CountyID = t3.CountyID
			LEFT JOIN [BI_Reporting_STG].[mls].[State] t4
			ON t3.StateID = t4.StateID
				LEFT JOIN BI_SL_STG..NewConstruction t5
				ON t1.ListingID = t5.ListingID
WHERE 
	t2.MLSID=9 AND 
	t4.StateName='Washington' AND
	t2.MLSCode in('510','520')

UNION ALL

SELECT 
	t1.ListingId,
	t1.MLSID,
	CASE 
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode IN('380','390','510','520','710') THEN 'Lake Washington'
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode IN('530','540') THEN 'Lake Sammamish'
	END AS Region,
	'Luxury' AS UpdateType,
	'Waterfront' as Waterfront,
	t5.PropertyType
FROM 
	BI_Reporting_STG.mls.Listing t1
	LEFT JOIN [BI_Reporting_STG].[mls].[MLSAreas] t2
	ON t1.MLSID = t2.MLSID AND t1.MLSListingID = t2.MLSListingID
		LEFT JOIN [BI_Reporting_STG].[mls].County t3
		ON t1.CountyID = t3.CountyID
			LEFT JOIN [BI_Reporting_STG].[mls].[State] t4
			ON t3.StateID = t4.StateID
				LEFT JOIN BI_SL_STG..NewConstruction t5
				ON t1.ListingID = t5.ListingID
WHERE 
	t2.MLSID=9 AND 
	t4.StateName='Washington' AND
	t2.MLSCode in('380','390','510','520','710','530','540')

UNION ALL

SELECT 
	t1.ListingId,
	t1.MLSID,
	CASE 
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode IN('100','110','120','130','140','300','310','320','330','340','350','360','380','385','390','500','510','520','530','540','550','560','600','610','140','700','701','705','710','720','715','800','948') 
		THEN 'King County Waterfront'
	END AS Region,
	'Luxury' AS UpdateType,
	'Waterfront' as Waterfront,
	t5.PropertyType
FROM 
	BI_Reporting_STG.mls.Listing t1
	LEFT JOIN [BI_Reporting_STG].[mls].[MLSAreas] t2
	ON t1.MLSID = t2.MLSID AND t1.MLSListingID = t2.MLSListingID
		LEFT JOIN [BI_Reporting_STG].[mls].County t3
		ON t1.CountyID = t3.CountyID
			LEFT JOIN [BI_Reporting_STG].[mls].[State] t4
			ON t3.StateID = t4.StateID
				LEFT JOIN BI_SL_STG..NewConstruction t5
				ON t1.ListingID = t5.ListingID
WHERE
	t2.MLSID=9 AND 
	t4.StateName='Washington' AND
	t2.MLSCode IN('100','110','120','130','140','300','310','320','330','340','350','360','380','385','390','500','510','520','530','540','550','560','600','610','140','700','701','705','710','720','715','800','948')
*/