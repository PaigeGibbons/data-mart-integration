USE [BI_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[DropAndCreateDYNTables]    Script Date: 1/28/2020 2:22:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--create procedure LoadDynamicsDimensions as
--BEGIN

ALTER PROCEDURE [dbo].[DropAndCreateDYNTables] as
BEGIN

IF OBJECT_ID('dim.JLSOffice', 'U') IS NOT NULL
DROP TABLE dim.JLSOffice

CREATE TABLE dim.JLSOffice(
	JLSOfficeKey int IDENTITY(1,1) NOT NULL,
	BranchNo smallint NULL,
	CompanyAbbrev char(5) NULL,
	BranchName char(31) NULL,
	BranchStatus smallint NULL,
	BeginDate smalldatetime NULL,
	EndDate smalldatetime NULL,
	AddressLine1 char(41) NULL,
	AddressLine2 char(41) NULL,
	AddressCity char(21) NULL,
	AddressState char(3) NULL,
	AddressZip char(9) NULL,
	AddressCounty char(31) NULL,
	BranchPhone char(11) NULL,
	BranchFax char(11) NULL,
	BranchEmailAddress char(41) NULL,
	BranchArea int NULL,
	BranchReportRegionNo char(5) NULL,
 CONSTRAINT pk_JLSOffice PRIMARY KEY CLUSTERED (JLSOfficeKey ASC) )


 END