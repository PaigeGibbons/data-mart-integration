USE [BI_Reporting]
GO

/****** Object:  View [dbo].[MLS_VIEW_ONE]    Script Date: 1/29/2020 2:41:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


















ALTER view [dbo].[MLS_VIEW_ONE] as (

SELECT lad.StateCode
     , c.CountyName
     , lad.City
     , r.ReportingRegion
	 ,r.ReportingArea
	 ,r.ReportingSubArea
     , m.MLSID
     , f.MLSListingID
     , m.MLSName
     , ma.MLSAreaCode
     , ma.MLSAreaAbbrev
     , p.PropTypeName
     , ListDate = d1.[Date]
     , ListYear = d1.[Year]
     , ListMonth = d1.[Month]
     , ListMonthName = d1.[MonthName]
     , PendDate = d3.[Date]
	 ,case when d3.[Date] is NULL AND st.StatusCategoryName like 'Active' AND DATEDIFF(mm,d1.[Date],Getdate()) <=6  -- listed within 6 months and not pended use List date
		Then d1.[Date]
		When d3.[Date] is NULL AND st.StatusCategoryName like 'Active' AND DATEDIFF(mm,d1.[Date],Getdate()) > 6 --list date is older than 6 months add 6 months to list date
		Then DATEADD (m,6,d1.[Date])
		When d3.[Date] is Null 		Then d1.[Date] -- no pend date and not active = use list date
		else d3.[Date] End as Reportdate -- else use pend date
     , PendYear = d3.[Year]
     , PendMonth = d3.[Month]
     , PendMonthName = d3.[MonthName]
     , CloseDate = d2.[Date]
     , CloseYear = d2.[Year]
     , CloseMonth = d2.[Month]
     , CloseMonthName = d2.[MonthName]
     , PrimaryListingAgent = a.AgentName
	 , PrimaryListingAgentisJLS = case when a.JLSAgentKey is not null then 'Y' else 'N' end
     , CoListingAgent = a2.AgentName
	 , CoListingAgentisJLS = case when a2.JLSAgentKey is not null then 'Y' else 'N' end
     , PrimarySellingAgent = a1.AgentName
	 , PrimarySellingAgentisJLS = case when a1.JLSAgentKey is not null then 'Y' else 'N' end
     , CoSellingAgent = a3.AgentName
	 , CoSellingAgentisJLS = case when a3.JLSAgentKey is not null then 'Y' else 'N' end
     , PrimaryListingOffice = o.OfficeName
	 , PrimaryListingOfficeisJLS = case when o.JLSOfficeKey is not null 
	                                      and (o.OfficeName like '%JLS%' or o.OfficeName like '%John%L%Scott%')
										then 'Y' else 'N' end
     , CoListingOffice = o1.OfficeName
	 , CoListingOfficeisJLS = case when o1.JLSOfficeKey is not null
	                                 and (o1.OfficeName like '%JLS%' or o1.OfficeName like '%John%L%Scott%')
								   then 'Y' else 'N' end
     , SellingOffice = o2.OfficeName
	 , SellingOfficeisJLS = case when o2.JLSOfficeKey is not null
	 	                           and (o2.OfficeName like '%JLS%' or o2.OfficeName like '%John%L%Scott%')
								 then 'Y' else 'N' end
     , CoSellingOffice = o3.OfficeName
	 , CoSellingOfficeisJLS = case when o3.JLSOfficeKey is not null
	 	                             and (o3.OfficeName like '%JLS%' or o3.OfficeName like '%John%L%Scott%')
								   then 'Y' else 'N' end
     , f.OriginalListPrice
     , f.ListPrice
     , pr.PriceCategoryName
     , f.ClosePrice
     , st.StatusName
     , st.StatusCategoryName
     , ld.NewConstruction
	 , ld.Waterfront
     , f.DOM
     , f.CDOM
     , lad.Longitude
     , lad.Latitude
     , la.AgentBonus
     , la.Commission
     , la.CommissionCode
     , la.CommissionComments
     , la.SellingOfficeCommissionPercentage
     , Days_to_Sell_Category = CASE 
                                   WHEN DateDiff(day,d1.Date,d3.Date)<=0 THEN '0 - 30'
                                   WHEN DateDiff(day,d1.Date,d3.Date)>=0 AND DateDiff(day,d1.Date,d3.Date)<=30 THEN '0 - 30'
                                   WHEN DateDiff(day,d1.Date,d3.Date) >=31 AND DateDiff(day,d1.Date,d3.Date)<=60 THEN '31 - 60'
                                   WHEN DateDiff(day,d1.Date,d3.Date)>=61 AND DateDiff(day,d1.Date,d3.Date)<=90 THEN '61 - 90'
                                   WHEN DateDiff(day,d1.Date,d3.Date)>=91 AND DateDiff(day,d1.Date,d3.Date)<=120 THEN '91 - 120'
                                   WHEN DateDiff(day,d1.Date,d3.Date)>=121 AND DateDiff(day,d1.Date,d3.Date)<=150 THEN '121 - 150'
                                   WHEN DateDiff(day,d1.Date,d3.Date)>=151 AND DateDiff(day,d1.Date,d3.Date)<=180 THEN '151 - 180'
                                   WHEN DateDiff(day,d1.Date,d3.Date) > 180 THEN '180+/Not Sold/Expired/Off Market'
                                   ELSE '180+/Not Sold/Expired/Off Market' END

FROM fact.listing					f
  LEFT JOIN dim.ListingAgent		la  ON  f.ListingAgentKey = la.ListingAgentKey
  LEFT JOIN dim.Agent				a   ON  la.PrimaryListingAgentKey = a.AgentKey
  LEFT JOIN dim.Agent				a1  ON  la.SellingAgentKey = a1.AgentKey
  LEFT JOIN dim.Agent				a2  ON  la.CoListingAgentKey = a2.AgentKey
  LEFT JOIN dim.Agent				a3  ON  la.CoSellingAgentKey = a3.AgentKey
  LEFT JOIN dim.ListingDetail		ld  ON  f.ListingDetailKey = ld.ListingDetailKey
  LEFT JOIN dim.ListingAddress      lad ON  f.ListingAddressKey = lad.ListingAddressKey
  LEFT JOIN	dim.County				c   ON  lad.Countykey = c.Countykey     
  LEFT JOIN dim.RegionHierarchy		r   ON  f.RegionHierarchyKey	= r.RegionHierarchyKey
  LEFT JOIN dim.MLS					m   ON  f.MLSKey	= m.MLSKey			
  LEFT JOIN dim.MLSArea				ma  ON  f.MLSAreaKey = ma.MLSAreaKey
  LEFT JOIN dim.PropType			p   ON  f.PropTypeKey = p.PropTypeKey
  LEFT JOIN dim.DateDimension       d1  ON  f.ListDateKey = d1.DateKey
  LEFT JOIN dim.DateDimension       d2  ON  f.CloseDateKey = d2.DateKey
  LEFT JOIN dim.DateDimension       d3  ON  f.PendingDateKey = d3.DateKey
  LEFT JOIN dim.StatusType			st  ON  f.StatusTypeKey = st.StatusTypeKey
  LEFT JOIN dim.PriceCategory		pr  ON  f.PriceCategoryKey = pr.PriceCategoryKey
  LEFT JOIN dim.ListingOffice		lo  ON  f.ListingOfficeKey = lo.ListingOfficeKey
  LEFT JOIN dim.Office				o   ON  lo.PrimaryOfficeKey = o.OfficeKey
  LEFT JOIN dim.Office				o1  ON  lo.CoListingOfficeKey = o1.OfficeKey
  LEFT JOIN dim.Office				o2  ON  lo.SellingOfficeKey = o2.OfficeKey
  LEFT JOIN dim.Office				o3  ON  lo.CoSellingOfficeKey = o3.OfficeKey

  WHERE d1.[Date] > '2016-11-30'
    AND p.PropCategoryName in ('Single Family','Condominium')
    AND r.StateCode in ('WA','OR','ID')

)
GO


