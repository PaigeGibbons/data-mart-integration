USE [BI_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[DropAndCreateTables]    Script Date: 10/18/2019 8:13:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[DropAndCreateTables] as

BEGIN

/* Drop Foreign Key Constraints from fact Tables */
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_ListDateKey
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_UpdateDateKey
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_CloseDateKey
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_StatusDateKey
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_PendingDateKey

ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_ListingDetailKey
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_OfficeKey
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_AgentKey
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_RegionKey

/* DROP and CREATE Dimension Tables */
DROP TABLE dim.County

CREATE TABLE dim.County (
      CountyKey int identity(1,1)
    , CountyName varchar(100) null
	, StateCode varchar(5) null
	, CONSTRAINT pk_County PRIMARY KEY (CountyKey)
	)

DROP TABLE dim.PropType

CREATE TABLE dim.PropType (
      PropTypeKey int identity(1,1)
	, PropTypeName varchar(50)
	, PropCategoryName varchar(50)
	, MLSID int 
	, MLSCode varchar(50)
	, MLSTableName varchar(50)
	, CONSTRAINT pk_PropType PRIMARY KEY (PropTypeKey)
	)

DROP TABLE dim.MLS

CREATE TABLE dim.MLS (
      MLSKey int identity(1,1)
	, MLSID int
	, MLSCode varchar(50)
	, MLSName varchar(100)
	, MLSAbbreviation varchar(10)
	, MLSFullName varchar(50)
	, CONSTRAINT pk_MLS PRIMARY KEY (MLSKey)
	)


DROP TABLE dim.MLSArea

CREATE TABLE dim.MLSArea (
      MLSCodeKey int identity(1,1) not null
	, MLSID int NOT NULL
	, MLSCode varchar(50) NOT NULL
	, MLSCodeDescription varchar(100) NULL
	, MLSCodeAbbrev varchar(100) NULL
	, CONSTRAINT pk_MLSArea PRIMARY KEY (MLSCodeKey)
) 


DROP TABLE dim.Region

CREATE TABLE dim.Region (
	  RegionKey int identity(1,1)
    , ListingID int
    , MLSID int
    , Region varchar(100)
    , UpdateType varchar(100)
    , Waterfront varchar(100) 
    , PropertyType varchar(100)
    , CONSTRAINT pk_Region PRIMARY KEY(RegionKey)
	)


DROP TABLE dim.ListingDetail

CREATE TABLE dim.ListingDetail (
      ListingDetailKey int identity(1,1)
    , MLSListingID varchar(25)
    , MLSKey int
    , MLSID int
    , MLSCode varchar(50)
    , StatusKey int
    , PropTypeKey int
    , StreetAddress varchar(100)
    , StreetAddress2 varchar(100)
    , City varchar(50)
    , CountyKey int
    , StateCode varchar(6)
    , PostalCode varchar(10)
    , PostalCodeExt varchar(5)
    , Latitude decimal(9,6)
    , Longitude decimal(9,6)
    , BuildingName varchar(100)
    , Bedrooms smallint
    , BathsFull smallint
    , BathsPartial smallint
    , BathsTotal smallint
    , SquareFootage int
    , LotSizeSF int
    , Acreage decimal(9  ,  2)
    , GarageSpaces smallint
    , ParkingSpaces smallint
    , YearBuilt smallint
    , TaxYearBuilt smallint
    , TaxID varchar(50)
    , PropertyTax decimal(19  ,  2)
    , GeogLocation geography
    , ElementarySchool varchar(50)
    , MiddleSchool varchar(50)
    , HighSchool varchar(50)
    , SchoolDistrict varchar(50)
    , IDX tinyint
    , Fireplaces smallint
    , ParentPropTypeID int
    , BankOwned tinyint
    , ShortSale tinyint
    , CDOM int
    , DOM int
    , VirtualTour varchar(max)
    , VideoTour varchar(max)
    , ShowAddress tinyint
    , ShowMap tinyint
    , Garage tinyint
    , JlsDom smallint
    , GeomLocation geometry
    , PropertyType varchar(50)
    , CONSTRAINT pk_ListingDetail PRIMARY KEY (ListingDetailKey)
    )


--DROP TABLE dim.Agent

--CREATE TABLE dim.Agent (
--  AgentKey int identity(1,1),
--  MLSID	int,
--  AgentMLSID varchar(50),
--  AgentName varchar(100),
--  isActive smallint,
--  AuxAgentID varchar(50),
--  MLSInternalAgentID varchar(50),
--  CONSTRAINT pk_Agent PRIMARY KEY (AgentKey) 
--  )


--DROP TABLE dim.Office

--CREATE TABLE dim.Office(
--      OfficeKey int IDENTITY(1,1) NOT NULL
--	, MLSID int NULL
--	, OfficeMLSID varchar(50) NULL
--	, OfficeName varchar(100) NULL
--	, OfficeStatus smallint NULL
--	, InsertedDate datetime NULL
--	, UpdatedDate datetime NULL
--    , CONSTRAINT [pk_Office] PRIMARY KEY (OfficeKey)
--    )


DROP TABLE dim.ListingAgent

CREATE TABLE dim.ListingAgent(
  	  ListingAgentKey int IDENTITY(1,1) NOT NULL
	, ListingID int NOT NULL
	, PrimaryAgent varchar(50) NULL
	, CoListingAgent varchar(50) NULL
	, SellingAgent varchar(50) NULL
	, CoSellingAgent varchar(50) NULL
	, CoListingAgent2 varchar(50) NULL
	, CoSellingAgent2 varchar(50) NULL
	, CONSTRAINT pk_ListingAgent PRIMARY KEY (ListingAgentKey)
    )

DROP TABLE dim.ListingOffice

CREATE TABLE dim.ListingOffice(
	  ListingOfficeKey int IDENTITY(1,1) NOT NULL
	, ListingID int NOT NULL
	, PrimaryOffice varchar(50) NULL
	, CoListingOffice varchar(50) NULL
	, SellingOffice varchar(50) NULL
	, CoSellingOffice varchar(50) NULL
	, CoListingOffice2 varchar(50) NULL
	, CoSellingOffice2 varchar(50) NULL
	, CONSTRAINT pk_ListingOffice PRIMARY KEY (ListingOfficeKey)
    )


/* DROP and CREATE FACT TABLES   */
  DROP TABLE fact.Listing

  CREATE TABLE fact.Listing (
        MLSListingID varchar(25)
      , ListingDetailKey int
      , AgentKey int
	  , OfficeKey int
	  , ListDateKey int
	  , UpdateDateKey int
	  , CloseDateKey int
	  , StatusDateKey int
	  , PendingDateKey int
	  , RegionKey int
	  , PriceDisplay int
	  , OriginalListPrice int
	  , ListPrice int
	  , ClosePrice int
	  , HOADues int

	  , CONSTRAINT fk_Listing_ListingDetailKey FOREIGN KEY (ListingDetailKey) REFERENCES dim.ListingDetail(ListingDetailKey)
	  , CONSTRAINT fk_Listing_AgentKey FOREIGN KEY (AgentKey) REFERENCES dim.Agent(AgentKey)
	  , CONSTRAINT fk_Listing_OfficeKey FOREIGN KEY (OfficeKey) REFERENCES dim.Office(OfficeKey)
	  , CONSTRAINT fk_Listing_ListDateKey FOREIGN KEY (ListDateKey) REFERENCES dim.DateDimension(DateKey)
	  , CONSTRAINT fk_Listing_UpdateDateKey FOREIGN KEY (UpdateDateKey) REFERENCES dim.DateDimension(DateKey)
	  , CONSTRAINT fk_Listing_CloseDateKey FOREIGN KEY (CloseDateKey) REFERENCES dim.DateDimension(DateKey)
	  , CONSTRAINT fk_Listing_StatusDateKey FOREIGN KEY (StatusDateKey) REFERENCES dim.DateDimension(DateKey)
	  , CONSTRAINT fk_Listing_PendingDateKey FOREIGN KEY (PendingDateKey) REFERENCES dim.DateDimension(DateKey)
	  , CONSTRAINT fk_Listing_RegionKey FOREIGN KEY (RegionKey) REFERENCES dim.Region(RegionKey)
	  )



	END