USE [BI_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[LoadFactTables]    Script Date: 9/20/2019 12:46:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[LoadFactTables] AS
BEGIN

TRUNCATE TABLE fact.Listing;

INSERT INTO fact.Listing

select l.MLSListingID
     , ld.ListingDetailKey
	 , AgentKey = agt.AgentKey  --Primary Agent for listing
	 , OfficeKey = ofc.OfficeKey  --Primary Office Key
	 , ListDateKey = d1.DateKey
	 , UpdateDateKey = d2.DateKey
	 , CloseDateKey = d3.DateKey
	 , StatusDateKey = d4.DateKey
	 , PendingDateKey = d5.DateKey
	 , RegionKey = r.RegionKey
	 , PriceDisplay
	 , OriginalListPrice
	 , ListPrice
	 , ClosePrice
	 , HOADues

from BI_Reporting_STG.mls.Listing l
 left join dim.ListingDetail ld
   on l.MLSListingId = ld.MLSListingID and ld.MLSID = l.MLSID 
 left join dim.DateDimension d1
   on convert(date,l.ListDate) = d1.[Date]
 left join dim.DateDimension d2
   on convert(date,l.UpdateDate) = d2.[Date]
 left join dim.DateDimension d3
   on convert(date,l.CloseDate) = d3.[Date]
 left join dim.DateDimension d4
   on convert(date,l.StatusDate) = d4.[Date]
 left join dim.DateDimension d5
   on convert(date,l.PendingDate) = d5.[Date]
 left join dim.ListingAgent la
   on l.ListingID = la.Listingid
 left join dim.Agent agt
   on la.PrimaryAgent = agt.AgentMLSID and l.MLSID = agt.MLSID
 left join dim.ListingOffice lo
   on l.ListingID = lo.ListingID
  left join dim.Office ofc
   on lo.PrimaryOffice = ofc.OfficeNumber and l.MLSID = ofc.MLSID
  left join dim.Region r
   on l.listingid = r.listingid and l.MLSID = r.MLSID


END