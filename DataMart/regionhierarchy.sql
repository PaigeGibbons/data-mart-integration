DROP TABLE dim.RegionHierarchy

CREATE TABLE dim.RegionHierarchy (
  RegionHierarchyKey int identity(1,1)
, StateCode varchar(5)
, CountyName varchar(100)
, City varchar(100)
, MLSCode varchar(50)
, ReportingState varchar(50)
, ReportingRegion varchar(200)
, ReportingArea varchar(200)
, ReportingSubArea varchar(200)
, MLSCodeDescription varchar(200)
, Waterfront varchar(5)
)

truncate table dim.RegionHierarchy