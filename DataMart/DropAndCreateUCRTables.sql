USE [BI_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[DropAndCreateUCRTables]    Script Date: 2/7/2020 2:06:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[DropAndCreateUCRTables] as
BEGIN

/* Drop constraints */
ALTER TABLE fact.CRMBrokerCampaignContacts DROP CONSTRAINT fk_CRMBrokerCampaignContacts_RosterContactKey
ALTER TABLE fact.CRMBrokerCampaignContacts DROP CONSTRAINT fk_CRMBrokerCampaignContacts_RosterListKey
ALTER TABLE fact.CRMBrokerCampaignContacts DROP CONSTRAINT fk_CRMBrokerCampaignContacts_CRMCampaignsKey
ALTER TABLE fact.CRMBrokerCampaignContacts DROP CONSTRAINT fk_CRMBrokerCampaignContacts_RosterOfficeKey
ALTER TABLE fact.CRMBrokerCampaignContacts DROP CONSTRAINT fk_CRMBrokerCampaignContacts_ConsumerSavedSearchKey
ALTER TABLE fact.CRMBrokerCampaignContacts DROP CONSTRAINT fk_CRMBrokerCampaignContacts_ConsumerNotificationKey


ALTER TABLE fact.CRMTeamCampaignContacts DROP CONSTRAINT fk_CRMTeamCampaignContacts_RosterTeamKey
ALTER TABLE fact.CRMTeamCampaignContacts DROP CONSTRAINT fk_CRMTeamCampaignContacts_CRMCampaignsKey
ALTER TABLE fact.CRMTeamCampaignContacts DROP CONSTRAINT fk_CRMTeamCampaignContacts_CRMContactsKey 
ALTER TABLE fact.CRMTeamCampaignContacts DROP CONSTRAINT fk_CRMTeamCampaignContacts_RosterOfficeKey

/* create dims */
IF OBJECT_ID('dim.CRMContacts', 'U') IS NOT NULL
DROP TABLE dim.CRMContacts

CREATE TABLE dim.CRMContacts (
      CRMContactsKey int identity(1,1) NOT NULL
	, ContactId varchar(250) NOT NULL
	, ContactFirstName varchar(250) NULL
	, ContactLastName varchar(300) NULL
	, ContactStatus varchar(250) NULL
	, ContactEmail varchar(2500) NULL
	, ContactOwner varchar(250) NULL
	, ContactOwnerId int NULL
	, ContactAddedDate datetime NULL
	, CONSTRAINT pk_CRMContacts PRIMARY KEY CLUSTERED (CRMContactsKey))


IF OBJECT_ID('dim.CRMCampaigns', 'U') IS NOT NULL
DROP TABLE dim.CRMCampaigns

CREATE TABLE dim.CRMCampaigns (
      CrmCampaignsKey int identity(1,1) NOT NULL
	, CampaignId char(36) NOT NULL
	, CampaignName varchar(250) NULL
	, CampaignStatus varchar(250) NULL
	, CampaignType varchar(250) NULL
	, OwnerId bigint NULL
	, CampaignCreated datetime NULL
	, CampaignUpdated datetime NULL
	, CampaignOwnerId varchar(max) NULL
	, CampaignOwner varchar(max) NULL
	, CONSTRAINT pk_CRMCampaigns PRIMARY KEY CLUSTERED (CRMCampaignsKey))
	

IF OBJECT_ID('dim.CRMEmails', 'U') IS NOT NULL
DROP TABLE dim.CRMEmails

CREATE TABLE dim.CRMEmails (
      CRMEmailsKey int identity(1,1) NOT NULL
	, StatusCode [int] NULL
	, RowId [bigint] NULL
	, [Value] [varchar](max) NULL
	, Created [bigint] NULL
	, Updated [bigint] NULL
	, Id [varchar](36) NULL
	, CONSTRAINT pk_CRMEmails PRIMARY KEY CLUSTERED (CRMEmailsKey))


IF OBJECT_ID('dim.CRMEmailRecipients', 'U') IS NOT NULL
DROP TABLE dim.CRMEmailRecipients

CREATE TABLE dim.CRMEmailRecipients (
      CRMEmailRecipientsKey int identity(1,1) NOT NULL
	, CampaignId varchar(36) NULL
	, ContactId varchar(36) NULL
	, EmailId varchar(36) NULL
	, EmailSentDate datetime NULL
	, CONSTRAINT pk_CRMEmailRecipients PRIMARY KEY CLUSTERED (CRMEmailRecipientsKey))


IF OBJECT_ID('dim.CRMLists', 'U') IS NOT NULL
DROP TABLE dim.CRMLists

CREATE TABLE dim.CRMLists (
      CRMListsKey int identity(1,1) NOT NULL
	, RowId bigint NOT NULL
	, [Value] text NULL
	, Created bigint NULL
	, Updated bigint NULL
	, Id char(36) NULL
	, OwnerId bigint NULL
    , CONSTRAINT pk_CRMLists PRIMARY KEY CLUSTERED (CRMListsKey))


IF OBJECT_ID('dim.CRMListContact', 'U') IS NOT NULL
DROP TABLE dim.CRMListContact

CREATE TABLE dim.CRMListContact (
      CRMListContactKey int identity(1,1) NOT NULL
	, RowId bigint NULL
	, ListId varchar(36) NULL
	, ContactId varchar(36) NULL
	, ContactOrderBy varchar(255) NULL
	, ContactOwner varchar(255) NULL
	, ContactOwnerId int NULL
	, ListOwner varchar(255) NULL
	, ListOwnerId int NULL
    , CONSTRAINT pk_CRMListContact PRIMARY KEY CLUSTERED (CRMListContactKey))


IF OBJECT_ID('dim.CRMCampaignLists', 'U') IS NOT NULL
DROP TABLE dim.CRMCampaignLists

CREATE TABLE dim.CRMCampaignLists (
      CRMCampaignListsKey int identity(1,1) NOT NULL
	, RowId bigint NULL
	, CampaignId char(36) NULL
	, ListId char(36) NULL
	, [Type] text NULL
	, [Owner] varchar(255) NULL
	, OwnerId int NULL
    , CONSTRAINT pk_CRMCampaignLists PRIMARY KEY CLUSTERED (CRMCampaignListsKey))

 
IF OBJECT_ID('dim.RosterContact', 'U') IS NOT NULL
DROP TABLE dim.RosterContact

CREATE TABLE dim.RosterContact (
      RosterContactKey int identity(1,1)
	, BrokerId int NULL
	, BrokerName varchar(129) NULL
	, Persona_1 varchar(250) NULL
	, Persona_2 varchar(250) NULL
	, Persona_3 varchar(250) NULL
	, EulaAgreedDate datetime NULL
	, OfficeName varchar(250) NULL
	, OfficeId int NULL
	, BrokerIsActive varchar(250) NULL
	, IsPrimaryOffice varchar(250) NULL
	, MLSId varchar(100) NULL
	, ContactId varchar(250) NULL
	, ContactAddedDate datetime NULL
	, ContactFirstName varchar(250) NULL
	, ContactLastName varchar(300) NULL
	, ContactStatus varchar(250) NULL
	, ContactEmail varchar(2500) NULL
	, TeamId int NULL
	, TeamName varchar(400) NULL
	, TeamMembers varchar(1000) NULL
	, TeamIsDeleted varchar(25) NULL
	, TeamIsActive varchar(25) NULL
	, TeamCreated datetime NULL
	, TeamUpdated datetime NULL
	, TeamIndicator smallint NULL
	, CONSTRAINT pk_RosterContact PRIMARY KEY CLUSTERED (RosterContactKey))


IF OBJECT_ID('dim.RosterList', 'U') IS NOT NULL
DROP TABLE dim.RosterList

CREATE TABLE dim.RosterList (
      RosterListKey int identity(1,1) NOT NULL
	, BrokerId int NULL
	, BrokerName varchar(129) NULL
	, Persona_1 varchar(250) NULL
	, Persona_2 varchar(250) NULL
	, Persona_3 varchar(250) NULL
	, EulaAgreedDate datetime NULL
	, OfficeName varchar(250) NULL
	, OfficeId int NULL
	, BrokerIsActive varchar(250) NULL
	, MLSId varchar(100) NULL
	, ContactAddedDate datetime NULL
	, CampaignId varchar(100) NULL
	, ContactId varchar(100) NULL
	, TeamId int NULL
	, TeamName varchar(400) NULL
	, TeamMemberId varchar(250) NULL
	, TeamMemberName varchar(250) NULL
	, TeamIsDeleted varchar(25) NULL
	, TeamIsActive varchar(25) NULL
	, TeamCreated datetime NULL
	, TeamUpdated datetime NULL
    , CONSTRAINT pk_RosterList PRIMARY KEY CLUSTERED (RosterListKey))


IF OBJECT_ID('dim.RosterOffice', 'U') IS NOT NULL
DROP TABLE dim.RosterOffice

CREATE TABLE dim.RosterOffice (
      RosterOfficeKey int identity(1,1) NOT NULL
	, OfficeId int NULL
	, OfficeTypeId tinyint NULL
	, OfficeTypeName varchar(50) NULL
	, IsAffiliate varchar(50) NULL
	, DBAAbbrev varchar(50) NULL
	, DBATitle varchar(100) NULL
	, DirectoryLocation varchar(250) NULL
	, MLSId int NULL
	, MLSName varchar(100) NULL
	, MLSCode varchar(50) NULL
	, LocationName varchar(100) NULL
	, Alias varchar(100) NULL
	, OfficeIsDeleted varchar(10) NULL
	, OfficeIsActive varchar(10) NULL
	, Latitude decimal(38, 6) NULL
	, Longitude decimal(38, 6) NULL
    , CONSTRAINT pk_RosterOffice PRIMARY KEY CLUSTERED (RosterOfficeKey))


IF OBJECT_ID('dim.RosterTeam', 'U') IS NOT NULL
DROP TABLE dim.RosterTeam

CREATE TABLE dim.RosterTeam (
      RosterTeamKey int identity(1,1) NOT NULL
	, TeamId int NOT NULL
	, TeamRosterAdminId int NULL
	, TeamName varchar(400) NULL
	, TeamGuid varchar(400) NULL
	, TeamType varchar(100) NULL
	, TeamTagLine varchar(400) NULL
	, TeamDesignations varchar(1000) NULL
	, TeamMembers varchar(1000) NULL
	, TeamMemberId1 varchar(250) NULL
	, TeamMemberId2 varchar(250) NULL
	, TeamMemberId3 varchar(250) NULL
	, TeamMemberId4 varchar(250) NULL
	, TeamMemberId5 varchar(250) NULL
	, TeamMemberId6 varchar(250) NULL
	, TeamMemberId7 varchar(250) NULL
	, TeamMemberId8 varchar(250) NULL
	, TeamMemberId9 varchar(250) NULL
	, TeamMemberId10 varchar(250) NULL
	, TeamMemberName1 varchar(250) NULL
	, TeamMemberName2 varchar(250) NULL
	, TeamMemberName3 varchar(250) NULL
	, TeamMemberName4 varchar(250) NULL
	, TeamMemberName5 varchar(250) NULL
	, TeamMemberName6 varchar(250) NULL
	, TeamMemberName7 varchar(250) NULL
	, TeamMemberName8 varchar(250) NULL
	, TeamMemberName9 varchar(250) NULL
	, TeamMemberName10 varchar(250) NULL
	, TeamAlias varchar(400) NULL
	, TeamIsDeleted varchar(25) NULL
	, TeamIsActive varchar(25) NULL
	, TeamOwner varchar(400) NULL
	, TeamCreated datetime NULL
	, TeamUpdated datetime NULL
    , CONSTRAINT pk_RosterTeam PRIMARY KEY CLUSTERED (RosterTeamKey))


IF OBJECT_ID('dim.RosterTeamUnpivot', 'U') IS NOT NULL
DROP TABLE dim.RosterTeamUnpivot

CREATE TABLE dim.RosterTeamUnpivot (
      RosterTeamUnpivotKey int identity(1,1) NOT NULL
	, TeamId int NOT NULL
	, TeamRosterAdminId int NULL
	, TeamName varchar(400) NULL
	, TeamType varchar(100) NULL
	, TeamTagLine varchar(400) NULL
	, TeamDesignations text NULL
	, TeamMemberId varchar(250) NULL
	, TeamMemberName varchar(250) NULL
	, TeamAlias varchar(400) NULL
	, TeamIsDeleted varchar(25) NULL
	, TeamIsActive varchar(25) NULL
	, TeamOwner varchar(400) NULL
	, TeamCreated datetime NULL
	, TeamUpdated datetime NULL
    , CONSTRAINT pk_RosterTeamUnpivot PRIMARY KEY CLUSTERED (RosterTeamUnpivotKey))


IF OBJECT_ID('dim.RosterUser', 'U') IS NOT NULL
DROP TABLE dim.RosterUser

CREATE TABLE dim.RosterUser (
      RosterUserKey int identity(1,1) NOT NULL
	, BrokerId int NULL
	, BrokerName varchar(129) NULL
	, Persona_1 varchar(250) NULL
	, Persona_2 varchar(250) NULL
	, Persona_3 varchar(250) NULL
	, EulaAgreedDate nvarchar(10) NULL
	, OfficeName varchar(250) NULL
	, OfficeId int NULL
	, BrokerIsActive varchar(250) NULL
	, IsPrimaryOffice varchar(250) NULL
	, MLSId varchar(100) NULL
    , CONSTRAINT pk_RosterUser PRIMARY KEY CLUSTERED (RosterUserKey))


IF OBJECT_ID('dim.TeamsContact', 'U') IS NOT NULL
DROP TABLE dim.TeamsContact

CREATE TABLE dim.TeamsContact (
	  TeamsContactKey int IDENTITY(1,1) NOT NULL
	, TeamId int NOT NULL
	, TeamName varchar(400) NULL
	, TeamMemberId varchar(250) NULL
	, TeamMemberName varchar(250) NULL
	, TeamIsDeleted varchar(25) NULL
	, TeamIsActive varchar(25) NULL
	, TeamCreated datetime NULL
	, TeamUpdated datetime NULL
	, ContactId varchar(250) NOT NULL
	, ContactAddedDate datetime NULL
	, ContactFirstName varchar(250) NULL
	, ContactLastName varchar(300) NULL
	, ContactStatus varchar(250) NULL
	, ContactEmail varchar(2500) NULL
    , CONSTRAINT pk_TeamsContact PRIMARY KEY CLUSTERED (TeamsContactKey ASC))
    
   
IF OBJECT_ID('dim.Consumer_Notification', 'U') IS NOT NULL
DROP TABLE dim.Consumer_Notification

CREATE TABLE dim.Consumer_Notification(
	ConsumerNotificationKey INT IDENTITY(1,1),
	SenderId	int NULL,
	CreatedUtc	datetime NULL,
	ConsumerId	int NULL,
	SavedSearchId	int NULL,
	FavoriteListingEvent int NULL,
	MarketInsights	varchar(4000) NULL,
	[Sent]	tinyint NULL,
	SentUtc	datetime NULL,
CONSTRAINT pk_ConsumerNotification PRIMARY KEY CLUSTERED (ConsumerNotificationKey))
-- select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'Consumer_Notification'


IF OBJECT_ID('dim.Consumer_SavedSearch', 'U') IS NOT NULL
DROP TABLE dim.Consumer_SavedSearch

CREATE TABLE dim.Consumer_SavedSearch(
	ConsumerSavedSearchKey INT IDENTITY(1,1),	
    ConsumerId INT NULL,
    Name VARCHAR(256) NULL,
    NotifyEnabled TINYINT NULL,
    LastNotifiedUtc DATETIME NULL,
    MarketInsightsEnabled INT NULL,
    MarketInsightsFrequency INT NULL,
    LastMarketInsightsUtc DATETIME NULL,
    Value TEXT NULL,
CONSTRAINT pk_ConsumerSavedSearch PRIMARY KEY CLUSTERED (ConsumerSavedSearchKey))



--facts
IF OBJECT_ID('fact.CRMBrokerCampaignContacts', 'U') IS NOT NULL
DROP TABLE fact.CRMBrokerCampaignContacts

CREATE TABLE fact.CRMBrokerCampaignContacts (
	  RosterContactKey int NOT NULL
	, RosterListKey int NULL
	, CRMCampaignsKey int NULL
    , RosterOfficeKey int NULL
    , ConsumerNotificationKey int NULL
	, ConsumerSavedSearchKey int NULL
    , CONSTRAINT fk_CRMBrokerCampaignContacts_RosterContactKey FOREIGN KEY (RosterContactKey) REFERENCES dim.RosterContact(RosterContactKey)
    , CONSTRAINT fk_CRMBrokerCampaignContacts_RosterListKey FOREIGN KEY (RosterListKey) REFERENCES dim.RosterList(RosterListKey)
    , CONSTRAINT fk_CRMBrokerCampaignContacts_CRMCampaignsKey FOREIGN KEY (CRMCampaignsKey) REFERENCES dim.CRMCampaigns(CRMCampaignsKey)
    , CONSTRAINT fk_CRMBrokerCampaignContacts_RosterOfficeKey FOREIGN KEY (RosterOfficeKey) REFERENCES dim.RosterOffice(RosterOfficeKey)
    , CONSTRAINT fk_CRMBrokerCampaignContacts_ConsumerNotificationKey FOREIGN KEY (ConsumerNotificationKey) REFERENCES dim.Consumer_Notification(ConsumerNotificationKey)
    , CONSTRAINT fk_CRMBrokerCampaignContacts_ConsumerSavedSearchKey FOREIGN KEY (ConsumerSavedSearchKey) REFERENCES dim.Consumer_SavedSearch(ConsumerSavedSearchKey)
	)

IF OBJECT_ID('fact.CRMTeamCampaignContacts', 'U') IS NOT NULL
DROP TABLE fact.CRMTeamCampaignContacts

CREATE TABLE fact.CRMTeamCampaignContacts (
	  RosterTeamKey int NOT NULL
	, CRMCampaignsKey int NULL
    , CRMContactsKey int NULL
    , RosterOfficeKey int NULL
    , CONSTRAINT fk_CRMTeamCampaignContacts_RosterTeamKey FOREIGN KEY (RosterTeamKey) REFERENCES dim.RosterTeam(RosterTeamKey)
    , CONSTRAINT fk_CRMTeamCampaignContacts_CRMCampaignsKey FOREIGN KEY (CRMCampaignsKey) REFERENCES dim.CRMCampaigns(CRMCampaignsKey)
    , CONSTRAINT fk_CRMTeamCampaignContacts_CRMContactsKey FOREIGN KEY (CRMContactsKey) REFERENCES dim.CRMContacts(CRMContactsKey)
    , CONSTRAINT fk_CRMTeamCampaignContacts_RosterOfficeKey FOREIGN KEY (RosterOfficeKey) REFERENCES dim.RosterOffice(RosterOfficeKey)
	)

END