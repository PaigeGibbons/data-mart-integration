USE [BI_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[LoadDimensions]    Script Date: 12/11/2019 1:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[LoadDimensions] as 

BEGIN

INSERT INTO dim.ADUsers
SELECT SamAccountName
     , [NAME]
     , Department
     , Title
FROM BI_Reporting_STG.ad.ADExportJLSCO
UNION
SELECT SamAccountName 
     , [Name]
     , Office
     , Title
FROM BI_Reporting_STG.ad.ADExportJLSRE


INSERT INTO dim.County 
select CountyName
	 , StateCode = s.StateAbbrev
from BI_Reporting_STG.mls.County c
  left join
     BI_Reporting_STG.mls.[state] s
	on c.stateid = s.stateid


INSERT INTO dim.MLS
SELECT MLSID
 	 , MLSName
 	 , MLSAbbreviation
	 , MLSFullName
FROM BI_Reporting_STG.mls.MLS


INSERT INTO dim.MLSArea
SELECT distinct MLSID
     , MLSCode
 	 , MLSCodeDescription
 	 , MLSCodeAbbrev
FROM BI_Reporting_STG.mls.MLSArea


INSERT INTO dim.PropType
SELECT PropTypeID
     , PropTypeName
     , PropCategoryName
	 , MLSID
	 , MLSCode
	 , MLSTableName
FROM BI_Reporting_STG.mls.PropType pt
  LEFT JOIN
     BI_Reporting_STG.mls.PropCategory pc
	on pt.PropCategoryID = pc.PropCategoryID


INSERT INTO dim.StatusType
SELECT StatusID
     , StatusName
	 , st.StatusCategoryID
	 , StatusCategoryName
	 , MLSID
	 , MLSCode
FROM BI_Reporting_STG.mls.StatusType st
  LEFT JOIN BI_Reporting_STG.mls.StatusCategory sc
    on st.StatusCategoryID = sc.StatusCategoryID

	
MERGE INTO dim.Agent AS tgt
USING (SELECT agt.MLSID
            , agt.AgentMLSID
            , AgentName = AgentFirstName + ' ' + AgentLastName
            , isActive = AgentActive
            , AuxAgentID = isnull(AuxAgentID,'')
            , MLSInternalAgentID = isnull(MLSInternalAgentID,'')
			, AgentUpdateDate
			, AgentModTime
       FROM BI_Reporting_STG.mls.Agent agt
	    join (SELECT MLSID, AgentMLSID, max(AgentUpdateDate) maxdate, max(AgentAddTime) maxadd
		      from BI_Reporting_STG.mls.Agent
			  group by MLSID, AgentMLSID) maxdt
		  on agt.MLSID = maxdt.MLSID and agt.AgentMLSID = maxdt.AgentMLSID and agt.AgentUpdateDate = maxdt.maxdate
		     and agt.AgentAddTime = maxdt.maxadd
	   WHERE agt.AgentMLSID <> '0' and AgentModTime > dateadd(dd,-7,getdate())
  ) AS src
  ON   tgt.MLSID = src.MLSID 
   AND tgt.AgentMLSID = src.AgentMLSID 

WHEN MATCHED THEN 
UPDATE SET tgt.AgentName = src.AgentName
         , tgt.isActive = src.isActive
		 , tgt.AuxAgentID = src.AuxAgentID 
         , tgt.MLSInternalAgentID = src.MLSInternalAgentID
		 , tgt.UpdatedDate = getdate()
WHEN NOT MATCHED BY TARGET THEN 
INSERT (MLSID, AgentMLSID, AgentName, isActive, AuxAgentID, MLSInternalAgentID, InsertedDate, UpdatedDate)
VALUES (
         src.MLSID
       , src.AgentMLSID
       , src.AgentName
       , src.isActive
       , src.AuxAgentID
       , src.MLSInternalAgentID
	   , getdate()
	   , getdate()
	   );


MERGE INTO dim.Office AS tgt
USING (SELECT ofc.MLSID
            , ofc.OfficeID
            , ofc.OfficeName
            , ofc.OfficeAddress1
            , ofc.OfficeAddress2
            , ofc.OfficeCity
            , ofc.OfficeState
            , ofc.OfficeZip
            , ofc.OfficeStatus
			, ofc.OfficeIDAux
			, OfficeUpdateDate
			, OfficeModTime
       FROM BI_Reporting_STG.mls.Office ofc
	    join (SELECT MLSID, OfficeID, max(OfficeUpdateDate) maxdate
		      from BI_Reporting_STG.mls.Office
			  group by MLSID, OfficeID) maxdt
		  on ofc.MLSID = maxdt.MLSID and ofc.OfficeID = maxdt.OfficeID and ofc.OfficeUpdateDate = maxdt.maxdate
	   WHERE OfficeModTime > dateadd(dd,-7,getdate()) 
  ) AS src
  ON   tgt.MLSID = src.MLSID 
   AND isnull(tgt.OfficeID,'') = isnull(src.OfficeID,'')

WHEN MATCHED THEN 
UPDATE SET tgt.OfficeName = src.OfficeName
         , tgt.OfficeAddress1 = src.OfficeAddress1
         , tgt.OfficeAddress2 = src.OfficeAddress2
         , tgt.OfficeCity = src.OfficeCity
         , tgt.OfficeState = src.OfficeState
         , tgt.OfficeZip = src.OfficeZip
         , tgt.OfficeStatus = src.OfficeStatus
		 , tgt.OfficeIDAux = src.OfficeIDAux
		 , tgt.UpdatedDate = getdate()
WHEN NOT MATCHED BY TARGET THEN 
INSERT (MLSID, OfficeID, OfficeName, OfficeAddress1, OfficeAddress2, OfficeCity, OfficeState, OfficeZip, OfficeStatus, OfficeIDAux, InsertedDate, UpdatedDate)
VALUES (
         src.MLSID
       , src.OfficeID
       , src.OfficeName
       , src.OfficeAddress1
       , src.OfficeAddress2
       , src.OfficeCity
       , src.OfficeState
       , src.OfficeZip
       , src.OfficeStatus
	   , src.OfficeIDAux
	   ,getdate()
	   ,getdate()
	   );


INSERT INTO dim.ListingAddress
SELECT StreetAddress
     , StreetAddress2
     , StreetDirPrefix
     , StreetDirSuffix
     , StreetName
     , StreetNumber
     , StreetTypeSuffix
     , StreetNumberModifier
     , City
	 , c2.CountyKey
     , s.StateAbbrev
     , PostalCode
     , PostalCodeExt
     , Latitude
     , Longitude
     , GeogLocation
     , GeomLocation
     , ListingID

from BI_Reporting_STG.mls.Listing l
  left join BI_Reporting_STG.mls.[State] s on l.StateID  = s.StateID
  left join BI_Reporting_STG.mls.County c on l.CountyID = c.Countyid 
  left join dim.County c2 on c.CountyName = c2.CountyName and s.StateAbbrev = c2.StateCode


INSERT INTO dim.ListingAgent
SELECT PrimaryListingAgentKey = agt1.AgentKey
     , CoListingAgentKey = agt2.AgentKey
     , CoListingAgent2Key = agt3.AgentKey
     , CoListingMemberID
     , SellingAgentKey = agt4.AgentKey
     , CoSellingAgentKey = agt5.AgentKey
     , CoSellingAgent2Key = agt6.AgentKey
     , CoSellingAgent3Key = agt7.AgentKey
     , AgentBonus
     , CommissionCode
     , CommissionComments
     , Commission
     , SellingOfficeCommissionPercentage
     , StagingListingKey = la.ListingID

FROM BI_Reporting_STG.mls.Listing la
  LEFT JOIN dim.Agent agt1 ON agt1.MLSID = la.MLSID and agt1.AgentMLSID = la.ListAgentID
  LEFT JOIN dim.Agent agt2 ON agt2.MLSID = la.MLSID and agt2.AgentMLSID = la.CoListAgentID
  LEFT JOIN dim.Agent agt3 ON agt3.MLSID = la.MLSID and agt3.AgentMLSID = la.CoListAgentID2
  LEFT JOIN dim.Agent agt4 ON agt4.MLSID = la.MLSID and agt4.AgentMLSID = la.SellingAgentID
  LEFT JOIN dim.Agent agt5 ON agt5.MLSID = la.MLSID and agt5.AgentMLSID = la.CoSellingAgentID
  LEFT JOIN dim.Agent agt6 ON agt6.MLSID = la.MLSID and agt6.AgentMLSID = la.CoSellingAgentID2
  LEFT JOIN dim.Agent agt7 ON agt7.MLSID = la.MLSID and agt7.AgentMLSID = la.CoSellingAgentID3


insert into dim.ListingOffice
Select PrimaryOfficeKey = o1.OfficeKey
	 , CoListingOfficeKey = o2.OfficeKey
	 , SellingOfficeKey = o3.OfficeKey
	 , CoSellingOfficeKey = o4.OfficeKey
	 , CoSellingOffice2Key = o5.OfficeKey
	 , StagingListingKey = ListingID
from BI_Reporting_STG.mls.Listing l
 left join dim.Office o1 on l.MLSID = o1.MLSID and l.ListingOfficeID = o1.OfficeID
 left join dim.Office o2 on l.MLSID = o2.MLSID and l.CoListingOfficeID = o2.OfficeID
 left join dim.Office o3 on l.MLSID = o3.MLSID and l.SellingOffice = o3.OfficeID
 left join dim.Office o4 on l.MLSID = o4.MLSID and l.CoSellingOffice = o4.OfficeID
 left join dim.Office o5 on l.MLSID = o5.MLSID and l.CoSellingOffice2 = o5.OfficeID
 

INSERT INTO dim.ListingDetail
SELECT Acreage
      , Age
      , AgentOwned
      , Auction
      , BankOwned
      , BathsFull
      , BathsPartial
      , BathsTotal
      , Bedrooms
      , BodyofWaterName
      , BuilderName
      , BuildingName
      , Community
      , ElementarySchool
      , Fireplaces
      , Garage
      , GarageSpaces
      , HighSchool
      , HowSold
      , IDX
      , LotSizeSF
      , MiddleSchool
      , NewConstruction = case when isnull(NewConstruction,'') in ('','No','N','Other','See Remarks','Unknown') 
	                           then 'No' 
		                       else 'Yes' end
      , ParkingSpaces
      , PictureCount
      , PropertyTax
      , SchoolDistrict
      , ShortSale
      , ShowAddress
      , ShowMap
      , SquareFootage
      , TaxID
      , TaxYearBuilt
      , VideoTour
      , VirtualTour
      , Waterfront = case when MLSID <> 9 and isnull(Waterfront,'') in ('','No','N','None','Other','Own Assoc','Sec Lot','See Remarks','Unknown') 
 	                      then 'No' 
                          when MLSID = 9 and isnull(Waterfront,'') in ('','No','None','Other','Own Assoc','Sec Lot','See Remarks','Unknown') 
	                      then 'No' --MLSID = 9 includes N as a valid waterfront type (Salt Water),  so don't want to set it to No 
		                  else 'Yes' end  
      , YearBuilt
      , Zoning
      , StagingListingKey = ListingID
  FROM BI_Reporting_STG.mls.Listing




				
/*
DELETE FROM dim.Region

INSERT INTO dim.Region
SELECT 
	t1.ListingId,
	t1.MLSID,
	CASE 
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode IN ('500','510','520','530','540','550','560','600','948') THEN 'Eastside King'
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode IN ('170') THEN 'Bainbridge Island'
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode IN ('705','390','380','385','701','720','710','700','715','800','140') THEN 'Seattle'
		WHEN t1.CountyID=48 AND t4.StateName='Oregon' AND t1.City='Bend' THEN 'Bend'
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode IN ('100','110','120','130','360') THEN 'SW King County'
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode IN ('310','320','300','330','340','350') THEN 'SE King County'
		WHEN t2.MLSCode in('141','142','143','144','145','146','147','148','149','150','161','162','163','164','165','166','167','168') THEN 'Kitsap wo Bainbridge'
		WHEN t2.MLSCode in('1','2','3','4','5','6','7','8','9') THEN 'Gig Harbor'
		WHEN t1.StateID=43 AND t1.CountyID IN (31,115) THEN 'East Portland'
		WHEN t1.StateID=43  AND t1.CountyID IN (37,167) THEN 'West Portland'
		WHEN t2.MLSID=127 AND StateName='Idaho' AND t1.CountyID = 1 THEN 'Boise'
		WHEN t2.MLSID=91 AND StateName='Oregon' AND MLSCode = 147 THEN 'Lake Oswego'
		ELSE t3.CountyName + ' County'
	END AS Region,
	CASE
		WHEN t3.CountyID IN (1,2,4,5,6,9,10,11,12,14,15) THEN 'Luxury and Standard'
		WHEN t3.CountyID IN (4,7,8,13,16,17,19) THEN 'Standard'
		ELSE 'Luxury'
	END AS UpdateType,
	NULL AS Waterfront,
	t5.PropertyType
FROM 
	BI_Reporting_STG.mls.Listing t1
	LEFT JOIN [BI_Reporting_STG].[mls].[MLSArea] t2
	ON t1.MLSID = t2.MLSID AND t1.MLSListingID = t2.MLSListingID
		LEFT JOIN [BI_Reporting_STG].[mls].County t3
		ON t1.CountyID = t3.CountyID
			LEFT JOIN [BI_Reporting_STG].[mls].[State] t4
			ON t3.StateID = t4.StateID
				LEFT JOIN BI_SL_STG..NewConstruction t5
				ON t1.ListingID = t5.ListingID
WHERE 
	(t4.StateName = 'Oregon' AND t3.CountyID IN(37,167,76,31,115,48)) OR
	(t4.StateName = 'Idaho' AND t3.CountyID IN(1,89)) OR
	(t4.StateName = 'Washington' AND t3.CountyID IN(77,84,85,127,146,147,153,168)) OR
	t2.MLSCode in('1','2','3','4','5','6','7','8','9'
					,'100','110','120','130','140','510','170','140','141','142','143','144','145','146','147','148','149','150','161','162','163','164','165','166','167','168','170'
					,'300','310','320','330','340','350','360','380','385','390'
					,'500','510','520','530','540','550','560','600'
					,'700','701','705','710','715','720','800','948')
UNION ALL

SELECT 
	t1.ListingId,
	t1.MLSID,
	CASE 
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode='520' THEN 'West Bellevue'
		WHEN t2.MLSID=9AND t4.StateName='Washington' AND MLSCode='510' THEN 'Mercer Island'
	END AS Region,
	'Luxury' AS UpdateType,
	NULL AS Waterfront,
	t5.PropertyType
FROM 
	BI_Reporting_STG.mls.Listing t1
	LEFT JOIN [BI_Reporting_STG].[mls].[MLSArea] t2
	ON t1.MLSID = t2.MLSID AND t1.MLSListingID = t2.MLSListingID
		LEFT JOIN [BI_Reporting_STG].[mls].County t3
		ON t1.CountyID = t3.CountyID
			LEFT JOIN [BI_Reporting_STG].[mls].[State] t4
			ON t3.StateID = t4.StateID
				LEFT JOIN BI_SL_STG..NewConstruction t5
				ON t1.ListingID = t5.ListingID
WHERE 
	t2.MLSID=9 AND 
	t4.StateName='Washington' AND
	t2.MLSCode in('510','520')

UNION ALL

SELECT 
	t1.ListingId,
	t1.MLSID,
	CASE 
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode IN('380','390','510','520','710') THEN 'Lake Washington'
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode IN('530','540') THEN 'Lake Sammamish'
	END AS Region,
	'Luxury' AS UpdateType,
	'Waterfront' as Waterfront,
	t5.PropertyType
FROM 
	BI_Reporting_STG.mls.Listing t1
	LEFT JOIN [BI_Reporting_STG].[mls].[MLSArea] t2
	ON t1.MLSID = t2.MLSID AND t1.MLSListingID = t2.MLSListingID
		LEFT JOIN [BI_Reporting_STG].[mls].County t3
		ON t1.CountyID = t3.CountyID
			LEFT JOIN [BI_Reporting_STG].[mls].[State] t4
			ON t3.StateID = t4.StateID
				LEFT JOIN BI_SL_STG..NewConstruction t5
				ON t1.ListingID = t5.ListingID
WHERE 
	t2.MLSID=9 AND 
	t4.StateName='Washington' AND
	t2.MLSCode in('380','390','510','520','710','530','540')

UNION ALL

SELECT 
	t1.ListingId,
	t1.MLSID,
	CASE 
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode IN('100','110','120','130','140','300','310','320','330','340','350','360','380','385','390','500','510','520','530','540','550','560','600','610','140','700','701','705','710','720','715','800','948') 
		THEN 'King County Waterfront'
	END AS Region,
	'Luxury' AS UpdateType,
	'Waterfront' as Waterfront,
	t5.PropertyType
FROM 
	BI_Reporting_STG.mls.Listing t1
	LEFT JOIN [BI_Reporting_STG].[mls].[MLSArea] t2
	ON t1.MLSID = t2.MLSID AND t1.MLSListingID = t2.MLSListingID
		LEFT JOIN [BI_Reporting_STG].[mls].County t3
		ON t1.CountyID = t3.CountyID
			LEFT JOIN [BI_Reporting_STG].[mls].[State] t4
			ON t3.StateID = t4.StateID
				LEFT JOIN BI_SL_STG..NewConstruction t5
				ON t1.ListingID = t5.ListingID
WHERE
	t2.MLSID=9 AND 
	t4.StateName='Washington' AND
	t2.MLSCode IN('100','110','120','130','140','300','310','320','330','340','350','360','380','385','390','500','510','520','530','540','550','560','600','610','140','700','701','705','710','720','715','800','948')
*/

END

