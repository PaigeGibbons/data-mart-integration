CREATE PROCEDURE LoadFactTables AS
BEGIN

TRUNCATE TABLE fact.Listing;

INSERT INTO fact.Listing

select l.MLSListingID
     , ld.ListingDetailKey
	 , AgentKey = NULL
	 , OfficeKey = NULL
	 , ListDateKey = d1.DateKey
	 , UpdateDateKey = d2.DateKey
	 , CloseDateKey = d3.DateKey
	 , StatusDateKey = d4.DateKey
	 , PendingDateKey = d5.DateKey
	 , PriceDisplay
	 , OriginalListPrice
	 , ListPrice
	 , ClosePrice
	 , HOADues

from BI_Reporting_STG.mls.Listing l
 left join dim.ListingDetail ld
   on l.MLSListingId = ld.MLSListingID and ld.MLSID = l.MLSID 
 left join dim.DateDimension d1
   on convert(date,l.ListDate) = d1.[Date]
 left join dim.DateDimension d2
   on convert(date,l.UpdateDate) = d2.[Date]
 left join dim.DateDimension d3
   on convert(date,l.CloseDate) = d3.[Date]
 left join dim.DateDimension d4
   on convert(date,l.StatusDate) = d4.[Date]
 left join dim.DateDimension d5
   on convert(date,l.PendingDate) = d5.[Date]


END