USE [BI_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[LoadFactTables]    Script Date: 11/20/2019 11:15:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[LoadFactTables] AS
BEGIN


INSERT INTO [fact].[Listing]
Select MLSListingID
     , MLSKey = mls.MLSKey
	 , MLSAreaKey = area.MLSAreaKey
	 , RegionHierarchyKey = region.RegionHierarchyKey
	 , ldtl.ListingDetailKey
     , ladd.ListingAddressKey
     , lagt.ListingAgentKey
     , loff.ListingOfficeKey
     , ListDateKey = d1.DateKey
     , UpdateDateKey = d2.DateKey
	 , CloseDateKey = d3.DateKey
     , StatusDateKey = d4.DateKey
     , PendingDateKey = d5.DateKey
     , CancelDateKey = d6.DateKey
     , EstCompletionDateKey = d7.DateKey
     , ExpirationDateKey = d8.DateKey
     , FallThroughDateKey = d9.DateKey
     , LeaseDateKey = d10.DateKey
	 , WithdrawDateKey = d11.DateKey
     , pt.PropTypeKey 
	 , st.StatusTypeKey
     , PriceDisplay
     , PriceCategoryKey = pc.PriceCategoryKey
     , OriginalListPrice
     , ListPrice
     , ClosePrice
     , HOADues
     , AgentHitCount
     , DailyTrafficCount
     , CDOM
     , DOM
     , JlsDom

from BI_Reporting_STG.mls.Listing l
 left join dim.MLS mls on l.MLSID = mls.MLSID
 left join dim.MLSArea area on l.MLSID = area.MLSID and l.Area = area.MLSAreaCode
 left join dim.ListingDetail ldtl on l.ListingID = ldtl.StagingListingKey
 left join dim.ListingAddress ladd on l.ListingID = ladd.StagingListingKey
 left join dim.ListingAgent lagt on l.ListingID = lagt.StagingListingKey
 left join dim.ListingOffice loff on l.ListingID = loff.StagingListingKey
 left join dim.DateDimension d1  on  d1.[Date] = cast(l.ListDate as date)
 left join dim.DateDimension d2  on  d2.[Date] = cast(l.UpdateDate as date)
 left join dim.DateDimension d3  on  d3.[Date] = cast(l.CloseDate as date)
 left join dim.DateDimension d4  on  d4.[Date] = cast(l.StatusDate as date)
 left join dim.DateDimension d5  on  d5.[Date] = cast(l.PendingDate as date)
 left join dim.DateDimension d6  on  d6.[Date] = cast(l.CancelDate as date)
 left join dim.DateDimension d7  on  d7.[Date] = try_convert(date,EstCompletionDate)
 left join dim.DateDimension d8  on  d8.[Date] = cast(l.ExpirationDate as date)
 left join dim.DateDimension d9  on  d9.[Date] = cast(l.FallThroughDate as date)
 left join dim.DateDimension d10 on d10.[Date] = cast(l.LeaseDate as date)
 left join dim.DateDimension d11 on d11.[Date] = cast(l.WithdrawDate as date)
 left join dim.PropType pt on pt.MLSID = l.MLSID and pt.PropTypeID = l.PropTypeID
 left join dim.StatusType st on st.MLSID = ltrim(rtrim(l.MLSID)) and st.StatusID = ltrim(rtrim(l.StatusID))
 left join dim.PriceCategory pc on l.ListPrice between pc.CategoryStartValue and pc.CategoryEndValue
 left join (select listingid, coalesce(rh.regionhierarchykey, rh2.RegionHierarchyKey, rh3.RegionHierarchyKey) RegionHierarchyKey
            from BI_Reporting_STG.mls.Listing l
             left join BI_Reporting_STG.mls.State s on l.StateID = s.StateID
             left join BI_Reporting_STG.mls.County c on l.CountyID = c.CountyID
             left join dim.RegionHierarchy rh on l.Area = rh.MLSCode and s.StateAbbrev = rh.StateCode
             left join dim.RegionHierarchy rh2 on s.StateAbbrev = rh2.StateCode and c.CountyName = rh2.CountyName and rh2.MLSCode = '' and rh2.City = ''
             left join dim.RegionHierarchy rh3 on s.StateAbbrev = rh3.StateCode and l.City = rh3.City and rh3.MLSCode = '') region
	on l.ListingId = region.ListingID


END
