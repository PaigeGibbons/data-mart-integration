USE [BI_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[LoadUCRDimensions]    Script Date: 2/7/2020 3:10:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[LoadUCRDimensions] as
BEGIN

INSERT INTO dim.CRMCampaignLists
SELECT RowId
      ,CampaignId
      ,ListId
      ,[Type]
      ,[Owner]
      ,OwnerId
  FROM BI_Reporting_STG.ucr.Crm_CampaignLists


INSERT INTO dim.CRMCampaigns
SELECT CampaignId
      ,CampaignName
      ,CampaignStatus
      ,CampaignType
      ,OwnerId
      ,CampaignCreated
      ,CampaignUpdated
      ,CampaignOwnerId
      ,CampaignOwner
FROM BI_Reporting_STG.ucr.rpt_CRM_Campaigns


INSERT INTO dim.CRMContacts
SELECT ContactId
      ,ContactFirstName
      ,ContactLastName
      ,ContactStatus
      ,ContactEmail
      ,ContactOwner
      ,ContactOwnerId
      ,ContactAddedDate
FROM BI_Reporting_STG.ucr.rpt_Contacts


INSERT INTO dim.CRMEmailRecipients
SELECT CampaignId
      ,ContactId
      ,EmailId
      ,EmailSentDate = cast(EmailSentDate as datetime)
FROM BI_Reporting_STG.ucr.rpt_CRM_EmailRecipients


INSERT INTO dim.CRMEmailRecipientsContacts
SELECT ContactId
      ,EmailId
      ,EmailSentDate
FROM BI_Reporting_STG.ucr.rpt_CRM_EmailRecipients_Contacts


INSERT INTO dim.CRMEmails
SELECT StatusCode
      ,RowId
	  ,[Value]
	  ,Created
	  ,Updated
	  ,Id
FROM BI_Reporting_STG.ucr.CRM_Emails


INSERT INTO dim.CRMListContact
SELECT RowId
      ,ListId
      ,ContactId
      ,ContactOrderBy
      ,ContactOwner
      ,ContactOwnerId
      ,ListOwner
      ,ListOwnerId
FROM BI_Reporting_STG.ucr.CRM_ListContact


INSERT INTO dim.CRMLists
SELECT RowId
      ,[Value]
      ,Created
      ,Updated
      ,Id
      ,OwnerId
  FROM BI_Reporting_STG.ucr.CRM_Lists


INSERT INTO dim.RosterContact
SELECT BrokerId
      ,BrokerName
      ,Persona_1
      ,Persona_2
      ,Persona_3
      ,EulaAgreedDate = cast(EulaAgreedDate as datetime)
      ,OfficeName
      ,OfficeId
      ,BrokerIsActive
      ,IsPrimaryOffice
	  ,MLSId
      ,ContactId
      ,ContactAddedDate
      ,ContactFirstName
      ,ContactLastName
      ,ContactStatus
      ,ContactEmail
	  ,TeamId
	  ,TeamName
	  ,TeamMembers
	  ,TeamIsDeleted
	  ,TeamIsActive
	  ,TeamCreated
	  ,TeamUpdated
	  ,TeamIndicator
FROM BI_Reporting_STG.ucr.rpt_Roster_Contact


INSERT INTO dim.RosterList
SELECT BrokerId
      ,BrokerName
      ,Persona_1
      ,Persona_2
      ,Persona_3
      ,EulaAgreedDate = cast(EulaAgreedDate as datetime)
      ,OfficeName
      ,OfficeId
      ,BrokerIsActive
	  ,MLSId
      ,ContactAddedDate = cast(ContactAddedDate as datetime)
      ,CampaignId
      ,ContactId
      ,TeamId
      ,TeamName
      ,TeamMemberId
      ,TeamMemberName
      ,TeamIsDeleted
      ,TeamIsActive
      ,TeamCreated
      ,TeamUpdated
FROM BI_Reporting_STG.ucr.rpt_Roster_List

INSERT INTO dim.RosterOffice
SELECT OfficeId
      ,OfficeTypeId
      ,OfficeTypeName
      ,IsAffiliate
      ,DBAAbbrev
      ,DBATitle
      ,DirectoryLocation
      ,MLSId
      ,MLSName
      ,MLSCode
      ,LocationName
      ,Alias
      ,OfficeIsDeleted
      ,OfficeIsActive
      ,Latitude
      ,Longitude
  FROM BI_Reporting_STG.ucr.rpt_Roster_Office

INSERT INTO dim.RosterTeam
SELECT TeamId
      ,TeamRosterAdminId
      ,TeamName
      ,TeamGuid
      ,TeamType
      ,TeamTagLine
      ,TeamDesignations
      ,TeamMembers
      ,TeamMemberId1
      ,TeamMemberId2
      ,TeamMemberId3
      ,TeamMemberId4
      ,TeamMemberId5
      ,TeamMemberId6
      ,TeamMemberId7
      ,TeamMemberId8
      ,TeamMemberId9
      ,TeamMemberId10
      ,TeamMemberName1
      ,TeamMemberName2
      ,TeamMemberName3
      ,TeamMemberName4
      ,TeamMemberName5
      ,TeamMemberName6
      ,TeamMemberName7
      ,TeamMemberName8
      ,TeamMemberName9
      ,TeamMemberName10
      ,TeamAlias
      ,TeamIsDeleted
      ,TeamIsActive
      ,TeamOwner
      ,TeamCreated
      ,TeamUpdated
  FROM BI_Reporting_STG.ucr.rpt_Roster_Team

INSERT INTO dim.RosterTeamUnpivot
SELECT TeamId
      ,TeamRosterAdminId
      ,TeamName
      ,TeamType
      ,TeamTagLine
      ,TeamDesignations
      ,TeamMemberId
      ,TeamMemberName
      ,TeamAlias
      ,TeamIsDeleted
      ,TeamIsActive
      ,TeamOwner
      ,TeamCreated
      ,TeamUpdated
  FROM BI_Reporting_STG.ucr.rpt_Roster_Team_Unpivot

INSERT INTO dim.RosterUser
SELECT BrokerId
      ,BrokerName
      ,Persona_1
      ,Persona_2
      ,Persona_3
      ,EulaAgreedDate
      ,OfficeName
      ,OfficeId
      ,BrokerIsActive
      ,IsPrimaryOffice
	  ,MLSId
  FROM BI_Reporting_STG.ucr.rpt_Roster_User

INSERT INTO dim.TeamsContact
SELECT TeamId
      ,TeamName
      ,TeamMemberId
      ,TeamMemberName
      ,TeamIsDeleted
      ,TeamIsActive
      ,TeamCreated
      ,TeamUpdated
      ,ContactId
      ,ContactAddedDate
      ,ContactFirstName
      ,ContactLastName
      ,ContactStatus
      ,ContactEmail
FROM BI_Reporting_STG.ucr.rpt_Teams_Contact


INSERT INTO dim.ConsumerSavedSearch
SELECT
    ConsumerId,
    [Name],
    NotifyEnabled,
    LastNotifiedUtc,
    MarketInsightsEnabled,
    MarketInsightsFrequency,
    LastMarketInsightsUtc,
    [Value]
FROM BI_Reporting_STG.ucr.Consumer_SavedSearch







/*
INSERT INTO dim.Consumer_Notification
SELECT distinct
	SenderId
	,CreatedUtc,
	ConsumerId,
	SavedSearchId,
	FavoriteListingEvent
	,left(MarketInsights, 4000) as MarketInsights,
	[Sent],
	SentUtc
FROM BI_Reporting_STG.ucr.Consumer_Notification
WHERE SenderId IS NOT NULL
*/





END