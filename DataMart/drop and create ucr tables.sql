USE [BI_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[DropAndCreateUCRTables]    Script Date: 12/31/2019 2:31:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[DropAndCreateUCRTables] as
BEGIN

IF OBJECT_ID('dim.CRMContacts', 'U') IS NOT NULL
DROP TABLE dim.CRMContacts

CREATE TABLE dim.CRMContacts(
    CRMContactsKey int identity(1,1) not null,
	ContactId varchar(250) NOT NULL,
	ContactFirstName varchar(250) NULL,
	ContactLastName varchar(300) NULL,
	ContactStatus varchar(250) NULL,
	ContactEmail varchar(2500) NULL,
	ContactOwner varchar(250) NULL,
	ContactOwnerId int NULL,
	ContactAddedDate datetime NULL,
 CONSTRAINT pk_CRMContacts PRIMARY KEY CLUSTERED (CRMContactsKey))


IF OBJECT_ID('dim.CRMCampaigns', 'U') IS NOT NULL
DROP TABLE dim.CRMCampaigns

CREATE TABLE dim.CRMCampaigns (
    CrmCampaignsKey int identity(1,1) not null,
	CampaignId char(36) NOT NULL,
	CampaignName varchar(250) NULL,
	CampaignStatus varchar(250) NULL,
	CampaignType varchar(250) NULL,
	OwnerId bigint NULL,
	CampaignCreated date NULL,
	CampaignUpdated date NULL,
	CampaignOwnerId varchar(max) NULL,
	CampaignOwner varchar(max) NULL,
 CONSTRAINT pk_CRMCampaigns PRIMARY KEY CLUSTERED (CRMCampaignsKey))


IF OBJECT_ID('dim.CRMEmailRecipients', 'U') IS NOT NULL
DROP TABLE dim.CRMEmailRecipients

CREATE TABLE dim.CRMEmailRecipients (
    CRMEmailRecipientsKey int identity(1,1) not null,
	RowId bigint NULL,
	EmailId varchar(36) NULL,
	CampaignId varchar(36) NULL,
	ContactId varchar(36) NULL,
	Contact varchar(max) NULL,
	[Owner] varchar(255) NULL,
	OwnerId int NULL,
	IsInvalidEmail smallint NULL
 CONSTRAINT pk_CRMEmailRecipients PRIMARY KEY CLUSTERED (CRMEmailRecipientsKey))


IF OBJECT_ID('dim.CRMLists', 'U') IS NOT NULL
DROP TABLE dim.CRMLists

CREATE TABLE dim.CRMLists(
    CRMListsKey int identity(1,1) not null,
	RowId bigint NOT NULL,
	[Value] text NULL,
	Created bigint NOT NULL,
	Updated bigint NULL,
	Id char(36) NOT NULL,
	OwnerId bigint NULL,
 CONSTRAINT pk_CRMLists PRIMARY KEY CLUSTERED (CRMListsKey))


IF OBJECT_ID('dim.CRMListContact', 'U') IS NOT NULL
DROP TABLE dim.CRMListContact

CREATE TABLE dim.CRMListContact(
    CRMListContactKey int identity(1,1) not null,
	RowId bigint NULL,
	ListId varchar(36) NULL,
	ContactId varchar(36) NULL,
	ContactOrderBy varchar(255) NULL,
	ContactOwner varchar(255) NULL,
	ContactOwnerId int NULL,
	ListOwner varchar(255) NULL,
	ListOwnerId int NULL
 CONSTRAINT pk_CRMListContact PRIMARY KEY CLUSTERED (CRMListContactKey))


IF OBJECT_ID('dim.CRMCampaignLists', 'U') IS NOT NULL
DROP TABLE dim.CRMCampaignLists

CREATE TABLE dim.CRMCampaignLists(
    CRMCampaignListsKey int identity(1,1) not null,
	RowId bigint,
	CampaignId char(36) not null,
	ListId char(36) not null,
	Type text not null,
	Owner varchar(255) null,
	OwnerId int null,
 CONSTRAINT pk_CRMCampaignLists PRIMARY KEY CLUSTERED (CRMCampaignListsKey))

 
IF OBJECT_ID('dim.RosterContact', 'U') IS NOT NULL
DROP TABLE dim.RosterContact

CREATE TABLE dim.RosterContact(
    RosterContactKey int identity(1,1),
	BrokerId int NULL,
	BrokerName varchar(129) NULL,
	Persona_1 varchar(250) NULL,
	Persona_2 varchar(250) NULL,
	Persona_3 varchar(250) NULL,
	EulaAgreedDate nvarchar(10) NULL,
	OfficeName varchar(250) NULL,
	OfficeId int NULL,
	BrokerIsActive varchar(250) NULL,
	IsPrimaryOffice varchar(250) NULL,
	ContactId varchar(250) NOT NULL,
	ContactAddedDate datetime NULL,
	ContactFirstName varchar(250) NULL,
	ContactLastName varchar(300) NULL,
	ContactStatus varchar(250) NULL,
	ContactEmail varchar(2500) NULL,
 CONSTRAINT pk_RosterContact PRIMARY KEY CLUSTERED (RosterContactKey))


IF OBJECT_ID('dim.RosterList', 'U') IS NOT NULL
DROP TABLE dim.RosterList

CREATE TABLE dim.RosterList(
    RosterListKey int identity(1,1) NOT NULL,
	BrokerId int NULL,
	BrokerName varchar(129) NULL,
	Persona_1 varchar(250) NULL,
	Persona_2 varchar(250) NULL,
	Persona_3 varchar(250) NULL,
	EulaAgreedDate nvarchar(10) NULL,
	OfficeName varchar(250) NULL,
	OfficeId int NULL,
	BrokerIsActive varchar(250) NULL,
	ContactAddedDate nvarchar(19) NULL,
	CampaignId varchar(100) NULL,
	ContactId varchar(100) NULL,
 CONSTRAINT pk_RosterList PRIMARY KEY CLUSTERED (RosterListKey))


IF OBJECT_ID('dim.RosterOffice', 'U') IS NOT NULL
DROP TABLE dim.RosterOffice

CREATE TABLE dim.RosterOffice(
    RosterOfficeKey int identity(1,1) NOT NULL,
	OfficeId int NULL,
	OfficeTypeId tinyint NULL,
	OfficeTypeName varchar(50) NULL,
	IsAffiliate varchar(50) NULL,
	DBAAbbrev varchar(50) NULL,
	DBATitle varchar(100) NULL,
	DirectoryLocation varchar(250) NULL,
	MLSId int NULL,
	MLSName varchar(100) NULL,
	MLSCode varchar(50) NULL,
	LocationName varchar(100) NULL,
	Alias varchar(100) NULL,
	OfficeIsDeleted varchar(10) NULL,
	OfficeIsActive varchar(10) NULL,
	Latitude decimal(38, 6) NULL,
	Longitude decimal(38, 6) NULL,
 CONSTRAINT pk_RosterOffice PRIMARY KEY CLUSTERED (RosterOfficeKey))


IF OBJECT_ID('dim.RosterTeam', 'U') IS NOT NULL
DROP TABLE dim.RosterTeam

CREATE TABLE dim.RosterTeam(
    RosterTeamKey int identity(1,1) NOT NULL,
	TeamId int NOT NULL,
	TeamRosterAdminId int NULL,
	TeamName varchar(400) NULL,
	TeamGuid varchar(400) NULL,
	TeamType varchar(100) NULL,
	TeamTagLine varchar(400) NULL,
	TeamDesignations varchar(1000) NULL,
	TeamMembers varchar(1000) NULL,
	TeamMemberId1 varchar(250) NULL,
	TeamMemberId2 varchar(250) NULL,
	TeamMemberId3 varchar(250) NULL,
	TeamMemberId4 varchar(250) NULL,
	TeamMemberId5 varchar(250) NULL,
	TeamMemberId6 varchar(250) NULL,
	TeamMemberId7 varchar(250) NULL,
	TeamMemberId8 varchar(250) NULL,
	TeamMemberId9 varchar(250) NULL,
	TeamMemberId10 varchar(250) NULL,
	TeamMemberName1 varchar(250) NULL,
	TeamMemberName2 varchar(250) NULL,
	TeamMemberName3 varchar(250) NULL,
	TeamMemberName4 varchar(250) NULL,
	TeamMemberName5 varchar(250) NULL,
	TeamMemberName6 varchar(250) NULL,
	TeamMemberName7 varchar(250) NULL,
	TeamMemberName8 varchar(250) NULL,
	TeamMemberName9 varchar(250) NULL,
	TeamMemberName10 varchar(250) NULL,
	TeamAlias varchar(400) NULL,
	TeamIsDeleted varchar(25) NULL,
	TeamIsActive varchar(25) NULL,
	TeamOwner varchar(400) NULL,
	TeamCreated datetime NULL,
	TeamUpdated datetime NULL,
 CONSTRAINT pk_RosterTeam PRIMARY KEY CLUSTERED (RosterTeamKey))


IF OBJECT_ID('dim.RosterTeamUnpivot', 'U') IS NOT NULL
DROP TABLE dim.RosterTeamUnpivot

CREATE TABLE dim.RosterTeamUnpivot (
    RosterTeamUnpivotKey int identity(1,1) NOT NULL,
	TeamId int NOT NULL,
	TeamRosterAdminId int NULL,
	TeamName varchar(400) NULL,
	TeamType varchar(100) NULL,
	TeamTagLine varchar(400) NULL,
	TeamDesignations text NULL,
	TeamMemberId varchar(250) NULL,
	TeamMemberName varchar(250) NULL,
	TeamAlias varchar(400) NULL,
	TeamIsDeleted varchar(25) NULL,
	TeamIsActive varchar(25) NULL,
	TeamOwner varchar(400) NULL,
	TeamCreated datetime NULL,
	TeamUpdated datetime NULL,
 CONSTRAINT pk_RosterTeamUnpivot PRIMARY KEY CLUSTERED (RosterTeamUnpivotKey))


IF OBJECT_ID('dim.RosterUser', 'U') IS NOT NULL
DROP TABLE dim.RosterUser

CREATE TABLE dim.RosterUser (
    RosterUserKey int identity(1,1) NOT NULL,
	BrokerId int NULL,
	BrokerName varchar(129) NULL,
	Persona_1 varchar(250) NULL,
	Persona_2 varchar(250) NULL,
	Persona_3 varchar(250) NULL,
	EulaAgreedDate nvarchar(10) NULL,
	OfficeName varchar(250) NULL,
	OfficeId int NULL,
	BrokerIsActive varchar(250) NULL,
	IsPrimaryOffice varchar(250) NULL,
 CONSTRAINT pk_RosterUser PRIMARY KEY CLUSTERED (RosterUserKey))


IF OBJECT_ID('dim.TeamsContact', 'U') IS NOT NULL
DROP TABLE dim.TeamsContact

CREATE TABLE dim.TeamsContact(
	TeamsContactKey int IDENTITY(1,1) NOT NULL,
	TeamId int NOT NULL,
	TeamName varchar(400) NULL,
	TeamMemberId varchar(250) NULL,
	TeamMemberName varchar(250) NULL,
	TeamIsDeleted varchar(25) NULL,
	TeamIsActive varchar(25) NULL,
	TeamCreated datetime NULL,
	TeamUpdated datetime NULL,
	ContactId varchar(250) NOT NULL,
	ContactAddedDate datetime NULL,
	ContactFirstName varchar(250) NULL,
	ContactLastName varchar(300) NULL,
	ContactStatus varchar(250) NULL,
	ContactEmail varchar(2500) NULL,
 CONSTRAINT pk_TeamsContact PRIMARY KEY CLUSTERED (TeamsContactKey ASC))
 

--facts
IF OBJECT_ID('fact.CRMEmailRecipientsContacts', 'U') IS NOT NULL
DROP TABLE fact.CRMEmailRecipientsContacts

CREATE TABLE fact.CRMEmailRecipientsContacts (
	RosterContactKey int NOT NULL,
	EmailRecipientsKey int NOT NULL,
	EmailSentDate [nvarchar](10) NULL
	)

END