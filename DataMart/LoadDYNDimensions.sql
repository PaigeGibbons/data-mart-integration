USE [BI_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[LoadDYNDimensions]    Script Date: 2/19/2020 12:28:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[LoadDYNDimensions] as
BEGIN


--Merge office records to create slowly changing dimension for history
--inserts new/updated records to table and updates any non-key fields in existing records with no date changes

MERGE [dim].[JLSOffice] AS tgt
USING (SELECT HCCbranchno
            , HCCcompanyabbrev
            , HCCbranchname
            , HCCbranchstatus
            , HCCbegindate
            , HCCenddate
            , HCCaddr1
            , HCCaddr2
            , HCCcity
            , HCCstate
            , HCCzip
            , HCCcounty
            , HCCbranchphone
            , HCCfax
            , HCCemailaddr
            , HCCarea
            , HCCreportregionno
FROM BI_Reporting_STG.dyn.HBrnMstr ) as src
on src.HCCbranchno = tgt.BranchNo and src.HCCbranchname = tgt.BranchName and src.HCCcompanyabbrev = tgt.CompanyAbbrev and src.HCCbranchstatus = tgt.BranchStatus
  and src.HCCbegindate = tgt.BeginDate and src.HCCenddate = tgt.EndDate and src.HCCaddr1 = tgt.AddressLine1

WHEN NOT MATCHED BY TARGET THEN 
INSERT (branchno, companyabbrev, branchname, branchstatus, begindate, enddate, addressline1, addressLine2, AddressCity, AddressState, AddressZip, AddressCounty, BranchPhone
      , BranchFax, BranchEmailAddress, BranchArea, BranchReportRegionNo, RecordStartDate, RecordEndDate, IsCurrentRecord)
VALUES (src.HCCbranchno
       , src.HCCcompanyabbrev
       , src.HCCbranchname
       , src.HCCbranchstatus
       , src.HCCbegindate
       , src.HCCenddate
       , src.HCCaddr1
       , src.HCCaddr2
       , src.HCCcity
       , src.HCCstate
       , src.HCCzip
       , src.HCCcounty
       , src.HCCbranchphone
       , src.HCCfax
       , src.HCCemailaddr
       , src.HCCarea
       , src.HCCreportregionno
	   , getdate()
	   , '9999-12-31'
	   , 'Y'
);


update jlsofc
set RecordEndDate = dateadd(dd,-1,upd2.enddate)
  , isCurrentRecord = 0
from dim.JLSOffice jlsofc
   , (
   select jo.BranchNo, jo.CompanyAbbrev, RecordStartDate, enddate = max(recordstartdate) over (partition by jo.BranchNo, jo.CompanyAbbrev),
       row_number() over (partition by jo.branchno, jo.companyabbrev order by jo.recordstartdate) rownum
      from dim.JLSOffice jo
        join (select BranchNo, CompanyAbbrev, count(*) rcount
              from dim.JLSOffice
              where RecordEndDate = '9999-12-31'
              group by BranchNo, CompanyAbbrev
              having count(*) > 1 ) upd
	on jo.BranchNo = upd.BranchNo and jo.CompanyAbbrev = upd.CompanyAbbrev
	) upd2
where  jlsofc.BranchNo = upd2.BranchNo 
   and jlsofc.CompanyAbbrev = upd2.CompanyAbbrev
   and upd2.rownum = 1 and jlsofc.RecordStartDate = upd2.RecordStartDate


--Merge agent records to create slowly changing dimension for history
--inserts new/updated records to table and updates any non-key fields in existing records with no date changes
merge dim.JLSAgent as tgt
using (SELECT HCCagentmlsid
             ,HCCcompanyabbrev
             ,HCCagentno
             ,HCCbranchno
             ,HCCagentstatus
             ,HCCagentfnamelegal
             ,HCCagentlnamelegal
             ,HCCagentfnameprefer
             ,HCCagentlnameprefer
             ,HCCeffectivedate
             ,HCChiredate
             ,HCCanniversarydate
             ,HCCtransferdate
             ,HCCfrombranchno
             ,HCCreleasedate
             ,HCCreleasedescr
             ,HCCreleasedto
             ,HCCindustrystartdate
             ,HCCdualagent
             ,HCCrelocation
             ,HCCrelodesignation
             ,HCCebusiness
             ,HCCbrokeragreement
             ,HCCvendortypeno
             ,HCCspacetypeno
             ,HCCemailaddr
             ,HCCincorporated
             ,HCClanguages
             ,HCCcareerlaunch
             ,HCCcandidate
             ,HCClegacybroker
             ,HCCnotengaged
             ,HCClicensedesignation
             ,HCCsource
             ,HCCfromcompany
             ,StartDate = case when HCCreleasedate <> '1900-01-01' then HCCReleasedate 
	                           when HCCreleasedate = '1900-01-01' and HCCtransferdate <> '1900-01-01' and HCCtransferdate > dateadd(dd,-2,getdate()) then HCCtransferdate
		                       else HCCdatestamp end 
             ,EndDate = '9999-12-31'
       FROM BI_Reporting_STG.dyn.HAgtMstr) as src
ON src.HCCagentno = tgt.AgentNo and src.HCCbranchno = tgt.BranchNo and src.HCCcompanyabbrev = tgt.CompanyAbbrev and src.startdate = tgt.recordstartDate

WHEN MATCHED 
THEN UPDATE 
   SET tgt.AgentMLSId = src.HCCagentmlsid
     , tgt.AgentStatus = src.HCCagentstatus
     , tgt.AgentLegalFirstName = src.HCCagentfnamelegal
     , tgt.AgentLegalLastName = src.HCCagentlnamelegal
     , tgt.AgentPreferredFirstName = src.HCCagentfnameprefer
     , tgt.AgentPreferredLastName = src.HCCagentlnameprefer
     , tgt.EffectiveDate = src.HCCeffectivedate
     , tgt.HireDate = src.HCChiredate
     , tgt.AnniversaryDate = src.HCCanniversarydate
     , tgt.TransferDate = src.HCCtransferdate
     , tgt.FromBranchNo = src.HCCfrombranchno
     , tgt.ReleaseDate = src.HCCreleasedate
     , tgt.ReleaseDescr = src.HCCreleasedescr
     , tgt.ReleasedTo = src.HCCreleasedto
     , tgt.IndustryStartDate = src.HCCindustrystartdate
     , tgt.DualAgent = src.HCCdualagent
     , tgt.Relocation = src.HCCrelocation
     , tgt.ReloDesignation = src.HCCrelodesignation
     , tgt.ebusiness = src.HCCebusiness
     , tgt.BrokerAgreement = src.HCCbrokeragreement
     , tgt.VendorTypeNo = src.HCCvendortypeno
     , tgt.SpaceTypeNo = src.HCCspacetypeno
     , tgt.EmailAddress = src.HCCemailaddr
     , tgt.Incorporated = src.HCCincorporated
     , tgt.Languages = src.HCClanguages
     , tgt.CareerLaunch = src.HCCcareerlaunch
     , tgt.Candidate = src.HCCcandidate
     , tgt.LegacyBroker = src.HCClegacybroker
     , tgt.NotEngaged = src.HCCnotengaged
     , tgt.LicenseDesignation = src.HCClicensedesignation
     , tgt.FromSource = src.HCCsource
     , tgt.FromCompany = src.HCCfromcompany

WHEN NOT MATCHED BY TARGET THEN
INSERT ( AgentMLSId, CompanyAbbrev, AgentNo, BranchNo, AgentStatus, AgentLegalFirstName, AgentLegalLastName, AgentPreferredFirstName
       , AgentPreferredLastName, EffectiveDate, HireDate, AnniversaryDate, TransferDate, FromBranchNo, ReleaseDate, ReleaseDescr, ReleasedTo
	   , IndustryStartDate, DualAgent, Relocation, ReloDesignation, ebusiness, BrokerAgreement, VendorTypeNo, SpaceTypeNo, EmailAddress, Incorporated
	   , Languages, CareerLaunch, Candidate, LegacyBroker, NotEngaged, LicenseDesignation, FromSource, FromCompany, RecordStartDate, RecordEndDate, isCurrentRecord)
VALUES ( src.HCCagentmlsid
       , src.HCCcompanyabbrev
       , src.HCCagentno
       , src.HCCbranchno
       , src.HCCagentstatus
       , src.HCCagentfnamelegal
       , src.HCCagentlnamelegal
       , src.HCCagentfnameprefer
       , src.HCCagentlnameprefer
       , src.HCCeffectivedate
       , src.HCChiredate
       , src.HCCanniversarydate
       , src.HCCtransferdate
       , src.HCCfrombranchno
       , src.HCCreleasedate
       , src.HCCreleasedescr
       , src.HCCreleasedto
       , src.HCCindustrystartdate
       , src.HCCdualagent
       , src.HCCrelocation
       , src.HCCrelodesignation
       , src.HCCebusiness
       , src.HCCbrokeragreement
       , src.HCCvendortypeno
       , src.HCCspacetypeno
       , src.HCCemailaddr
       , src.HCCincorporated
       , src.HCClanguages
       , src.HCCcareerlaunch
       , src.HCCcandidate
       , src.HCClegacybroker
       , src.HCCnotengaged
       , src.HCClicensedesignation
       , src.HCCsource
       , src.HCCfromcompany
       , src.StartDate
       , src.EndDate
	   , 'Y'  --isCurrentRecord
	   )
;



-- change the end date of prior record to one day prior to start of new record
update jlsagt
set RecordEndDate = dateadd(dd,-1,enddate)
  , isCurrentRecord = 0
from dim.JLSAgent jlsagt
   , (select ja.AgentNo, ja.BranchNo, ja.CompanyAbbrev, recordstartdate, enddate = max(recordstartdate) over (partition by ja.AgentNo, ja.BranchNo, ja.CompanyAbbrev),
       row_number() over (partition by ja.agentno, ja.branchno, ja.companyabbrev order by ja.recordstartdate) rownum
      from dim.JLSAgent ja
        join (select AgentNo, BranchNo, CompanyAbbrev, count(*) rcount
              from dim.JLSAgent
              where RecordEndDate = '9999-12-31'
              group by AgentNo, BranchNo, CompanyAbbrev
              having count(*) > 1 ) upd
	on ja.AgentNo = upd.AgentNo and ja.BranchNo = upd.BranchNo and ja.CompanyAbbrev = upd.CompanyAbbrev) upd2
where  jlsagt.AgentNo = upd2.AgentNo 
   and jlsagt.BranchNo = upd2.BranchNo 
   and jlsagt.CompanyAbbrev = upd2.CompanyAbbrev
   and upd2.rownum = 1 and jlsagt.RecordStartDate = upd2.RecordStartDate



 END