--pended
select PendYear
     , PendMonth
     , PendMonthName
	 , case when datediff(dd,ListDate,PendDate) <= 30 THEN '0-30'
	        when datediff(dd,ListDate,PendDate) <= 60 THEN '31-60'
			when datediff(dd,ListDate,PendDate) <= 90 THEN '61-90'
			when datediff(dd,ListDate,PendDate) <= 120 THEN '91-120'
			when datediff(dd,ListDate,PendDate) <= 150 THEN '121-150'
			when datediff(dd,ListDate,PendDate) <= 180 THEN '151-180'
			when datediff(dd,ListDate,PendDate) > 180 or datediff(dd,ListDate,PendDate) is null THEN '180+/Not Sold/Expired/Off Market'
       end as DaysToSell
	 , case when ListPrice < 1000000 then PriceCategoryName when ListPrice >= 1000000 then 'Above 1,000,000' else 'Error' end
     , StateCode
     , County = CountyName + ' County'
	 , ReportingRegion
	 , Region = CASE WHEN MLSID=9 AND MLSAreaCode IN ('705','390','380','385','701','720','710','700','715','800','140') THEN 'Seattle'
                     WHEN MLSID=9 AND MLSAreaCode IN ('500','510','520','530','540','550','560','600','948') THEN 'Eastside King'
                     WHEN MLSID=9 AND MLSAreaCode IN ('100','110','120','130','360') THEN 'SW King County'
                     WHEN MLSID=9 AND MLSAreaCode IN ('310','320','300','330','340','350') THEN 'SE King County'
	                 WHEN StateCode = 'WA' AND CountyName in ('King','Kitsap','Pierce','Snohomish','Whatcom','Clark','Spokane','Thurston','Jefferson') THEN CountyName + ' County'
                     WHEN CountyName ='Deschutes' AND City='Bend' THEN 'Bend OR'
                     WHEN StateCode IN ('OR') AND CountyName IN ('Clackamas','Multnomah') THEN 'East Portland'
                     WHEN StateCode IN ('OR') AND CountyName IN ('Clearwater','Washington') THEN 'West Portland'
	                 WHEN StateCode = 'OR' AND CountyName IN ('Jackson','Columbia') THEN CountyName + ' County'
	                 WHEN StateCode in ('ID') AND CountyName = 'Kootenai' THEN 'Kootenai County'
                     ELSE 'Other' END

	   , PendedCount = count(mlslistingid)

from dbo.MLS_VIEW_ONE

where datediff(mm,PendDate, cast(getdate() as date)) between 0 and 18
  and StateCode in ('WA','OR','ID')

group by PendYear
     , PendMonth
     , PendMonthName
	 , case when datediff(dd,ListDate,PendDate) <= 30 THEN '0-30'
	        when datediff(dd,ListDate,PendDate) <= 60 THEN '31-60'
			when datediff(dd,ListDate,PendDate) <= 90 THEN '61-90'
			when datediff(dd,ListDate,PendDate) <= 120 THEN '91-120'
			when datediff(dd,ListDate,PendDate) <= 150 THEN '121-150'
			when datediff(dd,ListDate,PendDate) <= 180 THEN '151-180'
			when datediff(dd,ListDate,PendDate) > 180 or datediff(dd,ListDate,PendDate) is null THEN '180+/Not Sold/Expired/Off Market'
       end
	 , case when ListPrice < 1000000 then PriceCategoryName when ListPrice >= 1000000 then 'Above 1,000,000' else 'Error' end
     , StateCode
     , CountyName + ' County'
	 , ReportingRegion
	 , CASE WHEN MLSID=9 AND MLSAreaCode IN ('705','390','380','385','701','720','710','700','715','800','140') THEN 'Seattle'
                     WHEN MLSID=9 AND MLSAreaCode IN ('500','510','520','530','540','550','560','600','948') THEN 'Eastside King'
                     WHEN MLSID=9 AND MLSAreaCode IN ('100','110','120','130','360') THEN 'SW King County'
                     WHEN MLSID=9 AND MLSAreaCode IN ('310','320','300','330','340','350') THEN 'SE King County'
	                 WHEN StateCode = 'WA' AND CountyName in ('King','Kitsap','Pierce','Snohomish','Whatcom','Clark','Spokane','Thurston','Jefferson') THEN CountyName + ' County'
                     WHEN CountyName ='Deschutes' AND City='Bend' THEN 'Bend OR'
                     WHEN StateCode IN ('OR') AND CountyName IN ('Clackamas','Multnomah') THEN 'East Portland'
                     WHEN StateCode IN ('OR') AND CountyName IN ('Clearwater','Washington') THEN 'West Portland'
	                 WHEN StateCode = 'OR' AND CountyName IN ('Jackson','Columbia') THEN CountyName + ' County'
	                 WHEN StateCode in ('ID') AND CountyName = 'Kootenai' THEN 'Kootenai County'
                     ELSE 'Other' END

order by 1,2,4,5,6,7


--listings
select dt2.[Year]
     , dt2.month
     , [Price Range] = CASE WHEN L.ListPrice >= 0  AND L.ListPrice <= 249999 THEN '0-249,999'
                            WHEN L.ListPrice > 249999 AND L.ListPrice <= 349999 THEN '250,000 - 349,999'
                            WHEN L.ListPrice > 349999 AND L.ListPrice <= 499999 THEN '350,000 - 499,999'
                            WHEN L.ListPrice > 499999 AND L.ListPrice <= 749999 THEN '500,000 - 749,999'
                            WHEN L.ListPrice > 749999 AND L.ListPrice <= 999999 THEN '750,000 - 999,999'
                            WHEN L.ListPrice > 999999  THEN 'Above 1,000,000'
                            ELSE 'Error' End 
	 , Region = CASE WHEN Ld.MLSID=9 AND ld.MLSCode IN ('705','390','380','385','701','720','710','700','715','800','140') THEN 'Seattle'
                     WHEN Ld.MLSID=9 AND ld.MLSCode IN ('500','510','520','530','540','550','560','600','948') THEN 'Eastside King'
                     WHEN Ld.MLSID=9 AND ld.MLSCode IN ('100','110','120','130','360') THEN 'SW King County'
                     WHEN Ld.MLSID=9 AND ld.MLSCode IN ('310','320','300','330','340','350') THEN 'SE King County'
	                 WHEN ld.StateCode = 'WA' AND cty.CountyName in ('King','Kitsap','Pierce','Snohomish','Whatcom','Clark','Spokane','Thurston','Jefferson') THEN cty.CountyName + ' County'
                     WHEN Cty.CountyName ='Deschutes' AND Ld.City='Bend' THEN 'Bend OR'
                     WHEN ld.StateCode IN ('OR') AND Cty.CountyName IN ('Clackamas','Multnomah') THEN 'East Portland'
                     WHEN ld.StateCode IN ('OR') AND Cty.CountyName IN ('Clearwater','Washington') THEN 'West Portland'
	                 WHEN ld.StateCode = 'OR' AND Cty.CountyName IN ('Jackson','Columbia') THEN cty.CountyName + ' County'
	                 WHEN ld.StateCode in ('ID') AND Cty.CountyName = 'Kootenai' THEN 'Kootenai County'
                     ELSE 'Other' END
	   , ListedCount = count(l.listingdetailkey)
from [fact].[Listing] l
  left join dim.DateDimension dt2
	on l.listdatekey = dt2.DateKey
  left join dim.ListingDetail ld
    on l.Listingdetailkey = ld.ListingDetailKey
  left join dim.County cty
    on ld.CountyKey = cty.CountyKey

where datediff(mm,dt2.[Date], cast(getdate() as date)) between 0 and 18
  and ld.StateCode in ('WA','OR','ID')
group by dt2.[Year]
       , dt2.month
	   ,CASE WHEN L.ListPrice >= 0  AND L.ListPrice <= 249999 THEN '0-249,999'
        WHEN L.ListPrice > 249999 AND L.ListPrice <= 349999 THEN '250,000 - 349,999'
        WHEN L.ListPrice > 349999 AND L.ListPrice <= 499999 THEN '350,000 - 499,999'
        WHEN L.ListPrice > 499999 AND L.ListPrice <= 749999 THEN '500,000 - 749,999'
        WHEN L.ListPrice > 749999 AND L.ListPrice <= 999999 THEN '750,000 - 999,999'
        WHEN L.ListPrice > 999999  THEN 'Above 1,000,000'
        Else 'Error' End
	, CASE WHEN Ld.MLSID=9 AND ld.MLSCode IN ('705','390','380','385','701','720','710','700','715','800','140') THEN 'Seattle'
       WHEN Ld.MLSID=9 AND ld.MLSCode IN ('500','510','520','530','540','550','560','600','948') THEN 'Eastside King'
       WHEN Ld.MLSID=9 AND ld.MLSCode IN ('100','110','120','130','360') THEN 'SW King County'
       WHEN Ld.MLSID=9 AND ld.MLSCode IN ('310','320','300','330','340','350') THEN 'SE King County'
	   WHEN ld.StateCode = 'WA' AND cty.CountyName in ('King','Kitsap','Pierce','Snohomish','Whatcom','Clark','Spokane','Thurston','Jefferson') THEN cty.CountyName + ' County'
       WHEN Cty.CountyName ='Deschutes' AND Ld.City='Bend' THEN 'Bend OR'
       WHEN ld.StateCode IN ('OR') AND Cty.CountyName IN ('Clackamas','Multnomah') THEN 'East Portland'
       WHEN ld.StateCode IN ('OR') AND Cty.CountyName IN ('Clearwater','Washington') THEN 'West Portland'
	   WHEN ld.StateCode = 'OR' AND Cty.CountyName IN ('Jackson','Columbia') THEN cty.CountyName + ' County'
	   When ld.StateCode in ('ID') AND Cty.CountyName = 'Kootenai' THEN 'Kootenai County'
      Else 'Other' END

order by 1,2,3,4









