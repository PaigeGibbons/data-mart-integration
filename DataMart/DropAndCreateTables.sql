USE [BI_Reporting]
GO

/****** Object:  StoredProcedure [dbo].[DropAndCreateTables]    Script Date: 11/15/2019 3:10:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[DropAndCreateTables] as

BEGIN

/* Drop Foreign Key Constraints from fact Tables */
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_MLSKey
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_MLSAreaKey
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_RegionKey
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_ListingDetailKey
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_ListingAddressKey
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_ListingAgentKey
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_ListingOfficeKey
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_ListDateKey
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_UpdateDateKey
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_CloseDateKey
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_StatusDateKey
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_PendingDateKey
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_CancelDateKey
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_EstCompletionDateKey
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_ExpirationDateKey
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_FallThroughDateKey
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_LeaseDateKey
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_WithdrawDateKey
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_PropTypeKey
ALTER TABLE fact.Listing DROP CONSTRAINT fk_Listing_StatusKey


/* DROP and CREATE Dimension Tables */
IF OBJECT_ID('dim.ADUsers', 'U') IS NOT NULL
DROP TABLE dim.ADUsers

CREATE TABLE dim.ADUsers (
  AccountKey int identity(1,1) not null,
  AccountID nvarchar(50),
  AccountName nvarchar(50),
  DeptOffice  nvarchar(50),
  Title  nvarchar(100)
  )


IF OBJECT_ID('dim.County', 'U') IS NOT NULL
DROP TABLE dim.County

CREATE TABLE dim.County (
      CountyKey int identity(1,1)
    , CountyName varchar(100) null
	, StateCode varchar(5) null
	, CONSTRAINT pk_County PRIMARY KEY (CountyKey)
	)


IF OBJECT_ID('dim.MLS', 'U') IS NOT NULL
DROP TABLE dim.MLS

CREATE TABLE dim.MLS (
      MLSKey int identity(1,1)
	, MLSID int
	, MLSName varchar(100)
	, MLSAbbreviation varchar(10)
	, MLSFullName varchar(50)
	, CONSTRAINT pk_MLS PRIMARY KEY (MLSKey)
	)


IF OBJECT_ID('dim.MLSArea', 'U') IS NOT NULL
DROP TABLE dim.MLSArea

CREATE TABLE dim.MLSArea (
      MLSAreaKey int identity(1,1) not null
	, MLSID int NOT NULL
	, MLSAreaCode varchar(50) NOT NULL
	, MLSAreaDescription varchar(100) NULL
	, MLSAreaAbbrev varchar(100) NULL
	, CONSTRAINT pk_MLSArea PRIMARY KEY (MLSAreaKey)
) 


IF OBJECT_ID('dim.PropType', 'U') IS NOT NULL
DROP TABLE dim.PropType

CREATE TABLE dim.PropType (
      PropTypeKey int identity(1,1)
	, PropTypeID int 
	, PropTypeName varchar(50)
	, PropCategoryName varchar(50)
	, MLSID int 
	, MLSCode varchar(50)
	, MLSTableName varchar(50)
	, CONSTRAINT pk_PropType PRIMARY KEY (PropTypeKey)
	)


IF OBJECT_ID('dim.StatusType', 'U') IS NOT NULL
DROP TABLE dim.StatusType

CREATE TABLE dim.StatusType (
      StatusTypeKey int identity(1,1)
	, StatusID int
	, StatusName varchar(50)
	, StatusCategoryID smallint
	, StatusCategoryName varchar(50)
	, MLSID int
	, MLSCode varchar(50)
	, CONSTRAINT pk_StatusType PRIMARY KEY (StatusTypeKey)
	)


IF OBJECT_ID('dim.ListingAgent', 'U') IS NOT NULL
DROP TABLE dim.ListingAgent

CREATE TABLE dim.ListingAgent(
  	  ListingAgentKey int IDENTITY(1,1) NOT NULL
	, PrimaryListingAgentKey int
	, CoListingAgentKey int
	, CoListingAgent2Key int
	, CoListingMemberID varchar(50)
	, SellingAgentKey int
	, CoSellingAgentKey int
	, CoSellingAgent2Key int
	, CoSellingAgent3Key int
	, AgentBonus varchar(10)
    , CommissionCode varchar(100) NULL
    , CommissionComments varchar(200) NULL
    , Commission decimal(19, 2) NULL
    , SellingOfficeCommissionPercentage decimal(19, 2) NULL
	, StagingListingKey int
	, CONSTRAINT pk_ListingAgent PRIMARY KEY (ListingAgentKey)
    )


IF OBJECT_ID('dim.ListingOffice', 'U') IS NOT NULL
DROP TABLE dim.ListingOffice

CREATE TABLE dim.ListingOffice(
	  ListingOfficeKey int IDENTITY(1,1) NOT NULL
	, PrimaryOfficeKey int
	, CoListingOfficeKey int
	, SellingOfficeKey int
	, CoSellingOfficeKey int
	, CoSellingOffice2Key int
	, StagingListingKey int
	, CONSTRAINT pk_ListingOffice PRIMARY KEY (ListingOfficeKey)
    )

IF OBJECT_ID('dim.ListingAddress', 'U') IS NOT NULL
DROP TABLE dim.ListingAddress

CREATE TABLE dim.ListingAddress(
	  ListingAddressKey int identity(1,1) not null
	, StreetAddress varchar(100) NULL
	, StreetAddress2 varchar(100) NULL
	, StreetDirPrefix varchar(25) NULL
	, StreetDirSuffix varchar(25) NULL
	, StreetName varchar(50) NULL
	, StreetNumber varchar(25) NULL
	, StreetTypeSuffix varchar(25) NULL
	, StreetNumberModifier varchar(50) NULL
	, City varchar(50) NULL
	, CountyKey int NULL
	, StateCode varchar(6) NULL
	, PostalCode varchar(10) NULL
	, PostalCodeExt varchar(5) NULL
	, Latitude decimal(9, 6) NULL
	, Longitude decimal(9, 6) NULL
	, GeogLocation geography NULL
	, GeomLocation geometry NULL
	, StagingListingKey int
	, CONSTRAINT pk_ListingAddress PRIMARY KEY (ListingAddressKey)
    )

IF OBJECT_ID('dim.ListingDetail', 'U') IS NOT NULL
DROP TABLE dim.ListingDetail

CREATE TABLE dim.ListingDetail (
      ListingDetailKey int identity(1,1)
    , Acreage decimal(9, 2) NULL
	, Age int NULL
	, AgentOwned char(1) NULL
	, Auction char(1) NULL 
	, BankOwned tinyint NULL
	, BathsFull smallint NULL
	, BathsPartial smallint NULL
	, BathsTotal smallint NULL
	, Bedrooms smallint NULL
	, BodyofWaterName varchar(100) NULL
	, BuilderName varchar(50) NULL
	, BuildingName varchar(100) NULL
	, Community varchar(100) NULL
	, ElementarySchool varchar(50) NULL
	, Fireplaces smallint NULL
	, Garage tinyint NULL
	, GarageSpaces smallint NULL
	, HighSchool varchar(50) NULL
	, HowSold varchar(50) NULL
	, IDX tinyint NULL
	, LotSizeSF int NULL
	, MiddleSchool varchar(50) NULL
	, NewConstruction varchar(50) NULL
	, ParkingSpaces smallint NULL
	, PictureCount int NULL
	, PropertyTax decimal(19, 2) NULL
	, SchoolDistrict varchar(50) NULL
	, ShortSale tinyint NULL
	, ShowAddress tinyint NULL
	, ShowMap tinyint NULL
	, SquareFootage int NULL
	, TaxID varchar(50) NULL
	, TaxYearBuilt smallint NULL
	, VideoTour varchar(max) NULL
	, VirtualTour varchar(max) NULL
	, Waterfront varchar(200) NULL
	, YearBuilt smallint NULL
	, Zoning varchar(50) NULL
	, StagingListingKey int
    , CONSTRAINT pk_ListingDetail PRIMARY KEY (ListingDetailKey)
    )


/* DROP and CREATE FACT TABLES   */
IF OBJECT_ID('fact.Listing', 'U') IS NOT NULL
  DROP TABLE fact.Listing

  CREATE TABLE fact.Listing (
        MLSListingID varchar(25)
	  , MLSKey int
	  , MLSAreaKey int
      , RegionKey int
      , ListingDetailKey int
      , ListingAddressKey int
      , ListingAgentKey int
	  , ListingOfficeKey int
	  , ListDateKey int
	  , UpdateDateKey int
	  , CloseDateKey int
	  , StatusDateKey int
	  , PendingDateKey int
	  , CancelDateKey int NULL
	  , EstCompletionDateKey int NULL
	  , ExpirationDateKey int NULL
	  , FallThroughDateKey int NULL
	  , LeaseDateKey int NULL
	  , WithdrawDateKey int NULL
      , PropTypeKey int null
      , StatusKey int null
	  , PriceDisplay int
	  , OriginalListPrice int
	  , ListPrice int
	  , ClosePrice int
	  , HOADues int
	  , AgentHitCount int NULL
	  , DailyTrafficCount decimal(19,6) NULL
	  , CDOM int NULL
	  , DOM int NULL
	  , JLSDom int NULL
	  , CONSTRAINT fk_Listing_MLSKey FOREIGN KEY (MLSKey) REFERENCES dim.MLS(MLSKey)
	  , CONSTRAINT fk_Listing_MLSAreaKey FOREIGN KEY (MLSAreaKey) REFERENCES dim.MLSArea(MLSAreaKey)
	  , CONSTRAINT fk_Listing_RegionKey FOREIGN KEY (RegionKey) REFERENCES dim.Region(RegionKey)
	  , CONSTRAINT fk_Listing_ListingDetailKey FOREIGN KEY (ListingDetailKey) REFERENCES dim.ListingDetail(ListingDetailKey)
	  , CONSTRAINT fk_Listing_ListingAddressKey FOREIGN KEY (ListingAddressKey) REFERENCES dim.ListingAddress(ListingAddressKey)
	  , CONSTRAINT fk_Listing_ListingAgentKey FOREIGN KEY (ListingAgentKey) REFERENCES dim.ListingAgent(ListingAgentKey)
	  , CONSTRAINT fk_Listing_ListingOfficeKey FOREIGN KEY (ListingOfficeKey) REFERENCES dim.ListingOffice(ListingOfficeKey)
	  , CONSTRAINT fk_Listing_ListDateKey FOREIGN KEY (ListDateKey) REFERENCES dim.DateDimension(DateKey)
	  , CONSTRAINT fk_Listing_UpdateDateKey FOREIGN KEY (UpdateDateKey) REFERENCES dim.DateDimension(DateKey)
	  , CONSTRAINT fk_Listing_CloseDateKey FOREIGN KEY (CloseDateKey) REFERENCES dim.DateDimension(DateKey)
	  , CONSTRAINT fk_Listing_StatusDateKey FOREIGN KEY (StatusDateKey) REFERENCES dim.DateDimension(DateKey)
	  , CONSTRAINT fk_Listing_PendingDateKey FOREIGN KEY (PendingDateKey) REFERENCES dim.DateDimension(DateKey)
	  , CONSTRAINT fk_Listing_CancelDateKey FOREIGN KEY (CancelDateKey) REFERENCES dim.DateDimension(DateKey)
	  , CONSTRAINT fk_Listing_EstCompletionDateKey FOREIGN KEY (EstCompletionDateKey) REFERENCES dim.DateDimension(DateKey)
	  , CONSTRAINT fk_Listing_ExpirationDateKey FOREIGN KEY (ExpirationDateKey) REFERENCES dim.DateDimension(DateKey)
	  , CONSTRAINT fk_Listing_FallThroughDateKey FOREIGN KEY (FallThroughDateKey) REFERENCES dim.DateDimension(DateKey)
	  , CONSTRAINT fk_Listing_LeaseDateKey FOREIGN KEY (LeaseDateKey) REFERENCES dim.DateDimension(DateKey)
	  , CONSTRAINT fk_Listing_WithdrawDateKey FOREIGN KEY (WithdrawDateKey) REFERENCES dim.DateDimension(DateKey)
	  , CONSTRAINT fk_Listing_PropTypeKey FOREIGN KEY (PropTypeKey) REFERENCES dim.PropType(PropTypeKey)
	  , CONSTRAINT fk_Listing_StatusKey FOREIGN KEY (StatusKey) REFERENCES dim.StatusType(StatusTypeKey)

	  )






/************************************************************************************************************/
/*   For reference only - do not drop and recreate tables unless they need to be rebuilt from scratch.      */
/*   The below tables are merged so don't need to be rebuilt                                                */
/************************************************************************************************************/
--DROP TABLE dim.Agent

--CREATE TABLE dim.Agent (
--  AgentKey int identity(1,1),
--  MLSID	int,
--  AgentMLSID varchar(50),
--  AgentName varchar(100),
--  isActive smallint,
--  AuxAgentID varchar(50),
--  MLSInternalAgentID varchar(50),
--  CONSTRAINT pk_Agent PRIMARY KEY (AgentKey) 
--  )


--DROP TABLE dim.Office

--CREATE TABLE dim.Office(
--      OfficeKey int IDENTITY(1,1) NOT NULL
--	, MLSID int NULL
--	, OfficeMLSID varchar(50) NULL
--	, OfficeName varchar(100) NULL
--	, OfficeStatus smallint NULL
--	, InsertedDate datetime NULL
--	, UpdatedDate datetime NULL
--    , CONSTRAINT [pk_Office] PRIMARY KEY (OfficeKey)
--    )




	END
GO

