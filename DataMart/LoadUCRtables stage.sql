USE [BI_Reporting_STG]
GO
/****** Object:  StoredProcedure [dbo].[LoadUCRTables]    Script Date: 12/31/2019 2:13:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[LoadUCRTables] as
BEGIN

--base tables
TRUNCATE TABLE ucr.CRM_Lists;
INSERT INTO ucr.CRM_Lists
SELECT * from Openquery([LNK_JLSDB_RO],
	'select RowId
           ,Value
           ,Created
           ,Updated
           ,Id
		   ,OwnerId
     from bi_reporting.Crm_Lists;')


TRUNCATE TABLE ucr.CRM_ListContact; 
INSERT INTO ucr.CRM_ListContact
SELECT * from Openquery([LNK_JLSDB_RO],
	'select RowId
           ,ListId
           ,ContactId
           ,ContactOrderBy
           ,ContactOwner
           ,ContactOwnerId
           ,ListOwner
           ,ListOwnerId
     from JlsDB.Crm_ListContact;')


TRUNCATE TABLE ucr.CRM_CampaignLists; 
INSERT INTO ucr.CRM_CampaignLists
SELECT * from Openquery([LNK_JLSDB_RO],
	'select RowId
           ,CampaignId
           ,ListId
           ,Type
		   ,Owner
		   ,OwnerId
     from JlsDB.Crm_CampaignLists;')

TRUNCATE TABLE ucr.CRM_Emails
INSERT INTO ucr.CRM_Emails
SELECT * from Openquery([LNK_JLSDB_RO],
	'select StatusCode
           ,RowId
           ,Value
           ,Created
           ,Updated
           ,Id
     from bi_reporting.Crm_Emails')

/* commenting out below base tables as they are not currently referenced and are causing intermittent failures
TRUNCATE TABLE ucr.CRM_Campaigns; 
INSERT INTO ucr.CRM_Campaigns
SELECT * from Openquery([LNK_JLSDB_RO],
	'select StatusCode
		   ,RowId
           ,Value
           ,Created
           ,Updated
           ,Id
     from bi_reporting.Crm_Campaigns;' )

--failed
TRUNCATE TABLE ucr.CRM_Contacts; 
INSERT INTO ucr.CRM_Contacts
SELECT * from Openquery([LNK_JLSDB_RO],
	'select ConsumerUserId 
           ,StatusCode
           ,RowId
           ,Value
           ,Created
           ,Updated
           ,Id
     from bi_reporting.Crm_Contacts;')

--failed
TRUNCATE TABLE ucr.CRM_EmailRecipients
INSERT INTO ucr.CRM_EmailRecipients
SELECT * from Openquery([LNK_JLSDB_RO],
	'select RowId
           ,EmailId
           ,CampaignId
           ,ContactId
           ,Contact
           ,Owner
           ,OwnerId
           ,IsInvalidEmail
     from JlsDB.Crm_EmailRecipients')



TRUNCATE TABLE ucr.CRM_EmailsIndex
INSERT INTO ucr.CRM_EmailsIndex
SELECT * from Openquery([LNK_JLSDB_RO],
	'select CampaignId
           ,DateScheduled
           ,DateSent
           ,Status
           ,Scope
           ,Owner
           ,OwnerId
           ,RowId
           ,Id
           ,Created
     from bi_reporting.Crm_EmailsIndex')


TRUNCATE TABLE ucr.Roster_Office; 
INSERT INTO ucr.Roster_Office
SELECT * from Openquery([LNK_JLSDB_RO],
	'select Latitude
      ,Longitude
      ,Id
      ,RosterAdminId
      ,RelianceId
      ,Name
      ,Guid
      ,Value
      ,Created
      ,Updated
from bi_reporting.Roster_Office')

TRUNCATE TABLE ucr.Roster_Team; 
INSERT INTO ucr.Roster_Team
SELECT * from Openquery([LNK_JLSDB_RO],
	'select Id
           ,RosterAdminId
           ,RelianceId
           ,Name
           ,Guid
           ,Value
           ,Created
           ,Updated
     from JlsDB.Roster_Team;')

TRUNCATE TABLE ucr.Roster_TeamMemberIndex; 
INSERT INTO ucr.Roster_TeamMemberIndex
SELECT * from Openquery([LNK_JLSDB_RO],
	'select UserId
           ,TeamId
           ,MemberType
           ,RowId
           ,Id
           ,Created
           ,Updated
     from JlsDB.Roster_TeamMemberIndex;')

TRUNCATE TABLE ucr.Roster_User; 
INSERT INTO ucr.Roster_User
SELECT * from Openquery([LNK_JLSDB_RO],
	'select HasImage
           ,FirstName
           ,LastName
           ,Id
           ,RosterAdminId
           ,RelianceId
           ,Name
           ,Guid
           ,Value
           ,Created
           ,Updated
     from JlsDB.Roster_User')
*/

--fact and dims
TRUNCATE TABLE ucr.rpt_Contacts; 
INSERT INTO ucr.rpt_Contacts
SELECT * from Openquery([LNK_JLSDB_RO],
	'select ContactId
           ,ContactFirstName
           ,ContactLastName
           ,ContactStatus
           ,ContactEmail
           ,ContactOwner
           ,ContactOwnerId
           ,ContactAddedDate
     from bi_reporting.rpt_Contacts;')

TRUNCATE TABLE ucr.rpt_Crm_Campaigns; 
INSERT INTO ucr.rpt_Crm_Campaigns
SELECT * from Openquery([LNK_JLSDB_RO],
	'select CampaignId
           ,CampaignName
           ,CampaignStatus
           ,CampaignType
           ,OwnerId
           ,CampaignCreated
           ,CampaignUpdated
           ,CampaignOwnerId
           ,CampaignOwner
     from bi_reporting.rpt_Crm_Campaigns')

TRUNCATE TABLE ucr.rpt_CRM_EmailRecipients; 
INSERT INTO ucr.rpt_CRM_EmailRecipients
SELECT * from Openquery([LNK_JLSDB_RO],
	'select CampaignId
           ,ContactId
           ,EmailId
           ,EmailSentDate
     from bi_reporting.rpt_CRM_EmailRecipients')

TRUNCATE TABLE ucr.rpt_CRM_EmailRecipients_Contacts; 
INSERT INTO ucr.rpt_CRM_EmailRecipients_Contacts
SELECT * from Openquery([LNK_JLSDB_RO],
	'select ContactId
           ,EmailId
           ,EmailSentDate
     from bi_reporting.rpt_CRM_EmailRecipients_Contacts')

TRUNCATE TABLE ucr.rpt_Roster_Contact; 
INSERT INTO ucr.rpt_Roster_Contact
SELECT * from Openquery([LNK_JLSDB_RO],
	'select BrokerId
           ,BrokerName
           ,Persona_1
           ,Persona_2
           ,Persona_3
           ,EulaAgreedDate
           ,OfficeName
           ,OfficeId
           ,BrokerIsActive
           ,IsPrimaryOffice
           ,ContactId
           ,ContactAddedDate
           ,ContactFirstName
           ,ContactLastName
           ,ContactStatus
           ,ContactEmail
     from bi_reporting.rpt_Roster_Contact')

TRUNCATE TABLE ucr.rpt_Roster_List; 
INSERT INTO ucr.rpt_Roster_List
SELECT * from Openquery([LNK_JLSDB_RO],
	'select BrokerId
           ,BrokerName
           ,Persona_1
           ,Persona_2
           ,Persona_3
           ,EulaAgreedDate
           ,OfficeName
           ,OfficeId
           ,BrokerIsActive
           ,ContactAddedDate
           ,CampaignId
           ,ContactId
     from bi_reporting.rpt_Roster_List')

TRUNCATE TABLE ucr.rpt_Roster_Office; 
INSERT INTO ucr.rpt_Roster_Office
SELECT * from Openquery([LNK_JLSDB_RO],
	'select OfficeId
           ,OfficeTypeId
           ,OfficeTypeName
           ,IsAffiliate
           ,DBAAbbrev
           ,DBATitle
           ,DirectoryLocation
           ,MLSId
           ,MLSName
           ,MLSCode
           ,LocationName
           ,Alias
           ,OfficeIsDeleted
           ,OfficeIsActive
           ,Latitude
           ,Longitude
     from bi_reporting.rpt_Roster_Office')

TRUNCATE TABLE ucr.rpt_Roster_Team; 
INSERT INTO ucr.rpt_Roster_Team
SELECT * from Openquery([LNK_JLSDB_RO],
	'select TeamId
           ,TeamRosterAdminId
           ,TeamName
           ,TeamGuid
           ,TeamType
           ,TeamTagLine
           ,TeamDesignations
           ,TeamMembers
           ,TeamMemberId1
           ,TeamMemberId2
           ,TeamMemberId3
           ,TeamMemberId4
           ,TeamMemberId5
           ,TeamMemberId6
           ,TeamMemberId7
           ,TeamMemberId8
           ,TeamMemberId9
           ,TeamMemberId10
           ,TeamMemberName1
           ,TeamMemberName2
           ,TeamMemberName3
           ,TeamMemberName4
           ,TeamMemberName5
           ,TeamMemberName6
           ,TeamMemberName7
           ,TeamMemberName8
           ,TeamMemberName9
           ,TeamMemberName10
           ,TeamAlias
           ,TeamIsDeleted
           ,TeamIsActive
           ,TeamOwner
           ,TeamCreated
           ,TeamUpdated
     from bi_reporting.rpt_Roster_Team')

TRUNCATE TABLE ucr.rpt_Roster_Team_Unpivot; 
INSERT INTO ucr.rpt_Roster_Team_Unpivot
SELECT * from Openquery([LNK_JLSDB_RO],
	'select TeamId
	       ,TeamRosterAdminId
	       ,TeamName
	       ,TeamType
	       ,TeamTagLine
	       ,TeamDesignations
	       ,TeamMemberId
	       ,TeamMemberName
	       ,TeamAlias
	       ,TeamIsDeleted
	       ,TeamIsActive
	       ,TeamOwner
	       ,TeamCreated
	       ,TeamUpdated
	 from bi_reporting.rpt_Roster_Team_Unpivot')

TRUNCATE TABLE ucr.rpt_Roster_User; 
INSERT INTO ucr.rpt_Roster_User
SELECT * from Openquery([LNK_JLSDB_RO],
	'select BrokerId
           ,BrokerName
           ,Persona_1
           ,Persona_2
           ,Persona_3
           ,EulaAgreedDate
           ,OfficeName
           ,OfficeId
           ,BrokerIsActive
           ,IsPrimaryOffice
     from bi_reporting.rpt_Roster_User')

TRUNCATE TABLE ucr.rpt_Teams_Contact; 
INSERT INTO ucr.rpt_Teams_Contact
SELECT * from Openquery([LNK_JLSDB_RO],
	'select TeamId
           ,TeamName
           ,TeamMemberId
           ,TeamMemberName
           ,TeamIsDeleted
           ,TeamIsActive
           ,TeamCreated
           ,TeamUpdated
           ,ContactId
           ,ContactAddedDate
           ,ContactFirstName
           ,ContactLastName
           ,ContactStatus
           ,ContactEmail
     from bi_reporting.rpt_Teams_Contact')

END