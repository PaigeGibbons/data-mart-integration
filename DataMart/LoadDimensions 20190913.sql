USE [BI_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[LoadDimensions]    Script Date: 9/13/2019 10:22:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[LoadDimensions] as 
BEGIN

TRUNCATE TABLE dim.County;

INSERT INTO dim.County 
select CountyName
	 , StateCode = s.StateAbbrev

from BI_Reporting_STG.mls.County c
  left join
     BI_Reporting_STG.mls.[state] s
	on c.stateid = s.stateid


TRUNCATE TABLE dim.PropType;
/* Need to figure out how to join NewConstruction to this for category */
INSERT INTO dim.PropType
SELECT PropTypeName
     , PropCategoryName
	 , MLSID
	 , MLSCode
	 , MLSTableName
FROM BI_Reporting_STG.mls.PropType pt
  LEFT JOIN
     BI_Reporting_STG.mls.PropCategory pc
	on pt.PropCategoryID = pc.PropCategoryID


TRUNCATE TABLE dim.MLS

INSERT INTO dim.MLS
SELECT ma.MLSCode
     , m.MLSID
 	 , m.MLSName
 	 , m.MLSAbbreviation
	 , m.MLSFullName
FROM (SELECT distinct MLSID, MLSCode
      FROM [BI_Reporting_STG].[mls].[MLSAreas]) ma
LEFT JOIN
	  BI_Reporting_STG.mls.MLS m
   on m.MLSID = ma.MLSID


TRUNCATE TABLE dim.ListingDetail

INSERT INTO dim.ListingDetail
SELECT list.MLSListingID
     , MLSKey = mlsdim.mlskey
	 , list.MLSID
	 , mls.MLSCode
	 , StatusKey = NULL
     , PropTypeKey
     , StreetAddress 
     , StreetAddress2 
     , City
     , CountyKey = ctydim.CountyKey
     , StateCode = cty.StateAbbrev
     , PostalCode
     , PostalCodeExt
	 , Latitude
     , Longitude
     , BuildingName 
     , Bedrooms 
     , BathsFull
     , BathsPartial 
     , BathsTotal 
     , SquareFootage
     , LotSizeSF
     , Acreage
     , GarageSpaces
     , ParkingSpaces
     , YearBuilt
     , TaxYearBuilt
     , TaxID
     , PropertyTax
     , GeogLocation
     , ElementarySchool
     , MiddleSchool
     , HighSchool
     , SchoolDistrict
     , IDX
     , Fireplaces
     , ParentPropTypeID
     , BankOwned
     , ShortSale
     , CDOM
     , DOM
     , VirtualTour
     , VideoTour
     , ShowAddress
     , ShowMap
     , Garage
     , JlsDom
     , GeomLocation
     , PropertyType =  'TBD'

FROM BI_Reporting_STG.mls.Listing list
  LEFT JOIN (select CountyID, CountyName, StateAbbrev
             from BI_Reporting_STG.mls.County c
               left join BI_Reporting_STG.mls.[State] s
                 on c.StateID = s.StateID ) cty
    on list.CountyID = cty.CountyID
  LEFT JOIN dim.County ctydim
    on cty.CountyName = ctydim.CountyName and cty.StateAbbrev = ctydim.StateCode
  LEFT JOIN (select MLSListingID, MLSCode, ma.MLSID, MLSAbbreviation
             from BI_Reporting_STG.mls.MLSAreas ma
               left join BI_Reporting_STG.mls.MLS m
                 on ma.mlsid = m.mlsid) mls
    on mls.MLSListingID = list.MLSListingID and mls.MLSID = list.MLSID
  LEFT JOIN dim.MLS mlsdim
    on mls.MLSCode = mlsdim.MLSCode and mls.MLSAbbreviation = mlsdim.MLSAbbreviation and mls.MLSID = mlsdim.MLSID
  LEFT JOIN dim.PropType pt
    on pt.MLSCode = mls.MLSCode and pt.MLSID = mls.MLSID

END