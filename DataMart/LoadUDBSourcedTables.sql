USE [BI_Reporting_STG]
GO
/****** Object:  StoredProcedure [dbo].[LoadUDBSourcedTables]    Script Date: 10/16/2019 11:55:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[LoadUDBSourcedTables] as
BEGIN

--Temporary Solution to refresh some of the dims that don't change frequently


TRUNCATE TABLE mls.County

INSERT INTO mls.County
SELECT CountyID
     , CountyName
	 , StateID
FROM UDB.dbo.County

TRUNCATE TABLE mls.[State]

INSERT INTO mls.[State]
SELECT StateID
     , StateAbbrev
	 , StateName
	 , CountryID
FROM UDB.dbo.[State]

TRUNCATE TABLE mls.MLS

INSERT INTO mls.MLS
SELECT MLSID
      ,MLSName
      ,MLSAbbreviation
      ,oldMLSID
      ,IDSubFranchise
      ,Disclaimer
      ,DisplayMLSIcon
      ,MLSFullName
      ,AllStatus
      ,MarketInsights
      ,DisplaySold
      ,DisplayPending
FROM UDB.dbo.MLS


END