USE [BI_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[LoadDimensions]    Script Date: 10/11/2019 3:44:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[LoadDimensions] as 

BEGIN


TRUNCATE TABLE dim.County;

INSERT INTO dim.County 
select CountyName
	 , StateCode = s.StateAbbrev

from BI_Reporting_STG.mls.County c
  left join
     BI_Reporting_STG.mls.[state] s
	on c.stateid = s.stateid


TRUNCATE TABLE dim.PropType;

INSERT INTO dim.PropType
SELECT PropTypeName
     , PropCategoryName
	 , MLSID
	 , MLSCode
	 , MLSTableName
FROM BI_Reporting_STG.mls.PropType pt
  LEFT JOIN
     BI_Reporting_STG.mls.PropCategory pc
	on pt.PropCategoryID = pc.PropCategoryID


TRUNCATE TABLE dim.MLS

INSERT INTO dim.MLS
SELECT m.MLSID
     , ma.MLSCode
 	 , m.MLSName
 	 , m.MLSAbbreviation as Company
	 , m.MLSFullName
FROM (SELECT distinct MLSID, MLSCode
      FROM [BI_Reporting_STG].[mls].[MLSAreas]) ma
LEFT JOIN
	  BI_Reporting_STG.mls.MLS m
   on m.MLSID = ma.MLSID


DELETE FROM dim.ListingDetail

INSERT INTO dim.ListingDetail
SELECT list.MLSListingID
     , MLSKey = mlsdim.mlskey
	 , list.MLSID
	 , mls.MLSCode
	 , StatusKey = NULL
     , PropTypeKey
     , StreetAddress 
     , StreetAddress2 
     , City
     , CountyKey = ctydim.CountyKey
     , StateCode = cty.StateAbbrev
     , PostalCode
     , PostalCodeExt
	 , Latitude
     , Longitude
     , BuildingName 
     , Bedrooms 
     , BathsFull
     , BathsPartial 
     , BathsTotal 
     , SquareFootage
     , LotSizeSF
     , Acreage
     , GarageSpaces
     , ParkingSpaces
     , YearBuilt
     , TaxYearBuilt
     , TaxID
     , PropertyTax
     , GeogLocation
     , ElementarySchool
     , MiddleSchool
     , HighSchool
     , SchoolDistrict
     , IDX
     , Fireplaces
     , ParentPropTypeID
     , BankOwned
     , ShortSale
     , CDOM
     , DOM
     , VirtualTour
     , VideoTour
     , ShowAddress
     , ShowMap
     , Garage
     , JlsDom
     , GeomLocation
     , PropertyType =  'TBD'

FROM BI_Reporting_STG.mls.Listing list
  LEFT JOIN (select CountyID, CountyName, StateAbbrev
             from BI_Reporting_STG.mls.County c
               left join BI_Reporting_STG.mls.[State] s
                 on c.StateID = s.StateID ) cty
    on list.CountyID = cty.CountyID
  LEFT JOIN dim.County ctydim
    on cty.CountyName = ctydim.CountyName and cty.StateAbbrev = ctydim.StateCode
  LEFT JOIN (select MLSListingID, MLSCode, ma.MLSID, MLSAbbreviation
             from BI_Reporting_STG.mls.MLSAreas ma
               left join BI_Reporting_STG.mls.MLS m
                 on ma.mlsid = m.mlsid) mls
    on mls.MLSListingID = list.MLSListingID and mls.MLSID = list.MLSID
  LEFT JOIN dim.MLS mlsdim
    on mls.MLSCode = mlsdim.MLSCode and mls.MLSAbbreviation = mlsdim.MLSAbbreviation and mls.MLSID = mlsdim.MLSID
  LEFT JOIN dim.PropType pt
    on pt.MLSCode = mls.MLSCode and pt.MLSID = mls.MLSID


MERGE INTO dim.Agent AS tgt
USING (SELECT agt.MLSID
            , agt.AgentMLSID
            , AgentName = AgentFirstName + ' ' + AgentLastName
            , isActive = AgentActive
            , AuxAgentID = isnull(AuxAgentID,'')
            , MLSInternalAgentID = isnull(MLSInternalAgentID,'')
			, AgentUpdateDate
			, AgentModTime
       FROM BI_Reporting_STG.mls.Agent_Test agt
	    join (SELECT MLSID, AgentMLSID, max(AgentModTime) maxdate
		      from BI_Reporting_STG.mls.Agent_Test
			  group by MLSID, AgentMLSID) maxdt
		  on agt.MLSID = maxdt.MLSID and agt.AgentMLSID = maxdt.AgentMLSID and agt.AgentModTime = maxdt.maxdate
	   WHERE AgentModTime > dateadd(dd,-7,getdate()) 
  ) AS src
  ON   tgt.MLSID = src.MLSID 
   AND tgt.AgentMLSID = src.AgentMLSID 
 
WHEN MATCHED THEN 
UPDATE SET tgt.AgentName = src.AgentName
         , tgt.isActive = src.isActive
		 , tgt.AuxAgentID = src.AuxAgentID 
         , tgt.MLSInternalAgentID = src.MLSInternalAgentID
		 , tgt.UpdatedDate = getdate()
WHEN NOT MATCHED BY TARGET THEN 
INSERT (MLSID, AgentMLSID, AgentName, isActive, AuxAgentID, MLSInternalAgentID, InsertedDate, UpdatedDate)
VALUES (
        src.MLSID
       ,src.AgentMLSID
       ,src.AgentName
       ,src.isActive
       ,src.AuxAgentID
       ,src.MLSInternalAgentID
	   ,getdate()
	   ,getdate()
	   );


DELETE FROM dim.Office

INSERT INTO dim.Office
SELECT DISTINCT
  t3.OfficeMLSID AS OfficeNumber,
  OfficeName,
  t3.MLSID,
  t4.MLSAbbreviation AS Company,
  OfficeStatus AS [Status],
  OfficeAddTime AS CreatedDate,
  OfficeUpdateDate AS ModifiedDate
FROM BI_Reporting_STG.mls.Listing t1
  JOIN BI_Reporting_STG.mls.ListingOffice t2
	ON t1.ListingID = t2.ListingID --AND t2.ListingOfficeTypeID = 1
  JOIN BI_Reporting_STG.mls.Office t3
	ON t2.OfficeID = t3.OfficeID
  JOIN BI_Reporting_STG.mls.MLS t4
	ON t3.MLSID = t4.MLSID


				

DELETE FROM dim.Region

INSERT INTO dim.Region
SELECT 
	t1.ListingId,
	t1.MLSID,
	CASE 
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode IN ('500','510','520','530','540','550','560','600','948') THEN 'Eastside King'
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode IN ('170') THEN 'Bainbridge Island'
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode IN ('705','390','380','385','701','720','710','700','715','800','140') THEN 'Seattle'
		WHEN t1.CountyID=48 AND t4.StateName='Oregon' AND t1.City='Bend' THEN 'Bend'
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode IN ('100','110','120','130','360') THEN 'SW King County'
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode IN ('310','320','300','330','340','350') THEN 'SE King County'
		WHEN t2.MLSCode in('141','142','143','144','145','146','147','148','149','150','161','162','163','164','165','166','167','168') THEN 'Kitsap wo Bainbridge'
		WHEN t2.MLSCode in('1','2','3','4','5','6','7','8','9') THEN 'Gig Harbor'
		WHEN t1.StateID=43 AND t1.CountyID IN (31,115) THEN 'East Portland'
		WHEN t1.StateID=43  AND t1.CountyID IN (37,167) THEN 'West Portland'
		WHEN t2.MLSID=127 AND StateName='Idaho' AND t1.CountyID = 1 THEN 'Boise'
		WHEN t2.MLSID=91 AND StateName='Oregon' AND MLSCode = 147 THEN 'Lake Oswego'
		ELSE t3.CountyName + ' County'
	END AS Region,
	CASE
		WHEN t3.CountyID IN (1,2,4,5,6,9,10,11,12,14,15) THEN 'Luxury and Standard'
		WHEN t3.CountyID IN (4,7,8,13,16,17,19) THEN 'Standard'
		ELSE 'Luxury'
	END AS UpdateType,
	NULL AS Waterfront,
	t5.PropertyType
FROM 
	BI_Reporting_STG.mls.Listing t1
	LEFT JOIN [BI_Reporting_STG].[mls].[MLSAreas] t2
	ON t1.MLSID = t2.MLSID AND t1.MLSListingID = t2.MLSListingID
		LEFT JOIN [BI_Reporting_STG].[mls].County t3
		ON t1.CountyID = t3.CountyID
			LEFT JOIN [BI_Reporting_STG].[mls].[State] t4
			ON t3.StateID = t4.StateID
				LEFT JOIN BI_SL_STG..NewConstruction t5
				ON t1.ListingID = t5.ListingID
WHERE 
	(t4.StateName = 'Oregon' AND t3.CountyID IN(37,167,76,31,115,48)) OR
	(t4.StateName = 'Idaho' AND t3.CountyID IN(1,89)) OR
	(t4.StateName = 'Washington' AND t3.CountyID IN(77,84,85,127,146,147,153,168)) OR
	t2.MLSCode in('1','2','3','4','5','6','7','8','9'
					,'100','110','120','130','140','510','170','140','141','142','143','144','145','146','147','148','149','150','161','162','163','164','165','166','167','168','170'
					,'300','310','320','330','340','350','360','380','385','390'
					,'500','510','520','530','540','550','560','600'
					,'700','701','705','710','715','720','800','948')
UNION ALL

SELECT 
	t1.ListingId,
	t1.MLSID,
	CASE 
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode='520' THEN 'West Bellevue'
		WHEN t2.MLSID=9AND t4.StateName='Washington' AND MLSCode='510' THEN 'Mercer Island'
	END AS Region,
	'Luxury' AS UpdateType,
	NULL AS Waterfront,
	t5.PropertyType
FROM 
	BI_Reporting_STG.mls.Listing t1
	LEFT JOIN [BI_Reporting_STG].[mls].[MLSAreas] t2
	ON t1.MLSID = t2.MLSID AND t1.MLSListingID = t2.MLSListingID
		LEFT JOIN [BI_Reporting_STG].[mls].County t3
		ON t1.CountyID = t3.CountyID
			LEFT JOIN [BI_Reporting_STG].[mls].[State] t4
			ON t3.StateID = t4.StateID
				LEFT JOIN BI_SL_STG..NewConstruction t5
				ON t1.ListingID = t5.ListingID
WHERE 
	t2.MLSID=9 AND 
	t4.StateName='Washington' AND
	t2.MLSCode in('510','520')

UNION ALL

SELECT 
	t1.ListingId,
	t1.MLSID,
	CASE 
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode IN('380','390','510','520','710') THEN 'Lake Washington'
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode IN('530','540') THEN 'Lake Sammamish'
	END AS Region,
	'Luxury' AS UpdateType,
	'Waterfront' as Waterfront,
	t5.PropertyType
FROM 
	BI_Reporting_STG.mls.Listing t1
	LEFT JOIN [BI_Reporting_STG].[mls].[MLSAreas] t2
	ON t1.MLSID = t2.MLSID AND t1.MLSListingID = t2.MLSListingID
		LEFT JOIN [BI_Reporting_STG].[mls].County t3
		ON t1.CountyID = t3.CountyID
			LEFT JOIN [BI_Reporting_STG].[mls].[State] t4
			ON t3.StateID = t4.StateID
				LEFT JOIN BI_SL_STG..NewConstruction t5
				ON t1.ListingID = t5.ListingID
WHERE 
	t2.MLSID=9 AND 
	t4.StateName='Washington' AND
	t2.MLSCode in('380','390','510','520','710','530','540')

UNION ALL

SELECT 
	t1.ListingId,
	t1.MLSID,
	CASE 
		WHEN t2.MLSID=9 AND t4.StateName='Washington' AND MLSCode IN('100','110','120','130','140','300','310','320','330','340','350','360','380','385','390','500','510','520','530','540','550','560','600','610','140','700','701','705','710','720','715','800','948') 
		THEN 'King County Waterfront'
	END AS Region,
	'Luxury' AS UpdateType,
	'Waterfront' as Waterfront,
	t5.PropertyType
FROM 
	BI_Reporting_STG.mls.Listing t1
	LEFT JOIN [BI_Reporting_STG].[mls].[MLSAreas] t2
	ON t1.MLSID = t2.MLSID AND t1.MLSListingID = t2.MLSListingID
		LEFT JOIN [BI_Reporting_STG].[mls].County t3
		ON t1.CountyID = t3.CountyID
			LEFT JOIN [BI_Reporting_STG].[mls].[State] t4
			ON t3.StateID = t4.StateID
				LEFT JOIN BI_SL_STG..NewConstruction t5
				ON t1.ListingID = t5.ListingID
WHERE
	t2.MLSID=9 AND 
	t4.StateName='Washington' AND
	t2.MLSCode IN('100','110','120','130','140','300','310','320','330','340','350','360','380','385','390','500','510','520','530','540','550','560','600','610','140','700','701','705','710','720','715','800','948')


DELETE FROM dim.ListingAgent 

INSERT INTO dim.ListingAgent 
SELECT ListingID
     , PrimaryAgent = min(PrimaryAgent)
	 , CoListingAgent = min(CoListingAgent)
	 , SellingAgent = min(SellingAgent)
	 , CoSellingAgent = min(CoSellingAgent)
	 , CoListingAgent2 = min(CoListingAgent2)
	 , CoSellingAgent2 = min(CoSellingAgent2)

FROM (
SELECT la.ListingID
     , PrimaryAgent = case when ListerType = 'Primary' then la.AgentMLSID end
	 , CoListingAgent = case when ListerType = 'Co-Lister' then la.AgentMLSID end
	 , SellingAgent = case when ListerType = 'Seller' then la.AgentMLSID end
	 , CoSellingAgent = case when ListerType = 'Co-Seller' then la.AgentMLSID end
	 , CoListingAgent2 = case when ListerType = 'Co-Lister2' then la.AgentMLSID end
	 , CoSellingAgent2 = case when ListerType = 'Co-Seller2' then la.AgentMLSID end
FROM BI_Reporting_STG.mls.ListingAgent la
  LEFT JOIN dim.Agent agt
    ON agt.AgentMLSID = la.AgentMLSID
) a
GROUP BY listingid


DELETE FROM dim.ListingOffice

INSERT INTO  dim.ListingOffice
SELECT ListingID
     , PrimaryOffice = min(PrimaryOffice)
	 , CoListingOffice = min(CoListingOffice)
	 , SellingOffice = min(SellingOffice)
	 , CoSellingOffice = min(CoSellingOffice)
	 , CoListingOffice2 = min(CoListingOffice2)
	 , CoSellingOffice2 = min(CoSellingOffice2)

FROM (
SELECT la.ListingID
     , PrimaryOffice = case when OfficeType = 'Primary' then la.OfficeMLSID end
	 , CoListingOffice = case when OfficeType = 'Co-Lister' then la.OfficeMLSID end
	 , SellingOffice = case when OfficeType = 'Seller' then la.OfficeMLSID end
	 , CoSellingOffice = case when OfficeType = 'Co-Seller' then la.OfficeMLSID end
	 , CoListingOffice2 = case when OfficeType = 'Co-Lister2' then la.OfficeMLSID end
	 , CoSellingOffice2 = case when OfficeType = 'Co-Seller2' then la.OfficeMLSID end
FROM BI_Reporting_STG.mls.ListingOffice la
) a
GROUP BY listingid


END

