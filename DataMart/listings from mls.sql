USE [BI_Reporting_STG]
GO
/****** Object:  StoredProcedure [dbo].[LoadMLSListingTable]    Script Date: 11/13/2019 7:24:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[LoadMLSListingTable] as
BEGIN

TRUNCATE TABLE mls.Listing

--CaSAOR
Insert Into mls.Listing
( ShowAddress, ShowMap, MLSID, PostalCodeExt, IDX, BankOwned, ShortSale, PostalCode, StreetAddress
, StreetDirPrefix, StreetNumber, StreetName, ListPrice, Bedrooms, CDOM, JLsDom, CloseDate, DOM
, GarageSpaces, BathsTotal, City, Garage, PropTypeID, ListDate, MLSDBListingModTime, MLSListingID
, Acreage, OriginalListPrice, ClosePrice, SquareFootage, UpdateDate, YearBuilt, Age, AgentHitCount
, ListAgentID, CoListAgentID, ListingOfficeID, CoListingOfficeID, Commission, Community, SellingAgentID
, CoSellingAgentID, CoSellingAgentID2, SellingOffice, CoSellingOffice, CoSellingOffice2, ExpirationDate
, PictureCount, VirtualTour, Waterfront, WithdrawDate, Zoning, HowSold,Area)
 Select 1,1,200000005,IIF(len(s.Zip)=10,Right(s.Zip,4),Null),IIF(s.IdxInclude=1,0,1),IIF(s.REO='N',0,1)
       ,IIF(s.ShortSale='N',0,1),Left(s.Zip,5),s.[Address],s.AddressDirection,s.AddressNumber,s.AddressStreet
	   ,s.AskingPrice,s.Beds,s.CDOM,s.CDOM,s.ClosingDate,s.DOM,s.GarageCapacity,s.JLSBaths,s.JLSCity
	   ,s.JLSGarage,s.JLSPropTypeID,s.ListingDate,s.ListingModTime,s.MLSNumber,s.NumAcres,s.OriginalPrice
	   ,s.SoldPrice,s.SquareFeet,s.UpdateDate,s.YearBuilt,s.ApxAge,s.AgentHitCount,s.ListAgent1
	   ,s.ListAgent2,s.ListOffice1,s.ListOffice2,s.CommPct,s.Subdivision,s.SellingAgent1
	   ,s.SellingAgent2,s.SellingAgent3,SellingOffice1,s.SellingOffice2,s.SellingOffice3,s.ExpirationDate
	   ,s.PictureCount,s.VirtualTour,s.Waterfront,s.JlsOffMarketDate,s.Zoning,l.LongValue,s.Area
 From CaSAOR.dbo.Residential s
   left join CaSAOR.dbo.Lookups l on l.Lookup = 'HowSold' and s.HowSold = l.[Value]


Insert Into mls.Listing 
( ShowAddress, ShowMap, MLSID, PostalCodeExt, IDX, BankOwned, ShortSale, PostalCode, StreetAddress, StreetDirPrefix
, StreetNumber, StreetName, ListPrice, CDOM, JLsDom, CloseDate, DOM, City, PropTypeID, ListDate
, MLSDBListingModTime, MLSListingID, Acreage, OriginalListPrice, ClosePrice, UpdateDate, AgentHitCount
, ListAgentID, CoListAgentID, ListingOfficeID, CoListingOfficeID, Commission, Community, SellingAgentID
, CoSellingAgentID, CoSellingAgentID2, SellingOffice, CoSellingOffice, CoSellingOffice2, ExpirationDate
, PictureCount, VirtualTour, Waterfront, WithdrawDate, Zoning, HowSold,Area) 
Select 1,1,200000005,IIF(len(s.Zip)=10,Right(s.Zip,4),Null),IIF(s.IdxInclude=1,0,1),IIF(s.REO='N',0,1),IIF(s.ShortSale='N',0,1)
     ,Left(s.Zip,5),s.[Address],s.AddressDirection,s.AddressNumber,s.AddressStreet,s.AskingPrice,s.CDOM,s.CDOM,s.ClosingDate
	 ,s.DOM,s.JLSCity,s.JLSPropTypeID,s.ListingDate,s.ListingModTime,s.MLSNumber,s.NumAcres,s.OriginalPrice,s.SoldPrice
	 ,s.UpdateDate,s.AgentHitCount,s.ListAgent1,s.ListAgent2,s.ListOffice1,s.ListOffice2,s.CommPct,s.Subdivision,s.SellingAgent1
	 ,s.SellingAgent2,s.SellingAgent3,SellingOffice1,s.SellingOffice2,s.SellingOffice3,s.ExpirationDate,s.PictureCount,s.VirtualTour
	 ,s.Waterfront,s.JlsOffMarketDate,s.Zoning,l.LongValue,s.Area
 From CaSAOR.dbo.Land s
    left join CaSAOR.dbo.Lookups l on l.Lookup = 'HowSold' and s.HowSold = l.[Value]

Insert Into mls.Listing
( ShowAddress, ShowMap, MLSID, PostalCodeExt, IDX, BankOwned, ShortSale, PostalCode, StreetAddress, StreetDirPrefix
, StreetNumber, StreetName, ListPrice, CDOM, JLsDom, CloseDate, DOM, BathsTotal, City, Garage, PropTypeID
, ListDate, MLSDBListingModTime, MLSListingID, Acreage, OriginalListPrice, ClosePrice, UpdateDate, AgentHitCount
, ListAgentID, CoListAgentID, ListingOfficeID, CoListingOfficeID, Commission, Community, SellingAgentID
, CoSellingAgentID, CoSellingAgentID2, SellingOffice, CoSellingOffice, CoSellingOffice2, ExpirationDate
, PictureCount, VirtualTour, Waterfront, WithdrawDate, Zoning, HowSold,Area) 
Select 1,1,200000005,IIF(len(s.Zip)=10,Right(s.Zip,4),Null),IIF(s.IdxInclude=1,0,1),IIF(s.REO='N',0,1),IIF(s.ShortSale='N',0,1)
     ,Left(s.Zip,5),s.[Address],s.AddressDirection,s.AddressNumber,s.AddressStreet,s.AskingPrice,s.CDOM,s.CDOM,s.ClosingDate
     ,s.DOM,s.JLSBaths,s.JLSCity,s.JLSGarage,s.JLSPropTypeID,s.ListingDate,s.ListingModTime,s.MLSNumber,s.NumAcres,s.OriginalPrice
	 ,s.SoldPrice,s.UpdateDate,s.AgentHitCount,s.ListAgent1,s.ListAgent2,s.ListOffice1,s.ListOffice2,s.CommPct,s.Subdivision
	 ,s.SellingAgent1,s.SellingAgent2,s.SellingAgent3,SellingOffice1,s.SellingOffice2,s.SellingOffice3,s.ExpirationDate
	 ,s.PictureCount,s.VirtualTour,s.Waterfront,s.JlsOffMarketDate,s.Zoning,l.LongValue,s.Area
 From CaSAOR.dbo.Ranch s
    left join CaSAOR.dbo.Lookups l on l.Lookup = 'HowSold' and s.HowSold = l.[Value]


Insert Into mls.Listing
( ShowAddress, ShowMap, MLSID, PostalCodeExt, IDX, BankOwned, ShortSale, PostalCode, StreetAddress, StreetDirPrefix
, StreetNumber, StreetName, ListPrice, CDOM, JLsDom, CloseDate, DOM, City, PropTypeID, ListDate
, MLSDBListingModTime, MLSListingID, OriginalListPrice, ClosePrice, SquareFootage, UpdateDate, YearBuilt
, Age, AgentHitCount, ListAgentID, CoListAgentID, ListingOfficeID, CoListingOfficeID, Commission, Community, SellingAgentID
, CoSellingAgentID, CoSellingAgentID2, SellingOffice, CoSellingOffice, CoSellingOffice2, ExpirationDate
, PictureCount, VirtualTour, Waterfront, WithdrawDate, Zoning, HowSold,Area) 
Select 1,1,200000005,IIF(len(s.Zip)=10,Right(s.Zip,4),Null),IIF(s.IdxInclude=1,0,1),IIF(s.REO='N',0,1),IIF(s.ShortSale='N',0,1)
     ,Left(s.Zip,5),s.[Address],s.AddressDirection,s.AddressNumber,s.AddressStreet,s.AskingPrice,s.CDOM,s.CDOM,s.ClosingDate
     ,s.DOM,s.JLSCity,s.JLSPropTypeID,s.ListingDate,s.ListingModTime,s.MLSNumber,s.OriginalPrice,s.SoldPrice,s.SquareFeet
     ,s.UpdateDate,s.YearBuilt,s.ApxBuildingAge,s.AgentHitCount,s.ListAgent1,s.ListAgent2,s.ListOffice1,s.ListOffice2,s.CommPct
	 ,s.Subdivision,s.SellingAgent1,s.SellingAgent2,s.SellingAgent3,SellingOffice1,s.SellingOffice2,s.SellingOffice3,s.ExpirationDate
     ,s.PictureCount,s.VirtualTour,s.Waterfront,s.JlsOffMarketDate,s.Zoning,l.LongValue,s.Area
 From CaSAOR.dbo.Business s
    left join CaSAOR.dbo.Lookups l on l.Lookup = 'HowSold' and s.HowSold = l.[Value]
	
Insert Into mls.Listing
( ShowAddress, ShowMap, MLSID, PostalCodeExt, IDX, BankOwned, ShortSale, PostalCode, StreetAddress, StreetDirPrefix
, StreetNumber, StreetName, ListPrice, Bedrooms, CDOM, JLsDom, CloseDate, DOM, GarageSpaces, BathsTotal, City
, Garage, PropTypeID, ListDate, MLSDBListingModTime, MLSListingID, Acreage, OriginalListPrice, ClosePrice
, SquareFootage, UpdateDate, YearBuilt, Age, AgentHitCount, ListAgentID, CoListAgentID, ListingOfficeID, CoListingOfficeID
, Commission, Community, SellingAgentID, CoSellingAgentID, CoSellingAgentID2, SellingOffice, CoSellingOffice, CoSellingOffice2
, ExpirationDate, PictureCount, VirtualTour, Waterfront, WithdrawDate, Zoning, HowSold,Area) 
Select 1,1,200000005,IIF(len(s.Zip)=10,Right(s.Zip,4),Null),IIF(s.IdxInclude=1,0,1),IIF(s.REO='N',0,1),IIF(s.ShortSale='N',0,1)
     ,Left(s.Zip,5),s.[Address],s.AddressDirection,s.AddressNumber,s.AddressStreet,s.AskingPrice,s.Beds,s.CDOM,s.CDOM
     ,s.ClosingDate,s.DOM,s.GarageCapacity,s.JLSBaths,s.JLSCity,s.JLSGarage,s.JLSPropTypeID,s.ListingDate,s.ListingModTime
     ,s.MLSNumber,s.NumAcres,s.OriginalPrice,s.SoldPrice,s.SquareFeet,s.UpdateDate,s.YearBuilt,s.ApxAge ,s.AgentHitCount,s.ListAgent1
	 ,s.ListAgent2,s.ListOffice1,s.ListOffice2,s.CommPct,s.Subdivision,s.SellingAgent1,s.SellingAgent2,s.SellingAgent3,SellingOffice1
	 ,s.SellingOffice2,s.SellingOffice3,s.ExpirationDate,s.PictureCount,s.VirtualTour,s.Waterfront,s.JlsOffMarketDate,s.Zoning
	 ,l.LongValue,s.Area
 From CaSAOR.dbo.Manufactured s
     left join CaSAOR.dbo.Lookups l on l.Lookup = 'HowSold' and s.HowSold = l.[Value]

Insert Into mls.Listing 
( ShowAddress, ShowMap, MLSID, PostalCodeExt, IDX, BankOwned, ShortSale, PostalCode, StreetAddress, StreetDirPrefix
, StreetNumber, StreetName, ListPrice, CDOM, JLsDom, CloseDate, DOM, GarageSpaces, BathsTotal, City, Garage
, PropTypeID, ListDate, MLSDBListingModTime, MLSListingID, Acreage, OriginalListPrice, ClosePrice, SquareFootage
, UpdateDate, YearBuilt, Age, AgentHitCount, ListAgentID, CoListAgentID, ListingOfficeID, CoListingOfficeID, Commission, Community
, SellingAgentID, CoSellingAgentID, CoSellingAgentID2, SellingOffice, CoSellingOffice, CoSellingOffice2, ExpirationDate, PictureCount
, VirtualTour, Waterfront, WithdrawDate, Zoning, HowSold,Area)
Select 1,1,200000005,IIF(len(s.Zip)=10,Right(s.Zip,4),Null),IIF(s.IdxInclude=1,0,1),IIF(s.REO='N',0,1),IIF(s.ShortSale='N',0,1)
     ,Left(s.Zip,5),s.[Address],s.AddressDirection,s.AddressNumber,s.AddressStreet,s.AskingPrice,s.CDOM,s.CDOM,s.ClosingDate
     ,s.DOM,s.GarageCapacity,s.JLSBaths,s.JLSCity,s.JLSGarage,s.JLSPropTypeID,s.ListingDate,s.ListingModTime,s.MLSNumber
     ,s.NumAcres,s.OriginalPrice,s.SoldPrice,s.SquareFeet,s.UpdateDate,s.YearBuilt,s.ApxAge ,s.AgentHitCount,s.ListAgent1
	 ,s.ListAgent2,s.ListOffice1,s.ListOffice2,s.CommPct,s.Subdivision,s.SellingAgent1,s.SellingAgent2,s.SellingAgent3,SellingOffice1
	 ,s.SellingOffice2,s.SellingOffice3,s.ExpirationDate,s.PictureCount,s.VirtualTour,s.Waterfront,s.JlsOffMarketDate,s.Zoning
	 ,l.LongValue,s.Area
From CaSAOR.dbo.Multifamily s
    left join CaSAOR.dbo.Lookups l on l.Lookup = 'HowSold' and s.HowSold = l.[Value]


--IdCDAMLS
Insert Into mls.Listing
([ShowMap],[MLSID],[ListPrice],[OriginalListPrice],[ClosePrice],[PropertyTax],[SquareFootage],[IDX],[Latitude],[Longitude]
,[VirtualTour],[DOM],[JLsDom],[StreetNumber],[BankOwned],[City],[Garage],[HOADues],[ParkingSpaces],[PostalCode],[PropTypeID]
,[SchoolDistrict],[ShortSale],[TaxYearBuilt],[MLSDBListingModTime],[Acreage],[MLSListingID],[StatusDate],[StreetDirPrefix]
,[StreetDirSuffix],[StreetName],[StreetTypeSuffix],[UpdateDate],[BathsTotal],[Bedrooms],[ShowAddress],[YearBuilt],[ListDate]
,[CloseDate],[PendingDate],BodyofWaterName,BuilderName,CancelDate,ListAgentID,CoListAgentID,CoListingMemberID,ListingOfficeID
,CoListingOfficeID,CommissionCode,Commission,SellingOfficeCommissionPercentage,Community,SellingAgentID,CoSellingAgentID
,SellingOffice,CoSellingOffice,EstCompletionDate,NewConstruction,PictureCount,WithdrawDate,Zoning,Area)
 Select 1,575009,floor(s.ListPrice),floor(s.OriginalListPrice),floor(s.SoldPrice),floor(s.Taxes),floor(s.TotalSqFt)
      ,IIF(s.DisplayOnPublicWebsites='N',0,1),iif(s.GeoLat is not null,s.GeoLat,null),iif(s.GeoLon is not null,s.GeoLon,null)
	  ,s.[BrandedVirtualTour],s.[DaysOnMarket],s.[DaysOnMarket],s.[HouseNumber],s.[JLSBankOwned],s.[JLSCity],s.[JLSGarage]
	  ,s.[JLSHOADues],s.[JLSParkingSpaces],s.[JLSPostalCode],s.[JLSPropTypeID],s.[JLSSchoolDistrict],s.[JLSShortSale],s.[JLSTaxYear]
	  ,s.[ListingModTime],s.[LotAcres],s.[MLSNumber],s.[StatusChangeDate],s.[StreetDirectionPfx],s.[StreetDirectionSfx],s.[StreetName]
	  ,s.[StreetSuffix],s.[Timestamp],s.[TotalBathrooms],s.[TotalBedrooms],s.[VOWAddressDisplay],s.[YearBuilt]
	  ,TRY_CONVERT(datetime,s.BeginDate),TRY_CONVERT(datetime,s.solddate),TRY_CONVERT(datetime,s.UnderContractDate)
	  ,s.Lake_RiverName,s.BuilderName,s.CancelDate,s.AgentID,s.CoListAgentID,s.CoListingMemberShortId,s.ListingOfficeShortId
	  ,s.CoListingOfficeShortId,BuyerAgentCashOrPct=l3.[LongValue],s.BuyerAgentFee,s.NonAgentFee,Subdivision=l2.LongValue
	  ,s.SellingAgentID,s.CoSellingAgentID,s.SellingOfficeShortId,s.CoSellingOfficeShortId,s.EstCompletionDate
	  ,NewConstruction=l.[LongValue],s.PictureCount,s.WithdrawalDate,s.Zoning,s.Area
 From IdCDAMLS.dbo.Residential s
  left join IdCDAMLS.dbo.Lookups l
   on s.NewConstruction = l.[Value]
  left join IdCDAMLS.dbo.Lookups l2
   on s.SubDivision = l2.[Value]
  left join IdCDAMLS.dbo.Lookups l3
   on s.BuyerAgentCashOrPct = l3.[Value]


Insert Into mls.Listing 
([ShowMap],[MLSID],[ListPrice],[OriginalListPrice],[ClosePrice],[PropertyTax],[SquareFootage],[IDX],[Latitude],[Longitude]
,[VirtualTour],[DOM],[JLsDom],[StreetNumber],[BankOwned],[City],[PostalCode],[PropTypeID],[SchoolDistrict],[ShortSale],[TaxYearBuilt]
,[MLSDBListingModTime],[Acreage],[MLSListingID],[StatusDate],[StreetDirPrefix],[StreetDirSuffix],[StreetName],[StreetTypeSuffix]
,[UpdateDate],[ShowAddress],[YearBuilt],[ListDate],[CloseDate],[PendingDate],BuilderName,CancelDate,ListAgentID
,CoListAgentID,CoListingMemberID,ListingOfficeID,CoListingOfficeID,CommissionCode,Commission,SellingOfficeCommissionPercentage
,Community,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice,FallThroughDate,NewConstruction
,PictureCount,WithdrawDate,Zoning,Area)
 Select 1,575009,floor(s.ListPrice),floor(s.OriginalListPrice),floor(s.SoldPrice),floor(s.Taxes),floor(s.TotalSqFt)
      ,IIF(s.DisplayOnPublicWebsites='N',0,1),iif(s.GeoLat is not null,s.GeoLat,null),iif(s.GeoLon is not null,s.GeoLon,null)
	  ,s.[BrandedVirtualTour],s.[DaysOnMarket],s.[DaysOnMarket],s.[HouseNumber],s.[JLSBankOwned],s.[JLSCity],s.[JLSPostalCode]
	  ,s.[JLSPropTypeID],s.[JLSSchoolDistrict],s.[JLSShortSale],s.[JLSTaxYear],s.[ListingModTime],s.[LotAcres],s.[MLSNumber]
	  ,s.[StatusChangeDate],s.[StreetDirectionPfx],s.[StreetDirectionSfx],s.[StreetName],s.[StreetSuffix],s.[Timestamp]
	  ,s.[VOWAddressDisplay],s.[YearBuilt],TRY_CONVERT(datetime,s.BeginDate),TRY_CONVERT(datetime,s.solddate)
	  ,TRY_CONVERT(datetime,s.UnderContractDate),s.BuilderName,s.CancelDate,s.AgentID,s.CoListAgentID
	  ,s.CoListingMemberShortId,s.ListingOfficeShortId,s.CoListingOfficeShortId,BuyerAgentCashOrPct=l3.[LongValue],s.BuyerAgentFee
	  ,s.NonAgentFee,Subdivision=l2.LongValue,s.SellingAgentID,s.CoSellingAgentID,s.SellingOfficeShortId,s.CoSellingOfficeShortId
	  ,FallThroughDate,NewConstruction=l.[LongValue],s.PictureCount,s.WithdrawalDate,s.Zoning,s.Area
 From IdCDAMLS.dbo.MultiFamily s
   left join IdCDAMLS.dbo.Lookups l
   on s.NewConstruction = l.[Value]
  left join IdCDAMLS.dbo.Lookups l2
   on s.SubDivision = l2.[Value]
  left join IdCDAMLS.dbo.Lookups l3
   on s.BuyerAgentCashOrPct = l3.[Value]

Insert Into mls.Listing 
([ShowMap],[MLSID],[ListPrice],[OriginalListPrice],[ClosePrice],[PropertyTax],[SquareFootage],[IDX],[Latitude],[Longitude]
,[VirtualTour],[DOM],[JLsDom],[StreetNumber],[BankOwned],[City],[PostalCode],[PropTypeID],[SchoolDistrict],[ShortSale],[TaxYearBuilt]
,[MLSDBListingModTime],[Acreage],[MLSListingID],[StatusDate],[StreetDirPrefix],[StreetDirSuffix],[StreetName],[StreetTypeSuffix]
,[UpdateDate],[ShowAddress],[ListDate],[CloseDate],[PendingDate],BodyofWaterName,CancelDate,ListAgentID,CoListAgentID
,CoListingMemberID,ListingOfficeID,CoListingOfficeID,CommissionCode,Commission,SellingOfficeCommissionPercentage,Community
,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice,FallThroughDate,PictureCount
,WithdrawDate,Zoning,Area)
 Select 1,575009,floor(s.ListPrice),floor(s.OriginalListPrice),floor(s.SoldPrice),floor(s.Taxes),floor(s.TotalSqFt)
      ,IIF(s.DisplayOnPublicWebsites='N',0,1),iif(s.GeoLat is not null,s.GeoLat,null),iif(s.GeoLon is not null,s.GeoLon,null)
	  ,s.[BrandedVirtualTour],s.[DaysOnMarket],s.[DaysOnMarket],s.[HouseNumber],s.[JLSBankOwned],s.[JLSCity],s.[JLSPostalCode]
	  ,s.[JLSPropTypeID],s.[JLSSchoolDistrict],s.[JLSShortSale],s.[JLSTaxYear],s.[ListingModTime],s.[LotAcres],s.[MLSNumber]
	  ,s.[StatusChangeDate],s.[StreetDirectionPfx],s.[StreetDirectionSfx],s.[StreetName],s.[StreetSuffix],s.[Timestamp]
	  ,s.[VOWAddressDisplay],TRY_CONVERT(datetime,s.BeginDate),TRY_CONVERT(datetime,s.solddate)
	  ,TRY_CONVERT(datetime,s.UnderContractDate),s.Lake_RiverName,s.CancelDate,s.AgentID,s.CoListAgentID
	  ,s.CoListingMemberShortId,s.ListingOfficeShortId,s.CoListingOfficeShortId,BuyerAgentCashOrPct=l3.[LongValue],s.BuyerAgentFee
	  ,s.NonAgentFee,Subdivision=l2.LongValue,s.SellingAgentID,s.CoSellingAgentID,s.SellingOfficeShortId,s.CoSellingOfficeShortId
	  ,FallThroughDate,s.PictureCount,s.WithdrawalDate,s.Zoning,s.Area
 From IdCDAMLS.dbo.Land s
  left join IdCDAMLS.dbo.Lookups l2
   on s.SubDivision = l2.[Value]
  left join IdCDAMLS.dbo.Lookups l3
   on s.BuyerAgentCashOrPct = l3.[Value]

Insert Into mls.Listing
([ShowMap],[MLSID],[ListPrice],[OriginalListPrice],[ClosePrice],[PropertyTax],[SquareFootage],[IDX],[Latitude],[Longitude]
,[VirtualTour],[DOM],[JLsDom],[StreetNumber],[BankOwned],[City],[PostalCode],[PropTypeID],[SchoolDistrict],[ShortSale],[TaxYearBuilt]
,[MLSDBListingModTime],[Acreage],[MLSListingID],[StatusDate],[StreetDirPrefix],[StreetDirSuffix],[StreetName],[StreetTypeSuffix]
,[UpdateDate],[ShowAddress],[YearBuilt],[ListDate],[CloseDate],[PendingDate],BuilderName,CancelDate,ListAgentID
,CoListAgentID,CoListingMemberID,ListingOfficeID,CoListingOfficeID,CommissionCode,Commission,SellingOfficeCommissionPercentage
,Community,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice,EstCompletionDate,FallThroughDate,NewConstruction
,PictureCount,WithdrawDate,Zoning,Area)
 Select 1,575009,floor(s.ListPrice),floor(s.OriginalListPrice),floor(s.SoldPrice),floor(s.Taxes),floor(s.TotalSqFt)
      ,IIF(s.DisplayOnPublicWebsites='N',0,1),iif(s.GeoLat is not null,s.GeoLat,null),iif(s.GeoLon is not null,s.GeoLon,null)
	  ,s.[BrandedVirtualTour],s.[DaysOnMarket],s.[DaysOnMarket],s.[HouseNumber],s.[JLSBankOwned],s.[JLSCity],s.[JLSPostalCode]
	  ,s.[JLSPropTypeID],s.[JLSSchoolDistrict],s.[JLSShortSale],s.[JLSTaxYear],s.[ListingModTime],s.[LotAcres],s.[MLSNumber]
	  ,s.[StatusChangeDate],s.[StreetDirectionPfx],s.[StreetDirectionSfx],s.[StreetName],s.[StreetSuffix],s.[Timestamp]
	  ,s.[VOWAddressDisplay],s.[YearBuilt],TRY_CONVERT(datetime,s.BeginDate),TRY_CONVERT(datetime,s.solddate)
	  ,TRY_CONVERT(datetime,s.UnderContractDate),s.BuilderName,s.CancelDate,s.AgentID,s.CoListAgentID
	  ,s.CoListingMemberShortId,s.ListingOfficeShortId,s.CoListingOfficeShortId,BuyerAgentCashOrPct=l3.[LongValue],s.BuyerAgentFee
	  ,s.NonAgentFee,Subdivision=l2.LongValue,s.SellingAgentID,s.CoSellingAgentID,s.SellingOfficeShortId,s.CoSellingOfficeShortId
	  ,s.EstCompletionDate,s.FallthroughDate,NewConstruction=l.[LongValue],s.PictureCount,s.WithdrawalDate,s.Zoning,s.Area
 From IdCDAMLS.dbo.Commercial s
   left join IdCDAMLS.dbo.Lookups l
   on s.NewConstruction = l.[Value]
  left join IdCDAMLS.dbo.Lookups l2
   on s.SubDivision = l2.[Value]
  left join IdCDAMLS.dbo.Lookups l3
   on s.BuyerAgentCashOrPct = l3.[Value]

Insert Into mls.Listing
([ShowMap],[MLSID],[ListPrice],[OriginalListPrice],[ClosePrice],[PropertyTax],[SquareFootage],[IDX],[Latitude],[Longitude]
,[VirtualTour],[DOM],[JLsDom],[StreetNumber],[BankOwned],[City],[PostalCode],[PropTypeID],[SchoolDistrict],[ShortSale],[TaxYearBuilt]
,[MLSDBListingModTime],[Acreage],[MLSListingID],[StatusDate],[StreetDirPrefix],[StreetDirSuffix],[StreetName],[StreetTypeSuffix]
,[UpdateDate],[BathsTotal],[Bedrooms],[ShowAddress],[YearBuilt],[ListDate],[CloseDate],[PendingDate],BodyofWaterName
,CancelDate,ListAgentID,CoListAgentID,CoListingMemberID,ListingOfficeID,CoListingOfficeID,CommissionCode,Commission
,SellingOfficeCommissionPercentage,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice
,FallThroughDate,NewConstruction,PictureCount,WithdrawDate,Zoning,Area)
 Select 1,575009,floor(s.ListPrice),floor(s.OriginalListPrice),floor(s.SoldPrice),floor(s.Taxes),floor(s.TotalSqFt)
      ,IIF(s.DisplayOnPublicWebsites='N',0,1),iif(s.GeoLat is not null,s.GeoLat,null),iif(s.GeoLon is not null,s.GeoLon,null)
      ,s.[BrandedVirtualTour],s.[DaysOnMarket],s.[DaysOnMarket],s.[HouseNumber],s.[JLSBankOwned],s.[JLSCity],s.[JLSPostalCode]
      ,s.[JLSPropTypeID],s.[JLSSchoolDistrict],s.[JLSShortSale],s.[JLSTaxYear],s.[ListingModTime],s.[LotAcres],s.[MLSNumber]
      ,s.[StatusChangeDate],s.[StreetDirectionPfx],s.[StreetDirectionSfx],s.[StreetName],s.[StreetSuffix],s.[Timestamp]
	  ,s.[TotalBathrooms],s.[TotalBedrooms],s.[VOWAddressDisplay],s.[YearBuilt],TRY_CONVERT(datetime,s.BeginDate)
	  ,TRY_CONVERT(datetime,s.solddate),TRY_CONVERT(datetime,s.UnderContractDate),s.Lake_RiverName,s.CancelDate
	  ,s.AgentID,s.CoListAgentID,s.CoListingMemberShortId,s.ListingOfficeShortId,s.CoListingOfficeShortId
	  ,BuyerAgentCashOrPct=l3.[LongValue],s.BuyerAgentFee,s.NonAgentFee,s.SellingAgentID,s.CoSellingAgentID
	  ,s.SellingOfficeShortId,s.CoSellingOfficeShortId,s.FallthroughDate,NewConstruction=l.[LongValue],s.PictureCount,s.WithdrawalDate
	  ,s.Zoning,s.Area
 From IdCDAMLS.dbo.FarmRanch s
   left join IdCDAMLS.dbo.Lookups l
   on s.NewConstruction = l.[Value]
  left join IdCDAMLS.dbo.Lookups l3
   on s.BuyerAgentCashOrPct = l3.[Value]

Insert Into mls.Listing
([ShowMap],[MLSID],[ListPrice],[OriginalListPrice],[ClosePrice],[PropertyTax],[IDX],[Latitude],[Longitude],[VirtualTour],[DOM]
,[JLsDom],[StreetNumber],[BankOwned],[City],[PostalCode],[PropTypeID],[SchoolDistrict],[ShortSale],[TaxYearBuilt],[MLSDBListingModTime]
,[MLSListingID],[StatusDate],[StreetDirPrefix],[StreetDirSuffix],[StreetName],[StreetTypeSuffix],[UpdateDate],[ShowAddress],[ListDate]
,[CloseDate],[PendingDate],CancelDate,ListAgentID,CoListAgentID,CoListingMemberID,ListingOfficeID
,CoListingOfficeID,CommissionCode,Commission,SellingOfficeCommissionPercentage,SellingAgentID,CoSellingAgentID,SellingOffice
,CoSellingOffice,FallThroughDate,PictureCount,WithdrawDate,Area)
 Select 1,575009,floor(s.ListPrice),floor(s.OriginalListPrice),floor(s.SoldPrice),floor(s.Taxes),IIF(s.DisplayOnPublicWebsites='N',0,1)
      ,iif(s.GeoLat is not null,s.GeoLat,null),iif(s.GeoLon is not null,s.GeoLon,null),s.[BrandedVirtualTour],s.[DaysOnMarket]
	  ,s.[DaysOnMarket],s.[HouseNumber],s.[JLSBankOwned],s.[JLSCity],s.[JLSPostalCode],s.[JLSPropTypeID],s.[JLSSchoolDistrict]
	  ,s.[JLSShortSale],s.[JLSTaxYear],s.[ListingModTime],s.[MLSNumber],s.[StatusChangeDate],s.[StreetDirectionPfx]
	  ,s.[StreetDirectionSfx],s.[StreetName],s.[StreetSuffix],s.[Timestamp],s.[VOWAddressDisplay],TRY_CONVERT(datetime,s.BeginDate)
	  ,TRY_CONVERT(datetime,s.solddate),TRY_CONVERT(datetime,s.UnderContractDate),s.CancelDate,s.AgentID
	  ,s.CoListAgentID,s.CoListingMemberShortId,s.ListingOfficeShortId,s.CoListingOfficeShortId,BuyerAgentCashOrPct=l3.[LongValue]
	  ,s.BuyerAgentFee,s.NonAgentFee,s.SellingAgentID,s.CoSellingAgentID,s.SellingOfficeShortId
	  ,s.CoSellingOfficeShortId,s.FallthroughDate,s.PictureCount,s.WithdrawalDate,s.Area
 From IdCDAMLS.dbo.Business s
  left join IdCDAMLS.dbo.Lookups l3
   on s.BuyerAgentCashOrPct = l3.[Value]

--IdIMLS
Insert Into mls.Listing 
([MLSID],[IDX],[PostalCodeExt],[ShortSale],[BankOwned],[ShowAddress],[PostalCode],[Acreage],[StreetAddress],[StreetAddress2]
,[StreetDirPrefix],[StreetNumber],[StreetName],[SquareFootage],[ListPrice],[BathsTotal],[Bedrooms],[CDOM],[JLsDom],[CloseDate]
,[PendingDate],[DOM],[GarageSpaces],[City],[Fireplaces],[Garage],[ElementarySchool],[MiddleSchool],[PropTypeID],[HighSchool],[Latitude]
,[ListDate],[MLSDBListingModTime],[Longitude],[MLSListingID],[OriginalListPrice],[SchoolDistrict],[ClosePrice],[StatusDate],[TaxID]
,[UpdateDate],[VirtualTour],[YearBuilt],Age,AgentHitCount,Auction,BuilderName,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID
,Community,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice,EstCompletionDate,ExpirationDate,NewConstruction,PictureCount
,WithdrawDate,Zoning,HowSold,Area)
 Select 127,abs(s.IdxInclude-1),IIF(Len(s.Zip)=10,Right(s.Zip,4),Null),IIF(s.PotentialShortSaleY_N='Y',1,0)
 ,IIF(s.REO_BankOwnedY_N='Yes',1,0),IIF(s.Showaddress='N',0,1),Left(s.Zip,5),s.[Acres],s.[Address],s.[Address2],s.[AddressDirection]
 ,s.[AddressNumber],s.[AddressStreet],s.[ApxSqFt],s.[AskingPrice],s.[Baths],s.[Beds],s.[CDOM],s.[CDOM],s.[ClosingDate],s.[ContractDate]
 ,s.[DOM],s.[GarageCapacity],s.[JlsCity],s.[JLSFireplaces],s.[JLSGarage],s.[JlsGradeSchool],s.[JlsJrHigh],s.[JLSPropTypeID]
 ,s.[JlsSrHigh],s.[Latitude],s.[ListingDate],s.[ListingModTime],s.[Longitude],s.[MLSNumber],s.[OriginalPrice],s.[SchoolDistrict]
 ,s.[SoldPrice],s.[StatusDate],s.[TaxPropertyID],s.[UpdateDate],s.[VTourURL],s.[YearBuilt],s.Age,s.AgentHitCount,s.AuctionY_N,s.Builder
 ,s.ListAgent1,s.ListAgent2,s.ListOffice1,s.ListOffice2,s.Subdivision,s.SellingAgent1,s.SellingAgent2,s.SellingOffice1,s.SellingOffice2
 ,s.NewConstrCompDate,s.ExpirationDate,NewConstruction = CASE WHEN s.NewConstrCompDate IS NOT NULL THEN 'Yes' else 'No' end
 ,s.PictureCount,s.OffMarketDate,s.Zoning,s.HowSold,s.Area
 From IdIMLS.dbo.Residential s
 
Insert Into mls.Listing 
([MLSID],[IDX],[PostalCodeExt],[ShortSale],[BankOwned],[ShowAddress],[PostalCode],[Acreage],[StreetAddress],[StreetAddress2]
,[StreetDirPrefix],[StreetNumber],[StreetName],[ListPrice],[CDOM],[JLsDom],[CloseDate],[PendingDate],[DOM],[City],[ElementarySchool]
,[MiddleSchool],[PropTypeID],[HighSchool],[Latitude],[ListDate],[MLSDBListingModTime],[Longitude],[MLSListingID],[OriginalListPrice]
,[SchoolDistrict],[ClosePrice],[StatusDate],[TaxID],[UpdateDate],[VirtualTour],AgentHitCount,Auction,ListAgentID
,CoListAgentID,ListingOfficeID,CoListingOfficeID,Community,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice
,ExpirationDate,PictureCount,Waterfront,WithdrawDate,Zoning,HowSold,Area)
 Select 127,abs(s.IdxInclude-1),IIF(Len(s.Zip)=10,Right(s.Zip,4),Null),IIF(s.PotentialShortSaleY_N='Y',1,0)
 ,IIF(s.REO_BankOwnedY_N='Yes',1,0),IIF(s.Showaddress='N',0,1),Left(s.Zip,5),s.[Acres],s.[Address],s.[Address2],s.[AddressDirection]
 ,s.[AddressNumber],s.[AddressStreet],s.[AskingPrice],s.[CDOM],s.[CDOM],s.[ClosingDate],s.[ContractDate],s.[DOM],s.[JlsCity]
 ,s.[JlsGradeSchool],s.[JlsJrHigh],s.[JLSPropTypeID],s.[JlsSrHigh],s.[Latitude],s.[ListingDate],s.[ListingModTime],s.[Longitude]
 ,s.[MLSNumber],s.[OriginalPrice],s.[SchoolDistrict],s.[SoldPrice],s.[StatusDate],s.[TaxPropertyID],s.[UpdateDate],s.[VTourURL]
 ,s.AgentHitCount,s.AuctionY_N,s.ListAgent1,s.ListAgent2,s.ListOffice1,s.ListOffice2,s.Subdivision,s.SellingAgent1,s.SellingAgent2
 ,s.SellingOffice1,s.SellingOffice2,s.ExpirationDate,s.PictureCount,s.WaterfrontY_N,s.OffMarketDate,s.Zoning,s.HowSold,s.Area
 From IdIMLS.dbo.Land s 
 
Insert Into mls.Listing 
([MLSID],[IDX],[PostalCodeExt],[ShortSale],[BankOwned],[ShowAddress],[PostalCode],[Acreage],[StreetAddress],[StreetAddress2]
,[StreetDirPrefix],[StreetNumber],[StreetName],[SquareFootage],[ListPrice],[CDOM],[JLsDom],[CloseDate],[PendingDate],[DOM],[City]
,[ElementarySchool],[MiddleSchool],[PropTypeID],[HighSchool],[Latitude],[ListDate],[MLSDBListingModTime],[Longitude],[MLSListingID]
,[OriginalListPrice],[ParkingSpaces],[SchoolDistrict],[ClosePrice],[StatusDate],[TaxID],[UpdateDate],[VirtualTour],[YearBuilt],Age
,AgentHitCount,Auction,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,Community,SellingAgentID,CoSellingAgentID
,SellingOffice,CoSellingOffice,ExpirationDate,PictureCount,WithdrawDate,Zoning,HowSold,Area)
 Select 127,abs(s.IdxInclude-1),IIF(Len(s.Zip)=10,Right(s.Zip,4),Null),IIF(s.PotentialShortSaleY_N='Y',1,0)
 ,IIF(s.REO_BankOwnedY_N='Yes',1,0),IIF(s.Showaddress='N',0,1),Left(s.Zip,5),s.[Acres],s.[Address],s.[Address2],s.[AddressDirection]
 ,s.[AddressNumber],s.[AddressStreet],s.[ApxSqFt],s.[AskingPrice],s.[CDOM],s.[CDOM],s.[ClosingDate],s.[ContractDate],s.[DOM]
 ,s.[JlsCity],s.[JlsGradeSchool],s.[JlsJrHigh],s.[JLSPropTypeID],s.[JlsSrHigh],s.[Latitude],s.[ListingDate],s.[ListingModTime]
 ,s.[Longitude],s.[MLSNumber],s.[OriginalPrice],s.[ParkingSpaces],s.[SchoolDistrict],s.[SoldPrice],s.[StatusDate],s.[TaxPropertyID]
 ,s.[UpdateDate],s.[VTourURL],s.[YearBuilt],s.Age,s.AgentHitCount,s.AuctionY_N,s.ListAgent1,s.ListAgent2,s.ListOffice1,s.ListOffice2
 ,s.Subdivision,s.SellingAgent1,s.SellingAgent2,s.SellingOffice1,s.SellingOffice2,s.ExpirationDate,s.PictureCount,s.OffMarketDate
 ,s.Zoning,s.HowSold,s.Area
 From IdIMLS.dbo.MultiFamily s
 
Insert Into mls.Listing 
([MLSID],[IDX],[PostalCodeExt],[ShortSale],[BankOwned],[ShowAddress],[PostalCode],[Acreage],[StreetAddress],[StreetAddress2]
,[StreetDirPrefix],[StreetNumber],[StreetName],[ListPrice],[Bedrooms],[CDOM],[JLsDom],[CloseDate],[PendingDate],[DOM],[City]
,[ElementarySchool],[MiddleSchool],[PropTypeID],[HighSchool],[Latitude],[ListDate],[MLSDBListingModTime],[Longitude],[MLSListingID]
,[OriginalListPrice],[ParkingSpaces],[ClosePrice],[StatusDate],[TaxID],[UpdateDate],[VirtualTour],[YearBuilt],Age,AgentHitCount
,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice
,EstCompletionDate,ExpirationDate,NewConstruction,PictureCount,WithdrawDate,Zoning,HowSold,Area)
 Select 127,abs(s.IdxInclude-1),IIF(Len(s.Zip)=10,Right(s.Zip,4),Null),IIF(s.PotentialShortSaleY_N='Y',1,0)
 ,IIF(s.REO_BankOwnedY_N='Yes',1,0),IIF(s.Showaddress='N',0,1),Left(s.Zip,5),s.[Acres],s.[Address],s.[Address2],s.[AddressDirection]
 ,s.[AddressNumber],s.[AddressStreet],s.[AskingPrice],s.[Beds],s.[CDOM],s.[CDOM],s.[ClosingDate],s.[ContractDate],s.[DOM],s.[JlsCity]
 ,s.[JlsGradeSchool],s.[JlsJrHigh],s.[JLSPropTypeID],s.[JlsSrHigh],s.[Latitude],s.[ListingDate],s.[ListingModTime],s.[Longitude]
 ,s.[MLSNumber],s.[OriginalPrice],s.[ParkingSpaces],s.[SoldPrice],s.[StatusDate],s.[TaxPropertyID],s.[UpdateDate],s.[VTourURL]
 ,s.[YearBuilt],s.Age,s.AgentHitCount,s.ListAgent1,s.ListAgent2,s.ListOffice1,s.ListOffice2,s.SellingAgent1,s.SellingAgent2,s.SellingOffice1,s.SellingOffice2,s.NewConstrCompDate,s.ExpirationDate
 ,NewConstruction = CASE WHEN s.NewConstrCompDate IS NOT NULL THEN 'Yes' else 'No' end,s.PictureCount,s.OffMarketDate
 ,s.Zoning,s.HowSold,s.Area
 From IdIMLS.dbo.Commercial s
 
Insert Into mls.Listing
([MLSID],[IDX],[PostalCodeExt],[ShortSale],[BankOwned],[ShowAddress],[PostalCode],[Acreage],[StreetAddress],[StreetAddress2]
,[StreetDirPrefix],[StreetNumber],[StreetName],[SquareFootage],[ListPrice],[BathsTotal],[Bedrooms],[CDOM],[JLsDom],[CloseDate]
,[PendingDate],[DOM],[GarageSpaces],[City],[Fireplaces],[Garage],[ElementarySchool],[MiddleSchool],[PropTypeID],[HighSchool],[Latitude]
,[ListDate],[MLSDBListingModTime],[Longitude],[MLSListingID],[OriginalListPrice],[SchoolDistrict],[ClosePrice],[StatusDate],[TaxID]
,[UpdateDate],[VirtualTour],Age,AgentHitCount,Auction,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,Community
,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice,ExpirationDate,PictureCount,WithdrawDate,Zoning,HowSold,Area)
 Select 127,abs(s.IdxInclude-1),IIF(Len(s.Zip)=10,Right(s.Zip,4),Null),IIF(s.PotentialShortSaleY_N='Y',1,0)
 ,IIF(s.REO_BankOwnedY_N='Yes',1,0),IIF(s.Showaddress='N',0,1),Left(s.Zip,5),s.[Acres],s.[Address],s.[Address2],s.[AddressDirection]
 ,s.[AddressNumber],s.[AddressStreet],s.[ApxSqFt],s.[AskingPrice],s.[Baths],s.[Beds],s.[CDOM],s.[CDOM],s.[ClosingDate],s.[ContractDate]
 ,s.[DOM],s.[GarageCapacity],s.[JlsCity],s.[JLSFireplaces],s.[JLSGarage],s.[JlsGradeSchool],s.[JlsJrHigh],s.[JLSPropTypeID]
 ,s.[JlsSrHigh],s.[Latitude],s.[ListingDate],s.[ListingModTime],s.[Longitude],s.[MLSNumber],s.[OriginalPrice],s.[SchoolDistrict]
 ,s.[SoldPrice],s.[StatusDate],s.[TaxPropertyID],s.[UpdateDate],s.[VTourURL],s.Age,s.AgentHitCount,s.AuctionY_N,s.ListAgent1
 ,s.ListAgent2,s.ListOffice1,s.ListOffice2,s.Subdivision,s.SellingAgent1,s.SellingAgent2,s.SellingOffice1,s.SellingOffice2
 ,s.ExpirationDate,s.PictureCount,s.OffMarketDate,s.Zoning,s.HowSold,s.Area
 From IdIMLS.dbo.FarmRanch s

--IdSKAOR
Insert Into mls.Listing 
([ShowMap],[MLSID],[IDX],[ShowAddress],[Latitude],[Longitude],[JLsDom],[PostalCode],[StreetAddress],[StreetAddress2],[StreetDirPrefix]
,[StreetNumber],[StreetName],[ListPrice],[Bedrooms],[CDOM],[CloseDate],[PendingDate],[DOM],[HOADues],[BankOwned],[City]
,[ElementarySchool],[Garage],[GarageSpaces],[HighSchool],[MiddleSchool],[ParkingSpaces],[PropTypeID],[SchoolDistrict],[ShortSale]
,[ListDate],[MLSDBListingModTime],[MLSListingID],[Acreage],[OriginalListPrice],[ClosePrice],[SquareFootage],[StatusDate],[PropertyTax]
,[TaxID],[TaxYearBuilt],[BathsTotal],[UpdateDate],[VirtualTour],[YearBuilt],[LotSizeSF],AgentHitCount,Auction,BuilderName,ListAgentID
,CoListAgentID,CoListAgentID2,ListingOfficeID,Community,SellingAgentID,CoSellingAgentID,CoSellingAgentID2,SellingOffice,ExpirationDate
,PictureCount,Waterfront,WithdrawDate,Zoning,HowSold,Area)
 Select 1,300000013,iif(s.IdxInclude=1,0,1),iif(s.IdxInclude=2,0,1),IIF(s.Latitude is null,Latitude,TRY_CONVERT(decimal(9,6),s.Latitude))
 ,IIF(s.longitude is null,Longitude,TRY_CONVERT(decimal(9,6),s.longitude)),IIF(s.StatusCatID=1,s.CDOM
 ,s.CDOM + DATEDIFF(day,s.UpdateDate,Coalesce(s.JlsOffMarketDate,s.ListingModTime))),left(s.Zip,5),s.[Address],s.[Address2]
 ,s.[AddressDirection],s.[AddressNumber],s.[AddressStreet],s.[AskingPrice],s.[Bedrooms],s.[CDOM],s.[ClosingDate],s.[ContractDate]
 ,s.[DOM],s.[HOADues],s.[JLSBankOwned],s.[JLSCity],s.[JLSElementarySchool],s.[JLSGarage],s.[JLSGarageSpaces],s.[JLSHighSchool]
 ,s.[JLSMiddleHigh],s.[JLSParkingSpaces],s.[JLSPropTypeID],s.[JLSSchoolDistrict],s.[JLSShortSale],s.[ListingDate],s.[ListingModTime]
 ,s.[MLSNumber],s.[NumAcres],s.[OriginalPrice],s.[SoldPrice],s.[SquareFeet],s.[StatusDate],s.[Taxes],s.[TaxPropertyID],s.[TaxYear]
 ,s.[TotalBaths],s.[UpdateDate],s.[VirtualTour],s.[YearBuilt],TRY_CONVERT(int,replace(s.Lotsizesqft,',','')),AgentHitCount,s.AuctionProperty
 ,s.Builder,s.ListAgent1,s.ListAgent2,s.ListAgent3,s.ListOffice1,s.Neighborhood,s.SellingAgent1,s.SellingAgent2,s.SellingAgent3
 ,s.SellingOffice1,s.ExpirationDate,s.PictureCount,s.WaterFrontage,s.JLSOffMarketDate,s.Zoning,s.HowSold,s.Area
 From IdSKAOR.dbo.Residential s
 
Insert Into mls.Listing 
([ShowMap],[MLSID],[IDX],[ShowAddress],[Latitude],[Longitude],[JLsDom],[PostalCode],[StreetAddress],[StreetAddress2],[StreetDirPrefix]
,[StreetNumber],[StreetName],[ListPrice],[CDOM],[CloseDate],[PendingDate],[DOM],[BankOwned],[City],[ElementarySchool],[HighSchool]
,[MiddleSchool],[PropTypeID],[SchoolDistrict],[ShortSale],[ListDate],[MLSDBListingModTime],[MLSListingID],[Acreage],[OriginalListPrice]
,[ClosePrice],[StatusDate],[PropertyTax],[TaxID],[TaxYearBuilt],[UpdateDate],[VirtualTour],[LotSizeSF],AgentHitCount,Auction
,ListAgentID,CoListAgentID,CoListAgentID2,ListingOfficeID,Community,SellingAgentID,CoSellingAgentID,CoSellingAgentID2
,SellingOffice,ExpirationDate,PictureCount,Waterfront,WithdrawDate,Zoning,HowSold,Area)
 Select 1,300000013,iif(s.IdxInclude=1,0,1),iif(s.IdxInclude=2,0,1),IIF(s.Latitude is null,Latitude,TRY_CONVERT(decimal(9,6),s.Latitude))
 ,IIF(s.longitude is null,Longitude,TRY_CONVERT(decimal(9,6),s.longitude)),IIF(s.StatusCatID=1,s.CDOM
 ,s.CDOM + DATEDIFF(day,s.UpdateDate,Coalesce(s.JlsOffMarketDate,s.ListingModTime))),left(s.Zip,5),s.[Address],s.[Address2]
 ,s.[AddressDirection],s.[AddressNumber],s.[AddressStreet],s.[AskingPrice],s.[CDOM],s.[ClosingDate],s.[ContractDate],s.[DOM]
 ,s.[JLSBankOwned],s.[JLSCity],s.[JLSElementarySchool],s.[JLSHighSchool],s.[JLSMiddleHigh],s.[JLSPropTypeID],s.[JLSSchoolDistrict]
 ,s.[JLSShortSale],s.[ListingDate],s.[ListingModTime],s.[MLSNumber],s.[NumAcres],s.[OriginalPrice],s.[SoldPrice],s.[StatusDate]
 ,s.[Taxes],s.[TaxPropertyID],s.[TaxYear],s.[UpdateDate],s.[VirtualTour],TRY_CONVERT(int,replace(s.Lotsizesqft,',',''))
 ,s.AgentHitCount,s.AuctionProperty,s.ListAgent1,s.ListAgent2,s.ListAgent3,s.ListOffice1,s.Neighborhood,s.SellingAgent1,s.SellingAgent2
 ,s.SellingAgent3,s.SellingOffice1,s.ExpirationDate,s.PictureCount,s.WaterFrontage,s.JLSOffMarketDate,s.Zoning,s.HowSold,s.Area
 From IdSKAOR.dbo.Land s
 
Insert Into mls.Listing 
([ShowMap],[MLSID],[IDX],[ShowAddress],[Latitude],[Longitude],[JLsDom],[PostalCode],[StreetAddress],[StreetAddress2],[StreetDirPrefix]
,[StreetNumber],[StreetName],[ListPrice],[CDOM],[CloseDate],[PendingDate],[DOM],[BankOwned],[City],[ElementarySchool],[Garage]
,[GarageSpaces],[HighSchool],[MiddleSchool],[ParkingSpaces],[PropTypeID],[SchoolDistrict],[ShortSale],[ListDate],[MLSDBListingModTime]
,[MLSListingID],[Acreage],[OriginalListPrice],[ClosePrice],[SquareFootage],[StatusDate],[PropertyTax],[TaxID],[TaxYearBuilt],[UpdateDate]
,[VirtualTour],[YearBuilt],[LotSizeSF],AgentHitCount,Auction,ListAgentID,CoListAgentID,CoListAgentID2,ListingOfficeID,SellingAgentID
,CoSellingAgentID,CoSellingAgentID2,SellingOffice,ExpirationDate,PictureCount,Waterfront,WithdrawDate,Zoning,HowSold,Area)
 Select 1,300000013,iif(s.IdxInclude=1,0,1),iif(s.IdxInclude=2,0,1),IIF(s.Latitude is null,Latitude,TRY_CONVERT(decimal(9,6),s.Latitude))
 ,IIF(s.longitude is null,Longitude,TRY_CONVERT(decimal(9,6),s.longitude)),IIF(s.StatusCatID=1,s.CDOM
 ,s.CDOM + DATEDIFF(day,s.UpdateDate,Coalesce(s.JlsOffMarketDate,s.ListingModTime))),left(s.Zip,5),s.[Address],s.[Address2]
 ,s.[AddressDirection],s.[AddressNumber],s.[AddressStreet],s.[AskingPrice],s.[CDOM],s.[ClosingDate],s.[ContractDate],s.[DOM]
 ,s.[JLSBankOwned],s.[JLSCity],s.[JLSElementarySchool],s.[JLSGarage],s.[JLSGarageSpaces],s.[JLSHighSchool],s.[JLSMiddleHigh]
 ,s.[JLSParkingSpaces],s.[JLSPropTypeID],s.[JLSSchoolDistrict],s.[JLSShortSale],s.[ListingDate],s.[ListingModTime],s.[MLSNumber]
 ,s.[NumAcres],s.[OriginalPrice],s.[SoldPrice],s.[SquareFeet],s.[StatusDate],s.[Taxes],s.[TaxPropertyID],s.[TaxYear],s.[UpdateDate]
 ,s.[VirtualTour],s.[YearBuilt],TRY_CONVERT(int,replace(s.Lotsizesqft,',','')),s.AgentHitCount,s.AuctionProperty,s.ListAgent1
 ,s.ListAgent2,s.ListAgent3,s.ListOffice1,s.SellingAgent1,s.SellingAgent2,s.SellingAgent3,s.SellingOffice1,s.ExpirationDate,s.PictureCount
 ,s.WaterFrontage,s.JLSOffMarketDate,s.Zoning,s.HowSold,s.Area
 From IdSKAOR.dbo.Commercial s
 
Insert Into mls.Listing 
([ShowMap],[MLSID],[IDX],[ShowAddress],[Latitude],[Longitude],[JLsDom],[BathsTotal],[PostalCode],[StreetAddress],[StreetAddress2]
,[StreetDirPrefix],[StreetNumber],[StreetName],[ListPrice],[CDOM],[CloseDate],[PendingDate],[DOM],[BankOwned],[City],[ElementarySchool]
,[Garage],[GarageSpaces],[HighSchool],[MiddleSchool],[ParkingSpaces],[PropTypeID],[SchoolDistrict],[ShortSale],[ListDate]
,[MLSDBListingModTime],[MLSListingID],[Acreage],[OriginalListPrice],[ClosePrice],[SquareFootage],[StatusDate],[PropertyTax],[TaxID]
,[TaxYearBuilt],[UpdateDate],[VirtualTour],[YearBuilt],[LotSizeSF],AgentHitCount,Auction,BuilderName,ListAgentID,CoListAgentID
,CoListAgentID2,ListingOfficeID,Community,SellingAgentID,CoSellingAgentID,CoSellingAgentID2,SellingOffice,ExpirationDate,PictureCount
,Waterfront,WithdrawDate,Zoning,HowSold,Area)
 Select 1,300000013,iif(s.IdxInclude=1,0,1),iif(s.IdxInclude=2,0,1),IIF(s.Latitude is null,Latitude,TRY_CONVERT(decimal(9,6),s.Latitude))
 ,IIF(s.longitude is null,Longitude,TRY_CONVERT(decimal(9,6),s.longitude)),IIF(s.StatusCatID=1,s.CDOM
 ,s.CDOM + DATEDIFF(day,s.UpdateDate,Coalesce(s.JlsOffMarketDate,s.ListingModTime)))
 ,isnull(s.Unit1TotalBaths,0) + isnull(s.Unit2TotalBaths,0) + isnull(s.Unit3TotalBaths,0) + isnull(s.Unit4TotalBaths,0) + isnull(s.Unit5TotalBaths,0)
 ,left(s.Zip,5),s.[Address],s.[Address2],s.[AddressDirection],s.[AddressNumber],s.[AddressStreet],s.[AskingPrice],s.[CDOM]
 ,s.[ClosingDate],s.[ContractDate],s.[DOM],s.[JLSBankOwned],s.[JLSCity],s.[JLSElementarySchool],s.[JLSGarage],s.[JLSGarageSpaces]
 ,s.[JLSHighSchool],s.[JLSMiddleHigh],s.[JLSParkingSpaces],s.[JLSPropTypeID],s.[JLSSchoolDistrict],s.[JLSShortSale],s.[ListingDate]
 ,s.[ListingModTime],s.[MLSNumber],s.[NumAcres],s.[OriginalPrice],s.[SoldPrice],s.[SquareFeet],s.[StatusDate],s.[Taxes]
 ,s.[TaxPropertyID],s.[TaxYear],s.[UpdateDate],s.[VirtualTour],s.[YearBuilt],TRY_CONVERT(int,replace(s.Lotsizesqft,',',''))
 ,s.AgentHitCount,s.AuctionProperty,s.Builder,s.ListAgent1,s.ListAgent2,s.ListAgent3,s.ListOffice1,s.Neighborhood,s.SellingAgent1
 ,s.SellingAgent2,s.SellingAgent3,s.SellingOffice1,s.ExpirationDate,s.PictureCount,s.WaterFrontage,s.JLSOffMarketDate,s.Zoning
 ,s.HowSold,s.Area
 From IdSKAOR.dbo.Multifamily s



 --IdSRMLS
 Insert Into mls.Listing 
 ([MLSID],[BankOwned],[ShortSale],[Latitude],[Longitude],[JLsDom],[Acreage],[CloseDate],[ClosePrice],[DOM],[City],[StreetDirPrefix]
 ,[UpdateDate],[ListDate],[MLSDBListingModTime],[ListPrice],[MLSListingID],[OriginalListPrice],[PendingDate],[PostalCode],[PropertyTax]
 ,[SquareFootage],[StreetName],[StreetNumber],[TaxID],[VirtualTour],[ShowAddress],[YearBuilt],[LotSizeSF],Age,AgentBonus,AgentOwned
 ,BuilderName,ListAgentID,CoListAgentID,ListingOfficeID,CommissionComments,Commission,Community,SellingAgentID,SellingOffice
 ,ExpirationDate,NewConstruction,PictureCount,WithdrawDate,Zoning,HowSold,Area)
 Select 200000002,IIF(s.DistressedProperty like '%REO%',1,0),IIF(s.DistressedProperty like '%Short%',1,0)
 ,IIF(s.Latitude is null,Latitude,TRY_CONVERT(decimal(9,6),s.Latitude)),IIF(s.longitude is null,Longitude,TRY_CONVERT(decimal(9,6)
 ,s.longitude)),IIF(s.PropertyStatusID='Z',s.Dom + Datediff(day,LastModifiedDateTime,Coalesce(s.JlsOffMarketDate,s.ListingModTime))
 ,s.Dom) ,s.[Acreage],s.[CloseDate],s.[ClosePrice],s.[DOM],s.[JLSCity],s.[JlsStreetDirPrefix],s.[LastModifiedDateTime],s.[ListDate]
 ,s.[ListingModTime],s.[ListPrice],s.[MLSNumber],s.[OriginalListPrice],s.[PendingDate],s.[PostalCode],s.[PropertyTax],s.[SquareFootage]
 ,s.[StreetName],s.[StreetNumber],s.[TaxID],s.[VirtualTour],s.[VOWShowAddress],s.[YearBuilt],Try_Convert(numeric,s.LotSizeSF)
 ,s.EffectiveAge,s.AgentBonus,s.AgentOwned,s.Builder,s.JLSListAgentID,s.JLSCoListAgentID,s.ListingOfficeNum,s.CommissionPaidOnSellerConcessions
 ,s.MinCommission,s.SubDivision,s.JLSSaleAgentID,s.SellingOfficeID,s.[ExpireDate]
 ,case when s.ConstructionStatus like '%14%' then 'New-Complete' when s.ConstructionStatus like '%15%' then 'New-Under Construction' else '' end
 ,s.PhotoCount,s.JLSOffMarketDate,coalesce(s.ZoningGeneral,s.ZoningSpecific),l1.LongValue,s.SubArea
 From IdSRMLS.dbo.Commercial s
  left join IdSRMLS.dbo.Lookups l1
    on s.FinanceHowSold = l1.[Value] and l1.[Lookup] = 'How_Sold'

 
Insert Into mls.Listing 
([MLSID],[BankOwned],[ShortSale],[Latitude],[Longitude],[JLsDom],[Acreage],[CloseDate],[ClosePrice],[DOM],[City],[ElementarySchool]
,[HighSchool],[MiddleSchool],[StreetDirPrefix],[UpdateDate],[ListDate],[MLSDBListingModTime],[ListPrice],[MLSListingID]
,[OriginalListPrice],[PendingDate],[PostalCode],[PropertyTax],[StreetName],[StreetNumber],[TaxID],[VirtualTour],[ShowAddress]
,[LotSizeSF],AgentBonus,AgentOwned,ListAgentID,CoListAgentID,ListingOfficeID,CommissionComments,Commission
,Community,SellingAgentID,SellingOffice,ExpirationDate,PictureCount,WithdrawDate,Zoning,HowSold,Area)
 Select 200000002,IIF(s.DistressedProperty like '%REO%',1,0),IIF(s.DistressedProperty like '%Short%',1,0)
 ,IIF(s.Latitude is null,Latitude,TRY_CONVERT(decimal(9,6),s.Latitude)),IIF(s.longitude is null,Longitude,TRY_CONVERT(decimal(9,6)
 ,s.longitude)),IIF(s.PropertyStatusID='Z',s.Dom + Datediff(day,LastModifiedDateTime,Coalesce(s.JlsOffMarketDate,s.ListingModTime))
 ,s.Dom) ,s.[Acreage],s.[CloseDate],s.[ClosePrice],s.[DOM],s.[JLSCity],s.[JLSElementarySchool],s.[JLSHighSchool],s.[JLSMiddleSchool]
 ,s.[JlsStreetDirPrefix],s.[LastModifiedDateTime],s.[ListDate],s.[ListingModTime],s.[ListPrice],s.[MLSNumber],s.[OriginalListPrice]
 ,s.[PendingDate],s.[PostalCode],s.[PropertyTax],s.[StreetName],s.[StreetNumber],s.[TaxID],s.[VirtualTour],s.[VOWShowAddress]
 ,Try_Convert(numeric,s.LotSizeSF),s.AgentBonus,s.AgentOwned,s.JLSListAgentID,s.JLSCoListAgentID,s.ListingOfficeNum,s.CommissionPaidOnSellerConcessions
 ,s.MinCommission,s.SubDivision,s.JLSSaleAgentID,s.SellingOfficeID,s.[ExpireDate],s.PhotoCount,s.JLSOffMarketDate
 ,coalesce(s.ZoningGeneral,s.ZoningSpecific),l1.LongValue,s.SubArea
 From IdSRMLS.dbo.CommercialLand s
   left join IdSRMLS.dbo.Lookups l1
    on s.FinanceHowSold = l1.[Value] and l1.[Lookup] = 'How_Sold'
 
Insert Into mls.Listing 
([MLSID],[Latitude],[Longitude],[JLsDom],[Acreage],[CloseDate],[ClosePrice],[DOM],[City],[StreetDirPrefix],[UpdateDate],[ListDate]
,[MLSDBListingModTime],[MLSListingID],[PendingDate],[PostalCode],[PropertyTax],[ListPrice],[SquareFootage],[StreetName],[StreetNumber]
,[TaxID],[VirtualTour],[ShowAddress],[YearBuilt],[LotSizeSF],AgentBonus,AgentOwned,ListAgentID,CoListAgentID
,ListingOfficeID,CommissionComments,Commission,SellingAgentID,SellingOffice,ExpirationDate,NewConstruction,PictureCount
,WithdrawDate,Zoning,HowSold,Area)
 Select 200000002,IIF(s.Latitude is null,Latitude,TRY_CONVERT(decimal(9,6),s.Latitude)),IIF(s.longitude is null,Longitude
 ,TRY_CONVERT(decimal(9,6),s.longitude)),IIF(s.PropertyStatusID='Z',s.Dom + Datediff(day,LastModifiedDateTime
 ,Coalesce(s.JlsOffMarketDate,s.ListingModTime)),s.Dom) ,s.[Acreage],s.[CloseDate],s.[ClosePrice],s.[DOM],s.[JLSCity]
 ,s.[JlsStreetDirPrefix],s.[LastModifiedDateTime],s.[ListDate],s.[ListingModTime],s.[MLSNumber],s.[PendingDate],s.[PostalCode]
 ,s.[PropertyTax],s.[RentalRate],s.[SquareFootage],s.[StreetName],s.[StreetNumber],s.[TaxID],s.[VirtualTour],s.[VOWShowAddress]
 ,s.[YearBuilt],Try_Convert(numeric,s.LotSizeSF),s.AgentBonus,s.AgentOwned,s.JLSListAgentID,s.JLSCoListAgentID,s.ListingOfficeNum,s.CommissionPaidOnSellerConcessions
 ,s.MinCommission,s.JLSSaleAgentID,s.SellingOfficeID,s.[ExpireDate]
 ,case when s.ConstructionStatus like '%14%' then 'New-Complete' when s.ConstructionStatus like '%15%' then 'New-Under Construction' else '' end
 ,s.PhotoCount,s.JLSOffMarketDate,coalesce(s.ZoningGeneral,s.ZoningSpecific),l1.LongValue,s.SubArea
 From IdSRMLS.dbo.CommercialLease s
   left join IdSRMLS.dbo.Lookups l1
    on s.FinanceHowSold = l1.[Value] and l1.[Lookup] = 'How_Sold'
 
Insert Into mls.Listing 
([MLSID],[BathsPartial],[BathsTotal],[BankOwned],[ShortSale],[Latitude],[Longitude],[JLsDom],[Acreage],[BathsFull],[Bedrooms]
,[CloseDate],[ClosePrice],[DOM],[City],[ElementarySchool],[Garage],[HighSchool],[MiddleSchool],[ParkingSpaces],[StreetDirPrefix]
,[UpdateDate],[ListDate],[MLSDBListingModTime],[ListPrice],[MLSListingID],[OriginalListPrice],[PendingDate],[PostalCode],[PropertyTax]
,[SquareFootage],[StreetName],[StreetNumber],[TaxID],[VirtualTour],[ShowAddress],[YearBuilt],[HOADues],[LotSizeSF],AgentBonus
,AgentOwned,ListAgentID,CoListAgentID,ListingOfficeID,CommissionComments,Commission,Community,SellingAgentID,SellingOffice
,ExpirationDate,NewConstruction,PictureCount,WithdrawDate,Zoning,HowSold,Area)
 Select 200000002,BathsThreeQtrs+BathsHalfTotal,IIF(s.BathsTotal<1000,s.BathsTotal,Null),IIF(s.DistressedProperty like '%REO%',1,0)
 ,IIF(s.DistressedProperty like '%Short%',1,0),IIF(s.Latitude is null,Latitude,TRY_CONVERT(decimal(9,6),s.Latitude))
 ,IIF(s.longitude is null,Longitude,TRY_CONVERT(decimal(9,6),s.longitude))
 ,IIF(s.PropertyStatusID='Z',s.Dom + Datediff(day,LastModifiedDateTime,Coalesce(s.JlsOffMarketDate,s.ListingModTime)),s.Dom) 
 ,s.[Acreage],s.[BathsFull],s.[BedroomsTotal],s.[CloseDate],s.[ClosePrice],s.[DOM],s.[JLSCity],s.[JLSElementarySchool],s.[JLSGarage]
 ,s.[JLSHighSchool],s.[JLSMiddleSchool],s.[JLSParkingSpaces],s.[JlsStreetDirPrefix],s.[LastModifiedDateTime],s.[ListDate]
 ,s.[ListingModTime],s.[ListPrice],s.[MLSNumber],s.[OriginalListPrice],s.[PendingDate],s.[PostalCode],s.[PropertyTax],s.[SquareFootage]
 ,s.[StreetName],s.[StreetNumber],s.[TaxID],s.[VirtualTour],s.[VOWShowAddress],s.[YearBuilt],Try_Convert(numeric,s.HOADUES)
 ,Try_Convert(numeric,s.LotSizeSF),s.AgentBonus,s.AgentOwned,s.JLSListAgentID,s.JLSCoListAgentID,s.ListingOfficeNum,s.CommissionPaidOnSellerConcessions
 ,s.MinCommission,s.SubDivision,s.JLSSaleAgentID,s.SellingOfficeID,s.[ExpireDate]
 ,case when s.ConstructionStatus like '%14%' then 'New-Complete' when s.ConstructionStatus like '%15%' then 'New-Under Construction' else '' end
 ,s.PhotoCount,s.JLSOffMarketDate,coalesce(s.ZoningGeneral,s.ZoningSpecific),l1.LongValue,s.SubArea
 From IdSRMLS.dbo.FarmRanch s
   left join IdSRMLS.dbo.Lookups l1
    on s.FinanceHowSold = l1.[Value] and l1.[Lookup] = 'How_Sold'
 
Insert Into mls.Listing 
([MLSID],[BathsPartial],[BathsTotal],[BankOwned],[ShortSale],[Latitude],[Longitude],[JLsDom],[Acreage],[BathsFull],[Bedrooms]
,[CloseDate],[ClosePrice],[DOM],[City],[ElementarySchool],[HighSchool],[MiddleSchool],[StreetDirPrefix],[UpdateDate],[ListDate]
,[MLSDBListingModTime],[ListPrice],[MLSListingID],[OriginalListPrice],[PendingDate],[PostalCode],[PropertyTax],[SquareFootage]
,[StreetName],[StreetNumber],[TaxID],[VirtualTour],[ShowAddress],[YearBuilt],[HOADues],[LotSizeSF],AgentBonus,AgentOwned
,BuilderName,ListAgentID,CoListAgentID,ListingOfficeID,CommissionComments,Commission,Community,SellingAgentID,SellingOffice
,ExpirationDate,NewConstruction,PictureCount,WithdrawDate,Zoning,HowSold,Area)
 Select 200000002,BathsThreeQtrs+BathsHalfTotal,IIF(s.BathsTotal<1000,s.BathsTotal,Null),IIF(s.DistressedProperty like '%REO%',1,0)
 ,IIF(s.DistressedProperty like '%Short%',1,0),IIF(s.Latitude is null,Latitude,TRY_CONVERT(decimal(9,6),s.Latitude))
 ,IIF(s.longitude is null,Longitude,TRY_CONVERT(decimal(9,6),s.longitude))
 ,IIF(s.PropertyStatusID='Z',s.Dom + Datediff(day,LastModifiedDateTime,Coalesce(s.JlsOffMarketDate,s.ListingModTime)),s.Dom) 
 ,s.[Acreage],s.[BathsFull],s.[BedroomsTotal],s.[CloseDate],s.[ClosePrice],s.[DOM],s.[JLSCity],s.[JLSElementarySchool],s.[JLSHighSchool]
 ,s.[JLSMiddleSchool],s.[JlsStreetDirPrefix],s.[LastModifiedDateTime],s.[ListDate],s.[ListingModTime],s.[ListPrice],s.[MLSNumber]
 ,s.[OriginalListPrice],s.[PendingDate],s.[PostalCode],s.[PropertyTax],s.[SquareFootage],s.[StreetName],s.[StreetNumber],s.[TaxID]
 ,s.[VirtualTour],s.[VOWShowAddress],s.[YearBuilt],Try_Convert(numeric,s.HOADUES),Try_Convert(numeric,s.LotSizeSF),s.AgentBonus
 ,s.AgentOwned,s.Builder,s.JLSListAgentID,s.JLSCoListAgentID,s.ListingOfficeNum,s.CommissionPaidOnSellerConcessions
 ,s.MinCommission,s.SubDivision,s.JLSSaleAgentID,s.SellingOfficeID,s.[ExpireDate]
 ,case when s.ConstructionStatus like '%14%' then 'New-Complete' when s.ConstructionStatus like '%15%' then 'New-Under Construction' else '' end
 ,s.PhotoCount,s.JLSOffMarketDate,coalesce(s.ZoningGeneral,s.ZoningSpecific),l1.LongValue,s.SubArea
 From IdSRMLS.dbo.MultiFamily s
   left join IdSRMLS.dbo.Lookups l1
    on s.FinanceHowSold = l1.[Value] and l1.[Lookup] = 'How_Sold'

Insert Into mls.Listing 
([MLSID],[BathsPartial],[BathsTotal],[Latitude],[Longitude],[JLsDom],[BathsFull],[Bedrooms],[CloseDate],[ClosePrice],[DOM],[City]
,[ElementarySchool],[Garage],[HighSchool],[MiddleSchool],[ParkingSpaces],[StreetDirPrefix],[UpdateDate],[ListDate],[MLSDBListingModTime]
,[ListPrice],[MLSListingID],[PendingDate],[PostalCode],[SquareFootage],[StreetName],[StreetNumber],[VirtualTour],[ShowAddress]
,[YearBuilt],AgentBonus,AgentOwned,ListAgentID,CoListAgentID,ListingOfficeID,CommissionComments,Commission,Community
,SellingAgentID,SellingOffice,ExpirationDate,NewConstruction,PictureCount,WithdrawDate,HowSold,Area)
 Select 200000002,BathsThreeQtrs+BathsHalfTotal,IIF(s.BathsTotal<1000,s.BathsTotal,Null),IIF(s.Latitude is null,Latitude
 ,TRY_CONVERT(decimal(9,6),s.Latitude)),IIF(s.longitude is null,Longitude,TRY_CONVERT(decimal(9,6),s.longitude))
 ,IIF(s.PropertyStatusID='Z',s.Dom + Datediff(day,LastModifiedDateTime,Coalesce(s.JlsOffMarketDate,s.ListingModTime)),s.Dom) 
 ,s.[BathsFull],s.[BedroomsTotal],s.[CloseDate],s.[ClosePrice],s.[DOM],s.[JLSCity],s.[JLSElementarySchool],s.[JLSGarage]
 ,s.[JLSHighSchool],s.[JLSMiddleSchool],s.[JLSParkingSpaces],s.[JlsStreetDirPrefix],s.[LastModifiedDateTime],s.[ListDate]
 ,s.[ListingModTime],s.[ListPrice],s.[MLSNumber],s.[PendingDate],s.[PostalCode],s.[SquareFootage],s.[StreetName],s.[StreetNumber]
 ,s.[VirtualTour],s.[VOWShowAddress],s.[YearBuilt],s.AgentBonus,s.AgentOwned,s.JLSListAgentID,s.JLSCoListAgentID,s.ListingOfficeNum,s.CommissionPaidOnSellerConcessions
 ,s.MinCommission,s.SubDivision,s.JLSSaleAgentID,s.SellingOfficeID,s.[ExpireDate]
 ,case when s.ConstructionStatus like '%14%' then 'New-Complete' when s.ConstructionStatus like '%15%' then 'New-Under Construction' else '' end
 ,s.PhotoCount,s.JLSOffMarketDate,l1.LongValue,s.SubArea
 From IdSRMLS.dbo.Rentals s
   left join IdSRMLS.dbo.Lookups l1
    on s.FinanceHowSold = l1.[Value] and l1.[Lookup] = 'How_Sold'

Insert Into mls.Listing 
([MLSID],[BathsPartial],[BathsTotal],[BankOwned],[ShortSale],[Latitude],[Longitude],[JLsDom],[Acreage],[BathsFull],[Bedrooms]
,[CloseDate],[ClosePrice],[DOM],[City],[ElementarySchool],[Garage],[HighSchool],[MiddleSchool],[ParkingSpaces],[StreetDirPrefix]
,[UpdateDate],[ListDate],[MLSDBListingModTime],[ListPrice],[MLSListingID],[OriginalListPrice],[PendingDate],[PostalCode],[PropertyTax]
,[SquareFootage],[StreetName],[StreetNumber],[TaxID],[VirtualTour],[ShowAddress],[YearBuilt],[HOADues],[LotSizeSF],AgentBonus
,AgentOwned,BuilderName,ListAgentID,CoListAgentID,ListingOfficeID,CommissionComments,Commission,Community,SellingAgentID,SellingOffice
,ExpirationDate,NewConstruction,PictureCount,WithdrawDate,Zoning,HowSold,Area)
 Select 200000002,BathsThreeQtrs+BathsHalfTotal,IIF(s.BathsTotal<1000,s.BathsTotal,Null),IIF(s.DistressedProperty like '%REO%',1,0)
 ,IIF(s.DistressedProperty like '%Short%',1,0),IIF(s.Latitude is null,Latitude,TRY_CONVERT(decimal(9,6),s.Latitude))
 ,IIF(s.longitude is null,Longitude,TRY_CONVERT(decimal(9,6),s.longitude))
 ,IIF(s.PropertyStatusID='Z',s.Dom + Datediff(day,LastModifiedDateTime,Coalesce(s.JlsOffMarketDate,s.ListingModTime)),s.Dom) 
 ,s.[Acreage],s.[BathsFull],s.[BedroomsTotal],s.[CloseDate],s.[ClosePrice],s.[DOM],s.[JLSCity],s.[JLSElementarySchool],s.[JLSGarage]
 ,s.[JLSHighSchool],s.[JLSMiddleSchool],s.[JLSParkingSpaces],s.[JlsStreetDirPrefix],s.[LastModifiedDateTime],s.[ListDate]
 ,s.[ListingModTime],s.[ListPrice],s.[MLSNumber],s.[OriginalListPrice],s.[PendingDate],s.[PostalCode],s.[PropertyTax],s.[SquareFootage]
 ,s.[StreetName],s.[StreetNumber],s.[TaxID],s.[VirtualTour],s.[VOWShowAddress],s.[YearBuilt],Try_Convert(numeric,s.HOADUES)
 ,Try_Convert(numeric,s.LotSizeSF),s.AgentBonus,s.AgentOwned,s.Builder,s.JLSListAgentID,s.JLSCoListAgentID,s.ListingOfficeNum,s.CommissionPaidOnSellerConcessions
 ,s.MinCommission,s.SubDivision,s.JLSSaleAgentID,s.SellingOfficeID,s.[ExpireDate]
 ,case when s.ConstructionStatus like '%14%' then 'New-Complete' when s.ConstructionStatus like '%15%' then 'New-Under Construction' else '' end
 ,s.PhotoCount,s.JLSOffMarketDate,coalesce(s.ZoningGeneral,s.ZoningSpecific),l1.LongValue,s.SubArea
 From IdSRMLS.dbo.Residential s
   left join IdSRMLS.dbo.Lookups l1
    on s.FinanceHowSold = l1.[Value] and l1.[Lookup] = 'How_Sold'

Insert Into mls.Listing 
([MLSID],[BankOwned],[ShortSale],[Latitude],[Longitude],[JLsDom],[Acreage],[CloseDate],[ClosePrice],[DOM],[City],[ElementarySchool]
,[HighSchool],[MiddleSchool],[StreetDirPrefix],[UpdateDate],[ListDate],[MLSDBListingModTime],[ListPrice],[MLSListingID]
,[OriginalListPrice],[PendingDate],[PostalCode],[PropertyTax],[StreetName],[StreetNumber],[TaxID],[VirtualTour],[ShowAddress]
,[LotSizeSF],AgentBonus,AgentOwned,ListAgentID,CoListAgentID,ListingOfficeID,CommissionComments,Commission
,Community,SellingAgentID,SellingOffice,ExpirationDate,PictureCount,WithdrawDate,Zoning,HowSold,Area)
 Select 200000002,IIF(s.DistressedProperty like '%REO%',1,0),IIF(s.DistressedProperty like '%Short%',1,0)
 ,IIF(s.Latitude is null,Latitude,TRY_CONVERT(decimal(9,6),s.Latitude)),IIF(s.longitude is null,Longitude
 ,TRY_CONVERT(decimal(9,6),s.longitude))
 ,IIF(s.PropertyStatusID='Z',s.Dom + Datediff(day,LastModifiedDateTime,Coalesce(s.JlsOffMarketDate,s.ListingModTime)),s.Dom) 
 ,s.[Acreage],s.[CloseDate],s.[ClosePrice],s.[DOM],s.[JLSCity],s.[JLSElementarySchool],s.[JLSHighSchool],s.[JLSMiddleSchool]
 ,s.[JlsStreetDirPrefix],s.[LastModifiedDateTime],s.[ListDate],s.[ListingModTime],s.[ListPrice],s.[MLSNumber],s.[OriginalListPrice]
 ,s.[PendingDate],s.[PostalCode],s.[PropertyTax],s.[StreetName],s.[StreetNumber],s.[TaxID],s.[VirtualTour],s.[VOWShowAddress]
 ,Try_Convert(numeric,s.LotSizeSF),s.AgentBonus,s.AgentOwned,s.JLSListAgentID,s.JLSCoListAgentID,s.ListingOfficeNum
 ,s.CommissionPaidOnSellerConcessions,s.MinCommission,s.SubDivision,s.JLSSaleAgentID,s.SellingOfficeID,s.[ExpireDate],s.PhotoCount
 ,s.JLSOffMarketDate,coalesce(s.ZoningGeneral,s.ZoningSpecific),l1.LongValue,s.SubArea
 From IdSRMLS.dbo.ResidentialLand s
   left join IdSRMLS.dbo.Lookups l1
    on s.FinanceHowSold = l1.[Value] and l1.[Lookup] = 'How_Sold'


 --OrLCBR
 Insert Into mls.Listing 
([MLSID],[PostalCodeExt],[PostalCode],[Bedrooms],[CDOM],[CloseDate],[ClosePrice],[DOM],[Latitude],[Longitude],[StreetNumber],[BankOwned]
,[City],[JLsDom],[Garage],[ParkingSpaces],[ParentPropTypeID],[PropTypeID],[ShortSale],[UpdateDate],[ListDate],[MLSDBListingModTime]
,[ListPrice],[MLSListingID],[Acreage],[PropertyTax],[StatusDate],[StreetDirPrefix],[StreetDirSuffix],[StreetName],[StreetTypeSuffix]
,[VirtualTour],[PendingDate],[YearBuilt],[BathsTotal],[OriginalListPrice],[SquareFootage],CancelDate,ListAgentID,CoListAgentID
,ListingOfficeID,CoListingOfficeID,CommissionCode,Commission,Community,SellingAgentID,CoSellingAgentID
,SellingOffice,CoSellingOffice,FallThroughDate,PictureCount,Waterfront,WithdrawDate,Zoning,Area)
 Select 575008,iif(LEN(s.postalcode)=10, right(s.postalcode,4), NULL),left(s.PostalCode,5),s.[BedroomsTotal],s.[CDOM],s.[CloseDate]
 ,s.[ClosePrice],s.[DOM],s.[GeoLatitude],s.[GeoLongitude],s.[HouseNumber],s.[JLSBankOwned],s.[JLSCity],s.[JlsDom],s.[JLSGarage]
 ,s.[JLSGarageSpaces],s.[JLSParentPropTypeID],s.[JLSPropTypeID],s.[JLSShortSale],s.[LastModifiedDateTime],s.[ListDate]
 ,s.[ListingModTime],s.[ListPrice],s.[MLSNumber],s.[NoOfAcres],s.[PropertyTaxes],s.[StatusChangeDate],s.[StreetDirectionPfx]
 ,s.[StreetDirectionSfx],s.[StreetName],s.[StreetSfx],s.[UnBrandedVirtualTour],s.[UnderContractDate],s.[YearBuilt]
 ,TRY_CONVERT(DECIMAL(5,2),s.[BathroomsTotal]),Try_Convert(int,s.OriginalListPrice),Try_Convert(numeric,s.TotalSqFt),s.CancelDate
 ,s.ListingAgentID,s.CoListAgentID,s.ListingOfficeShortID,s.CoListingOfficeShortID,BACommType=l1.LongValue,s.BAComm,SubDivision=l2.LongValue
 ,s.SellingAgentID,s.CoSellingAgentID,s.SellingOfficeShortId,s.CoSellingOfficeShortId,s.FallThroughDate,s.PictureCount
 ,Frontage=case when l3.LongValue not in ('None','Highway','Golf Course') then l3.LongValue else '' end
 ,s.WithdrawalDate,Zoning=l4.LongValue,s.Area
 From OrLCBR.dbo.Business s
  left join OrLCBR.dbo.Lookups l1
    on s.BACommType = l1.[Value] and l1.[Lookup] = '20160201102310277691000000'
  left join OrLCBR.dbo.Lookups l2
    on s.Subdivision = l2.[Value] and l2.[Lookup] = '19991209173913996673000000'
  left join OrLCBR.dbo.Lookups l3
    on s.Frontage = l3.[Value] and l3.[Lookup] = '19991209173913999315000000'
  left join OrLCBR.dbo.Lookups l4
    on s.Zoning = l4.[Value] and l4.[Lookup] = '19991209173914040374000000'

 Insert Into mls.Listing
([MLSID],[PostalCodeExt],[PostalCode],[Bedrooms],[CDOM],[CloseDate],[ClosePrice],[DOM],[Latitude],[Longitude],[StreetNumber],[BankOwned]
,[City],[JLsDom],[Garage],[ParkingSpaces],[ParentPropTypeID],[PropTypeID],[ShortSale],[UpdateDate],[ListDate],[MLSDBListingModTime]
,[ListPrice],[MLSListingID],[Acreage],[TaxID],[PropertyTax],[StatusDate],[StreetDirPrefix],[StreetDirSuffix],[StreetName]
,[StreetTypeSuffix],[VirtualTour],[PendingDate],[YearBuilt],[BathsTotal],[OriginalListPrice],[SquareFootage],CancelDate,ListAgentID
,CoListAgentID,ListingOfficeID,CoListingOfficeID,CommissionCode,CommissionComments,Commission,Community,SellingAgentID
,CoSellingAgentID,SellingOffice,CoSellingOffice,FallThroughDate,PictureCount,Waterfront,WithdrawDate,Zoning,Area)
 Select 575008,iif(LEN(s.postalcode)=10, right(s.postalcode,4), NULL),left(s.PostalCode,5),s.[BedroomsTotal],s.[CDOM],s.[CloseDate]
 ,s.[ClosePrice],s.[DOM],s.[GeoLatitude],s.[GeoLongitude],s.[HouseNumber],s.[JLSBankOwned],s.[JLSCity],s.[JlsDom],s.[JLSGarage]
 ,s.[JLSGarageSpaces],s.[JLSParentPropTypeID],s.[JLSPropTypeID],s.[JLSShortSale],s.[LastModifiedDateTime],s.[ListDate]
 ,s.[ListingModTime],s.[ListPrice],s.[MLSNumber],s.[NoOfAcres],s.[PropertyID],s.[PropertyTaxes],s.[StatusChangeDate]
 ,s.[StreetDirectionPfx],s.[StreetDirectionSfx],s.[StreetName],s.[StreetSfx],s.[UnBrandedVirtualTour],s.[UnderContractDate]
 ,s.[YearBuilt],TRY_CONVERT(DECIMAL(5,2),s.[BathroomsTotal]),Try_Convert(int,s.OriginalListPrice),Try_Convert(numeric,s.TotalSqFt)
 ,s.CancelDate,s.ListingAgentID,s.CoListAgentID,s.ListingOfficeShortID,s.CoListingOfficeShortID,BACommType=l1.LongValue,s.CommissionCode
 ,s.BAComm,SubDivision=l2.LongValue,s.SellingAgentID,s.CoSellingAgentID,s.SellingOfficeShortId,s.CoSellingOfficeShortId,s.FallThroughDate
 ,s.PictureCount,Frontage=case when l3.LongValue not in ('None','Highway','Golf Course') then l3.LongValue else '' end
 ,s.WithdrawalDate,Zoning=l4.LongValue,s.Area
 From OrLCBR.dbo.Commercial s
   left join OrLCBR.dbo.Lookups l1
    on s.BACommType = l1.[Value] and l1.[Lookup] = '20160201102310277691000000'
  left join OrLCBR.dbo.Lookups l2
    on s.Subdivision = l2.[Value] and l2.[Lookup] = '19991209173913728250000000'
  left join OrLCBR.dbo.Lookups l3
    on s.Frontage = l3.[Value] and l3.[Lookup] = '19991209173913730655000000'
  left join OrLCBR.dbo.Lookups l4
    on s.Zoning = l4.[Value] and l4.[Lookup] = '19991209173913734245000000'

Insert Into mls.Listing 
([MLSID],[PostalCodeExt],[PostalCode],[Bedrooms],[CDOM],[CloseDate],[ClosePrice],[DOM],[Latitude],[Longitude],[StreetNumber],[BankOwned]
,[City],[JLsDom],[Garage],[ParkingSpaces],[ParentPropTypeID],[PropTypeID],[ShortSale],[UpdateDate],[ListDate],[MLSDBListingModTime]
,[ListPrice],[MLSListingID],[Acreage],[TaxID],[PropertyTax],[StatusDate],[StreetDirPrefix],[StreetDirSuffix],[StreetName]
,[StreetTypeSuffix],[VirtualTour],[PendingDate],[YearBuilt],[BathsTotal],[OriginalListPrice],[HOADues],[SquareFootage],CancelDate
,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,CommissionCode,CommissionComments,Commission,Community
,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice,FallThroughDate,PictureCount,Waterfront,WithdrawDate
,Zoning,Area)
 Select 575008,iif(LEN(s.postalcode)=10, right(s.postalcode,4), NULL),left(s.PostalCode,5),s.[BedroomsTotal],s.[CDOM],s.[CloseDate]
 ,s.[ClosePrice],s.[DOM],s.[GeoLatitude],s.[GeoLongitude],s.[HouseNumber],s.[JLSBankOwned],s.[JLSCity],s.[JlsDom],s.[JLSGarage]
 ,s.[JLSGarageSpaces],s.[JLSParentPropTypeID],s.[JLSPropTypeID],s.[JLSShortSale],s.[LastModifiedDateTime],s.[ListDate],s.[ListingModTime]
 ,s.[ListPrice],s.[MLSNumber],s.[NoOfAcres],s.[PropertyID],s.[PropertyTaxes],s.[StatusChangeDate],s.[StreetDirectionPfx]
 ,s.[StreetDirectionSfx],s.[StreetName],s.[StreetSfx],s.[UnBrandedVirtualTour],s.[UnderContractDate],s.[YearBuilt]
 ,TRY_CONVERT(DECIMAL(5,2),s.[BathroomsTotal]),Try_Convert(int,s.OriginalListPrice),Try_Convert(numeric,replace(s.AssocFees,'$',''))
 ,Try_Convert(numeric,s.TotalSqFt),s.CancelDate,s.ListingAgentID,s.CoListAgentID,s.ListingOfficeShortID,s.CoListingOfficeShortID
 ,BACommType=l1.LongValue,s.CommissionCode,s.BAComm,SubDivision=l2.LongValue,s.SellingAgentID,s.CoSellingAgentID,s.SellingOfficeShortId
 ,s.CoSellingOfficeShortId,s.FallThroughDate,s.PictureCount
 ,Frontage=case when l3.LongValue not in ('None','Highway','Golf Course') then l3.LongValue else '' end,s.WithdrawalDate
 ,Zoning=l4.LongValue,s.Area
 From OrLCBR.dbo.CondoTownHouse s
  left join OrLCBR.dbo.Lookups l1
    on s.BACommType = l1.[Value] and l1.[Lookup] = '20160201102310277691000000'
  left join OrLCBR.dbo.Lookups l2
    on s.Subdivision = l2.[Value] and l2.[Lookup] = '19991209173913843279000000'
  left join OrLCBR.dbo.Lookups l3
    on s.Frontage = l3.[Value] and l3.[Lookup] = '19991209173913883242000000'
  left join OrLCBR.dbo.Lookups l4
    on s.Zoning = l4.[Value] and l4.[Lookup] = '19991209173913886771000000'

Insert Into mls.Listing
([MLSID],[PostalCodeExt],[PostalCode],[CDOM],[CloseDate],[ClosePrice],[DOM],[Latitude],[Longitude],[StreetNumber],[BankOwned],[City]
,[JLsDom],[ParentPropTypeID],[PropTypeID],[ShortSale],[UpdateDate],[ListDate],[MLSDBListingModTime],[ListPrice],[MLSListingID],[Acreage]
,[TaxID],[PropertyTax],[StatusDate],[StreetDirPrefix],[StreetDirSuffix],[StreetName],[StreetTypeSuffix],[VirtualTour],[PendingDate]
,[OriginalListPrice],[HOADues],CancelDate,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,CommissionCode
,CommissionComments,Commission,Community,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice,FallThroughDate
,PictureCount,Waterfront,WithdrawDate,Zoning,Area)
 Select 575008,iif(LEN(s.postalcode)=10, right(s.postalcode,4), NULL),left(s.PostalCode,5),s.[CDOM],s.[CloseDate],s.[ClosePrice]
 ,s.[DOM],s.[GeoLatitude],s.[GeoLongitude],s.[HouseNumber],s.[JLSBankOwned],s.[JLSCity],s.[JlsDom],s.[JLSParentPropTypeID]
 ,s.[JLSPropTypeID],s.[JLSShortSale],s.[LastModifiedDateTime],s.[ListDate],s.[ListingModTime],s.[ListPrice],s.[MLSNumber],s.[NoOfAcres]
 ,s.[PropertyID],s.[PropertyTaxes],s.[StatusChangeDate],s.[StreetDirectionPfx],s.[StreetDirectionSfx],s.[StreetName],s.[StreetSfx]
 ,s.[UnBrandedVirtualTour],s.[UnderContractDate],Try_Convert(int,s.OriginalListPrice),Try_Convert(numeric,replace(s.AssocFees,'$',''))
 ,s.CancelDate,s.ListingAgentID,s.CoListAgentID,s.ListingOfficeShortID,s.CoListingOfficeShortID,BACommType=l1.LongValue,s.CommissionCode
 ,s.BAComm,SubDivision=l2.LongValue,s.SellingAgentID,s.CoSellingAgentID,s.SellingOfficeShortId,s.CoSellingOfficeShortId,s.FallThroughDate
 ,s.PictureCount,Frontage=case when l3.LongValue not in ('None','Highway','Golf Course') then l3.LongValue else '' end,s.WithdrawalDate
 ,Zoning=l4.LongValue,s.Area
  From OrLCBR.dbo.Land_Commercial s
   left join OrLCBR.dbo.Lookups l1
    on s.BACommType = l1.[Value] and l1.[Lookup] = '20160201102310277691000000'
  left join OrLCBR.dbo.Lookups l2
    on s.Subdivision = l2.[Value] and l2.[Lookup] = '19991209173913784829000000'
  left join OrLCBR.dbo.Lookups l3
    on s.Frontage = l3.[Value] and l3.[Lookup] = '19991209173913787037000000'
  left join OrLCBR.dbo.Lookups l4
    on s.Zoning = l4.[Value] and l4.[Lookup] = '19991209173913791101000000'

Insert Into mls.Listing 
([MLSID],[PostalCodeExt],[PostalCode],[CDOM],[CloseDate],[ClosePrice],[DOM],[Latitude],[Longitude],[StreetNumber],[BankOwned],[City]
,[JLsDom],[ParentPropTypeID],[PropTypeID],[ShortSale],[UpdateDate],[ListDate],[MLSDBListingModTime],[ListPrice],[MLSListingID]
,[Acreage],[TaxID],[PropertyTax],[StatusDate],[StreetDirPrefix],[StreetDirSuffix],[StreetName],[StreetTypeSuffix],[VirtualTour]
,[PendingDate],[OriginalListPrice],[HOADues],CancelDate,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID
,CommissionCode,CommissionComments,Commission,Community,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice
,FallThroughDate,PictureCount,Waterfront,WithdrawDate,Zoning,Area)
 Select 575008,iif(LEN(s.postalcode)=10, right(s.postalcode,4), NULL),left(s.PostalCode,5),s.[CDOM],s.[CloseDate],s.[ClosePrice]
 ,s.[DOM],s.[GeoLatitude],s.[GeoLongitude],s.[HouseNumber],s.[JLSBankOwned],s.[JLSCity],s.[JlsDom],s.[JLSParentPropTypeID]
 ,s.[JLSPropTypeID],s.[JLSShortSale],s.[LastModifiedDateTime],s.[ListDate],s.[ListingModTime],s.[ListPrice],s.[MLSNumber],s.[NoOfAcres]
 ,s.[PropertyID],s.[PropertyTaxes],s.[StatusChangeDate],s.[StreetDirectionPfx],s.[StreetDirectionSfx],s.[StreetName],s.[StreetSfx]
 ,s.[UnBrandedVirtualTour],s.[UnderContractDate],Try_Convert(int,s.OriginalListPrice),Try_Convert(numeric,replace(s.AssocFees,'$',''))
 ,s.CancelDate,s.ListingAgentID,s.CoListAgentID,s.ListingOfficeShortID,s.CoListingOfficeShortID,BACommType=l1.LongValue,s.CommissionCode
 ,s.BAComm,SubDivision=l2.LongValue,s.SellingAgentID,s.CoSellingAgentID,s.SellingOfficeShortId,s.CoSellingOfficeShortId,s.FallThroughDate
 ,s.PictureCount,Frontage=case when l3.LongValue not in ('None','Highway','Golf Course') then l3.LongValue else '' end,s.WithdrawalDate
 ,Zoning=l4.LongValue,s.Area
 From OrLCBR.dbo.Land_Residential s 
   left join OrLCBR.dbo.Lookups l1
    on s.BACommType = l1.[Value] and l1.[Lookup] = '20160201102310277691000000'
  left join OrLCBR.dbo.Lookups l2
    on s.Subdivision = l2.[Value] and l2.[Lookup] = '19991209173913607516000000'
  left join OrLCBR.dbo.Lookups l3
    on s.Frontage = l3.[Value] and l3.[Lookup] = '19991209173913609718000000'
  left join OrLCBR.dbo.Lookups l4
    on s.Zoning = l4.[Value] and l4.[Lookup] = '19991209173913650346000000'

Insert Into mls.Listing 
([MLSID],[PostalCodeExt],[PostalCode],[Bedrooms],[CDOM],[CloseDate],[ClosePrice],[DOM],[Latitude],[Longitude],[BathsPartial]
,[StreetNumber],[BankOwned],[City],[JLsDom],[Garage],[ParkingSpaces],[ParentPropTypeID],[PropTypeID],[ShortSale],[UpdateDate]
,[ListDate],[MLSDBListingModTime],[ListPrice],[MLSListingID],[Acreage],[TaxID],[PropertyTax],[StatusDate],[StreetDirPrefix]
,[StreetDirSuffix],[StreetName],[StreetTypeSuffix],[VirtualTour],[PendingDate],[YearBuilt],[BathsTotal],[OriginalListPrice],[HOADues]
,[SquareFootage],CancelDate,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,CommissionCode,CommissionComments
,Commission,Community,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice,FallThroughDate,PictureCount
,Waterfront,WithdrawDate,Zoning,Area)
 Select 575008,iif(LEN(s.postalcode)=10, right(s.postalcode,4), NULL),left(s.PostalCode,5),s.[BedroomsTotal],s.[CDOM],s.[CloseDate]
 ,s.[ClosePrice],s.[DOM],s.[GeoLatitude],s.[GeoLongitude],s.[HalfBathNoOfRooms],s.[HouseNumber],s.[JLSBankOwned],s.[JLSCity],s.[JlsDom]
 ,s.[JLSGarage],s.[JLSGarageSpaces],s.[JLSParentPropTypeID],s.[JLSPropTypeID],s.[JLSShortSale],s.[LastModifiedDateTime],s.[ListDate]
 ,s.[ListingModTime],s.[ListPrice],s.[MLSNumber],s.[NoOfAcres],s.[PropertyID],s.[PropertyTaxes],s.[StatusChangeDate]
 ,s.[StreetDirectionPfx],s.[StreetDirectionSfx],s.[StreetName],s.[StreetSfx],s.[UnBrandedVirtualTour],s.[UnderContractDate],s.[YearBuilt]
 ,TRY_CONVERT(DECIMAL(5,2),s.[BathroomsTotal]),Try_Convert(int,s.OriginalListPrice),Try_Convert(numeric,replace(s.AssocFees,'$',''))
 ,Try_Convert(numeric,s.TotalSqFt),s.CancelDate,s.ListingAgentID,s.CoListAgentID,s.ListingOfficeShortID,s.CoListingOfficeShortID
 ,BACommType=l1.LongValue,s.CommissionCode,s.BAComm,SubDivision=l2.LongValue,s.SellingAgentID,s.CoSellingAgentID,s.SellingOfficeShortId
 ,s.CoSellingOfficeShortId,s.FallThroughDate,s.PictureCount
 ,Frontage=case when l3.LongValue not in ('None','Highway','Golf Course') then l3.LongValue else '' end,s.WithdrawalDate
 ,Zoning=l4.LongValue,s.Area
 From OrLCBR.dbo.Manufactured s
   left join OrLCBR.dbo.Lookups l1
    on s.BACommType = l1.[Value] and l1.[Lookup] = '20160201102310277691000000'
  left join OrLCBR.dbo.Lookups l2
    on s.Subdivision = l2.[Value] and l2.[Lookup] = '19991209173913938865000000'
  left join OrLCBR.dbo.Lookups l3
    on s.Frontage = l3.[Value] and l3.[Lookup] = '19991209173913941024000000'
  left join OrLCBR.dbo.Lookups l4
    on s.Zoning = l4.[Value] and l4.[Lookup] = '19991209173913944967000000'

Insert Into mls.Listing 
([MLSID],[PostalCodeExt],[PostalCode],[Bedrooms],[CDOM],[CloseDate],[ClosePrice],[DOM],[Latitude],[Longitude],[StreetNumber],[BankOwned]
,[City],[JLsDom],[Garage],[ParkingSpaces],[ParentPropTypeID],[PropTypeID],[ShortSale],[UpdateDate],[ListDate],[MLSDBListingModTime]
,[ListPrice],[MLSListingID],[Acreage],[TaxID],[PropertyTax],[StatusDate],[StreetDirPrefix],[StreetDirSuffix],[StreetName]
,[StreetTypeSuffix],[VirtualTour],[PendingDate],[YearBuilt],[BathsTotal],[OriginalListPrice],[HOADues],[SquareFootage],CancelDate
,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,CommissionCode,Commission,Community
,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice,FallThroughDate,PictureCount,Waterfront,WithdrawDate
,Zoning,Area)
 Select 575008,iif(LEN(s.postalcode)=10, right(s.postalcode,4), NULL),left(s.PostalCode,5),s.[BedroomsTotal],s.[CDOM],s.[CloseDate]
 ,s.[ClosePrice],s.[DOM],s.[GeoLatitude],s.[GeoLongitude],s.[HouseNumber],s.[JLSBankOwned],s.[JLSCity],s.[JlsDom],s.[JLSGarage]
 ,s.[JLSGarageSpaces],s.[JLSParentPropTypeID],s.[JLSPropTypeID],s.[JLSShortSale],s.[LastModifiedDateTime],s.[ListDate],s.[ListingModTime]
 ,s.[ListPrice],s.[MLSNumber],s.[NoOfAcres],s.[PropertyID],s.[PropertyTaxes],s.[StatusChangeDate],s.[StreetDirectionPfx]
 ,s.[StreetDirectionSfx],s.[StreetName],s.[StreetSfx],s.[UnBrandedVirtualTour],s.[UnderContractDate],s.[YearBuilt]
 ,TRY_CONVERT(DECIMAL(5,2),s.[BathroomsTotal]),Try_Convert(int,s.OriginalListPrice),Try_Convert(numeric,replace(s.AssocFees,'$',''))
 ,Try_Convert(numeric,s.TotalSqFt),s.CancelDate,s.ListingAgentID,s.CoListAgentID,s.ListingOfficeShortID,s.CoListingOfficeShortID
 ,BACommType=l1.LongValue,s.BAComm,SubDivision=l2.LongValue,s.SellingAgentID,s.CoSellingAgentID,s.SellingOfficeShortId
 ,s.CoSellingOfficeShortId,s.FallThroughDate,s.PictureCount,
 Frontage=case when l3.LongValue not in ('None','Highway','Golf Course') then l3.LongValue else '' end,s.WithdrawalDate
 ,Zoning=l4.LongValue,s.Area
 From OrLCBR.dbo.ManufacturedOnly s
   left join OrLCBR.dbo.Lookups l1
    on s.BACommType = l1.[Value] and l1.[Lookup] = '20160201102310277691000000'
  left join OrLCBR.dbo.Lookups l2
    on s.Subdivision = l2.[Value] and l2.[Lookup] = '19991209173914092186000000'
  left join OrLCBR.dbo.Lookups l3
    on s.Frontage = l3.[Value] and l3.[Lookup] = '19991209173914094425000000'
  left join OrLCBR.dbo.Lookups l4
    on s.Zoning = l4.[Value] and l4.[Lookup] = '19991209173914098150000000'

Insert Into mls.Listing 
([MLSID],[PostalCodeExt],[PostalCode],[Bedrooms],[CDOM],[CloseDate],[ClosePrice],[DOM],[Latitude],[Longitude],[StreetNumber],[BankOwned]
,[City],[JLsDom],[Garage],[ParkingSpaces],[ParentPropTypeID],[PropTypeID],[ShortSale],[UpdateDate],[ListDate],[MLSDBListingModTime]
,[ListPrice],[MLSListingID],[Acreage],[TaxID],[PropertyTax],[StatusDate],[StreetDirPrefix],[StreetDirSuffix],[StreetName]
,[StreetTypeSuffix],[VirtualTour],[PendingDate],[YearBuilt],[BathsTotal],[OriginalListPrice],[HOADues],[SquareFootage],CancelDate
,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,CommissionCode,CommissionComments,Commission,Community
,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice,FallThroughDate,PictureCount,Waterfront,WithdrawDate
,Zoning,Area)
 Select 575008,iif(LEN(s.postalcode)=10, right(s.postalcode,4), NULL),left(s.PostalCode,5),s.[BedroomsTotal],s.[CDOM],s.[CloseDate]
 ,s.[ClosePrice],s.[DOM],s.[GeoLatitude],s.[GeoLongitude],s.[HouseNumber],s.[JLSBankOwned],s.[JLSCity],s.[JlsDom],s.[JLSGarage]
 ,s.[JLSGarageSpaces],s.[JLSParentPropTypeID],s.[JLSPropTypeID],s.[JLSShortSale],s.[LastModifiedDateTime],s.[ListDate],s.[ListingModTime]
 ,s.[ListPrice],s.[MLSNumber],s.[NoOfAcres],s.[PropertyID],s.[PropertyTaxes],s.[StatusChangeDate],s.[StreetDirectionPfx]
 ,s.[StreetDirectionSfx],s.[StreetName],s.[StreetSfx],s.[UnBrandedVirtualTour],s.[UnderContractDate],s.[YearBuilt]
 ,TRY_CONVERT(DECIMAL(5,2),s.[BathroomsTotal]),Try_Convert(int,s.OriginalListPrice),Try_Convert(numeric,replace(s.AssocFees,'$',''))
 ,Try_Convert(numeric,s.TotalSqFt),s.CancelDate,s.ListingAgentID,s.CoListAgentID,s.ListingOfficeShortID,s.CoListingOfficeShortID
 ,BACommType=l1.LongValue,s.CommissionCode,s.BAComm,SubDivision=l2.LongValue,s.SellingAgentID,s.CoSellingAgentID,s.SellingOfficeShortId
 ,s.CoSellingOfficeShortId,s.FallThroughDate,s.PictureCount
 ,Frontage=case when l3.LongValue not in ('None','Highway','Golf Course') then l3.LongValue else '' end,s.WithdrawalDate
 ,Zoning=l4.LongValue,s.Area
 From OrLCBR.dbo.MultiFamily s
   left join OrLCBR.dbo.Lookups l1
    on s.BACommType = l1.[Value] and l1.[Lookup] = '20160201102310277691000000'
  left join OrLCBR.dbo.Lookups l2
    on s.Subdivision = l2.[Value] and l2.[Lookup] = '19991209173913549699000000'
  left join OrLCBR.dbo.Lookups l3
    on s.Frontage = l3.[Value] and l3.[Lookup] = '19991209173913551876000000'
  left join OrLCBR.dbo.Lookups l4
    on s.Zoning = l4.[Value] and l4.[Lookup] = '19991209173913555652000000'

Insert Into mls.Listing 
([MLSID],[PostalCodeExt],[PostalCode],[Bedrooms],[CDOM],[CloseDate],[ClosePrice],[DOM],[Latitude],[Longitude],[StreetNumber],[BankOwned]
,[City],[JLsDom],[Garage],[ParkingSpaces],[ParentPropTypeID],[PropTypeID],[ShortSale],[UpdateDate],[ListDate],[MLSDBListingModTime]
,[ListPrice],[MLSListingID],[Acreage],[TaxID],[PropertyTax],[StatusDate],[StreetDirPrefix],[StreetDirSuffix],[StreetName]
,[StreetTypeSuffix],[VirtualTour],[PendingDate],[YearBuilt],[BathsTotal],[OriginalListPrice],[HOADues],[SquareFootage],CancelDate
,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,CommissionCode,CommissionComments,Commission,Community
,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice,FallThroughDate,PictureCount,Waterfront,WithdrawDate
,Zoning,Area)
 Select 575008,iif(LEN(s.postalcode)=10, right(s.postalcode,4), NULL),left(s.PostalCode,5),s.[BedroomsTotal],s.[CDOM],s.[CloseDate]
 ,s.[ClosePrice],s.[DOM],s.[GeoLatitude],s.[GeoLongitude],s.[HouseNumber],s.[JLSBankOwned],s.[JLSCity],s.[JlsDom],s.[JLSGarage]
 ,s.[JLSGarageSpaces],s.[JLSParentPropTypeID],s.[JLSPropTypeID],s.[JLSShortSale],s.[LastModifiedDateTime],s.[ListDate],s.[ListingModTime]
 ,s.[ListPrice],s.[MLSNumber],s.[NoOfAcres],s.[PropertyID],s.[PropertyTaxes],s.[StatusChangeDate],s.[StreetDirectionPfx]
 ,s.[StreetDirectionSfx],s.[StreetName],s.[StreetSfx],s.[UnBrandedVirtualTour],s.[UnderContractDate],s.[YearBuilt]
 ,TRY_CONVERT(DECIMAL(5,2),s.[BathroomsTotal]),Try_Convert(int,s.OriginalListPrice),Try_Convert(numeric,replace(s.AssocFees,'$',''))
 ,Try_Convert(numeric,s.TotalSqFt),s.CancelDate,s.ListingAgentID,s.CoListAgentID,s.ListingOfficeShortID,s.CoListingOfficeShortID
 ,BACommType=l1.LongValue,s.CommissionCode,s.BAComm,SubDivision=l2.LongValue,s.SellingAgentID,s.CoSellingAgentID,s.SellingOfficeShortId
 ,s.CoSellingOfficeShortId,s.FallThroughDate,s.PictureCount
 ,Frontage=case when l3.LongValue not in ('None','Highway','Golf Course') then l3.LongValue else '' end,s.WithdrawalDate
 ,Zoning=l4.LongValue,s.Area
 From OrLCBR.dbo.Residential s
  left join OrLCBR.dbo.Lookups l1
    on s.BACommType = l1.[Value] and l1.[Lookup] = '20160201102310277691000000'
  left join OrLCBR.dbo.Lookups l2
    on s.Subdivision = l2.[Value] and l2.[Lookup] = '19991209173913456686000000'
  left join OrLCBR.dbo.Lookups l3
    on s.Frontage = l3.[Value] and l3.[Lookup] = '19991209173913495390000000'
  left join OrLCBR.dbo.Lookups l4
    on s.Zoning = l4.[Value] and l4.[Lookup] = '19991209173913498989000000'

Insert Into mls.Listing
([MLSID],[PostalCodeExt],[PostalCode],[Bedrooms],[CDOM],[CloseDate],[ClosePrice],[DOM],[Latitude],[Longitude],[StreetNumber],[BankOwned]
,[City],[JLsDom],[Garage],[ParkingSpaces],[ParentPropTypeID],[PropTypeID],[ShortSale],[UpdateDate],[ListDate],[MLSDBListingModTime]
,[ListPrice],[MLSListingID],[Acreage],[TaxID],[PropertyTax],[StatusDate],[StreetDirPrefix],[StreetDirSuffix],[StreetName]
,[StreetTypeSuffix],[VirtualTour],[PendingDate],[YearBuilt],[BathsTotal],[OriginalListPrice],[HOADues],[SquareFootage],CancelDate
,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,CommissionCode,CommissionComments,Commission,Community
,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice,FallThroughDate,PictureCount,Waterfront,WithdrawDate
,Zoning,Area)
 Select 575008,iif(LEN(s.postalcode)=10, right(s.postalcode,4), NULL),left(s.PostalCode,5),s.[BedroomsTotal],s.[CDOM],s.[CloseDate]
 ,s.[ClosePrice],s.[DOM],s.[GeoLatitude],s.[GeoLongitude],s.[HouseNumber],s.[JLSBankOwned],s.[JLSCity],s.[JlsDom],s.[JLSGarage]
 ,s.[JLSGarageSpaces],s.[JLSParentPropTypeID],s.[JLSPropTypeID],s.[JLSShortSale],s.[LastModifiedDateTime],s.[ListDate]
 ,s.[ListingModTime],s.[ListPrice],s.[MLSNumber],s.[NoOfAcres],s.[PropertyID],s.[PropertyTaxes],s.[StatusChangeDate]
 ,s.[StreetDirectionPfx],s.[StreetDirectionSfx],s.[StreetName],s.[StreetSfx],s.[UnBrandedVirtualTour],s.[UnderContractDate],s.[YearBuilt]
 ,TRY_CONVERT(DECIMAL(5,2),s.[BathroomsTotal]),Try_Convert(int,s.OriginalListPrice),Try_Convert(numeric,replace(s.AssocFees,'$',''))
 ,Try_Convert(numeric,s.TotalSqFt),s.CancelDate,s.ListingAgentID,s.CoListAgentID,s.ListingOfficeShortID,s.CoListingOfficeShortID
 ,BACommType=l1.LongValue,s.CommissionCode,s.BAComm,SubDivision=l2.LongValue,s.SellingAgentID,s.CoSellingAgentID,s.SellingOfficeShortId
 ,s.CoSellingOfficeShortId,s.FallThroughDate,s.PictureCount
 ,Frontage=case when l3.LongValue not in ('None','Highway','Golf Course') then l3.LongValue else '' end,s.WithdrawalDate
 ,Zoning=l4.LongValue,s.Area
 From OrLCBR.dbo.SharedOwnership s
   left join OrLCBR.dbo.Lookups l1
    on s.BACommType = l1.[Value] and l1.[Lookup] = '20160201102310277691000000'
  left join OrLCBR.dbo.Lookups l2
    on s.Subdivision = l2.[Value] and l2.[Lookup] = '19991209173914150838000000'
  left join OrLCBR.dbo.Lookups l3
    on s.Frontage = l3.[Value] and l3.[Lookup] = '19991209173914153080000000'
  left join OrLCBR.dbo.Lookups l4
    on s.Zoning = l4.[Value] and l4.[Lookup] = '19991209173914156979000000'


 --OrMLSCO
 Insert Into mls.Listing 
 ([MLSID],[Latitude],[Longitude],[ListDate],[PendingDate],[CloseDate],[Acreage],[City],[DOM],[JLsDom],[BankOwned],[ParentPropTypeID]
 ,[PropTypeID],[ShortSale],[StreetAddress],[UpdateDate],[ListingAddTime],[MLSDBListingModTime],[ListPrice],[MLSListingID],[ParkingSpaces]
 ,[ClosePrice],[StatusDate],[StreetDirPrefix],[StreetName],[StreetNumber],[StreetNumberModifier],[StreetTypeSuffix],[VirtualTour]
 ,[YearBuilt],[PostalCode],ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,SellingAgentID,CoSellingAgentID,SellingOffice
 ,CoSellingOffice,LeaseDate,PictureCount,WithdrawDate,Zoning,Area)
 Select 124,IIF(IsNull(s.Longitude,0) > -110,null,s.Latitude),IIF(IsNull(s.Longitude,0) > -110,null,s.Longitude)
 ,NullIf(s.ListDate,'1800-01-01 00:00:00.000'),NullIf(s.PendingDate,'1800-01-01 00:00:00.000')
 ,NullIf(s.SellDate,'1800-01-01 00:00:00.000'),s.[Acres],s.[City],s.[DaysOnMkt],s.[DaysOnMkt],s.[JLSBankOwned],s.[JLSParentPropTypeID]
 ,s.[JLSPropTypeID],s.[JLSShortSale],s.[JLSStreetAddress],s.[LastModDateTime],s.[ListingAddTime],s.[ListingModTime],s.[ListPrice]
 ,s.[MLSNumber],s.[NumOfParkingSpaces],s.[SellPrice],s.[StatusDate],s.[StreetDirection],s.[StreetName],s.[StreetNum],s.[StreetNumModifier]
 ,s.[StreetSuffix],s.[VirtualTourURL],s.[YearBuilt],s.[ZIP],s.ListAgentNum,s.ListCoAgentNum,s.ListOfficeNum,s.ListCoOfficeNum
 ,s.SellAgentNum,s.SellCoAgentNum,s.SellOfficeNum,s.SellCoOfficeNum,s.LeaseStartDate,s.PictureCount,s.InactiveDate,s.Zoning,s.Area
 From OrMLSCO.dbo.Business s

 Insert Into mls.Listing
 ([MLSID],[Latitude],[Longitude],[ListDate],[PendingDate],[CloseDate],[Acreage],[City],[DOM],[JLsDom],[BankOwned],[ParentPropTypeID]
 ,[PropTypeID],[ShortSale],[StreetAddress],[UpdateDate],[ListingAddTime],[MLSDBListingModTime],[ListPrice],[LotSizeSF],[MLSListingID]
 ,[ParkingSpaces],[ClosePrice],[SquareFootage],[StatusDate],[StreetDirPrefix],[StreetName],[StreetNumber],[StreetNumberModifier]
 ,[StreetTypeSuffix],[PropertyTax],[VirtualTour],[YearBuilt],[PostalCode],ListAgentID,CoListAgentID,ListingOfficeID
 ,CoListingOfficeID,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice,EstCompletionDate,NewConstruction
 ,PictureCount,Area)
 Select 124,IIF(IsNull(s.Longitude,0) > -110,null,s.Latitude),IIF(IsNull(s.Longitude,0) > -110,null,s.Longitude)
 ,NullIf(s.ListDate,'1800-01-01 00:00:00.000'),NullIf(s.PendingDate,'1800-01-01 00:00:00.000')
 ,NullIf(s.SellDate,'1800-01-01 00:00:00.000'),s.[Acres],s.[City],s.[DaysOnMkt],s.[DaysOnMkt],s.[JLSBankOwned],s.[JLSParentPropTypeID]
 ,s.[JLSPropTypeID],s.[JLSShortSale],s.[JLSStreetAddress],s.[LastModDateTime],s.[ListingAddTime],s.[ListingModTime],s.[ListPrice]
 ,s.[LotSqFt],s.[MLSNumber],s.[NumOfParkingSpaces],s.[SellPrice],s.[SqFt],s.[StatusDate],s.[StreetDirection],s.[StreetName]
 ,s.[StreetNum],s.[StreetNumModifier],s.[StreetSuffix],s.[Taxes],s.[VirtualTourURL],s.[YearBuilt],s.[ZIP],s.ListAgentNum
 ,s.ListCoAgentNum,s.ListOfficeNum,s.ListCoOfficeNum,s.SellAgentNum,s.SellCoAgentNum,s.SellOfficeNum,s.SellCoOfficeNum
 ,s.EstCompletionDate,s.NewConstruction,s.PictureCount,s.Area
 From OrMLSCO.dbo.Commercial s 

 Insert Into mls.Listing
 ([MLSID],[Latitude],[Longitude],[ListDate],[PendingDate],[CloseDate],[Acreage],[BathsTotal],[Bedrooms],[City],[DOM],[JLsDom],[BankOwned]
 ,[ElementarySchool],[Fireplaces],[Garage],[GarageSpaces],[HighSchool],[MiddleSchool],[ParentPropTypeID],[PropTypeID],[ShortSale]
 ,[StreetAddress],[UpdateDate],[ListingAddTime],[MLSDBListingModTime],[ListPrice],[LotSizeSF],[MLSListingID],[ClosePrice],[SquareFootage]
 ,[StatusDate],[StreetDirPrefix],[StreetName],[StreetNumber],[StreetNumberModifier],[StreetTypeSuffix],[PropertyTax],[VirtualTour]
 ,[YearBuilt],[PostalCode],ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,SellingAgentID
 ,CoSellingAgentID,SellingOffice,CoSellingOffice,PictureCount,WithdrawDate,Zoning,Area)
 Select 124,IIF(IsNull(s.Longitude,0) > -110,null,s.Latitude),IIF(IsNull(s.Longitude,0) > -110,null,s.Longitude)
 ,NullIf(s.ListDate,'1800-01-01 00:00:00.000'),NullIf(s.PendingDate,'1800-01-01 00:00:00.000')
 ,NullIf(s.SellDate,'1800-01-01 00:00:00.000'),s.[Acres],s.[Bathrooms],s.[Bedrooms],s.[City],s.[DaysOnMkt],s.[DaysOnMkt],s.[JLSBankOwned]
 ,s.[JLSElementarySchool],s.[JLSFireplaces],s.[JLSGarage],s.[JLSGarageSpaces],s.[JLSHighSchool],s.[JLSMiddleSchool],s.[JLSParentPropTypeID]
 ,s.[JLSPropTypeID],s.[JLSShortSale],s.[JLSStreetAddress],s.[LastModDateTime],s.[ListingAddTime],s.[ListingModTime],s.[ListPrice]
 ,s.[LotSqFt],s.[MLSNumber],s.[SellPrice],s.[SqFt],s.[StatusDate],s.[StreetDirection],s.[StreetName],s.[StreetNum],s.[StreetNumModifier]
 ,s.[StreetSuffix],s.[Taxes],s.[VirtualTourURL],s.[YearBuilt],s.[ZIP],s.ListAgentNum,s.ListCoAgentNum,s.ListOfficeNum
 ,s.ListCoOfficeNum,s.SellAgentNum,s.SellCoAgentNum,s.SellOfficeNum,s.SellCoOfficeNum,s.PictureCount,s.InactiveDate,s.Zoning,s.Area
 From OrMLSCO.dbo.Farm s

 Insert Into mls.Listing 
 ([MLSID],[Latitude],[Longitude],[ListDate],[PendingDate],[CloseDate],[Acreage],[City],[DOM],[JLsDom],[HOADues],[BankOwned]
 ,[ElementarySchool],[HighSchool],[MiddleSchool],[ParentPropTypeID],[PropTypeID],[ShortSale],[StreetAddress],[UpdateDate]
 ,[ListingAddTime],[MLSDBListingModTime],[ListPrice],[LotSizeSF],[MLSListingID],[ClosePrice],[StatusDate],[StreetDirPrefix],[StreetName]
 ,[StreetNumber],[StreetNumberModifier],[StreetTypeSuffix],[PropertyTax],[VirtualTour],[PostalCode],Age,ListAgentID
 ,CoListAgentID,ListingOfficeID,CoListingOfficeID,Community,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice
 ,PictureCount,WithdrawDate,Zoning,Area)
 Select 124,IIF(IsNull(s.Longitude,0) > -110,null,s.Latitude),IIF(IsNull(s.Longitude,0) > -110,null,s.Longitude)
 ,NullIf(s.ListDate,'1800-01-01 00:00:00.000'),NullIf(s.PendingDate,'1800-01-01 00:00:00.000')
 ,NullIf(s.SellDate,'1800-01-01 00:00:00.000'),s.[Acres],s.[City],s.[DaysOnMkt],s.[DaysOnMkt],s.[HOATotalAmt],s.[JLSBankOwned]
 ,s.[JLSElementarySchool],s.[JLSHighSchool],s.[JLSMiddleSchool],s.[JLSParentPropTypeID],s.[JLSPropTypeID],s.[JLSShortSale]
 ,s.[JLSStreetAddress],s.[LastModDateTime],s.[ListingAddTime],s.[ListingModTime],s.[ListPrice],s.[LotSqFt],s.[MLSNumber],s.[SellPrice]
 ,s.[StatusDate],s.[StreetDirection],s.[StreetName],s.[StreetNum],s.[StreetNumModifier],s.[StreetSuffix],s.[Taxes],s.[VirtualTourURL]
 ,s.[ZIP],s.Age,s.ListAgentNum,s.ListCoAgentNum,s.ListOfficeNum,s.ListCoOfficeNum,s.Community,s.SellAgentNum,s.SellCoAgentNum
 ,s.SellOfficeNum,s.SellCoOfficeNum,s.PictureCount,s.InactiveDate,s.Zoning,s.Area
 From OrMLSCO.dbo.Land s 

Insert Into mls.Listing 
([MLSID],[Latitude],[Longitude],[ListDate],[PendingDate],[CloseDate],[Acreage],[City],[BankOwned],[ParentPropTypeID],[PropTypeID]
,[ShortSale],[StreetAddress],[UpdateDate],[ListingAddTime],[MLSDBListingModTime],[ListPrice],[LotSizeSF],[MLSListingID],[ParkingSpaces]
,[ClosePrice],[StatusDate],[StreetDirPrefix],[StreetName],[StreetNumber],[StreetNumberModifier],[StreetTypeSuffix],[TaxID],[VirtualTour]
,[YearBuilt],[PostalCode],ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,SellingAgentID
,CoSellingAgentID,SellingOffice,CoSellingOffice,EstCompletionDate,NewConstruction,PictureCount,Zoning,Area)
 Select 124,IIF(IsNull(s.Longitude,0) > -110,null,s.Latitude),IIF(IsNull(s.Longitude,0) > -110,null,s.Longitude)
 ,NullIf(s.ListDate,'1800-01-01 00:00:00.000'),NullIf(s.PendingDate,'1800-01-01 00:00:00.000')
 ,NullIf(s.SellDate,'1800-01-01 00:00:00.000'),s.[Acres],s.[City],s.[JLSBankOwned],s.[JLSParentPropTypeID],s.[JLSPropTypeID]
 ,s.[JLSShortSale],s.[JLSStreetAddress],s.[LastModDateTime],s.[ListingAddTime],s.[ListingModTime],s.[ListPrice],s.[LotSqFt]
 ,s.[MLSNumber],s.[NumOfParkingSpaces],s.[SellPrice],s.[StatusDate],s.[StreetDirection],s.[StreetName],s.[StreetNum]
 ,s.[StreetNumModifier],s.[StreetSuffix],s.[TaxID],s.[VirtualTourURL],s.[YearBuilt],s.[ZIP],s.ListAgentNum
 ,s.ListCoAgentNum,s.ListOfficeNum,s.ListCoOfficeNum,s.SellAgentNum,s.SellCoAgentNum,s.SellOfficeNum,s.SellCoOfficeNum
 ,s.EstCompletionDate,s.NewConstruction,s.PictureCount,s.Zoning,s.Area
 From OrMLSCO.dbo.Lease s

Insert Into mls.Listing 
([MLSID],[Latitude],[Longitude],[ListDate],[PendingDate],[CloseDate],[Acreage],[BathsTotal],[Bedrooms],[City],[DOM],[JLsDom],[BankOwned]
,[Fireplaces],[Garage],[GarageSpaces],[ParentPropTypeID],[PropTypeID],[ShortSale],[StreetAddress],[UpdateDate],[ListingAddTime]
,[MLSDBListingModTime],[ListPrice],[LotSizeSF],[MLSListingID],[ClosePrice],[SquareFootage],[StatusDate],[StreetDirPrefix],[StreetName]
,[StreetNumber],[StreetNumberModifier],[StreetTypeSuffix],[PropertyTax],[TaxID],[VirtualTour],[YearBuilt],[PostalCode],Age,BuilderName
,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,Community,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice
,EstCompletionDate,NewConstruction,PictureCount,WithdrawDate,Zoning,Area)
 Select 124,IIF(IsNull(s.Longitude,0) > -110,null,s.Latitude),IIF(IsNull(s.Longitude,0) > -110,null,s.Longitude)
 ,NullIf(s.ListDate,'1800-01-01 00:00:00.000'),NullIf(s.PendingDate,'1800-01-01 00:00:00.000')
 ,NullIf(s.SellDate,'1800-01-01 00:00:00.000'),s.[Acres],s.[Bathrooms],s.[Bedrooms],s.[City],s.[DaysOnMkt],s.[DaysOnMkt],s.[JLSBankOwned]
 ,s.[JLSFireplaces],s.[JLSGarage],s.[JLSGarageSpaces],s.[JLSParentPropTypeID],s.[JLSPropTypeID],s.[JLSShortSale],s.[JLSStreetAddress]
 ,s.[LastModDateTime],s.[ListingAddTime],s.[ListingModTime],s.[ListPrice],s.[LotSqFt],s.[MLSNumber],s.[SellPrice],s.[SqFt],s.[StatusDate]
 ,s.[StreetDirection],s.[StreetName],s.[StreetNum],s.[StreetNumModifier],s.[StreetSuffix],s.[Taxes],s.[TaxID],s.[VirtualTourURL]
 ,s.[YearBuilt],s.[ZIP],s.Age,s.Builder,s.ListAgentNum,s.ListCoAgentNum,s.ListOfficeNum,s.ListCoOfficeNum,s.Community,s.SellAgentNum
 ,s.SellCoAgentNum,s.SellOfficeNum,s.SellCoOfficeNum,s.EstCompletionDate,s.NewConstruction,s.PictureCount,s.InactiveDate
 ,s.Zoning,s.Area
 From OrMLSCO.dbo.MultiFamily s

Insert Into mls.Listing 
([MLSID],[Latitude],[Longitude],[ListDate],[PendingDate],[CloseDate],[Acreage],[BathsTotal],[Bedrooms],[City],[DOM],[JLsDom],[HOADues]
,[BankOwned],[ElementarySchool],[Fireplaces],[Garage],[GarageSpaces],[HighSchool],[MiddleSchool],[ParentPropTypeID],[PropTypeID]
,[ShortSale],[StreetAddress],[UpdateDate],[ListingAddTime],[MLSDBListingModTime],[ListPrice],[LotSizeSF],[MLSListingID],[ClosePrice]
,[SquareFootage],[StatusDate],[StreetDirPrefix],[StreetName],[StreetNumber],[StreetNumberModifier],[StreetTypeSuffix],[PropertyTax]
,[TaxID],[VirtualTour],[YearBuilt],[PostalCode],Age,BuilderName,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,Community
,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice,EstCompletionDate,NewConstruction,PictureCount,WithdrawDate
,Zoning,Area)
 Select 124,IIF(IsNull(s.Longitude,0) > -110,null,s.Latitude),IIF(IsNull(s.Longitude,0) > -110,null,s.Longitude)
 ,NullIf(s.ListDate,'1800-01-01 00:00:00.000'),NullIf(s.PendingDate,'1800-01-01 00:00:00.000')
 ,NullIf(s.SellDate,'1800-01-01 00:00:00.000'),s.[Acres],s.[Bathrooms],s.[Bedrooms],s.[City],s.[DaysOnMkt],s.[DaysOnMkt],s.[HOATotalAmt]
 ,s.[JLSBankOwned],s.[JLSElementarySchool],s.[JLSFireplaces],s.[JLSGarage],s.[JLSGarageSpaces],s.[JLSHighSchool],s.[JLSMiddleSchool]
 ,s.[JLSParentPropTypeID],s.[JLSPropTypeID],s.[JLSShortSale],s.[JLSStreetAddress],s.[LastModDateTime],s.[ListingAddTime]
 ,s.[ListingModTime],s.[ListPrice],s.[LotSqFt],s.[MLSNumber],s.[SellPrice],s.[SqFt],s.[StatusDate],s.[StreetDirection],s.[StreetName]
 ,s.[StreetNum],s.[StreetNumModifier],s.[StreetSuffix],s.[Taxes],s.[TaxID],s.[VirtualTourURL],s.[YearBuilt],s.[ZIP],s.Age,s.Builder
 ,s.ListAgentNum,s.ListCoAgentNum,s.ListOfficeNum,s.ListCoOfficeNum,s.Community,s.SellAgentNum,s.SellCoAgentNum,s.SellOfficeNum
 ,s.SellCoOfficeNum,s.EstCompletionDate,s.NewConstruction,s.PictureCount,s.InactiveDate,s.Zoning,s.Area
 From OrMLSCO.dbo.Residential s


 --OrRMLS
 Insert Into mls.Listing
([MLSID],[ShowAddress],[IDX],[JLsDom],[ListPrice],[Acreage],[BathsFull],[BathsPartial],[BathsTotal],[Bedrooms],[CDOM],[CloseDate]
,[ListDate],[PendingDate],[StatusDate],[UpdateDate],[DOM],[Fireplaces],[StreetAddress],[ParkingSpaces],[HOADues],[City],[Garage]
,[PropTypeID],[Latitude],[MLSDBListingModTime],[Longitude],[MLSListingID],[ClosePrice],[OriginalListPrice],[ElementarySchool],[HighSchool]
,[MiddleSchool],[SquareFootage],[StreetDirPrefix],[StreetDirSuffix],[StreetName],[StreetNumber],[StreetTypeSuffix],[PropertyTax],[TaxID]
,[TaxYearBuilt],[VirtualTour],[VideoTour],[YearBuilt],[PostalCode],[PostalCodeExt],Auction,BodyofWaterName,CancelDate,ListAgentID
,CoListAgentID,ListingOfficeID,CoListingOfficeID,CommissionCode,Commission,Community,SellingAgentID,SellingOffice,ExpirationDate
,PictureCount,WithdrawDate,Zoning,Area)
 Select 91,IIF(InternetAddressDisplayYN='N',0,1),IIF(InternetEntireListingDisplayYN='N',0,1)
 ,IIF(s.CumulativeDaysOnMarket is null,DateDiff(day,s.DateList,getdate()),s.CumulativeDaysOnMarket)
 ,IIF(s.PriceType='Range Price',s.PriceMaximum,s.PriceList),s.[Acres],s.[BathsFull],s.[BathsPartial],s.[BathsTotal],s.[Beds]
 ,s.[CumulativeDaysOnMarket],s.[DateClose],s.[DateList],s.[DatePending],s.[DateStatusChange],s.[DateTimeModified],s.[DaysOnMarket]
 ,s.[FireplacesTotal],s.[FullStreetAddress],s.[GarageOrParkingSpaces],s.[HOAFee],s.[JLSCity],s.[JLSGarage],s.[JLSPropTypeID],s.[Latitude]
 ,s.[ListingModTime],s.[Longitude],s.[MLSNumber],s.[PriceClose],s.[PriceListOriginal],s.[SchoolElementary],s.[SchoolHigh],s.[SchoolMiddle]
 ,s.[SqFtApproximateTotal],s.[StreetDirPrefix],s.[StreetDirSuffix],s.[StreetName],s.[StreetNumber],s.[StreetTypeSuffix],s.[TaxAmount]
 ,s.[TaxID],s.[TaxYear],s.[VideoVirtualTour1URL],s.[VideoVirtualTour2URL],s.[YearBuilt],s.[ZipCode],s.[ZipCodePlus4]
 ,Auction = case when s.AuctionType is not null then 'Y' else 'N' end,s.WaterName,s.DateCanceled,s.ListAgentID,s.CoListAgentID,s.ListOfficeID
 ,s.CoListOfficeID,s.BuyerAgencyCompensationType,IsNull(Try_Convert(decimal(19,2),s.BuyerAgencyCompensation),0.00),s.Subdivision,s.SaleAgentID
 ,s.SaleOfficeID,s.DateExpiration,s.PhotosCount,s.DateWithdrawn,s.Zoning,s.Area
 From OrRMLS.dbo.Residential s

 Insert Into mls.Listing
([MLSID],[ShowAddress],[IDX],[JLsDom],[ListPrice],[CDOM],[CloseDate],[ListDate],[PendingDate],[StatusDate],[UpdateDate],[DOM]
,[StreetAddress],[ParkingSpaces],[City],[PropTypeID],[Latitude],[MLSDBListingModTime],[Longitude],[MLSListingID],[ClosePrice]
,[OriginalListPrice],[ElementarySchool],[HighSchool],[MiddleSchool],[StreetDirPrefix],[StreetDirSuffix],[StreetName],[StreetNumber]
,[StreetTypeSuffix],[PropertyTax],[TaxID],[TaxYearBuilt],[VirtualTour],[VideoTour],[YearBuilt],[PostalCode],[PostalCodeExt],Auction
,BodyofWaterName,CancelDate,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,CommissionCode,Commission,Community
,SellingAgentID,SellingOffice,ExpirationDate,PictureCount,WithdrawDate,Zoning,Area)
 Select 91,IIF(InternetAddressDisplayYN='N',0,1),IIF(InternetEntireListingDisplayYN='N',0,1)
 ,IIF(s.CumulativeDaysOnMarket is null,DateDiff(day,s.DateList,getdate()),s.CumulativeDaysOnMarket)
 ,IIF(s.PriceType='Range Price',s.PriceMaximum,s.PriceList),s.[CumulativeDaysOnMarket],s.[DateClose],s.[DateList],s.[DatePending]
 ,s.[DateStatusChange],s.[DateTimeModified],s.[DaysOnMarket],s.[FullStreetAddress],s.[GarageOrParkingSpaces],s.[JLSCity],s.[JLSPropTypeID]
 ,s.[Latitude],s.[ListingModTime],s.[Longitude],s.[MLSNumber],s.[PriceClose],s.[PriceListOriginal],s.[SchoolElementary],s.[SchoolHigh]
 ,s.[SchoolMiddle],s.[StreetDirPrefix],s.[StreetDirSuffix],s.[StreetName],s.[StreetNumber],s.[StreetTypeSuffix],s.[TaxAmount],s.[TaxID]
 ,s.[TaxYear],s.[VideoVirtualTour1URL],s.[VideoVirtualTour2URL],s.[YearBuilt],s.[ZipCode],s.[ZipCodePlus4]
 ,Auction = case when s.AuctionType is not null then 'Y' else 'N' end,s.WaterName,s.DateCanceled,s.ListAgentID,s.CoListAgentID,s.ListOfficeID
 ,s.CoListOfficeID,s.BuyerAgencyCompensationType,IsNull(Try_Convert(decimal(19,2),s.BuyerAgencyCompensation),0.00)
 ,s.Subdivision,s.SaleAgentID,s.SaleOfficeID,s.DateExpiration,s.PhotosCount,s.DateWithdrawn,s.Zoning,s.Area
 From OrRMLS.dbo.MultiFamily s

 Insert Into mls.Listing 
 ([MLSID],[ShowAddress],[IDX],[JLsDom],[ListPrice],[Acreage],[CDOM],[CloseDate],[ListDate],[PendingDate],[StatusDate],[UpdateDate],[DOM]
 ,[StreetAddress],[ParkingSpaces],[City],[PropTypeID],[Latitude],[MLSDBListingModTime],[Longitude],[MLSListingID],[ClosePrice]
 ,[OriginalListPrice],[StreetDirPrefix],[StreetDirSuffix],[StreetName],[StreetNumber],[StreetTypeSuffix],[PropertyTax],[TaxID]
 ,[TaxYearBuilt],[VirtualTour],[VideoTour],[YearBuilt],[PostalCode],[PostalCodeExt],Auction,BodyofWaterName,CancelDate,ListAgentID
 ,CoListAgentID,ListingOfficeID,CoListingOfficeID,CommissionCode,Commission,SellingAgentID,SellingOffice,ExpirationDate,PictureCount
 ,WithdrawDate,Zoning,Area)
 Select 91,IIF(InternetAddressDisplayYN='N',0,1),IIF(InternetEntireListingDisplayYN='N',0,1)
 ,IIF(s.CumulativeDaysOnMarket is null,DateDiff(day,s.DateList,getdate()),s.CumulativeDaysOnMarket)
 ,IIF(s.PriceType='Range Price',s.PriceMaximum,s.PriceList),s.[Acres],s.[CumulativeDaysOnMarket],s.[DateClose],s.[DateList]
 ,s.[DatePending],s.[DateStatusChange],s.[DateTimeModified],s.[DaysOnMarket],s.[FullStreetAddress],s.[GarageOrParkingSpaces],s.[JLSCity]
 ,s.[JLSPropTypeID],s.[Latitude],s.[ListingModTime],s.[Longitude],s.[MLSNumber],s.[PriceClose],s.[PriceListOriginal],s.[StreetDirPrefix]
 ,s.[StreetDirSuffix],s.[StreetName],s.[StreetNumber],s.[StreetTypeSuffix],s.[TaxAmount],s.[TaxID],s.[TaxYear],s.[VideoVirtualTour1URL]
 ,s.[VideoVirtualTour2URL],s.[YearBuilt],s.[ZipCode],s.[ZipCodePlus4],Auction = case when s.AuctionType is not null then 'Y' else 'N' end
 ,s.WaterName,s.DateCanceled,s.ListAgentID,s.CoListAgentID,s.ListOfficeID,s.CoListOfficeID,s.BuyerAgencyCompensationType
 ,IsNull(Try_Convert(decimal(19,2),s.BuyerAgencyCompensation),0.00),s.SaleAgentID,s.SaleOfficeID,s.DateExpiration,s.PhotosCount
 ,s.DateWithdrawn,s.Zoning,s.Area
 From OrRMLS.dbo.Commercial s

Insert Into mls.Listing 
([MLSID],[ShowAddress],[IDX],[JLsDom],[ListPrice],[Acreage],[CDOM],[CloseDate],[ListDate],[PendingDate],[StatusDate],[UpdateDate],[DOM]
,[StreetAddress],[HOADues],[City],[PropTypeID],[Latitude],[MLSDBListingModTime],[Longitude],[MLSListingID],[ClosePrice]
,[OriginalListPrice],[ElementarySchool],[HighSchool],[MiddleSchool],[StreetDirPrefix],[StreetDirSuffix],[StreetName],[StreetNumber]
,[StreetTypeSuffix],[PropertyTax],[TaxID],[TaxYearBuilt],[VirtualTour],[VideoTour],[PostalCode],[PostalCodeExt],Auction,BodyofWaterName
,CancelDate,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,CommissionCode,Commission,Community,SellingAgentID
,SellingOffice,ExpirationDate,PictureCount,WithdrawDate,Zoning,Area)
 Select 91,IIF(InternetAddressDisplayYN='N',0,1),IIF(InternetEntireListingDisplayYN='N',0,1)
 ,IIF(s.CumulativeDaysOnMarket is null,DateDiff(day,s.DateList,getdate()),s.CumulativeDaysOnMarket)
 ,IIF(s.PriceType='Range Price',s.PriceMaximum,s.PriceList),s.[Acres],s.[CumulativeDaysOnMarket],s.[DateClose],s.[DateList]
 ,s.[DatePending],s.[DateStatusChange],s.[DateTimeModified],s.[DaysOnMarket],s.[FullStreetAddress],s.[HOAFee],s.[JLSCity]
 ,s.[JLSPropTypeID],s.[Latitude],s.[ListingModTime],s.[Longitude],s.[MLSNumber],s.[PriceClose],s.[PriceListOriginal],s.[SchoolElementary]
 ,s.[SchoolHigh],s.[SchoolMiddle],s.[StreetDirPrefix],s.[StreetDirSuffix],s.[StreetName],s.[StreetNumber],s.[StreetTypeSuffix]
 ,s.[TaxAmount],s.[TaxID],s.[TaxYear],s.[VideoVirtualTour1URL],s.[VideoVirtualTour2URL],s.[ZipCode],s.[ZipCodePlus4]
 ,Auction = case when s.AuctionType is not null then 'Y' else 'N' end,s.WaterName,s.DateCanceled,s.ListAgentID,s.CoListAgentID
 ,s.ListOfficeID,s.CoListOfficeID,s.BuyerAgencyCompensationType,IsNull(Try_Convert(decimal(19,2),s.BuyerAgencyCompensation),0.00)
 ,s.Subdivision,s.SaleAgentID,s.SaleOfficeID,s.DateExpiration,s.PhotosCount,s.DateWithdrawn,s.Zoning,s.Area
 From OrRMLS.dbo.Land s

Insert Into mls.Listing 
([MLSID],[ShowAddress],[IDX],[JLsDom],[ListPrice],[Acreage],[CDOM],[CloseDate],[ListDate],[StatusDate],[UpdateDate],[DOM],[StreetAddress]
,[ParkingSpaces],[City],[PropTypeID],[Latitude],[MLSDBListingModTime],[Longitude],[MLSListingID],[SquareFootage],[StreetDirPrefix]
,[StreetDirSuffix],[StreetName],[StreetNumber],[StreetTypeSuffix],[TaxID],[VirtualTour],[VideoTour],[YearBuilt],[PostalCode]
,[PostalCodeExt],CancelDate,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,CommissionCode
,Commission,SellingAgentID,CoSellingAgentID,CoSellingAgentID2,CoSellingAgentID3,SellingOffice,CoSellingOffice,CoSellingOffice2
,ExpirationDate,LeaseDate,PictureCount,WithdrawDate,Zoning,Area)
 Select 91,IIF(InternetAddressDisplayYN='N',0,1),IIF(InternetEntireListingDisplayYN='N',0,1)
 ,IIF(s.CumulativeDaysOnMarket is null,DateDiff(day,s.DateList,getdate()),s.CumulativeDaysOnMarket)
 ,IsNull(Try_Convert(numeric,s.PriceLease),0),s.[Acres],s.[CumulativeDaysOnMarket],s.[DateClose],s.[DateList],s.[DateStatusChange]
 ,s.[DateTimeModified],s.[DaysOnMarket],s.[FullStreetAddress],s.[GarageOrParkingSpaces],s.[JLSCity],s.[JLSPropTypeID],s.[Latitude]
 ,s.[ListingModTime],s.[Longitude],s.[MLSNumber],s.[SqFtApproximateTotal],s.[StreetDirPrefix],s.[StreetDirSuffix],s.[StreetName]
 ,s.[StreetNumber],s.[StreetTypeSuffix],s.[TaxID],s.[VideoVirtualTour1URL],s.[VideoVirtualTour2URL],s.[YearBuilt],s.[ZipCode],s.[ZipCodePlus4]
 ,s.DateCanceled,s.ListAgentID,s.CoListAgentID,s.ListOfficeID,s.CoListOfficeID,s.BuyerAgencyCompensationType
 ,IsNull(Try_Convert(decimal(19,2),s.BuyerAgencyCompensation),0.00),s.SaleAgentID,s.SaleAgentID1,s.SaleAgentID2,s.SaleAgentID3
 ,s.SaleOfficeID,s.SaleOfficeID1,s.SaleOfficeID2,s.DateExpiration,s.DateLeased1,s.PhotosCount,s.DateWithdrawn,s.Zoning,s.Area
 From OrRMLS.dbo.CommercialLease s 


 --OrSOMLS
 Insert Into mls.Listing 
([MLSID],[Latitude],[Longitude],[UpdateDate],[ListDate],[PendingDate],[CloseDate],[StatusDate],[Acreage],[CDOM],[JLsDom],[City],[DOM]
,[ParentPropTypeID],[PropTypeID],[MLSDBListingModTime],[ListPrice],[LotSizeSF],[MLSListingID],[ParkingSpaces],[OriginalListPrice]
,[ClosePrice],[SquareFootage],[StreetDirPrefix],[StreetName],[StreetNumber],[StreetTypeSuffix],[TaxID],[PropertyTax]
,[StreetNumberModifier],[VirtualTour],[YearBuilt],[PostalCode],[PostalCodeExt],[BathsPartial],Age,ListAgentID,CoListAgentID
,ListingOfficeID,CoListingOfficeID,CommissionComments,Commission,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice
,ExpirationDate,FallThroughDate,PictureCount,WithdrawDate,Zoning,Area)
 Select 575004,IIF(IsNull(s.Longitude,0) > -110,null,s.Latitude),IIF(IsNull(s.Longitude,0) > -110,null,s.Longitude)
 ,NullIf(s.LastModifiedDateTime,'1800-01-01 00:00:00.000'),NullIf(s.ListingDate,'1800-01-01 00:00:00.000')
 ,NullIf(s.PendingDate,'1800-01-01 00:00:00.000'),NullIf(s.SellingDate,'1800-01-01 00:00:00.000')
 ,NullIf(s.StatusDate,'1800-01-01 00:00:00.000'),s.[Acres],s.[CDOM],s.[CDOM],s.[City],s.[DOM],s.[JLSParentPropTypeID],s.[JLSPropTypeID]
 ,s.[ListingModTime],s.[ListingPrice],s.[LotSquareFootage],s.[MLSNumber],s.[NumOfParkingSpaces],s.[OriginalPrice],s.[SellingPrice]
 ,s.[SquareFootage],s.[StreetDirection],s.[StreetName],s.[StreetNumber],s.[StreetSuffix],s.[TaxAccountNum],s.[Taxes],s.[Unit]
 ,s.[VirtualTourURL],s.[YearBuilt],s.[ZIP],s.[ZipPlus4],ThreeQuarterBathrooms,s.Age,s.ListingAgentNumber,s.ListingCoAgentNumber
 ,s.ListingOfficeNumber,s.ListingCoOfficeNumber,s.CommissionComments,s.Commission3,s.SellingAgentNumber,s.SellingCoAgentNumber
 ,s.SellingOfficeNumber,s.SellingCoOfficeNumber,s.ExpirationDate,s.ContingentExpirationDate,s.PictureCount,s.InactiveDate,s.Zoning,s.Area
From OrSOMLS.dbo.Commercial s 

Insert Into mls.Listing 
([MLSID],[Latitude],[Longitude],[UpdateDate],[ListDate],[PendingDate],[CloseDate],[StatusDate],[Acreage],[CDOM],[JLsDom],[City],[DOM]
,[ElementarySchool],[HighSchool],[MiddleSchool],[ParentPropTypeID],[PropTypeID],[MLSDBListingModTime],[ListPrice],[LotSizeSF]
,[MLSListingID],[OriginalListPrice],[ClosePrice],[SquareFootage],[StreetDirPrefix],[StreetName],[StreetNumber],[StreetTypeSuffix],[TaxID]
,[PropertyTax],[StreetNumberModifier],[VirtualTour],[YearBuilt],[PostalCode],[PostalCodeExt],[BathsPartial],Age,ListAgentID
,CoListAgentID,ListingOfficeID,CoListingOfficeID,CommissionComments,Commission,Community,SellingAgentID,CoSellingAgentID,SellingOffice
,CoSellingOffice,ExpirationDate,FallThroughDate,PictureCount,Waterfront,WithdrawDate,Zoning,Area)
 Select 575004,IIF(IsNull(s.Longitude,0) > -110,null,s.Latitude),IIF(IsNull(s.Longitude,0) > -110,null,s.Longitude)
 ,NullIf(s.LastModifiedDateTime,'1800-01-01 00:00:00.000'),NullIf(s.ListingDate,'1800-01-01 00:00:00.000')
 ,NullIf(s.PendingDate,'1800-01-01 00:00:00.000'),NullIf(s.SellingDate,'1800-01-01 00:00:00.000')
 ,NullIf(s.StatusDate,'1800-01-01 00:00:00.000'),s.[Acres],s.[CDOM],s.[CDOM],s.[City],s.[DOM],s.[JLSElementarySchool],s.[JLSHighSchool]
 ,s.[JLSMiddleSchool],s.[JLSParentPropTypeID],s.[JLSPropTypeID],s.[ListingModTime],s.[ListingPrice],s.[LotSquareFootage],s.[MLSNumber]
 ,s.[OriginalPrice],s.[SellingPrice],s.[SquareFootage],s.[StreetDirection],s.[StreetName],s.[StreetNumber],s.[StreetSuffix]
 ,s.[TaxAccountNum],s.[Taxes],s.[Unit],s.[VirtualTourURL],s.[YearBuilt],s.[ZIP],s.[ZipPlus4],ThreeQuarterBathrooms,s.Age
 ,s.ListingAgentNumber,s.ListingCoAgentNumber,s.ListingOfficeNumber,s.ListingCoOfficeNumber,s.CommissionComments,s.Commission3
 ,s.Subdivision_Developm,s.SellingAgentNumber,s.SellingCoAgentNumber,s.SellingOfficeNumber,s.SellingCoOfficeNumber,s.ExpirationDate
 ,s.ContingentExpirationDate,s.PictureCount,Water=l.[LongValue],s.InactiveDate,s.Zoning,s.Area
From OrSOMLS.dbo.Land s 
 left join OrSOMLS.dbo.Lookups l
   on s.Water = l.[Value] and [Lookup] = 'LOTLWTRF'

Insert Into mls.Listing 
([MLSID],[BathsPartial],[Latitude],[Longitude],[UpdateDate],[ListDate],[PendingDate],[CloseDate],[StatusDate],[Acreage],[HOADues]
,[BathsTotal],[Bedrooms],[CDOM],[JLsDom],[City],[DOM],[BathsFull],[ElementarySchool],[Garage],[GarageSpaces],[HighSchool],[MiddleSchool]
,[ParentPropTypeID],[PropTypeID],[MLSDBListingModTime],[ListPrice],[LotSizeSF],[MLSListingID],[OriginalListPrice],[ClosePrice]
,[SquareFootage],[StreetDirPrefix],[StreetName],[StreetNumber],[StreetTypeSuffix],[TaxID],[PropertyTax],[VirtualTour],[YearBuilt]
,[PostalCode],[PostalCodeExt],Age,BuilderName,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,CommissionComments,Commission
,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice,EstCompletionDate,ExpirationDate,FallThroughDate,NewConstruction
,PictureCount,Waterfront,WithdrawDate,Zoning,Area)
 Select 575004,HalfBathrooms+QuarterBathrooms+ThreeQuarterBathrooms,IIF(IsNull(s.Longitude,0) > -110,null,s.Latitude)
 ,IIF(IsNull(s.Longitude,0) > -110,null,s.Longitude),NullIf(s.LastModifiedDateTime,'1800-01-01 00:00:00.000')
 ,NullIf(s.ListingDate,'1800-01-01 00:00:00.000'),NullIf(s.PendingDate,'1800-01-01 00:00:00.000')
 ,NullIf(s.SellingDate,'1800-01-01 00:00:00.000'),NullIf(s.StatusDate,'1800-01-01 00:00:00.000'),s.[Acres],s.[Assoc_SpaceRent]
 ,s.[Bathrooms],s.[Bedrooms],s.[CDOM],s.[CDOM],s.[City],s.[DOM],s.[FullBathrooms],s.[JLSElementarySchool],s.[JLSGarage]
 ,s.[JLSGarageSpaces],s.[JLSHighSchool],s.[JLSMiddleSchool],s.[JLSParentPropTypeID],s.[JLSPropTypeID],s.[ListingModTime]
 ,s.[ListingPrice],s.[LotSquareFootage],s.[MLSNumber],s.[OriginalPrice],s.[SellingPrice],s.[SquareFootage],s.[StreetDirection]
 ,s.[StreetName],s.[StreetNumber],s.[StreetSuffix],s.[TaxAccountNum],s.[Taxes],s.[VirtualTourURL],s.[YearBuilt],s.[ZIP],s.[ZipPlus4]
 ,s.Age,s.Builder,s.ListingAgentNumber,s.ListingCoAgentNumber,s.ListingOfficeNumber,s.ListingCoOfficeNumber,s.CommissionComments
 ,s.Commission3,s.SellingAgentNumber,s.SellingCoAgentNumber,s.SellingOfficeNumber,s.SellingCoOfficeNumber,s.EstCompletionDate
 ,s.ExpirationDate,s.ContingentExpirationDate,s.UnderConstruction,s.PictureCount,Water=l.LongValue,s.InactiveDate,s.Zoning,s.Area
From OrSOMLS.dbo.Mobile s
  left join OrSOMLS.dbo.Lookups l
   on s.Water = l.[Value] and [Lookup] = 'MOBLWTRF'

Insert Into mls.Listing 
([MLSID],[Latitude],[Longitude],[UpdateDate],[ListDate],[PendingDate],[CloseDate],[StatusDate],[Acreage],[CDOM],[JLsDom],[City],[DOM]
,[ElementarySchool],[HighSchool],[MiddleSchool],[ParentPropTypeID],[PropTypeID],[MLSDBListingModTime],[ListPrice],[LotSizeSF]
,[MLSListingID],[OriginalListPrice],[ClosePrice],[SquareFootage],[StreetDirPrefix],[StreetName],[StreetNumber],[StreetTypeSuffix],[TaxID]
,[PropertyTax],[StreetNumberModifier],[VirtualTour],[YearBuilt],[PostalCode],[PostalCodeExt],[BathsPartial],Age,ListAgentID
,CoListAgentID,ListingOfficeID,CoListingOfficeID,CommissionComments,Commission,SellingAgentID,CoSellingAgentID,SellingOffice
,CoSellingOffice,EstCompletionDate,ExpirationDate,FallThroughDate,NewConstruction,PictureCount,WithdrawDate,Zoning,Area)
 Select 575004,IIF(IsNull(s.Longitude,0) > -110,null,s.Latitude),IIF(IsNull(s.Longitude,0) > -110,null,s.Longitude)
 ,NullIf(s.LastModifiedDateTime,'1800-01-01 00:00:00.000'),NullIf(s.ListingDate,'1800-01-01 00:00:00.000')
 ,NullIf(s.PendingDate,'1800-01-01 00:00:00.000'),NullIf(s.SellingDate,'1800-01-01 00:00:00.000')
 ,NullIf(s.StatusDate,'1800-01-01 00:00:00.000'),s.[Acres],s.[CDOM],s.[CDOM],s.[City],s.[DOM],s.[JLSElementarySchool],s.[JLSHighSchool]
 ,s.[JLSMiddleSchool],s.[JLSParentPropTypeID],s.[JLSPropTypeID],s.[ListingModTime],s.[ListingPrice],s.[LotSquareFootage],s.[MLSNumber]
 ,s.[OriginalPrice],s.[SellingPrice],s.[SquareFootage],s.[StreetDirection],s.[StreetName],s.[StreetNumber],s.[StreetSuffix]
 ,s.[TaxAccountNum],s.[Taxes],s.[Unit],s.[VirtualTourURL],s.[YearBuilt],s.[ZIP],s.[ZipPlus4],ThreeQuarterBathrooms,s.Age
 ,s.ListingAgentNumber,s.ListingCoAgentNumber,s.ListingOfficeNumber,s.ListingCoOfficeNumber,s.CommissionComments,s.Commission3
 ,s.SellingAgentNumber,s.SellingCoAgentNumber,s.SellingOfficeNumber,s.SellingCoOfficeNumber,s.EstCompletionDate
 ,s.ExpirationDate,s.ContingentExpirationDatE,s.UnderConstruction,s.PictureCount,s.InactiveDate,s.Zoning,s.Area
From OrSOMLS.dbo.MultiFamily s
 
Insert Into mls.Listing
([MLSID],[BathsPartial],[Latitude],[Longitude],[UpdateDate],[ListDate],[PendingDate],[CloseDate],[StatusDate],[Acreage],[HOADues]
,[BathsTotal],[Bedrooms],[CDOM],[JLsDom],[City],[DOM],[BathsFull],[ElementarySchool],[Garage],[GarageSpaces],[HighSchool],[MiddleSchool]
,[ParentPropTypeID],[PropTypeID],[MLSDBListingModTime],[ListPrice],[LotSizeSF],[MLSListingID],[OriginalListPrice],[ClosePrice]
,[SquareFootage],[StreetDirPrefix],[StreetName],[StreetNumber],[StreetTypeSuffix],[TaxID],[PropertyTax],[StreetNumberModifier]
,[VirtualTour],[YearBuilt],[PostalCode],[PostalCodeExt],Age,BuilderName,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID
,CommissionComments,Commission,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice,EstCompletionDate,ExpirationDate
,FallThroughDate,PictureCount,Waterfront,WithdrawDate,Zoning,Area)
 Select 575004,HalfBathrooms+QuarterBathrooms+ThreeQuarterBathrooms,IIF(IsNull(s.Longitude,0) > -110,null,s.Latitude)
 ,IIF(IsNull(s.Longitude,0) > -110,null,s.Longitude),NullIf(s.LastModifiedDateTime,'1800-01-01 00:00:00.000')
 ,NullIf(s.ListingDate,'1800-01-01 00:00:00.000'),NullIf(s.PendingDate,'1800-01-01 00:00:00.000')
 ,NullIf(s.SellingDate,'1800-01-01 00:00:00.000'),NullIf(s.StatusDate,'1800-01-01 00:00:00.000'),s.[Acres],s.[AssocFeesAmount]
 ,s.[Bathrooms],s.[Bedrooms],s.[CDOM],s.[CDOM],s.[City],s.[DOM],s.[FullBathrooms],s.[JLSElementarySchool],s.[JLSGarage]
 ,s.[JLSGarageSpaces],s.[JLSHighSchool],s.[JLSMiddleSchool],s.[JLSParentPropTypeID],s.[JLSPropTypeID],s.[ListingModTime]
 ,s.[ListingPrice],s.[LotSquareFootage],s.[MLSNumber],s.[OriginalPrice],s.[SellingPrice],s.[SquareFootage],s.[StreetDirection]
 ,s.[StreetName],s.[StreetNumber],s.[StreetSuffix],s.[TaxAccountNum],s.[Taxes],s.[Unit],s.[VirtualTourURL],s.[YearBuilt],s.[ZIP]
 ,s.[ZipPlus4],s.Age,s.Builder,s.ListingAgentNumber,s.ListingCoAgentNumber,s.ListingOfficeNumber,s.ListingCoOfficeNumber
 ,s.CommissionComments,s.Commission3,s.SellingAgentNumber,s.SellingCoAgentNumber,s.SellingOfficeNumber,s.SellingCoOfficeNumber
 ,s.EstCompletionDate,s.ExpirationDate,s.ContingentExpirationDate,s.PictureCount,Water=l.LongValue,s.InactiveDate,s.Zoning,s.Area
From OrSOMLS.dbo.Residential s
  left join OrSOMLS.dbo.Lookups l
   on s.Water = l.[Value] and [Lookup] = 'RESIWTRF'

Insert Into mls.Listing 
([MLSID],[Latitude],[Longitude],[UpdateDate],[ListDate],[PendingDate],[CloseDate],[StatusDate],[Acreage],[BathsTotal],[Bedrooms],[CDOM]
,[JLsDom],[City],[DOM],[ElementarySchool],[Garage],[GarageSpaces],[HighSchool],[MiddleSchool],[ParentPropTypeID],[PropTypeID]
,[MLSDBListingModTime],[ListPrice],[LotSizeSF],[MLSListingID],[OriginalListPrice],[ClosePrice],[SquareFootage],[StreetDirPrefix]
,[StreetName],[StreetNumber],[StreetTypeSuffix],[TaxID],[PropertyTax],[StreetNumberModifier],[VirtualTour],[YearBuilt],[PostalCode]
,[PostalCodeExt],Age,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,SellingAgentID,CoSellingAgentID,SellingOffice
,CoSellingOffice,ExpirationDate,PictureCount,Waterfront,WithdrawDate,Zoning,Area)
 Select 575004,IIF(IsNull(s.Longitude,0) > -110,null,s.Latitude),IIF(IsNull(s.Longitude,0) > -110,null,s.Longitude)
 ,NullIf(s.LastModifiedDateTime,'1800-01-01 00:00:00.000'),NullIf(s.ListingDate,'1800-01-01 00:00:00.000')
 ,NullIf(s.PendingDate,'1800-01-01 00:00:00.000'),NullIf(s.SellingDate,'1800-01-01 00:00:00.000')
 ,NullIf(s.StatusDate,'1800-01-01 00:00:00.000'),s.[Acres],s.[Bathrooms],s.[Bedrooms],s.[CDOM],s.[CDOM],s.[City],s.[DOM]
 ,s.[JLSElementarySchool],s.[JLSGarage],s.[JLSGarageSpaces],s.[JLSHighSchool],s.[JLSMiddleSchool],s.[JLSParentPropTypeID]
 ,s.[JLSPropTypeID],s.[ListingModTime],s.[ListingPrice],s.[LotSquareFootage],s.[MLSNumber],s.[OriginalPrice],s.[SellingPrice]
 ,s.[SquareFootage],s.[StreetDirection],s.[StreetName],s.[StreetNumber],s.[StreetSuffix],s.[TaxAccountNum],s.[Taxes],s.[Unit]
 ,s.[VirtualTourURL],s.[YearBuilt],s.[ZIP],s.[ZipPlus4],s.Age,s.ListingAgentNumber,s.ListingCoAgentNumber,s.ListingOfficeNumber
 ,s.ListingCoOfficeNumber,s.SellingAgentNumber,s.SellingCoAgentNumber,s.SellingOfficeNumber,s.SellingCoOfficeNumber,s.ExpirationDate
 ,s.PictureCount,Water=l.LongValue,s.InactiveDate,s.Zoning,s.Area
From OrSOMLS.dbo.Farm s
   left join OrSOMLS.dbo.Lookups l
   on s.Water = l.[Value] and [Lookup] = 'LOTLWTRF'

 --OrWVMLS
 Insert Into mls.Listing 
([ShowMap],[MLSID],[VirtualTour],[DOM],[JLsDom],[IDX],[StreetDirPrefix],[StreetNumber],[StreetName],[LotSizeSF],[ListPrice],[Bedrooms]
,[CloseDate],[PendingDate],[StreetAddress],[BathsFull],[ParkingSpaces],[BathsPartial],[BankOwned],[City],[ElementarySchool],[Garage]
,[MiddleSchool],[PropTypeID],[ShortSale],[HighSchool],[Latitude],[ListDate],[MLSDBListingModTime],[Longitude],[MLSListingID],[Acreage]
,[OriginalListPrice],[StreetDirSuffix],[ClosePrice],[SquareFootage],[StatusDate],[PropertyTax],[TaxID],[BathsTotal],[UpdateDate]
,[YearBuilt],[PostalCode],AgentHitCount,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,SellingOfficeCommissionPercentage
,Community,SellingAgentID,SellingOffice,NewConstruction,PictureCount,Zoning,HowSold,Area)
 Select 1,129,IIF(Len(s.VirtualTour) < 301,s.VirtualTour,null),IIF(s.DOM < 32767,s.DOM,null),IIF(s.DOM < 32767,s.DOM,null)
 ,iif(s.IdxInclude=1,0,1),s.[AddressDirection],s.[AddressNumber],s.[AddressStreet],s.[ApproxLotSqFt],s.[AskingPrice],s.[Beds]
 ,s.[ClosingDate],s.[ContractDate],s.[FullAddress],s.[FullBaths],s.[GarageCapacity],s.[HalfBaths],s.[JlsBankOwned],s.[JlsCity]
 ,s.[JlsElementary],s.[JLSGarage],s.[JlsJrHigh],s.[JLSPropTypeID],s.[JlsShortSale],s.[JlsSrHigh],s.[Latitude],s.[ListingDate]
 ,s.[ListingModTime],s.[Longitude],s.[MLSNumber],s.[NumAcres],s.[OriginalPrice],s.[PostDirection],s.[SoldPrice],s.[SquareFeet]
 ,s.[StatusDate],s.[TaxAmount],s.[TaxPropertyID],s.[TotalBaths],s.[UpdateDate],s.[YearBuilt],s.[Zip],s.AgentHitCount,s.ListAgent1
 ,s.ListAgent2,s.ListOffice1,s.ListOffice2,s.SellingOfficeComm,s.SubDivision,s.SellingAgent1,s.SellingOffice1
 ,s.NewConstruction,s.PictureCount,s.Zoning,HowSold=l.LongValue,s.Area
 From OrWVMLS.dbo.Residential s
  left join OrWVMLS.dbo.lookups l
   on s.HowSold = l.[Value] and l.[Lookup] = 'HowSold' and l.[Resource] = 'Property'

Insert Into mls.Listing 
([ShowMap],[MLSID],[VirtualTour],[DOM],[JLsDom],[IDX],[StreetDirPrefix],[StreetNumber],[StreetName],[LotSizeSF],[ListPrice],[CloseDate]
,[PendingDate],[StreetAddress],[BankOwned],[City],[ElementarySchool],[MiddleSchool],[PropTypeID],[ShortSale],[HighSchool],[Latitude]
,[ListDate],[MLSDBListingModTime],[Longitude],[MLSListingID],[Acreage],[OriginalListPrice],[StreetDirSuffix],[ClosePrice],[StatusDate]
,[PropertyTax],[TaxID],[UpdateDate],[PostalCode],AgentHitCount,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID
,SellingOfficeCommissionPercentage,Community,SellingAgentID,SellingOffice,PictureCount,Zoning,HowSold,Area)
 Select 1,129,IIF(Len(s.VirtualTour) < 301,s.VirtualTour,null),IIF(s.DOM < 32767,s.DOM,null),IIF(s.DOM < 32767,s.DOM,null)
 ,iif(s.IdxInclude=1,0,1),s.[AddressDirection],s.[AddressNumber],s.[AddressStreet],s.[ApproxLotSqFt],s.[AskingPrice],s.[ClosingDate]
 ,s.[ContractDate],s.[FullAddress],s.[JlsBankOwned],s.[JlsCity],s.[JlsElementary],s.[JlsJrHigh],s.[JLSPropTypeID],s.[JlsShortSale]
 ,s.[JlsSrHigh],s.[Latitude],s.[ListingDate],s.[ListingModTime],s.[Longitude],s.[MLSNumber],s.[NumAcres],s.[OriginalPrice]
 ,s.[PostDirection],s.[SoldPrice],s.[StatusDate],s.[TaxAmount],s.[TaxPropertyID],s.[UpdateDate],s.[Zip],s.AgentHitCount,s.ListAgent1
 ,s.ListAgent2,s.ListOffice1,s.ListOffice2,s.SellingOfficeComm,s.SubDivision,s.SellingAgent1,s.SellingOffice1,s.PictureCount,s.Zoning
 ,HowSold=l.LongValue,s.Area
 From OrWVMLS.dbo.Land s
   left join OrWVMLS.dbo.lookups l
   on s.HowSold = l.[Value] and l.[Lookup] = 'HowSold' and l.[Resource] = 'Property'

Insert Into mls.Listing 
([ShowMap],[MLSID],[VirtualTour],[DOM],[JLsDom],[IDX],[StreetDirPrefix],[StreetNumber],[StreetName],[LotSizeSF],[ListPrice],[CloseDate]
,[PendingDate],[StreetAddress],[BankOwned],[City],[ElementarySchool],[MiddleSchool],[PropTypeID],[ShortSale],[HighSchool],[Latitude]
,[ListDate],[MLSDBListingModTime],[Longitude],[MLSListingID],[Acreage],[OriginalListPrice],[StreetDirSuffix],[ClosePrice],[SquareFootage]
,[StatusDate],[PropertyTax],[TaxID],[BathsTotal],[UpdateDate],[YearBuilt],[PostalCode],AgentHitCount,ListAgentID,CoListAgentID
,ListingOfficeID,CoListingOfficeID,SellingOfficeCommissionPercentage,Community,SellingAgentID,SellingOffice,NewConstruction,PictureCount
,Zoning,HowSold,Area)
 Select 1,129,IIF(Len(s.VirtualTour) < 301,s.VirtualTour,null),IIF(s.DOM < 32767,s.DOM,null),IIF(s.DOM < 32767,s.DOM,null)
 ,iif(s.IdxInclude=1,0,1),s.[AddressDirection],s.[AddressNumber],s.[AddressStreet],s.[ApproxLotSqFt],s.[AskingPrice],s.[ClosingDate]
 ,s.[ContractDate],s.[FullAddress],s.[JlsBankOwned],s.[JlsCity],s.[JlsElementary],s.[JlsJrHigh],s.[JLSPropTypeID],s.[JlsShortSale]
 ,s.[JlsSrHigh],s.[Latitude],s.[ListingDate],s.[ListingModTime],s.[Longitude],s.[MLSNumber],s.[NumAcres],s.[OriginalPrice]
 ,s.[PostDirection],s.[SoldPrice],s.[SquareFeet],s.[StatusDate],s.[TaxAmount],s.[TaxPropertyID],s.[TotalBaths],s.[UpdateDate]
 ,s.[YearBuilt],s.[Zip],s.AgentHitCount,s.ListAgent1,s.ListAgent2,s.ListOffice1,s.ListOffice2,s.SellingOfficeComm,s.SubDivision
 ,s.SellingAgent1,s.SellingOffice1,s.NewConstruction,s.PictureCount,s.Zoning,HowSold=l.LongValue,s.Area
 From OrWVMLS.dbo.MultiFamily s
   left join OrWVMLS.dbo.lookups l
   on s.HowSold = l.[Value] and l.[Lookup] = 'HowSold' and l.[Resource] = 'Property'

Insert Into mls.Listing 
([ShowMap],[MLSID],[VirtualTour],[DOM],[JLsDom],[IDX],[StreetDirPrefix],[StreetNumber],[StreetName],[LotSizeSF],[ListPrice],[CloseDate]
,[PendingDate],[StreetAddress],[BankOwned],[City],[ElementarySchool],[MiddleSchool],[PropTypeID],[ShortSale],[HighSchool],[Latitude]
,[ListDate],[MLSDBListingModTime],[Longitude],[MLSListingID],[Acreage],[OriginalListPrice],[StreetDirSuffix],[ClosePrice],[SquareFootage]
,[StatusDate],[TaxID],[UpdateDate],[PostalCode],AgentHitCount,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID
,SellingAgentID,SellingOffice,DailyTrafficCount,PictureCount,Zoning,HowSold,Area)
 Select 1,129,IIF(Len(s.VirtualTour) < 301,s.VirtualTour,null),IIF(s.DOM < 32767,s.DOM,null),IIF(s.DOM < 32767,s.DOM,null)
 ,iif(s.IdxInclude=1,0,1),s.[AddressDirection],s.[AddressNumber],s.[AddressStreet],s.[ApproxLotSqFt],s.[AskingPrice],s.[ClosingDate]
 ,s.[ContractDate],s.[FullAddress],s.[JlsBankOwned],s.[JlsCity],s.[JlsElementary],s.[JlsJrHigh],s.[JLSPropTypeID],s.[JlsShortSale]
 ,s.[JlsSrHigh],s.[Latitude],s.[ListingDate],s.[ListingModTime],s.[Longitude],s.[MLSNumber],s.[NumAcres],s.[OriginalPrice]
 ,s.[PostDirection],s.[SoldPrice],s.[SquareFeet],s.[StatusDate],s.[TaxPropertyID],s.[UpdateDate],s.[Zip],s.AgentHitCount,s.ListAgent1
 ,s.ListAgent2,s.ListOffice1,s.ListOffice2,s.SellingAgent1,s.SellingOffice1,s.TrafficCount,s.PictureCount,s.Zoning,HowSold=l.LongValue
 ,s.Area
 From OrWVMLS.dbo.Lease s
   left join OrWVMLS.dbo.lookups l
   on s.HowSold = l.[Value] and l.[Lookup] = 'HowSold' and l.[Resource] = 'Property'

Insert Into mls.Listing 
([ShowMap],[MLSID],[VirtualTour],[DOM],[JLsDom],[IDX],[StreetDirPrefix],[StreetNumber],[StreetName],[LotSizeSF],[ListPrice],[Bedrooms]
,[CloseDate],[PendingDate],[StreetAddress],[ParkingSpaces],[BankOwned],[BathsTotal],[City],[ElementarySchool],[Garage],[MiddleSchool]
,[PropTypeID],[ShortSale],[HighSchool],[Latitude],[ListDate],[MLSDBListingModTime],[Longitude],[MLSListingID],[OriginalListPrice]
,[StreetDirSuffix],[ClosePrice],[SquareFootage],[StatusDate],[PropertyTax],[TaxID],[UpdateDate],[YearBuilt],[PostalCode],AgentHitCount
,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,SellingOfficeCommissionPercentage,SellingAgentID,SellingOffice
,NewConstruction,PictureCount,HowSold,Area)
 Select 1,129,IIF(Len(s.VirtualTour) < 301,s.VirtualTour,null),IIF(s.DOM < 32767,s.DOM,null),IIF(s.DOM < 32767,s.DOM,null)
 ,iif(s.IdxInclude=1,0,1),s.[AddressDirection],s.[AddressNumber],s.[AddressStreet],s.[ApproxLotSqFt],s.[AskingPrice],s.[Beds]
 ,s.[ClosingDate],s.[ContractDate],s.[FullAddress],s.[GarageCapacity],s.[JlsBankOwned],s.[JLSBaths],s.[JlsCity],s.[JlsElementary]
 ,s.[JLSGarage],s.[JlsJrHigh],s.[JLSPropTypeID],s.[JlsShortSale],s.[JlsSrHigh],s.[Latitude],s.[ListingDate],s.[ListingModTime]
 ,s.[Longitude],s.[MLSNumber],s.[OriginalPrice],s.[PostDirection],s.[SoldPrice],s.[SquareFeet],s.[StatusDate],s.[TaxAmount]
 ,s.[TaxPropertyID],s.[UpdateDate],s.[YearBuilt],s.[Zip],s.AgentHitCount,s.ListAgent1,s.ListAgent2,s.ListOffice1,s.ListOffice2
 ,s.SellingOfficeComm,s.SellingAgent1,s.SellingOffice1,s.NewConstruction,s.PictureCount,HowSold=l.LongValue,s.Area
 From OrWVMLS.dbo.Manufactured s
   left join OrWVMLS.dbo.lookups l
   on s.HowSold = l.[Value] and l.[Lookup] = 'HowSold' and l.[Resource] = 'Property'

Insert Into mls.Listing 
([ShowMap],[MLSID],[VirtualTour],[DOM],[JLsDom],[IDX],[StreetDirPrefix],[StreetNumber],[StreetName],[LotSizeSF],[ListPrice],[CloseDate]
,[PendingDate],[StreetAddress],[BankOwned],[City],[ElementarySchool],[MiddleSchool],[PropTypeID],[ShortSale],[HighSchool],[Latitude]
,[ListDate],[MLSDBListingModTime],[Longitude],[MLSListingID],[Acreage],[OriginalListPrice],[StreetDirSuffix],[ClosePrice],[SquareFootage]
,[StatusDate],[PropertyTax],[TaxID],[UpdateDate],[YearBuilt],[PostalCode],AgentHitCount,ListAgentID,CoListAgentID,ListingOfficeID
,CoListingOfficeID,SellingOfficeCommissionPercentage,SellingAgentID,SellingOffice,NewConstruction,PictureCount,Zoning,HowSold,Area)
 Select 1,129,IIF(Len(s.VirtualTour) < 301,s.VirtualTour,null),IIF(s.DOM < 32767,s.DOM,null),IIF(s.DOM < 32767,s.DOM,null)
 ,iif(s.IdxInclude=1,0,1),s.[AddressDirection],s.[AddressNumber],s.[AddressStreet],s.[ApproxLotSqFt],s.[AskingPrice],s.[ClosingDate]
 ,s.[ContractDate],s.[FullAddress],s.[JlsBankOwned],s.[JlsCity],s.[JlsElementary],s.[JlsJrHigh],s.[JLSPropTypeID],s.[JlsShortSale]
 ,s.[JlsSrHigh],s.[Latitude],s.[ListingDate],s.[ListingModTime],s.[Longitude],s.[MLSNumber],s.[NumAcres],s.[OriginalPrice]
 ,s.[PostDirection],s.[SoldPrice],s.[SquareFeet],s.[StatusDate],s.[TaxAmount],s.[TaxPropertyID],s.[UpdateDate],s.[YearBuilt],s.[Zip]
 ,s.AgentHitCount,s.ListAgent1,s.ListAgent2,s.ListOffice1,s.ListOffice2,s.SellingOfficeComm,s.SellingAgent1,s.SellingOffice1
 ,NewConstruction,s.PictureCount,s.Zoning,HowSold=l.LongValue,s.Area
 From OrWVMLS.dbo.Commercial s
   left join OrWVMLS.dbo.lookups l
   on s.HowSold = l.[Value] and l.[Lookup] = 'HowSold' and l.[Resource] = 'Property'


 --WaNCWAR
 Insert Into mls.Listing 
([IDX],[MLSID],[VirtualTour],[Latitude],[Longitude],[Acreage],[DOM],[JLsDom],[City],[ElementarySchool],[HighSchool],[MiddleSchool]
,[ParentPropTypeID],[PostalCode],[PropTypeID],[ShortSale],[StreetDirPrefix],[StreetTypeSuffix],[UpdateDate],[ListDate]
,[MLSDBListingModTime],[ListPrice],[MLSListingID],[CloseDate],[ClosePrice],[SquareFootage],[StatusDate],[StreetName],[StreetNumber]
,[StreetNumberModifier],[PendingDate],[YearBuilt],[PropertyTax],[OriginalListPrice],BodyofWaterName,CancelDate,ListAgentID
,CoListAgentID,CoListingMemberID,ListingOfficeID,CoListingOfficeID,SellingOfficeCommissionPercentage,SellingAgentID
,CoSellingAgentID,SellingOffice,CoSellingOffice,ExpirationDate,FallThroughDate,PictureCount,Waterfront,WithdrawDate,Zoning,HowSold,Area)
 Select 1,575006,coalesce(s.BrandedVirtualTour,s.UnBrandedVirtualTour),IIF(s.GeoLatitude is null,null,TRY_CONVERT(decimal(9,6)
 ,s.GeoLatitude)),IIF(s.GeoLongitude is null,null,TRY_CONVERT(decimal(9,6),s.GeoLongitude)),s.[AcresNumOf],s.[DaysOnMarket]
 ,s.[DaysOnMarket],s.[JLSCity],s.[JLSElementarySchool],s.[JLSHighSchool],s.[JLSMiddleSchool],s.[JLSParentPropTypeID],s.[JLSPostalCode]
 ,s.[JLSPropTypeID],s.[JLSShortSale],s.[JLSStreetDirPrefix],s.[JLSStreetSuffix],s.[LastModifiedDateTime],s.[ListDate],s.[ListingModTime]
 ,s.[ListPrice],s.[MLSNumber],s.[SoldDate],s.[SoldPrice],s.[SqFtTl],s.[StatusChangeDate],s.[StreetName],s.[StreetNumber]
 ,s.[StreetNumberModifier],s.[UnderContractDate],s.[YearBuilt],TRY_CONVERT(decimal(19,2),replace(replace(s.taxes,'$',''),',',''))
 ,TRY_CONVERT(int,s.[OriginalListPrice]),BodyOfWater = l1.LongValue,s.CancelDate,s.AgentID,s.CoListAgentID,s.CoListingMemberShortID
 ,s.ListingOfficeShortID,s.CoListingOfficeShortID,TRY_CONVERT(decimal(19,2),replace(s.SellingOfcComm,'%','')),s.SellingAgentID,s.CoSellingAgentID
 ,s.SellingOfficeShortID,s.CoSellingOfficeShortID,s.[ExpireDate],s.FallThroughDate,s.PictureCount,Waterfront=l3.LongValue, s.WithdrawalDate
 ,s.Zoning,HowSold=l2.LongValue,s.Area
 From WaNCWAR.dbo.Business s
   left join WaNCWAR.dbo.Lookups l1
     on case when charindex(',',Waterfront) > 0 then substring(Waterfront,1,charindex(',',waterfront)-1) else waterfront end = l1.[Value]
	   and l1.[Lookup]= 'GFLU20120918165055973364000000'
   left join WaNCWAR.dbo.Lookups l2
     on s.HowSold = l2.[Value] and l2.[Lookup]= 'GFLU20120918165055973364000000'
   left join WaNCWAR.dbo.Lookups l3
     on s.Waterfront1 = l3.[Value] and l3.[Lookup]= '20050811185432661654000000'

Insert Into mls.Listing
([IDX],[MLSID],[VirtualTour],[Latitude],[Longitude],[Acreage],[Bedrooms],[DOM],[JLsDom],[City],[ElementarySchool],[GarageSpaces]
,[HighSchool],[MiddleSchool],[ParentPropTypeID],[PostalCode],[PropTypeID],[ShortSale],[StreetDirPrefix],[StreetTypeSuffix],[UpdateDate]
,[ListDate],[MLSDBListingModTime],[ListPrice],[MLSListingID],[CloseDate],[ClosePrice],[SquareFootage],[StatusDate],[StreetName]
,[StreetNumber],[PendingDate],[YearBuilt],[PropertyTax],[OriginalListPrice],CancelDate,ListAgentID,CoListAgentID
,CoListingMemberID,ListingOfficeID,CoListingOfficeID,SellingOfficeCommissionPercentage,Community,SellingAgentID,CoSellingAgentID
,SellingOffice,CoSellingOffice,ExpirationDate,FallThroughDate,PictureCount,WithdrawDate,Zoning,HowSold,Area)
 Select 1,575006,coalesce(s.BrandedVirtualTour,s.UnBrandedVirtualTour),IIF(s.GeoLatitude is null,null,TRY_CONVERT(decimal(9,6)
 ,s.GeoLatitude)),IIF(s.GeoLongitude is null,null,TRY_CONVERT(decimal(9,6),s.GeoLongitude)),s.[AcresTotal],s.[BedroomsTotal]
 ,s.[DaysOnMarket],s.[DaysOnMarket],s.[JLSCity],s.[JLSElementarySchool],s.[JLSGarageSpaces],s.[JLSHighSchool],s.[JLSMiddleSchool]
 ,s.[JLSParentPropTypeID],s.[JLSPostalCode],s.[JLSPropTypeID],s.[JLSShortSale],s.[JLSStreetDirPrefix],s.[JLSStreetSuffix]
 ,s.[LastModifiedDateTime],s.[ListDate],s.[ListingModTime],s.[ListPrice],s.[MLSNumber],s.[SoldDate],s.[SoldPrice],s.[SqFtTl]
 ,s.[StatusChangeDate],s.[StreetName],s.[StreetNumber],s.[UnderContractDate],s.[YearBuilt],TRY_CONVERT(decimal(19,2)
 ,replace(replace(s.taxes,'$',''),',','')),TRY_CONVERT(int,s.[OriginalListPrice]),s.CancelDate,s.AgentID
 ,s.CoListAgentID,s.CoListingMemberShortID,s.ListingOfficeShortID,s.CoListingOfficeShortID,TRY_CONVERT(decimal(19,2)
 ,replace(s.SellingOfcComm,'%','')),s.SubDivision,s.SellingAgentID,s.CoSellingAgentID,s.SellingOfficeShortID,s.CoSellingOfficeShortID
 ,s.[ExpireDate],s.FallThroughDate,s.PictureCount,s.WithdrawalDate,s.Zoning,HowSold=l.LongValue,s.Area
 From WaNCWAR.dbo.FarmRanch s
    left join WaNCWAR.dbo.Lookups l
     on s.HowSold = l.[Value] and l.[Lookup]= '20050811185432663386000000'

Insert Into mls.Listing
([IDX],[MLSID],[VirtualTour],[Latitude],[Longitude],[Acreage],[DOM],[JLsDom],[City],[ElementarySchool],[HighSchool],[MiddleSchool]
,[ParentPropTypeID],[PostalCode],[PropTypeID],[ShortSale],[StreetDirPrefix],[StreetTypeSuffix],[UpdateDate],[ListDate]
,[MLSDBListingModTime],[ListPrice],[MLSListingID],[CloseDate],[ClosePrice],[StatusDate],[StreetName],[StreetNumber],[PendingDate]
,[PropertyTax],[OriginalListPrice],BodyofWaterName,CancelDate,ListAgentID,CoListAgentID,CoListingMemberID,ListingOfficeID
,CoListingOfficeID,SellingOfficeCommissionPercentage,Community,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice
,ExpirationDate,FallThroughDate,PictureCount,Waterfront,WithdrawDate,Zoning,HowSold,Area)
 Select 1,575006,coalesce(s.BrandedVirtualTour,s.UnBrandedVirtualTour),IIF(s.GeoLatitude is null,null,TRY_CONVERT(decimal(9,6)
 ,s.GeoLatitude)),IIF(s.GeoLongitude is null,null,TRY_CONVERT(decimal(9,6),s.GeoLongitude)),s.[AcresNumOf],s.[DaysOnMarket]
 ,s.[DaysOnMarket],s.[JLSCity],s.[JLSElementarySchool],s.[JLSHighSchool],s.[JLSMiddleSchool],s.[JLSParentPropTypeID],s.[JLSPostalCode]
 ,s.[JLSPropTypeID],s.[JLSShortSale],s.[JLSStreetDirPrefix],s.[JLSStreetSuffix],s.[LastModifiedDateTime],s.[ListDate],s.[ListingModTime]
 ,s.[ListPrice],s.[MLSNumber],s.[SoldDate],s.[SoldPrice],s.[StatusChangeDate],s.[StreetName],s.[StreetNumber],s.[UnderContractDate]
 ,TRY_CONVERT(decimal(19,2),replace(replace(s.taxes,'$',''),',','')),TRY_CONVERT(int,s.[OriginalListPrice]),BodyOfWater=l1.LongValue
 ,s.CancelDate,s.AgentID,s.CoListAgentID,s.CoListingMemberShortID,s.ListingOfficeShortID,s.CoListingOfficeShortID
 ,TRY_CONVERT(decimal(19,2),replace(s.SellingOfcComm,'%','')),s.SubDivision,s.SellingAgentID,s.CoSellingAgentID,s.SellingOfficeShortID
 ,s.CoSellingOfficeShortID,s.[ExpireDate],s.FallThroughDate ,s.PictureCount,Waterfront=l3.LongValue,s.WithdrawalDate,s.Zoning
 ,HowSold=l2.LongValue,s.Area
 From WaNCWAR.dbo.Land s
   left join WaNCWAR.dbo.Lookups l1
     on case when charindex(',',Waterfront) > 0 then substring(Waterfront,1,charindex(',',waterfront)-1) else waterfront end = l1.[Value]
	 and l1.[Lookup]= 'GFLU20120918163202012786000000'
   left join WaNCWAR.dbo.Lookups l2
     on s.HowSold = l2.[Value] and l2.[Lookup]= '20050811185432663341000000'
   left join WaNCWAR.dbo.Lookups l3
     on s.Waterfront1 = l3.[Value] and l3.[Lookup]= '20050811185432662835000000'

Insert Into mls.Listing 
([IDX],[MLSID],[VirtualTour],[Latitude],[Longitude],[Acreage],[DOM],[JLsDom],[City],[ElementarySchool],[HighSchool],[MiddleSchool]
,[ParentPropTypeID],[PostalCode],[PropTypeID],[ShortSale],[StreetDirPrefix],[StreetTypeSuffix],[UpdateDate],[ListDate]
,[MLSDBListingModTime],[ListPrice],[MLSListingID],[CloseDate],[ClosePrice],[SquareFootage],[StatusDate],[StreetName],[StreetNumber]
,[StreetNumberModifier],[PendingDate],[YearBuilt],[PropertyTax],[OriginalListPrice],BodyofWaterName,CancelDate,ListAgentID
,CoListAgentID,CoListingMemberID,ListingOfficeID,CoListingOfficeID,SellingOfficeCommissionPercentage,SellingAgentID
,CoSellingAgentID,SellingOffice,CoSellingOffice,ExpirationDate,FallThroughDate,PictureCount,Waterfront,WithdrawDate,Zoning,HowSold,Area)
 Select 1,575006,coalesce(s.BrandedVirtualTour,s.UnBrandedVirtualTour),IIF(s.GeoLatitude is null,null,TRY_CONVERT(decimal(9,6)
 ,s.GeoLatitude)),IIF(s.GeoLongitude is null,null,TRY_CONVERT(decimal(9,6),s.GeoLongitude)),s.[AcresNumOf],s.[DaysOnMarket]
 ,s.[DaysOnMarket],s.[JLSCity],s.[JLSElementarySchool],s.[JLSHighSchool],s.[JLSMiddleSchool],s.[JLSParentPropTypeID],s.[JLSPostalCode]
 ,s.[JLSPropTypeID],s.[JLSShortSale],s.[JLSStreetDirPrefix],s.[JLSStreetSuffix],s.[LastModifiedDateTime],s.[ListDate],s.[ListingModTime]
 ,s.[ListPrice],s.[MLSNumber],s.[SoldDate],s.[SoldPrice],s.[SqFtTl],s.[StatusChangeDate],s.[StreetName],s.[StreetNumber]
 ,s.[StreetNumberModifier],s.[UnderContractDate],s.[YearBuilt],TRY_CONVERT(decimal(19,2),replace(replace(s.taxes,'$',''),',',''))
 ,TRY_CONVERT(int,s.[OriginalListPrice]),Waterfront=l1.LongValue,s.CancelDate,s.AgentID,s.CoListAgentID,s.CoListingMemberShortID
 ,s.ListingOfficeShortID,s.CoListingOfficeShortID,TRY_CONVERT(decimal(19,2),replace(s.SellingOfcComm,'%',''))
 ,s.SellingAgentID,s.CoSellingAgentID,s.SellingOfficeShortID,s.CoSellingOfficeShortID,s.[ExpireDate],s.FallThroughDate,s.PictureCount
 ,Waterfront=l3.LongValue,s.WithdrawalDate,s.Zoning,HowSold=l2.LongValue,s.Area
 From WaNCWAR.dbo.MultiFamily s
    left join WaNCWAR.dbo.Lookups l1
     on case when charindex(',',Waterfront) > 0 then substring(Waterfront,1,charindex(',',waterfront)-1) else waterfront end = l1.[Value] and l1.[Lookup]= 'GFLU20120918164514023315000000'
   left join WaNCWAR.dbo.Lookups l2
     on s.HowSold = l2.[Value] and l2.[Lookup]= '20050811185432659279000000'
    left join WaNCWAR.dbo.Lookups l3
     on s.Waterfront1 = l3.[Value] and l3.[Lookup]= '20050811185432655734000000'

Insert Into mls.Listing 
([IDX],[MLSID],[VirtualTour],[Latitude],[Longitude],[Garage],[Acreage],[BathsTotal],[Bedrooms],[DOM],[JLsDom],[City],[ElementarySchool]
,[GarageSpaces],[HighSchool],[MiddleSchool],[ParentPropTypeID],[PostalCode],[PropTypeID],[ShortSale],[StreetDirPrefix],[StreetTypeSuffix]
,[UpdateDate],[ListDate],[MLSDBListingModTime],[ListPrice],[MLSListingID],[CloseDate],[ClosePrice],[SquareFootage],[StatusDate]
,[StreetName],[StreetNumber],[StreetNumberModifier],[PendingDate],[YearBuilt],[PropertyTax],[OriginalListPrice],BodyofWaterName
,CancelDate,ListAgentID,CoListAgentID,CoListingMemberID,ListingOfficeID,CoListingOfficeID,SellingOfficeCommissionPercentage,Community
,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice,ExpirationDate,FallThroughDate,PictureCount,Waterfront,WithdrawDate
,Zoning,HowSold,Area)
 Select 1,575006,coalesce(s.BrandedVirtualTour,s.UnBrandedVirtualTour),IIF(s.GeoLatitude is null,null,TRY_CONVERT(decimal(9,6)
 ,s.GeoLatitude)),IIF(s.GeoLongitude is null,null,TRY_CONVERT(decimal(9,6),s.GeoLongitude)),IIF(s.jlsgaragespaces>0,1,null)
 ,s.[AcresNumOf],s.[BathroomsTotal],s.[BedroomsTotal],s.[DaysOnMarket],s.[DaysOnMarket],s.[JLSCity],s.[JLSElementarySchool]
 ,s.[JLSGarageSpaces],s.[JLSHighSchool],s.[JLSMiddleSchool],s.[JLSParentPropTypeID],s.[JLSPostalCode],s.[JLSPropTypeID],s.[JLSShortSale]
 ,s.[JLSStreetDirPrefix],s.[JLSStreetSuffix],s.[LastModifiedDateTime],s.[ListDate],s.[ListingModTime],s.[ListPrice],s.[MLSNumber]
 ,s.[SoldDate],s.[SoldPrice],s.[SqFtTl],s.[StatusChangeDate],s.[StreetName],s.[StreetNumber],s.[StreetNumberModifier],s.[UnderContractDate]
 ,s.[YearBuilt],TRY_CONVERT(decimal(19,2),replace(replace(s.taxes,'$',''),',','')),TRY_CONVERT(int,s.[OriginalListPrice])
 ,BodyofWater=l1.LongValue,s.CancelDate,s.AgentID,s.CoListAgentID,s.CoListingMemberShortID,s.ListingOfficeShortID,s.CoListingOfficeShortID
 ,TRY_CONVERT(decimal(19,2),replace(s.SellingOfcComm,'%','')),s.SubDivision,s.SellingAgentID,s.CoSellingAgentID,s.SellingOfficeShortID
 ,s.CoSellingOfficeShortID,s.[ExpireDate],s.FallThroughDate,s.PictureCount,Waterfront=l3.LongValue,s.WithdrawalDate,s.Zoning
 ,HowSold=l2.LongValue,s.Area
 From WaNCWAR.dbo.Residential s
   left join WaNCWAR.dbo.Lookups l1
     on case when charindex(',',Waterfront) > 0 then substring(Waterfront,1,charindex(',',waterfront)-1) else waterfront end = l1.[Value]
	 and l1.[Lookup]= 'GFLU20120918161721103387000000'
   left join WaNCWAR.dbo.Lookups l2
     on s.HowSold = l2.[Value] and l2.[Lookup]= '20050811185432658883000000'
   left join WaNCWAR.dbo.Lookups l3
     on s.Waterfront1 = l3.[Value] and l3.[Lookup]= '20050811185432651267000000'

Insert Into mls.Listing 
([IDX],[MLSID],[VirtualTour],[Latitude],[Longitude],[Acreage],[DOM],[JLsDom],[BathsFull],[BathsPartial],[City],[ElementarySchool]
,[HighSchool],[MiddleSchool],[ParentPropTypeID],[PostalCode],[PropTypeID],[ShortSale],[StreetDirPrefix],[StreetTypeSuffix],[UpdateDate]
,[ListDate],[MLSDBListingModTime],[ListPrice],[MLSListingID],[CloseDate],[ClosePrice],[StatusDate],[StreetName],[StreetNumber]
,[PendingDate],[YearBuilt],[PropertyTax],[OriginalListPrice],Age,CancelDate,ListAgentID,CoListAgentID,CoListingMemberID
,ListingOfficeID,CoListingOfficeID,SellingOfficeCommissionPercentage,Community,SellingAgentID,CoSellingAgentID,SellingOffice
,CoSellingOffice,ExpirationDate,FallThroughDate,PictureCount,WithdrawDate,Zoning,HowSold,Area)
 Select 1,575006,coalesce(s.BrandedVirtualTour,s.UnBrandedVirtualTour),IIF(s.GeoLatitude is null,null,TRY_CONVERT(decimal(9,6)
 ,s.GeoLatitude)),IIF(s.GeoLongitude is null,null,TRY_CONVERT(decimal(9,6),s.GeoLongitude)),s.[AcresNumOf],s.[DaysOnMarket]
 ,s.[DaysOnMarket],s.[FullBathNumOf],s.[HalfBathNumOf],s.[JLSCity],s.[JLSElementarySchool],s.[JLSHighSchool],s.[JLSMiddleSchool]
 ,s.[JLSParentPropTypeID],s.[JLSPostalCode],s.[JLSPropTypeID],s.[JLSShortSale],s.[JLSStreetDirPrefix],s.[JLSStreetSuffix]
 ,s.[LastModifiedDateTime],s.[ListDate],s.[ListingModTime],s.[ListPrice],s.[MLSNumber],s.[SoldDate],s.[SoldPrice],s.[StatusChangeDate]
 ,s.[StreetName],s.[StreetNumber],s.[UnderContractDate],s.[YearBuilt],TRY_CONVERT(decimal(19,2),replace(replace(s.taxes,'$',''),',',''))
 ,TRY_CONVERT(int,s.[OriginalListPrice]),TRY_CONVERT(int,s.Age),s.CancelDate,s.AgentID,s.CoListAgentID,s.CoListingMemberShortID
 ,s.ListingOfficeShortID,s.CoListingOfficeShortID,TRY_CONVERT(decimal(19,2),replace(s.SellingOfcComm,'%','')),s.SubDivision
 ,s.SellingAgentID,s.CoSellingAgentID,s.SellingOfficeShortID,s.CoSellingOfficeShortID,s.[ExpireDate],s.FallThroughDate,s.PictureCount
 ,s.WithdrawalDate,s.Zoning,HowSold=l.LongValue,s.Area
 From WaNCWAR.dbo.ResidentialIncome s
   left join WaNCWAR.dbo.Lookups l
     on s.HowSold = l.[Value] and l.[Lookup]= '20050811185432658746000000'

 --WaNWMLS
 Insert Into mls.Listing 
([MLSID],[BankOwned],[ShortSale],[YearBuilt],[StreetNumberModifier],[CDOM],[JLsDom],[IDX],[ShowAddress],[ShowMap],[CloseDate],[ListDate]
,[PendingDate],[StatusDate],[UpdateDate],[SquareFootage],[Bedrooms],[BathsTotal],[City],[StreetDirPrefix],[StreetDirSuffix],[HOADues]
,[StreetNumber],[Latitude],[MLSDBListingModTime],[MLSListingID],[Longitude],[ListPrice],[PriceDisplay],[LotSizeSF],[OriginalListPrice]
,[PostalCodeExt],[ClosePrice],[StreetTypeSuffix],[StreetName],[TaxID],[VirtualTour],[PostalCode],Age,Auction,ListAgentID
,CoListAgentID,ListingOfficeID,CoListingOfficeID,SellingOfficeCommissionPercentage,Community,SellingAgentID,CoSellingAgentID
,SellingOffice,CoSellingOffice,PictureCount,Area)
 Select 9,case when breo = 'y' then 1 else 0 end,case when parq = 'c' then 1 else 0 end,Case When YBT > 9999 Then Null Else YBT End
 ,coalesce(s.hsna,s.unt),IIF(s.CDOM < 32767,s.CDOM,null),IIF(s.CDOM < 32767,s.CDOM,null),IIF(s.NIA='N',0,1),IIF(s.SHOADR='N',0,1)
 ,IIF(s.SML='N',0,1),NullIf(s.CLO,'1800-01-01 00:00:00.000')
 ,NullIf(s.LD,'1800-01-01 00:00:00.000'),NullIf(s.PDR,'1800-01-01 00:00:00.000'),NullIf(s.SDT,'1800-01-01 00:00:00.000')
 ,NullIf(s.UD,'1800-01-01 00:00:00.000'),s.[ASF],s.[BR],s.[BTH],s.[CIT],s.[DRP],s.[DRS],s.[HOD],s.[HSN],s.[LAT],s.[ListingModTime]
 ,s.[LN],s.[LONG],s.[LP],s.[LP],s.[LSF],s.[OLP],s.[PL4],s.[SP],s.[SSUF],s.[STR],s.[TAX],s.[VIRT],s.[ZIP],s.YBT,s.Auction,s.[LAG]
 ,s.CLA,s.LO,s.COLO,try_convert(decimal(19,2),replace(replace(s.SOC,'%',''),'$','')),s.DSR,s.SAG,s.SCA,s.SO,s.SCO,s.PIC,s.AR
 From WaNWMLS.dbo.Business s

Insert Into mls.Listing 
([MLSID],[BankOwned],[ShortSale],[TaxYearBuilt],[YearBuilt],[StreetNumberModifier],[CDOM],[JLsDom],[IDX],[ShowAddress],[ShowMap]
,[CloseDate],[ListDate],[PendingDate],[StatusDate],[UpdateDate],[Bedrooms],[BathsTotal],[City],[StreetDirPrefix],[StreetDirSuffix]
,[HOADues],[StreetNumber],[Latitude],[MLSDBListingModTime],[MLSListingID],[Longitude],[ListPrice],[PriceDisplay],[LotSizeSF]
,[OriginalListPrice],[PostalCodeExt],[SquareFootage],[ClosePrice],[StreetTypeSuffix],[StreetName],[TaxID],[PropertyTax],[VirtualTour]
,[PostalCode],Age,Auction,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,SellingOfficeCommissionPercentage
,Community,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice,PictureCount,Area)
 Select 9,case when breo = 'y' then 1 else 0 end,case when parq = 'c' then 1 else 0 end,Case When TXY > 9999 Then Null Else TXY End
 ,Case When YBT > 9999 Then Null Else YBT End,coalesce(s.hsna,s.unt),IIF(s.CDOM < 32767,s.CDOM,null),IIF(s.CDOM < 32767,s.CDOM,null)
 ,IIF(s.NIA='N',0,1),IIF(s.SHOADR='N',0,1),IIF(s.SML='N',0,1),NullIf(s.CLO,'1800-01-01 00:00:00.000'),NullIf(s.LD,'1800-01-01 00:00:00.000')
 ,NullIf(s.PDR,'1800-01-01 00:00:00.000'),NullIf(s.SDT,'1800-01-01 00:00:00.000'),NullIf(s.UD,'1800-01-01 00:00:00.000'),s.[BR],s.[BTH]
 ,s.[CIT],s.[DRP],s.[DRS],s.[HOD],s.[HSN],s.[LAT],s.[ListingModTime],s.[LN],s.[LONG],s.[LP],s.[LP],s.[LSF],s.[OLP],s.[PL4],s.[SIZ],s.[SP]
 ,s.[SSUF],s.[STR],s.[TAX],s.[TX],s.[VIRT],s.[ZIP],s.YBT,s.Auction,s.[LAG],s.CLA,s.LO,s.COLO
 ,try_convert(decimal(19,2),replace(replace(s.SOC,'%',''),'$','')),s.DSR,s.SAG,s.SCA,s.SO,s.SCO,s.PIC,s.AR
 From WaNWMLS.dbo.Commercial s

Insert Into mls.Listing 
([MLSID],[BankOwned],[ShortSale],[TaxYearBuilt],[YearBuilt],[StreetNumberModifier],[Garage],[CDOM],[JLsDom],[IDX],[ShowAddress],[ShowMap]
,[CloseDate],[ListDate],[PendingDate],[StatusDate],[UpdateDate],[BathsPartial],[SquareFootage],[Bedrooms],[BathsTotal],[City]
,[StreetDirPrefix],[StreetDirSuffix],[ElementarySchool],[BathsFull],[Fireplaces],[HOADues],[StreetNumber],[MiddleSchool],[Latitude]
,[MLSDBListingModTime],[MLSListingID],[Longitude],[ListPrice],[PriceDisplay],[LotSizeSF],[ParkingSpaces],[OriginalListPrice]
,[PostalCodeExt],[HighSchool],[ClosePrice],[StreetTypeSuffix],[StreetName],[TaxID],[PropertyTax],[VirtualTour],[PostalCode],Age,Auction
,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,SellingOfficeCommissionPercentage,Community,SellingAgentID
,CoSellingAgentID,SellingOffice,CoSellingOffice,NewConstruction,PictureCount,Waterfront,Area)
 Select 9,case when breo = 'y' then 1 else 0 end,case when parq = 'c' then 1 else 0 end,Case When TXY > 9999 Then Null Else TXY End
 ,Case When YBT > 9999 Then Null Else YBT End,coalesce(s.hsna,s.unt),iif(pkg like '%D%' or pkg like '%G%',1,0),IIF(s.CDOM < 32767,s.CDOM
 ,null),IIF(s.CDOM < 32767,s.CDOM,null),IIF(s.NIA='N',0,1),IIF(s.SHOADR='N',0,1),IIF(s.SML='N',0,1),NullIf(s.CLO,'1800-01-01 00:00:00.000')
 ,NullIf(s.LD,'1800-01-01 00:00:00.000'),NullIf(s.PDR,'1800-01-01 00:00:00.000'),NullIf(s.SDT,'1800-01-01 00:00:00.000')
 ,NullIf(s.UD,'1800-01-01 00:00:00.000'),QBT+HBT+TQBT,s.[ASF],s.[BR],s.[BTH],s.[CIT],s.[DRP],s.[DRS],s.[EL],s.[FBT],s.[FP],s.[HOD]
 ,s.[HSN],s.[JH],s.[LAT],s.[ListingModTime],s.[LN],s.[LONG],s.[LP],s.[LP],s.[LSF],s.[NAS],s.[OLP],s.[PL4],s.[SH],s.[SP],s.[SSUF],s.[STR]
 ,s.[TAX],s.[TX],s.[VIRT],s.[ZIP],s.YBT,s.Auction,s.[LAG],s.CLA,s.LO,s.COLO,try_convert(decimal(19,2),replace(replace(s.SOC,'%',''),'$',''))
 ,s.DSR,s.SAG,s.SCA,s.SO,s.SCO,coalesce(s.NC,s.NewConstruction),s.PIC,s.WFT,s.AR
 From WaNWMLS.dbo.Condominium s

Insert Into mls.Listing 
([MLSID],[BankOwned],[ShortSale],[TaxYearBuilt],[YearBuilt],[StreetNumberModifier],[Garage],[CDOM],[JLsDom],[IDX],[ShowAddress],[ShowMap]
,[CloseDate],[ListDate],[PendingDate],[StatusDate],[UpdateDate],[BathsPartial],[SquareFootage],[Bedrooms],[BathsTotal],[City]
,[StreetDirPrefix],[StreetDirSuffix],[ElementarySchool],[BathsFull],[Fireplaces],[ParkingSpaces],[StreetNumber],[MiddleSchool],[Latitude]
,[MLSDBListingModTime],[MLSListingID],[Longitude],[ListPrice],[PriceDisplay],[LotSizeSF],[OriginalListPrice],[PostalCodeExt],[HighSchool]
,[ClosePrice],[StreetTypeSuffix],[StreetName],[TaxID],[PropertyTax],[VirtualTour],[PostalCode],Age,Auction,ListAgentID
,CoListAgentID,ListingOfficeID,CoListingOfficeID,SellingOfficeCommissionPercentage,Community,SellingAgentID,CoSellingAgentID
,SellingOffice,CoSellingOffice,NewConstruction,PictureCount,Area)
 Select 9,case when breo = 'y' then 1 else 0 end,case when parq = 'c' then 1 else 0 end,Case When TXY > 9999 Then Null Else TXY End
 ,Case When YBT > 9999 Then Null Else YBT End,coalesce(s.hsna,s.unt),iif(gr like '%D%' or gr like '%E%' or gr like '%F%' or gr like '%G%',1,0)
 ,IIF(s.CDOM < 32767,s.CDOM,null),IIF(s.CDOM < 32767,s.CDOM,null),IIF(s.NIA='N',0,1),IIF(s.SHOADR='N',0,1),IIF(s.SML='N',0,1)
 ,NullIf(s.CLO,'1800-01-01 00:00:00.000'),NullIf(s.LD,'1800-01-01 00:00:00.000'),NullIf(s.PDR,'1800-01-01 00:00:00.000')
 ,NullIf(s.SDT,'1800-01-01 00:00:00.000'),NullIf(s.UD,'1800-01-01 00:00:00.000'),QBT+HBT+TQBT,s.[ASF],s.[BR],s.[BTH],s.[CIT],s.[DRP]
 ,s.[DRS],s.[EL],s.[FBT],s.[FP],s.[GAR],s.[HSN],s.[JH],s.[LAT],s.[ListingModTime],s.[LN],s.[LONG],s.[LP],s.[LP],s.[LSF],s.[OLP],s.[PL4]
 ,s.[SH],s.[SP],s.[SSUF],s.[STR],s.[TAX],s.[TX],s.[VIRT],s.[ZIP],s.YBT,s.Auction,s.[LAG],s.CLA,s.LO,s.COLO
 ,try_convert(decimal(19,2),replace(replace(s.SOC,'%',''),'$','')),s.DSR,s.SAG,s.SCA,s.SO,s.SCO,coalesce(s.NC,s.NewConstruction)
 ,s.PIC,s.AR
 From WaNWMLS.dbo.FarmRanch s

Insert Into mls.Listing 
([MLSID],[BankOwned],[ShortSale],[TaxYearBuilt],[YearBuilt],[StreetNumberModifier],[Garage],[CDOM],[JLsDom],[IDX],[ShowAddress],[ShowMap]
,[CloseDate],[ListDate],[PendingDate],[StatusDate],[UpdateDate],[BathsPartial],[SquareFootage],[Bedrooms],[BathsTotal],[City]
,[StreetDirPrefix],[StreetDirSuffix],[ElementarySchool],[BathsFull],[Fireplaces],[StreetNumber],[MiddleSchool],[Latitude]
,[MLSDBListingModTime],[MLSListingID],[Longitude],[ListPrice],[PriceDisplay],[LotSizeSF],[ParkingSpaces],[OriginalListPrice]
,[PostalCodeExt],[HighSchool],[ClosePrice],[StreetTypeSuffix],[StreetName],[TaxID],[PropertyTax],[VirtualTour],[PostalCode],Age,Auction
,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,SellingOfficeCommissionPercentage,Community,SellingAgentID
,CoSellingAgentID,SellingOffice,CoSellingOffice,PictureCount,Waterfront,Area)
 Select 9,case when breo = 'y' then 1 else 0 end,case when parq = 'c' then 1 else 0 end,Case When TXY > 9999 Then Null Else TXY End
 ,Case When YBT > 9999 Then Null Else YBT End,coalesce(s.hsna,s.unt),iif(pkg like '%D%' or pkg like '%G%',1,0),IIF(s.CDOM < 32767,s.CDOM,null)
 ,IIF(s.CDOM < 32767,s.CDOM,null),IIF(s.NIA='N',0,1),IIF(s.SHOADR='N',0,1),IIF(s.SML='N',0,1),NullIf(s.CLO,'1800-01-01 00:00:00.000')
 ,NullIf(s.LD,'1800-01-01 00:00:00.000'),NullIf(s.PDR,'1800-01-01 00:00:00.000'),NullIf(s.SDT,'1800-01-01 00:00:00.000')
 ,NullIf(s.UD,'1800-01-01 00:00:00.000'),QBT+HBT+TQBT,s.[ASF],s.[BR],s.[BTH],s.[CIT],s.[DRP],s.[DRS],s.[EL],s.[FBT],s.[FP],s.[HSN],s.[JH]
 ,s.[LAT],s.[ListingModTime],s.[LN],s.[LONG],s.[LP],s.[LP],s.[LSF],s.[NAS],s.[OLP],s.[PL4],s.[SH],s.[SP],s.[SSUF],s.[STR],s.[TAX],s.[TX]
 ,s.[VIRT],s.[ZIP],s.YBT,s.Auction,s.[LAG],s.CLA,s.LO,s.COLO,try_convert(decimal(19,2),replace(replace(s.SOC,'%',''),'$',''))
 ,s.DSR,s.SAG,s.SCA,s.SO,s.SCO,s.PIC,s.WFT,s.AR
 From WaNWMLS.dbo.Manufactured s

Insert Into mls.Listing 
([MLSID],[BankOwned],[ShortSale],[TaxYearBuilt],[YearBuilt],[StreetNumberModifier],[CDOM],[JLsDom],[IDX],[ShowAddress],[ShowMap]
,[CloseDate],[ListDate],[PendingDate],[StatusDate],[UpdateDate],[BathsPartial],[SquareFootage],[Bedrooms],[BathsTotal],[City]
,[StreetDirPrefix],[StreetDirSuffix],[ElementarySchool],[BathsFull],[GarageSpaces],[HOADues],[StreetNumber],[MiddleSchool],[Latitude]
,[MLSDBListingModTime],[MLSListingID],[Longitude],[ListPrice],[PriceDisplay],[LotSizeSF],[OriginalListPrice],[PostalCodeExt],[HighSchool]
,[ClosePrice],[StreetTypeSuffix],[StreetName],[TaxID],[PropertyTax],[VirtualTour],[PostalCode],Age,Auction,ListAgentID
,CoListAgentID,ListingOfficeID,CoListingOfficeID,SellingOfficeCommissionPercentage,Community,SellingAgentID,CoSellingAgentID
,SellingOffice,CoSellingOffice,NewConstruction,PictureCount,Waterfront,Area)
 Select 9,case when breo = 'y' then 1 else 0 end,case when parq = 'c' then 1 else 0 end,Case When TXY > 9999 Then Null Else TXY End
 ,Case When YBT > 9999 Then Null Else YBT End,coalesce(s.hsna,s.unt),IIF(s.CDOM < 32767,s.CDOM,null),IIF(s.CDOM < 32767,s.CDOM,null)
 ,IIF(s.NIA='N',0,1),IIF(s.SHOADR='N',0,1),IIF(s.SML='N',0,1),NullIf(s.CLO,'1800-01-01 00:00:00.000'),NullIf(s.LD,'1800-01-01 00:00:00.000')
 ,NullIf(s.PDR,'1800-01-01 00:00:00.000'),NullIf(s.SDT,'1800-01-01 00:00:00.000'),NullIf(s.UD,'1800-01-01 00:00:00.000'),QBT+HBT+TQBT
 ,s.[ASF],s.[BR],s.[BTH],s.[CIT],s.[DRP],s.[DRS],s.[EL],s.[FBT],s.[GSP],s.[HOD],s.[HSN],s.[JH],s.[LAT],s.[ListingModTime],s.[LN],s.[LONG]
 ,s.[LP],s.[LP],s.[LSF],s.[OLP],s.[PL4],s.[SH],s.[SP],s.[SSUF],s.[STR],s.[TAX],s.[TX],s.[VIRT],s.[ZIP],s.YBT,s.Auction,s.[LAG],s.CLA
 ,s.LO,s.COLO,try_convert(decimal(19,2),replace(replace(s.SOC,'%',''),'$','')),s.DSR,s.SAG,s.SCA,s.SO,s.SCO,coalesce(s.NC,s.NewConstruction)
 ,s.PIC,s.WFT,s.AR
 From WaNWMLS.dbo.MultiFamily s

Insert Into mls.Listing 
([MLSID],[YearBuilt],[StreetNumberModifier],[Garage],[CDOM],[JLsDom],[IDX],[ShowAddress],[ShowMap],[CloseDate],[ListDate],[PendingDate]
,[StatusDate],[UpdateDate],[SquareFootage],[Bedrooms],[BathsTotal],[City],[StreetDirPrefix],[StreetDirSuffix],[ElementarySchool]
,[Fireplaces],[StreetNumber],[MiddleSchool],[Latitude],[MLSDBListingModTime],[MLSListingID],[Longitude],[ListPrice],[PriceDisplay]
,[LotSizeSF],[ParkingSpaces],[OriginalListPrice],[PostalCodeExt],[HighSchool],[ClosePrice],[StreetTypeSuffix],[StreetName],[TaxID]
,[PostalCode],Age,Auction,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,SellingOfficeCommissionPercentage
,Community,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice,PictureCount,Waterfront,Area)
 Select 9,Case When YBT > 9999 Then Null Else YBT End,coalesce(s.hsna,s.unt),iif(gr like '%D%' or gr like '%E%' or gr like '%F%' or gr like '%G%',1,0)
 ,IIF(s.CDOM < 32767,s.CDOM,null),IIF(s.CDOM < 32767,s.CDOM,null),IIF(s.NIA='N',0,1),IIF(s.SHOADR='N',0,1),IIF(s.SML='N',0,1)
 ,NullIf(s.CLO,'1800-01-01 00:00:00.000'),NullIf(s.LD,'1800-01-01 00:00:00.000'),NullIf(s.PDR,'1800-01-01 00:00:00.000')
 ,NullIf(s.SDT,'1800-01-01 00:00:00.000'),NullIf(s.UD,'1800-01-01 00:00:00.000'),s.[ASF],s.[BR],s.[BTH],s.[CIT],s.[DRP],s.[DRS],s.[EL]
 ,s.[FP],s.[HSN],s.[JH],s.[LAT],s.[ListingModTime],s.[LN],s.[LONG],s.[LP],s.[LP],s.[LSF],s.[NAS],s.[OLP],s.[PL4],s.[SH],s.[SP],s.[SSUF]
 ,s.[STR],s.[TAX],s.[ZIP],s.YBT,s.Auction,s.[LAG],s.CLA,s.LO,s.COLO,try_convert(decimal(19,2),replace(replace(s.SOC,'%',''),'$',''))
 ,s.DSR,s.SAG,s.SCA,s.SO,s.SCO,s.PIC,s.WFT,s.AR
 From WaNWMLS.dbo.Rental s

Insert Into mls.Listing 
([MLSID],[BankOwned],[ShortSale],[TaxYearBuilt],[YearBuilt],[StreetNumberModifier],[Garage],[CDOM],[JLsDom],[IDX],[ShowAddress],[ShowMap]
,[CloseDate],[ListDate],[PendingDate],[StatusDate],[UpdateDate],[BathsPartial],[SquareFootage],[Bedrooms],[BathsTotal],[City]
,[StreetDirPrefix],[StreetDirSuffix],[ElementarySchool],[BathsFull],[Fireplaces],[ParkingSpaces],[HOADues],[StreetNumber],[MiddleSchool]
,[Latitude],[MLSDBListingModTime],[MLSListingID],[Longitude],[ListPrice],[PriceDisplay],[LotSizeSF],[OriginalListPrice],[PostalCodeExt]
,[HighSchool],[ClosePrice],[StreetTypeSuffix],[StreetName],[TaxID],[PropertyTax],[VirtualTour],[PostalCode],Age,Auction,BuilderName
,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,SellingOfficeCommissionPercentage,Community,SellingAgentID,CoSellingAgentID
,SellingOffice,CoSellingOffice,NewConstruction,PictureCount,Waterfront,Zoning,Area)
 Select 9,case when breo = 'y' then 1 else 0 end,case when parq = 'c' then 1 else 0 end,Case When TXY > 9999 Then Null Else TXY End,
 Case When YBT > 9999 Then Null Else YBT End,coalesce(s.hsna,s.unt),iif(gr like '%D%' or gr like '%E%' or gr like '%F%' or gr like '%G%',1,0)
 ,IIF(s.CDOM < 32767,s.CDOM,null),IIF(s.CDOM < 32767,s.CDOM,null),IIF(s.NIA='N',0,1),IIF(s.SHOADR='N',0,1),IIF(s.SML='N',0,1)
 ,NullIf(s.CLO,'1800-01-01 00:00:00.000'),NullIf(s.LD,'1800-01-01 00:00:00.000'),NullIf(s.PDR,'1800-01-01 00:00:00.000')
 ,NullIf(s.SDT,'1800-01-01 00:00:00.000'),NullIf(s.UD,'1800-01-01 00:00:00.000'),QBT+HBT+TQBT,s.[ASF],s.[BR],s.[BTH],s.[CIT],s.[DRP]
 ,s.[DRS],s.[EL],s.[FBT],s.[FP],s.[GAR],s.[HOD],s.[HSN],s.[JH],s.[LAT],s.[ListingModTime],s.[LN],s.[LONG],s.[LP],s.[LP],s.[LSF],s.[OLP]
 ,s.[PL4],s.[SH],s.[SP],s.[SSUF],s.[STR],s.[TAX],s.[TX],s.[VIRT],s.[ZIP],s.YBT,s.Auction,s.BLD,s.[LAG],s.CLA,s.LO,s.COLO
 ,try_convert(decimal(19,2),replace(replace(s.SOC,'%',''),'$','')),s.DSR,s.SAG,s.SCA,s.SO,s.SCO,coalesce(s.NC,s.NewConstruction),s.PIC
 ,s.WFT,s.ZNC,s.AR
 From WaNWMLS.dbo.Residential s

Insert Into mls.Listing 
([MLSID],[YearBuilt],[StreetNumberModifier],[Garage],[CDOM],[JLsDom],[IDX],[ShowAddress],[ShowMap],[CloseDate],[ListDate],[PendingDate]
,[StatusDate],[UpdateDate],[SquareFootage],[Bedrooms],[BathsTotal],[City],[StreetDirPrefix],[StreetDirSuffix],[BathsFull],[Fireplaces]
,[HOADues],[StreetNumber],[Latitude],[MLSDBListingModTime],[MLSListingID],[Longitude],[ListPrice],[PriceDisplay],[LotSizeSF]
,[ParkingSpaces],[OriginalListPrice],[PostalCodeExt],[ClosePrice],[StreetTypeSuffix],[StreetName],[TaxID],[VirtualTour],[PostalCode]
,Age,Auction,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,SellingOfficeCommissionPercentage,Community
,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice,NewConstruction,PictureCount,Waterfront,Area)
 Select 9,Case When YBT > 9999 Then Null Else YBT End,coalesce(s.hsna,s.unt),iif(pkg like '%D%' or pkg like '%G%',1,0)
 ,IIF(s.CDOM < 32767,s.CDOM,null),IIF(s.CDOM < 32767,s.CDOM,null),IIF(s.NIA='N',0,1),IIF(s.SHOADR='N',0,1),IIF(s.SML='N',0,1)
 ,NullIf(s.CLO,'1800-01-01 00:00:00.000'),NullIf(s.LD,'1800-01-01 00:00:00.000'),NullIf(s.PDR,'1800-01-01 00:00:00.000')
 ,NullIf(s.SDT,'1800-01-01 00:00:00.000'),NullIf(s.UD,'1800-01-01 00:00:00.000'),s.[ASF],s.[BR],s.[BTH],s.[CIT],s.[DRP],s.[DRS],s.[FBT]
 ,s.[FP],s.[HOD],s.[HSN],s.[LAT],s.[ListingModTime],s.[LN],s.[LONG],s.[LP],s.[LP],s.[LSF],s.[NAS],s.[OLP],s.[PL4],s.[SP],s.[SSUF],s.[STR]
 ,s.[TAX],s.[VIRT],s.[ZIP],s.YBT,s.Auction,s.[LAG],s.CLA,s.LO,s.COLO,try_convert(decimal(19,2),replace(replace(s.SOC,'%',''),'$',''))
 ,s.DSR,s.SAG,s.SCA,s.SO,s.SCO,coalesce(s.NC,s.NewConstruction),s.PIC,s.WFT,s.AR
 From WaNWMLS.dbo.TimeShare s

Insert Into mls.Listing 
([MLSID],[BankOwned],[ShortSale],[TaxYearBuilt],[YearBuilt],[StreetNumberModifier],[BathsPartial],[CDOM],[JLsDom],[IDX],[ShowAddress]
,[ShowMap],[CloseDate],[ListDate],[PendingDate],[StatusDate],[UpdateDate],[SquareFootage],[Bedrooms],[BathsTotal],[City]
,[StreetDirPrefix],[StreetDirSuffix],[ElementarySchool],[BathsFull],[StreetNumber],[MiddleSchool],[Latitude],[MLSDBListingModTime]
,[MLSListingID],[Longitude],[ListPrice],[PriceDisplay],[LotSizeSF],[OriginalListPrice],[PostalCodeExt],[HighSchool],[ClosePrice]
,[StreetTypeSuffix],[StreetName],[TaxID],[PropertyTax],[VirtualTour],[PostalCode],Age,Auction,ListAgentID,CoListAgentID
,ListingOfficeID,CoListingOfficeID,SellingOfficeCommissionPercentage,Community,SellingAgentID,CoSellingAgentID,SellingOffice
,CoSellingOffice,PictureCount,Waterfront,Area)
 Select 9,case when breo = 'y' then 1 else 0 end,case when parq = 'c' then 1 else 0 end,Case When TXY > 9999 Then Null Else TXY End
 ,Case When YBT > 9999 Then Null Else YBT End,coalesce(s.hsna,s.unt),HBT+TQBT,IIF(s.CDOM < 32767,s.CDOM,null),IIF(s.CDOM < 32767,s.CDOM
 ,null),IIF(s.NIA='N',0,1),IIF(s.SHOADR='N',0,1),IIF(s.SML='N',0,1),NullIf(s.CLO,'1800-01-01 00:00:00.000'),NullIf(s.LD,'1800-01-01 00:00:00.000')
 ,NullIf(s.PDR,'1800-01-01 00:00:00.000'),NullIf(s.SDT,'1800-01-01 00:00:00.000'),NullIf(s.UD,'1800-01-01 00:00:00.000'),s.[ASF],s.[BR]
 ,s.[BTH],s.[CIT],s.[DRP],s.[DRS],s.[EL],s.[FBT],s.[HSN],s.[JH],s.[LAT],s.[ListingModTime],s.[LN],s.[LONG],s.[LP],s.[LP],s.[LSF],s.[OLP]
 ,s.[PL4],s.[SH],s.[SP],s.[SSUF],s.[STR],s.[TAX],s.[TX],s.[VIRT],s.[ZIP],s.YBT,s.Auction,s.[LAG],s.CLA,s.LO,s.COLO
 ,try_convert(decimal(19,2),replace(replace(s.SOC,'%',''),'$','')),s.DSR,s.SAG,s.SCA,s.SO,s.SCO,s.PIC,s.WFT,s.AR
 From WaNWMLS.dbo.VacantLand s

 --WaOLS
 Insert Into mls.Listing
 ([ShowAddress],[ShowMap],[MLSID],[City],[YearBuilt],[IDX],
 [BankOwned],[ShortSale],[StreetAddress],[StreetAddress2],[StreetDirPrefix],[StreetNumber]
 ,[StreetName],[ListPrice],[Bedrooms],[CDOM],[JLsDom],[CloseDate],[PendingDate],[DOM]
 ,[BathsFull],[BathsPartial],[Garage],[GarageSpaces],[PropTypeID],[SchoolDistrict]
 ,[Latitude],[ListDate],[MLSDBListingModTime],[Longitude],[MLSListingID],[Acreage]
 ,[OriginalListPrice],[ClosePrice],[SquareFootage],[StatusDate],[PropertyTax],[TaxID]
 ,[UpdateDate],[VirtualTour],[PostalCode],[BathsTotal],AgentHitCount,ListAgentID
 ,CoListAgentID,ListingOfficeID,CoListingOfficeID,SellingOfficeCommissionPercentage
 ,Community,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice,ExpirationDate
 ,PictureCount,Waterfront,Zoning,HowSold,Area)
 Select 1,1,700002,Coalesce(s.PrimaryCity,s.City),IIF(Len(s.YearBuilt)=4,s.YearBuilt,Null)
 ,IIF(s.IdxInclude=1,0,1),IIF(s.REO='Y',1,0),IIF(s.ShortSale='Y',1,0),s.[Address]
 ,s.[Address2],s.[AddressDirection],s.[AddressNumber],s.[AddressStreet],s.[AskingPrice]
 ,s.[Beds],s.[CDOM],s.[CDOM],s.[ClosingDate],s.[ContractDate],s.[DOM],s.[FullBaths]
 ,s.[HalfBaths],s.[JLSGarage],s.[JLSGarageSpaces],s.[JLSPropTypeID],s.[JLSSchoolDistrict]
 ,s.[Latitude],s.[ListingDate],s.[ListingModTime],s.[Longitude],s.[MLSNumber],s.[NumAcres]
 ,s.[OriginalPrice],s.[SoldPrice],s.[SquareFeet],s.[StatusDate],s.[Taxes],s.[TaxPropertyID]
 ,s.[UpdateDate],s.[VirtualTour],s.[Zip],TRY_CONVERT(decimal(5,2),s.FullBaths + .5 * s.HalfBaths)
 ,s.AgentHitCount,s.ListAgent1,s.ListAgent2,s.ListOffice1,s.ListOffice2,s.SellSideComm
 ,s.SubDivision,s.SellingAgent1,s.SellingAgent2,s.SellingOffice1,s.SellingOffice2
 ,s.ExpirationDate,s.PictureCount,Waterfront=l.LongValue,s.ZoningCode,s.HowSold,s.Area
From WaOLS.dbo.Residential s
left join WaOLS.dbo.Lookups l
 on s.Waterfront = l.[Value] and [Lookup] = 'Keyword_1_5' and [Resource] = 'Property'

Insert Into mls.Listing
([ShowAddress],[ShowMap],[MLSID],[City],[IDX],[BankOwned],[ShortSale],[StreetAddress]
,[StreetAddress2],[StreetDirPrefix],[StreetNumber],[StreetName],[ListPrice],[CDOM]
,[JLsDom],[CloseDate],[PendingDate],[DOM],[PropTypeID],[SchoolDistrict],[Latitude]
,[ListDate],[MLSDBListingModTime],[Longitude],[MLSListingID],[Acreage],[OriginalListPrice]
,[ClosePrice],[StatusDate],[PropertyTax],[TaxID],[UpdateDate],[VirtualTour],[PostalCode]
,AgentHitCount,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID
,SellingOfficeCommissionPercentage,Community,SellingAgentID,CoSellingAgentID,SellingOffice
,CoSellingOffice,ExpirationDate,PictureCount,Waterfront,Zoning,HowSold,Area)
 Select 1,1,700002,Coalesce(s.PrimaryCity,s.City),IIF(s.IdxInclude=1,0,1)
 ,IIF(s.REO='Y',1,0),IIF(s.ShortSale='Y',1,0),s.[Address],s.[Address2],s.[AddressDirection]
 ,s.[AddressNumber],s.[AddressStreet],s.[AskingPrice],s.[CDOM],s.[CDOM],s.[ClosingDate]
 ,s.[ContractDate],s.[DOM],s.[JLSPropTypeID],s.[JLSSchoolDistrict],s.[Latitude]
 ,s.[ListingDate],s.[ListingModTime],s.[Longitude],s.[MLSNumber],s.[NumAcres]
 ,s.[OriginalPrice],s.[SoldPrice],s.[StatusDate],s.[Taxes],s.[TaxPropertyID],s.[UpdateDate]
 ,s.[VirtualTour],s.[Zip],s.AgentHitCount,s.ListAgent1,s.ListAgent2,s.ListOffice1
 ,s.ListOffice2,s.SellSideComm,s.SubDivision,s.SellingAgent1,s.SellingAgent2
 ,s.SellingOffice1,s.SellingOffice2,s.ExpirationDate,s.PictureCount,Waterfront=l.LongValue
 ,s.ZoningCode,s.HowSold,s.Area
 From WaOLS.dbo.Land s 
left join WaOLS.dbo.Lookups l
 on s.Waterfront = l.[Value] and [Lookup] = 'Keyword_1_5' and [Resource] = 'Property'

Insert Into mls.Listing 
([ShowAddress],[ShowMap],[MLSID],[City],[YearBuilt],[IDX],[BankOwned],[ShortSale]
,[StreetAddress],[StreetAddress2],[StreetDirPrefix],[StreetNumber],[StreetName],[ListPrice]
,[CDOM],[JLsDom],[CloseDate],[PendingDate],[DOM],[PropTypeID],[SchoolDistrict],[Latitude]
,[ListDate],[MLSDBListingModTime],[Longitude],[MLSListingID],[Acreage],[OriginalListPrice]
,[ClosePrice],[SquareFootage],[StatusDate],[PropertyTax],[TaxID],[UpdateDate],[VirtualTour]
,[PostalCode],AgentHitCount,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID
,SellingOfficeCommissionPercentage,SellingAgentID,CoSellingAgentID,SellingOffice
,CoSellingOffice,ExpirationDate,PictureCount,Zoning,HowSold,Area)
 Select 1,1,700002,Coalesce(s.PrimaryCity,s.City),IIF(Len(s.YearBuilt)=4,s.YearBuilt,Null)
 ,IIF(s.IdxInclude=1,0,1),IIF(s.REO='Y',1,0),IIF(s.ShortSale='Y',1,0),s.[Address]
 ,s.[Address2],s.[AddressDirection],s.[AddressNumber],s.[AddressStreet],s.[AskingPrice]
 ,s.[CDOM],s.[CDOM],s.[ClosingDate],s.[ContractDate],s.[DOM],s.[JLSPropTypeID]
 ,s.[JLSSchoolDistrict],s.[Latitude],s.[ListingDate],s.[ListingModTime],s.[Longitude]
 ,s.[MLSNumber],s.[NumAcres],s.[OriginalPrice],s.[SoldPrice],s.[SquareFeet],s.[StatusDate]
 ,s.[Taxes],s.[TaxPropertyID],s.[UpdateDate],s.[VirtualTour],s.[Zip],s.AgentHitCount
 ,s.ListAgent1,s.ListAgent2,s.ListOffice1,s.ListOffice2,s.SellSideComm
 ,s.SellingAgent1,s.SellingAgent2,s.SellingOffice1,s.SellingOffice2,s.ExpirationDate
 ,s.PictureCount,s.ZoningCode,s.HowSold,s.Area
 From WaOLS.dbo.Commercial s

Insert Into mls.Listing 
([ShowAddress],[ShowMap],[MLSID],[City],[YearBuilt],[IDX],[BankOwned],[ShortSale]
,[StreetAddress],[StreetAddress2],[StreetDirPrefix],[StreetNumber],[StreetName],[ListPrice]
,[CDOM],[JLsDom],[CloseDate],[PendingDate],[DOM],[PropTypeID],[SchoolDistrict],[Latitude]
,[ListDate],[MLSDBListingModTime],[Longitude],[MLSListingID],[Acreage],[OriginalListPrice]
,[ClosePrice],[SquareFootage],[StatusDate],[PropertyTax],[TaxID],[UpdateDate],[VirtualTour]
,[PostalCode],AgentHitCount,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID
,SellingOfficeCommissionPercentage,Community,SellingAgentID,CoSellingAgentID,SellingOffice
,CoSellingOffice,ExpirationDate,PictureCount,Waterfront,Zoning,HowSold,Area)
 Select 1,1,700002,Coalesce(s.PrimaryCity,s.City),IIF(Len(s.YearBuilt)=4,s.YearBuilt,Null)
 ,IIF(s.IdxInclude=1,0,1),IIF(s.REO='Y',1,0),IIF(s.ShortSale='Y',1,0),s.[Address]
 ,s.[Address2],s.[AddressDirection],s.[AddressNumber],s.[AddressStreet],s.[AskingPrice]
 ,s.[CDOM],s.[CDOM],s.[ClosingDate],s.[ContractDate],s.[DOM],s.[JLSPropTypeID]
 ,s.[JLSSchoolDistrict],s.[Latitude],s.[ListingDate],s.[ListingModTime],s.[Longitude]
 ,s.[MLSNumber],s.[NumAcres],s.[OriginalPrice],s.[SoldPrice],s.[SquareFeet],s.[StatusDate]
 ,s.[Taxes],s.[TaxPropertyID],s.[UpdateDate],s.[VirtualTour],s.[Zip],s.AgentHitCount
 ,s.ListAgent1,s.ListAgent2,s.ListOffice1,s.ListOffice2,s.SellSideComm,s.SubDivision
 ,s.SellingAgent1,s.SellingAgent2,s.SellingOffice1,s.SellingOffice2,s.ExpirationDate
 ,s.PictureCount,Waterfront=l.LongValue,s.ZoningCode,s.HowSold,s.Area
 From WaOLS.dbo.Multifamily s
left join WaOLS.dbo.Lookups l
 on s.Waterfront = l.[Value] and [Lookup] = 'Keyword_1_5' and [Resource] = 'Property'


 --WaSAR
 Insert Into mls.Listing
([MLSID],[IDX],[City],[PropertyTax],[BankOwned],[ShowAddress],[ShowMap],[ShortSale]
,[PostalCode],[BathsTotal],[Bedrooms],[StreetAddress],[StreetAddress2],[StreetDirPrefix]
,[StreetNumber],[StreetName],[LotSizeSF],[ListPrice],[CloseDate],[PendingDate],[DOM]
,[JLsDom],[ElementarySchool],[Garage],[PropTypeID],[StreetTypeSuffix],[MiddleSchool]
,[Latitude],[ListDate],[MLSDBListingModTime],[Longitude],[MLSListingID],[Acreage]
,[Fireplaces],[OriginalListPrice],[SchoolDistrict],[ClosePrice],[SquareFootage]
,[HighSchool],[StatusDate],[TaxID],[UpdateDate],[VirtualTour],[YearBuilt],[GarageSpaces]
,AgentHitCount,BodyofWaterName,BuilderName,ListAgentID,CoListAgentID,CoListAgentID2
,ListingOfficeID,CoListingOfficeID,Community,SellingAgentID,CoSellingAgentID
,CoSellingAgentID2,SellingOffice,CoSellingOffice,CoSellingOffice2,ExpirationDate
,NewConstruction,PictureCount,Waterfront,Zoning,HowSold,Area)
 Select 128,abs(s.idxinclude-1),Coalesce(s.JLSCity,s.City)
 ,IIF(isnumeric(s.Taxes) = 1,Replace(Replace(s.Taxes,'$',''),',',''),Null)
 ,IIF(s.BankApproved='Y',1,0),IIF(s.DisplayAddress=1,0,1),IIF(s.DisplayAddress=1,0,1)
 ,IIF(s.PotentialShortSale='Y',1,0),Left(s.Zip,5),Replace(s.Bathrooms,'+','')
 ,Replace(s.Bedrooms,'+',''),s.[Address],s.[Address2],s.[AddressDirection],s.[AddressNumber]
 ,s.[AddressStreet],s.[ApxLotSizeSqFt],s.[AskingPrice],s.[ClosingDate],s.[ContractDate]
 ,s.[DOM],s.[DOM],s.[Elementary],s.[JLSGarage],s.[JLSPropTypeID],s.[JLSStreetTypeSuffix]
 ,s.[JrHigh],s.[Latitude],s.[ListingDate],s.[ListingModTime],s.[Longitude],s.[MLSNumber]
 ,s.[NumAcres],s.[NumberFireplaces],s.[OriginalPrice],s.[SchoolDistrict],s.[SoldPrice]
 ,s.[SquareFeet],s.[SrHigh],s.[StatusDate],s.[TaxPropertyID],s.[UpdateDate],s.[VTourURL]
 ,s.[YearBuilt],TRY_CONVERT(int, replace(s.Garsize,'+','')),s.AgentHitCount,s.BodyofWaterName
 ,s.BuilderName,s.ListAgent1,s.ListAgent2,s.ListAgent3,s.ListOffice1,s.ListOffice2
 ,s.CommonInterestCommunity,s.SellingAgent1,s.SellingAgent2,s.SellingAgent3,s.SellingOffice1
 ,s.SellingOffice2,s.SellingOffice3,s.ExpirationDate,s.NewConstruction,s.PictureCount
 ,WtrfrontProp=l1.LongValue,s.[Zone],HowSold=l2.LongValue,s.Area
 From WaSAR.dbo.Residential s
 left join WaSAR.dbo.Lookups l1
   on l1.[Value] = case when charindex(',',s.WtrfrontProp) > 0 then SUBSTRING(s.WtrfrontProp,1,charindex(',',s.WtrfrontProp)-1) else s.WtrfrontProp end
   and l1.[Lookup] = 'WTRFRNTPRP_Lkp_1'
 left join WaSAR.dbo.Lookups l2
   on l2.[Value] = s.HowSold and l2.[Lookup] = 'HowSold'

Insert Into mls.Listing
([MLSID],[IDX],[City],[PropertyTax],[ShowAddress],[ShowMap],[PostalCode],[StreetAddress]
,[StreetAddress2],[StreetDirPrefix],[StreetNumber],[StreetName],[LotSizeSF],[ListPrice]
,[CloseDate],[PendingDate],[DOM],[JLsDom],[ElementarySchool],[PropTypeID],[StreetTypeSuffix]
,[MiddleSchool],[Latitude],[ListDate],[MLSDBListingModTime],[Longitude],[MLSListingID]
,[Acreage],[OriginalListPrice],[SchoolDistrict],[ClosePrice],[HighSchool],[StatusDate]
,[TaxID],[UpdateDate],[VirtualTour],AgentHitCount,BodyofWaterName,ListAgentID
,CoListAgentID,CoListAgentID2,ListingOfficeID,CoListingOfficeID,Community,SellingAgentID
,CoSellingAgentID,CoSellingAgentID2,SellingOffice,CoSellingOffice,CoSellingOffice2
,ExpirationDate,PictureCount,Waterfront,Zoning,HowSold,Area)
 Select 128,abs(s.idxinclude-1),Coalesce(s.JLSCity,s.City)
 ,IIF(isnumeric(s.Taxes) = 1,Replace(Replace(s.Taxes,'$',''),',',''),Null)
 ,IIF(s.DisplayAddress=1,0,1),IIF(s.DisplayAddress=1,0,1),Left(s.Zip,5),s.[Address]
 ,s.[Address2],s.[AddressDirection],s.[AddressNumber],s.[AddressStreet],s.[ApxLotSizeSqFt]
 ,s.[AskingPrice],s.[ClosingDate],s.[ContractDate],s.[DOM],s.[DOM],s.[Elementary]
 ,s.[JLSPropTypeID],s.[JLSStreetTypeSuffix],s.[JrHigh],s.[Latitude],s.[ListingDate]
 ,s.[ListingModTime],s.[Longitude],s.[MLSNumber],s.[NumAcres],s.[OriginalPrice]
 ,s.[SchoolDistrict],s.[SoldPrice],s.[SrHigh],s.[StatusDate],s.[TaxPropertyID]
 ,s.[UpdateDate],s.[VTourURL],s.AgentHitCount,s.BodyofWaterName,s.ListAgent1
 ,s.ListAgent2,s.ListAgent3,s.ListOffice1,s.ListOffice2,s.CommonInterestCommunity
 ,s.SellingAgent1,s.SellingAgent2,s.SellingAgent3,s.SellingOffice1,s.SellingOffice2
 ,s.SellingOffice3,s.ExpirationDate,s.PictureCount,WtrfrontProp=l1.LongValue
 ,s.[Zone],HowSold=l2.LongValue,s.Area
From WaSAR.dbo.Land s 
 left join WaSAR.dbo.Lookups l1
   on l1.[Value] = case when charindex(',',s.WtrfrontProp) > 0 then SUBSTRING(s.WtrfrontProp,1,charindex(',',s.WtrfrontProp)-1) else s.WtrfrontProp end
   and l1.[Lookup] = 'WTRFRNTPRP_Lkp_2'
 left join WaSAR.dbo.Lookups l2
   on l2.[Value] = s.HowSold and l2.[Lookup] = 'HowSold'

Insert Into mls.Listing
([MLSID],[IDX],[City],[PropertyTax],[ShowAddress],[ShowMap],[PostalCode],[StreetAddress]
,[StreetAddress2],[StreetDirPrefix],[StreetNumber],[StreetName],[LotSizeSF],[ListPrice]
,[CloseDate],[PendingDate],[DOM],[JLsDom],[ElementarySchool],[PropTypeID],[StreetTypeSuffix]
,[MiddleSchool],[Latitude],[ListDate],[MLSDBListingModTime],[Longitude],[MLSListingID]
,[Acreage],[OriginalListPrice],[SchoolDistrict],[ClosePrice],[SquareFootage],[HighSchool]
,[StatusDate],[TaxID],[UpdateDate],[VirtualTour],[YearBuilt],[GarageSpaces],AgentHitCount
,BodyofWaterName,ListAgentID,CoListAgentID,CoListAgentID2,ListingOfficeID
,CoListingOfficeID,Community,SellingAgentID,CoSellingAgentID,CoSellingAgentID2
,SellingOffice,CoSellingOffice,CoSellingOffice2,ExpirationDate,PictureCount
,Waterfront,Zoning,HowSold,Area)
 Select 128,abs(s.idxinclude-1),Coalesce(s.JLSCity,s.City)
 ,IIF(isnumeric(s.Taxes) = 1,Replace(Replace(s.Taxes,'$',''),',',''),Null)
 ,IIF(s.DisplayAddress=1,0,1),IIF(s.DisplayAddress=1,0,1),Left(s.Zip,5),s.[Address]
 ,s.[Address2],s.[AddressDirection],s.[AddressNumber],s.[AddressStreet],s.[ApxLotSizeSqFt]
 ,s.[AskingPrice],s.[ClosingDate],s.[ContractDate],s.[DOM],s.[DOM],s.[Elementary]
 ,s.[JLSPropTypeID],s.[JLSStreetTypeSuffix],s.[JrHigh],s.[Latitude],s.[ListingDate]
 ,s.[ListingModTime],s.[Longitude],s.[MLSNumber],s.[NumAcres],s.[OriginalPrice]
 ,s.[SchoolDistrict],s.[SoldPrice],s.[SquareFeet],s.[SrHigh],s.[StatusDate],s.[TaxPropertyID]
 ,s.[UpdateDate],s.[VTourURL],s.[YearBuilt],TRY_CONVERT(int, replace(s.Garsize,'+',''))
 ,s.AgentHitCount,s.BodyofWaterName,s.ListAgent1,s.ListAgent2,s.ListAgent3
 ,s.ListOffice1,s.ListOffice2,s.CommonInterestCommunity,s.SellingAgent1,s.SellingAgent2
 ,s.SellingAgent3,s.SellingOffice1,s.SellingOffice2,s.SellingOffice3,s.ExpirationDate
 ,s.PictureCount,WtrfrontProp=l1.LongValue,s.[Zone],HowSold=l2.LongValue,s.Area
From WaSAR.dbo.Commercial s
 left join WaSAR.dbo.Lookups l1
   on l1.[Value] = case when charindex(',',s.WtrfrontProp) > 0 then SUBSTRING(s.WtrfrontProp,1,charindex(',',s.WtrfrontProp)-1) else s.WtrfrontProp end
   and l1.[Lookup] = 'WTRFRNTPRP_Lkp_3'
 left join WaSAR.dbo.Lookups l2
   on l2.[Value] = s.HowSold and l2.[Lookup] = 'HowSold'

Insert Into mls.Listing
([MLSID],[IDX],[City],[PropertyTax],[BankOwned],[ShowAddress],[ShowMap],[ShortSale]
,[PostalCode],[StreetAddress],[StreetAddress2],[StreetDirPrefix],[StreetNumber],[StreetName]
,[LotSizeSF],[ListPrice],[CloseDate],[PendingDate],[DOM],[JLsDom],[ElementarySchool],[Garage]
,[PropTypeID],[StreetTypeSuffix],[MiddleSchool],[Latitude],[ListDate],[MLSDBListingModTime]
,[Longitude],[MLSListingID],[Acreage],[OriginalListPrice],[SchoolDistrict],[ClosePrice]
,[SquareFootage],[HighSchool],[StatusDate],[TaxID],[UpdateDate],[VirtualTour],[YearBuilt]
,[GarageSpaces],AgentHitCount,BodyofWaterName,ListAgentID,CoListAgentID
,CoListAgentID2,ListingOfficeID,CoListingOfficeID,Community,SellingAgentID,CoSellingAgentID
,CoSellingAgentID2,SellingOffice,CoSellingOffice,CoSellingOffice2,ExpirationDate
,PictureCount,Waterfront,Zoning,HowSold,Area)
 Select 128,abs(s.idxinclude-1),Coalesce(s.JLSCity,s.City)
 ,IIF(isnumeric(s.Taxes) = 1,Replace(Replace(s.Taxes,'$',''),',',''),Null)
 ,IIF(s.BankApproved='Y',1,0),IIF(s.DisplayAddress=1,0,1),IIF(s.DisplayAddress=1,0,1)
 ,IIF(s.PotentialShortSale='Y',1,0),Left(s.Zip,5),s.[Address],s.[Address2],s.[AddressDirection]
 ,s.[AddressNumber],s.[AddressStreet],s.[ApxLotSizeSqFt],s.[AskingPrice],s.[ClosingDate]
 ,s.[ContractDate],s.[DOM],s.[DOM],s.[Elementary],s.[JLSGarage],s.[JLSPropTypeID]
 ,s.[JLSStreetTypeSuffix],s.[JrHigh],s.[Latitude],s.[ListingDate],s.[ListingModTime]
 ,s.[Longitude],s.[MLSNumber],s.[NumAcres],s.[OriginalPrice],s.[SchoolDistrict]
 ,s.[SoldPrice],s.[SquareFeet],s.[SrHigh],s.[StatusDate],s.[TaxPropertyID],s.[UpdateDate]
 ,s.[VTourURL],s.[YearBuilt],TRY_CONVERT(int, replace(s.Garsize,'+','')),s.AgentHitCount
 ,s.BodyofWaterName,s.ListAgent1,s.ListAgent2,s.ListAgent3,s.ListOffice1
 ,s.ListOffice2,s.CommonInterestCommunity,s.SellingAgent1,s.SellingAgent2,s.SellingAgent3
 ,s.SellingOffice1,s.SellingOffice2,s.SellingOffice3,s.ExpirationDate,s.PictureCount
 ,WtrfrontProp=l1.LongValue,s.[Zone],HowSold=l2.LongValue,s.Area
 From WaSAR.dbo.MultiFamily s
  left join WaSAR.dbo.Lookups l1
   on l1.[Value] = case when charindex(',',s.WtrfrontProp) > 0 then SUBSTRING(s.WtrfrontProp,1,charindex(',',s.WtrfrontProp)-1) else s.WtrfrontProp end
   and l1.[Lookup] = 'WTRFRNTPRP_Lkp_4'
 left join WaSAR.dbo.Lookups l2
   on l2.[Value] = s.HowSold and l2.[Lookup] = 'HowSold'
 

 --WaTCAR
 Insert Into mls.Listing 
([ShowAddress],[ShowMap],[MLSID],[PropertyTax],[IDX],[Latitude],[Longitude],[BathsTotal],[YearBuilt],[PostalCode],[StreetAddress]
,[StreetDirPrefix],[StreetNumber],[StreetName],[ListPrice],[BathsFull],[Bedrooms],[CDOM],[JLsDom],[CloseDate],[PendingDate],[DOM]
,[ParkingSpaces],[City],[Garage],[PropTypeID],[ListDate],[MLSDBListingModTime],[LotSizeSF],[MLSListingID],[Acreage],[OriginalListPrice]
,[SchoolDistrict],[ClosePrice],[StatusDate],[TaxID],[SquareFootage],[UpdateDate],[VideoTour],[VirtualTour],[BathsPartial]
,[TaxYearBuilt],AgentHitCount,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,Commission,Community,SellingAgentID
,CoSellingAgentID,SellingOffice,CoSellingOffice,EstCompletionDate,ExpirationDate,NewConstruction,PictureCount,Zoning,HowSold,Area)
 Select 1,1,1350003,IIF(isnumeric(s.annualtaxes) = 1 and len(s.annualtaxes) > 1,Replace(Replace(s.annualtaxes,'$',''),',',''),Null)
 ,iif(s.IdxInclude=1,0,1),IIF(s.Latitude is null,Latitude,TRY_CONVERT(decimal(9,6),s.Latitude))
 ,IIF(s.longitude is null,Longitude,TRY_CONVERT(decimal(9,6),s.longitude))
 ,isnull(s.BathsFull,0) + isnull(s.Baths3Qtr,0)*.75 + isnull(s.BathsHalf,0)*.5
 ,LEFT (s.YearBuilt, 4),left(s.Zip,5),s.[Address],s.[AddressDirection],s.[AddressNumber],s.[AddressStreet],s.[AskingPrice],s.[BathsFull]
 ,s.[Bedrooms],s.[CDOM],s.[CDOM],s.[ClosingDate],s.[ContractDate],s.[DOM],s.[GarageCapacity],s.[JLSCity],s.[JLSGarage],s.[JLSProptypeid]
 ,s.[ListingDate],s.[ListingModTime],s.[LotSqFt],s.[MLSNumber],s.[NumAcres],s.[OriginalPrice],s.[SchoolDistrict],s.[SoldPrice]
 ,s.[StatusDate] ,s.[TaxPropertyID],s.[TotalSqFt],s.[UpdateDate],s.[VideoTour],s.[VirtualTour]
 ,s.Baths3Qtr+s.BathsHalf,TRY_CONVERT(smallint,s.TaxYear),s.AgentHitCount,s.ListAgent1,s.ListAgent2,s.ListOffice1,s.ListOffice2
 ,s.Compensation_SA,s.Neighborhood,s.SellingAgent1,s.SellingAgent2,s.SellingOffice1,s.SellingOffice2,s.NewConst_EndDate,s.ExpirationDate
 ,s.NewConstruction_YN,s.PictureCount,s.ZoningType,HowSold=l.LongValue,s.County
 From WaTCAR.dbo.Residential s 
  left join WaTCAR.dbo.Lookups l
    on s.HowSold = l.[Value] and l.[Lookup] = 'HowSold' and l.[Resource] = 'Property'

Insert Into mls.Listing 
([ShowAddress],[ShowMap],[MLSID],[PropertyTax],[IDX],[Latitude],[Longitude],[PostalCode]
,[StreetAddress],[StreetDirPrefix],[StreetNumber],[StreetName],[ListPrice],[CDOM],[JLsDom]
,[CloseDate],[PendingDate],[DOM],[City],[PropTypeID],[ListDate],[MLSDBListingModTime]
,[LotSizeSF],[MLSListingID],[Acreage],[OriginalListPrice],[ClosePrice],[StatusDate],[TaxID]
,[UpdateDate],[VideoTour],[VirtualTour],[TaxYearBuilt],AgentHitCount,ListAgentID,CoListAgentID
,ListingOfficeID,CoListingOfficeID,Commission,Community,SellingAgentID,CoSellingAgentID
,SellingOffice,CoSellingOffice,ExpirationDate,PictureCount,Waterfront,Zoning,HowSold,Area)
 Select 1,1,1350003,IIF(isnumeric(s.annualtaxes) = 1 and len(s.annualtaxes) > 1,Replace(Replace(s.annualtaxes,'$',''),',',''),Null)
 ,iif(s.IdxInclude=1,0,1),IIF(s.Latitude is null,Latitude,TRY_CONVERT(decimal(9,6),s.Latitude))
 ,IIF(s.longitude is null,Longitude,TRY_CONVERT(decimal(9,6),s.longitude)),left(s.Zip,5),s.[Address],s.[AddressDirection]
 ,s.[AddressNumber],s.[AddressStreet],s.[AskingPrice],s.[CDOM],s.[CDOM],s.[ClosingDate],s.[ContractDate],s.[DOM],s.[JLSCity]
 ,s.[JLSProptypeid] ,s.[ListingDate],s.[ListingModTime],s.[LotSqFt],s.[MLSNumber],s.[NumAcres],s.[OriginalPrice],s.[SoldPrice]
 ,s.[StatusDate],s.[TaxPropertyID],s.[UpdateDate],s.[VideoTour],s.[VirtualTour],TRY_CONVERT(smallint,s.TaxYear),s.AgentHitCount
 ,s.ListAgent1,s.ListAgent2,s.ListOffice1,s.ListOffice2,s.Compensation_SA,s.Neighborhood,s.SellingAgent1,s.SellingAgent2
 ,s.SellingOffice1,s.SellingOffice2,s.ExpirationDate,s.PictureCount,Waterfront=l2.LongValue,s.ZoningType,HowSold=l.LongValue,s.County
 From WaTCAR.dbo.Land s
  left join WaTCAR.dbo.Lookups l
    on s.HowSold = l.[Value] and l.[Lookup] = 'HowSold' and l.[Resource] = 'Property'
  left join WaTCAR.dbo.Lookups l2
    on s.Waterfront = l2.[Value] and l2.[Lookup] = 'Keyword_2_9' and l.[Resource] = 'Property'

Insert Into mls.Listing 
([ShowAddress],[ShowMap],[MLSID],[PropertyTax],[IDX],[Latitude],[Longitude],[YearBuilt],[PostalCode],[StreetAddress],[StreetDirPrefix]
,[StreetNumber],[StreetName],[ListPrice],[CDOM],[JLsDom],[CloseDate],[PendingDate],[DOM],[City],[PropTypeID],[ListDate]
,[MLSDBListingModTime],[LotSizeSF],[MLSListingID],[Acreage],[OriginalListPrice],[ClosePrice],[StatusDate],[TaxID],[UpdateDate]
,[VideoTour],[VirtualTour],[TaxYearBuilt],AgentHitCount,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,Commission
,Community,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice,DailyTrafficCount,ExpirationDate,PictureCount,Zoning
,HowSold,Area)
 Select 1,1,1350003,IIF(isnumeric(s.annualtaxes) = 1 and len(s.annualtaxes) > 1,Replace(Replace(s.annualtaxes,'$',''),',',''),Null)
 ,iif(s.IdxInclude=1,0,1),IIF(s.Latitude is null,Latitude,TRY_CONVERT(decimal(9,6),s.Latitude))
 ,IIF(s.longitude is null,Longitude,TRY_CONVERT(decimal(9,6),s.longitude)),LEFT (s.YearBuilt, 4)
 ,left(s.Zip,5),s.[Address],s.[AddressDirection],s.[AddressNumber],s.[AddressStreet],s.[AskingPrice],s.[CDOM],s.[CDOM],s.[ClosingDate]
 ,s.[ContractDate],s.[DOM],s.[JLSCity] ,s.[JLSProptypeid],s.[ListingDate],s.[ListingModTime],s.[LotSqFt],s.[MLSNumber],s.[NumAcres]
 ,s.[OriginalPrice],s.[SoldPrice],s.[StatusDate],s.[TaxPropertyID],s.[UpdateDate],s.[VideoTour],s.[VirtualTour]
 ,TRY_CONVERT(smallint,s.TaxYear),s.AgentHitCount,s.ListAgent1,s.ListAgent2,s.ListOffice1,s.ListOffice2,s.Compensation_SA
 ,s.Neighborhood,s.SellingAgent1,s.SellingAgent2 ,s.SellingOffice1,s.SellingOffice2,try_convert(decimal(19,6),s.DailyTrafficCount,null)
 ,s.ExpirationDate,s.PictureCount,s.ZoningType,HowSold=l.LongValue,s.County
 From WaTCAR.dbo.Commercial s
  left join WaTCAR.dbo.Lookups l
    on s.HowSold = l.[Value] and l.[Lookup] = 'HowSold' and l.[Resource] = 'Property'

Insert Into mls.Listing 
([ShowAddress],[ShowMap],[MLSID],[PropertyTax],[IDX],[Latitude],[Longitude],[YearBuilt],[PostalCode],[StreetAddress],[StreetDirPrefix]
,[StreetNumber],[StreetName],[ListPrice],[Bedrooms],[CDOM],[JLsDom],[CloseDate],[PendingDate],[DOM],[City],[Garage],[GarageSpaces]
,[PropTypeID],[ListDate],[MLSDBListingModTime],[LotSizeSF],[MLSListingID],[Acreage],[OriginalListPrice],[SchoolDistrict],[ClosePrice]
,[StatusDate],[TaxID],[SquareFootage],[UpdateDate],[VideoTour],[VirtualTour],[TaxYearBuilt],AgentHitCount,ListAgentID,CoListAgentID
,ListingOfficeID,CoListingOfficeID,Commission,Community,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice,ExpirationDate
,PictureCount,Zoning,HowSold,Area)
 Select 1,1,1350003,IIF(isnumeric(s.annualtaxes) = 1 and len(s.annualtaxes) > 1,Replace(Replace(s.annualtaxes,'$',''),',',''),Null)
 ,iif(s.IdxInclude=1,0,1),IIF(s.Latitude is null,Latitude,TRY_CONVERT(decimal(9,6),s.Latitude))
 ,IIF(s.longitude is null,Longitude,TRY_CONVERT(decimal(9,6),s.longitude)),LEFT (s.YearBuilt, 4),left(s.Zip,5),s.[Address]
 ,s.[AddressDirection],s.[AddressNumber],s.[AddressStreet],s.[AskingPrice],s.[Bedrooms],s.[CDOM],s.[CDOM],s.[ClosingDate],s.[ContractDate]
 ,s.[DOM],s.[JLSCity],s.[JLSGarage],s.[JLSGarageSpaces],s.[JLSProptypeid],s.[ListingDate],s.[ListingModTime],s.[LotSqFt],s.[MLSNumber]
 ,s.[NumAcres],s.[OriginalPrice],s.[SchoolDistrict],s.[SoldPrice],s.[StatusDate],s.[TaxPropertyID],s.[TotalSqFt],s.[UpdateDate]
 ,s.[VideoTour],s.[VirtualTour],TRY_CONVERT(smallint,s.TaxYear),s.AgentHitCount,s.ListAgent1,s.ListAgent2,s.ListOffice1,s.ListOffice2
 ,s.Compensation_SA,s.Neighborhood,s.SellingAgent1,s.SellingAgent2,s.SellingOffice1,s.SellingOffice2,s.ExpirationDate,s.PictureCount
 ,s.ZoningType,HowSold=l.LongValue,s.County
From WaTCAR.dbo.MultiFamily s
  left join WaTCAR.dbo.Lookups l
    on s.HowSold = l.[Value] and l.[Lookup] = 'HowSold' and l.[Resource] = 'Property'
 
Insert Into mls.Listing 
([ShowAddress],[ShowMap],[MLSID],[PropertyTax],[IDX],[Latitude],[Longitude],[PostalCode],[StreetAddress],[StreetDirPrefix],[StreetNumber]
,[StreetName],[ListPrice],[CDOM],[JLsDom],[CloseDate],[PendingDate],[DOM],[City],[PropTypeID],[ListDate],[MLSDBListingModTime]
,[LotSizeSF],[MLSListingID],[Acreage],[OriginalListPrice],[ClosePrice],[StatusDate],[TaxID],[UpdateDate],[VideoTour],[VirtualTour]
,[TaxYearBuilt],AgentHitCount,ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,Commission,Community,SellingAgentID
,CoSellingAgentID,SellingOffice,CoSellingOffice,ExpirationDate,PictureCount,Waterfront,Zoning,HowSold,Area)
 Select 1,1,1350003,IIF(isnumeric(s.annualtaxes) = 1 and len(s.annualtaxes) > 1,Replace(Replace(s.annualtaxes,'$',''),',',''),Null)
 ,iif(s.IdxInclude=1,0,1),IIF(s.Latitude is null,Latitude,TRY_CONVERT(decimal(9,6),s.Latitude))
 ,IIF(s.longitude is null,Longitude,TRY_CONVERT(decimal(9,6),s.longitude)),left(s.Zip,5),s.[Address],s.[AddressDirection]
 ,s.[AddressNumber],s.[AddressStreet],s.[AskingPrice] ,s.[CDOM],s.[CDOM],s.[ClosingDate],s.[ContractDate],s.[DOM],s.[JLSCity]
 ,s.[JLSProptypeid] ,s.[ListingDate],s.[ListingModTime],s.[LotSqFt],s.[MLSNumber],s.[NumAcres],s.[OriginalPrice],s.[SoldPrice]
 ,s.[StatusDate],s.[TaxPropertyID],s.[UpdateDate],s.[VideoTour],s.[VirtualTour],TRY_CONVERT(smallint,s.TaxYear),s.AgentHitCount
 ,s.ListAgent1,s.ListAgent2,s.ListOffice1,s.ListOffice2,s.Compensation_SA,s.Neighborhood,s.SellingAgent1,s.SellingAgent2
 ,s.SellingOffice1,s.SellingOffice2,s.ExpirationDate,s.PictureCount,Waterfront=l2.longvalue,s.ZoningType,HowSold=l.LongValue,s.County
  From WaTCAR.dbo.FarmRanch s
    left join WaTCAR.dbo.Lookups l
    on s.HowSold = l.[Value] and l.[Lookup] = 'HowSold' and l.[Resource] = 'Property'
  left join WaTCAR.dbo.Lookups l2
    on s.Waterfront = l2.[Value] and l2.[Lookup] = 'Keyword_2_9' and l.[Resource] = 'Property'


 --WaWWAR
 Insert Into mls.Listing 
([MLSID],[StreetNumber],[JLsDom],[Latitude],[Longitude],[StreetAddress2],[Acreage]
,[BuildingName],[DOM],[BankOwned],[City],[ParentPropTypeID],[PropTypeID],[ShortSale]
,[UpdateDate],[ListDate],[MLSDBListingModTime],[ListPrice],[MLSListingID],[OriginalListPrice]
,[PendingDate],[TaxID],[CloseDate],[ClosePrice],[StreetDirPrefix],[StreetName],[VirtualTour]
,[YearBuilt],[PostalCode],[LotSizeSF],[PropertyTax],ListAgentID,CoListAgentID,ListingOfficeID
,CoListingOfficeID,Commission,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice
,ExpirationDate,PictureCount,Waterfront,WithdrawDate,Zoning,HowSold,Area)
 Select 1350004,IIF(len(s.StreetNumber)<26,s.StreetNumber,Null),IIF(s.JlsDom > -1,s.JlsDom
 ,JlsDom),IIF(s.Latitude is null,Latitude,TRY_CONVERT(decimal(9,6),s.Latitude))
 ,IIF(s.longitude is null,Longitude,TRY_CONVERT(decimal(9,6),s.longitude)),s.[Address2]
 ,s.[ApproxAcres],s.[Complex_ParkName],s.[DaysOnMarket],s.[JLSBankOwned],s.[JLSCity]
 ,s.[JLSParentPropTypeID],s.[JLSPropTypeID],s.[JlsShortSale],s.[LastModifiedDateTime]
 ,s.[ListDate],s.[ListingModTime],s.[ListPrice],s.[MLSNumber],s.[OriginalListPrice]
 ,s.[PendingDate],s.[PropertyID],s.[SaleDate],s.[SoldPrice],s.[StreetDirection]
 ,s.[StreetName],s.[VirtualTour],s.[YearBuilt],s.[ZipCode],Try_Convert(decimal(9,2)
 ,s.LotSize),Try_Convert(int,floor(s.Taxes)),s.JLSListAgentId,s.JLSCoListAgentId
 ,s.ListOfficeID,s.CoListOfficeID,s.CommissionPercentage,s.JLSSaleAgentId,s.JLSCoSaleAgentId
 ,s.SaleOfficeID,s.CoSaleOfficeID,s.[ExpireDate],s.PhotoCount,Shoreline_Wetland=l.LongValue
 ,s.Withdraw,s.Zoning,HowSold=l2.LongValue,s.Area
  From WaWWAR.dbo.Commercial s
   left join WaWWAR.dbo.Lookups l
     on s.Shoreline_Wetland = l.[Value] and l.[Lookup]= '1CF_T_Shoreline_Wetland'
   left join WaWWAR.dbo.Lookups l2
     on s.HowSold = l.[Value] and l.[Lookup]= 'How_Sold'

Insert Into mls.Listing 
([MLSID],[StreetNumber],[JLsDom],[Latitude],[Longitude],[StreetAddress2],[Acreage]
,[BathsTotal],[Bedrooms],[DOM],[BankOwned],[City],[Garage],[GarageSpaces],[ParentPropTypeID]
,[PropTypeID],[ShortSale],[UpdateDate],[ListDate],[MLSDBListingModTime],[ListPrice]
,[MLSListingID],[OriginalListPrice],[PendingDate],[TaxID],[CloseDate],[ClosePrice]
,[StreetDirPrefix],[StreetName],[VirtualTour],[YearBuilt],[PostalCode],[LotSizeSF]
,[PropertyTax],ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID,CommissionCode
,Commission,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice
,ExpirationDate,PictureCount,Waterfront,WithdrawDate,Zoning,HowSold,Area)
 Select 1350004,IIF(len(s.StreetNumber)<26,s.StreetNumber,Null),IIF(s.JlsDom > -1,s.JlsDom
 ,JlsDom),IIF(s.Latitude is null,Latitude,TRY_CONVERT(decimal(9,6),s.Latitude))
 ,IIF(s.longitude is null,Longitude,TRY_CONVERT(decimal(9,6),s.longitude)),s.[Address2]
 ,s.[ApproxAcres],s.[BathsTotal],s.[Bedrooms],s.[DaysOnMarket],s.[JLSBankOwned],s.[JLSCity]
 ,s.[JLSGarage],s.[JLSGarageSpaces],s.[JLSParentPropTypeID],s.[JLSPropTypeID],s.[JlsShortSale]
 ,s.[LastModifiedDateTime],s.[ListDate],s.[ListingModTime],s.[ListPrice],s.[MLSNumber]
 ,s.[OriginalListPrice],s.[PendingDate],s.[PropertyID],s.[SaleDate],s.[SoldPrice]
 ,s.[StreetDirection],s.[StreetName],s.[VirtualTour],s.[YearBuilt],s.[ZipCode]
 ,Try_Convert(decimal(9,2),s.LotSize),Try_Convert(int,floor(s.Taxes)),s.JLSListAgentId
 ,s.JLSCoListAgentId,s.ListOfficeID,s.CoListOfficeID,s.Commission1,s.CommissionPercentage
 ,s.JLSSaleAgentId,s.JLSCoSaleAgentId,s.SaleOfficeID,s.CoSaleOfficeID,s.[ExpireDate]
 ,s.PhotoCount,Shoreline_Wetland=l.LongValue,s.Withdraw,s.Zoning,HowSold=l2.LongValue,s.Area
 From WaWWAR.dbo.FarmRanch s
    left join WaWWAR.dbo.Lookups l
     on s.Shoreline_Wetland = l.[Value] and l.[Lookup]= '1CF_T_Shoreline_Wetland'
   left join WaWWAR.dbo.Lookups l2
     on s.HowSold = l.[Value] and l.[Lookup]= 'How_Sold'

Insert Into mls.Listing
([MLSID],[StreetNumber],[JLsDom],[Latitude],[Longitude],[StreetAddress2],[Acreage],[DOM]
,[BankOwned],[City],[ParentPropTypeID],[PropTypeID],[ShortSale],[UpdateDate],[ListDate]
,[MLSDBListingModTime],[ListPrice],[MLSListingID],[OriginalListPrice],[PendingDate],[TaxID]
,[CloseDate],[ClosePrice],[StreetDirPrefix],[StreetName],[VirtualTour],[PostalCode]
,[LotSizeSF],[PropertyTax],ListAgentID,CoListAgentID,ListingOfficeID,CoListingOfficeID
,CommissionCode,Commission,Community,SellingAgentID,CoSellingAgentID,SellingOffice
,CoSellingOffice,ExpirationDate,PictureCount,Waterfront,WithdrawDate,Zoning,HowSold,Area)
 Select 1350004,IIF(len(s.StreetNumber)<26,s.StreetNumber,Null)
 ,IIF(s.JlsDom > -1,s.JlsDom,JlsDom),IIF(s.Latitude is null,Latitude,TRY_CONVERT(decimal(9,6),s.Latitude))
 ,IIF(s.longitude is null,Longitude,TRY_CONVERT(decimal(9,6),s.longitude)),s.[Address2]
 ,s.[ApproxAcres],s.[DaysOnMarket],s.[JLSBankOwned],s.[JLSCity],s.[JLSParentPropTypeID]
 ,s.[JLSPropTypeID],s.[JlsShortSale],s.[LastModifiedDateTime],s.[ListDate],s.[ListingModTime]
 ,s.[ListPrice],s.[MLSNumber],s.[OriginalListPrice],s.[PendingDate],s.[PropertyID],s.[SaleDate]
 ,s.[SoldPrice],s.[StreetDirection],s.[StreetName],s.[VirtualTour],s.[ZipCode]
 ,Try_Convert(decimal(9,2),s.LotSize),Try_Convert(int,floor(s.Taxes)),s.JLSListAgentId
 ,s.JLSCoListAgentId,s.ListOfficeID,s.CoListOfficeID,s.Commission1,s.CommissionPercentage
 ,s.Development_Subdivision,s.JLSSaleAgentId,s.JLSCoSaleAgentId,s.SaleOfficeID
 ,s.CoSaleOfficeID,s.[ExpireDate],s.PhotoCount,Shoreline_Wetland=l.LongValue,s.Withdraw
 ,s.Zoning,HowSold=l.LongValue,s.Area
From WaWWAR.dbo.Land s 
   left join WaWWAR.dbo.Lookups l
     on s.Shoreline_Wetland = l.[Value] and l.[Lookup]= '1CF_T_Shoreline_Wetland'
   left join WaWWAR.dbo.Lookups l2
     on s.HowSold = l.[Value] and l.[Lookup]= 'How_Sold'

Insert Into mls.Listing
([MLSID],[StreetNumber],[JLsDom],[Latitude],[Longitude],[StreetAddress2],[Acreage]
,[SquareFootage],[BuildingName],[DOM],[BankOwned],[City],[ParentPropTypeID],[PropTypeID]
,[ShortSale],[UpdateDate],[ListDate],[MLSDBListingModTime],[ListPrice],[MLSListingID]
,[OriginalListPrice],[PendingDate],[TaxID],[CloseDate],[ClosePrice],[StreetDirPrefix]
,[StreetName],[VirtualTour],[YearBuilt],[PostalCode],[LotSizeSF],[PropertyTax],ListAgentID
,CoListAgentID,ListingOfficeID,CoListingOfficeID,CommissionCode,Commission,SellingAgentID
,CoSellingAgentID,SellingOffice,CoSellingOffice,ExpirationDate,PictureCount
,Waterfront,WithdrawDate,Zoning,HowSold,Area)
 Select 1350004,IIF(len(s.StreetNumber)<26,s.StreetNumber,Null),IIF(s.JlsDom > -1,s.JlsDom,JlsDom)
 ,IIF(s.Latitude is null,Latitude,TRY_CONVERT(decimal(9,6),s.Latitude))
 ,IIF(s.longitude is null,Longitude,TRY_CONVERT(decimal(9,6),s.longitude))
 ,s.[Address2],s.[ApproxAcres],s.[ApproxBldgSqFt],s.[Complex_ParkName],s.[DaysOnMarket]
 ,s.[JLSBankOwned],s.[JLSCity],s.[JLSParentPropTypeID],s.[JLSPropTypeID],s.[JlsShortSale]
 ,s.[LastModifiedDateTime],s.[ListDate],s.[ListingModTime],s.[ListPrice],s.[MLSNumber]
 ,s.[OriginalListPrice],s.[PendingDate],s.[PropertyID],s.[SaleDate],s.[SoldPrice]
 ,s.[StreetDirection],s.[StreetName],s.[VirtualTour],s.[YearBuilt],s.[ZipCode]
 ,Try_Convert(decimal(9,2),s.LotSize),Try_Convert(int,floor(s.Taxes)),s.JLSListAgentId
 ,s.JLSCoListAgentId,s.ListOfficeID,s.CoListOfficeID,s.Commission1,s.CommissionPercentage
 ,s.JLSSaleAgentId,s.JLSCoSaleAgentId,s.SaleOfficeID,s.CoSaleOfficeID,s.[ExpireDate]
 ,s.PhotoCount,Shoreline_Wetland=l.LongValue,s.Withdraw,s.Zoning,HowSold=l2.LongValue,s.Area
From WaWWAR.dbo.MultiFamily s
   left join WaWWAR.dbo.Lookups l
     on s.Shoreline_Wetland = l.[Value] and l.[Lookup]= '1CF_T_Shoreline_Wetland'
   left join WaWWAR.dbo.Lookups l2
     on s.HowSold = l.[Value] and l.[Lookup]= 'How_Sold'

Insert Into mls.Listing 
([MLSID],[StreetNumber],[JLsDom],[Latitude],[Longitude],[StreetAddress2],[Acreage],[SquareFootage],[BathsTotal],[Bedrooms]
,[BuildingName],[DOM],[BankOwned],[City],[Garage],[GarageSpaces],[ParentPropTypeID],[PropTypeID],[ShortSale],[UpdateDate],[ListDate]
,[MLSDBListingModTime],[ListPrice],[MLSListingID],[OriginalListPrice],[PendingDate],[TaxID],[CloseDate],[ClosePrice],[StreetDirPrefix]
,[StreetName],[VirtualTour],[YearBuilt],[PostalCode],[LotSizeSF],[PropertyTax],ListAgentID,CoListAgentID,ListingOfficeID
,CoListingOfficeID,CommissionCode,Commission,Community,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice,ExpirationDate
,PictureCount,Waterfront,WithdrawDate,Zoning,HowSold,Area)
 Select 1350004,IIF(len(s.StreetNumber)<26,s.StreetNumber,Null),IIF(s.JlsDom > -1,s.JlsDom,JlsDom)
 ,IIF(s.Latitude is null,Latitude,TRY_CONVERT(decimal(9,6),s.Latitude))
 ,IIF(s.longitude is null,Longitude,TRY_CONVERT(decimal(9,6),s.longitude)),s.[Address2]
 ,s.[ApproxAcres],s.[ApproxTotalSqFt],s.[BathsTotal],s.[Bedrooms],s.[Complex_ParkName],s.[DaysOnMarket],s.[JLSBankOwned],s.[JLSCity]
 ,s.[JLSGarage],s.[JLSGarageSpaces],s.[JLSParentPropTypeID],s.[JLSPropTypeID],s.[JlsShortSale],s.[LastModifiedDateTime]
 ,s.[ListDate],s.[ListingModTime],s.[ListPrice],s.[MLSNumber],s.[OriginalListPrice],s.[PendingDate],s.[PropertyID],s.[SaleDate]
 ,s.[SoldPrice],s.[StreetDirection],s.[StreetName],s.[VirtualTour],s.[YearBuilt],s.[ZipCode],Try_Convert(decimal(9,2),s.LotSize)
 ,Try_Convert(int,floor(s.Taxes)),s.JLSListAgentId,s.JLSCoListAgentId,s.ListOfficeID,s.CoListOfficeID,s.Commission1
 ,s.CommissionPercentage,s.Development_Subdivision,s.JLSSaleAgentId,s.JLSCoSaleAgentId,s.SaleOfficeID,s.CoSaleOfficeID,s.[ExpireDate]
 ,s.PhotoCount,Shoreline_Wetland=l.LongValue ,s.Withdraw,s.Zoning,HowSold=l2.LongValue,s.Area
From WaWWAR.dbo.Residential s
   left join WaWWAR.dbo.Lookups l
     on s.Shoreline_Wetland = l.[Value] and l.[Lookup]= '1CF_T_Shoreline_Wetland'
   left join WaWWAR.dbo.Lookups l2
     on s.HowSold = l.[Value] and l.[Lookup]= 'How_Sold'


 --WaYMLS
 Insert Into mls.Listing 
([MLSID],[ListPrice],[OriginalListPrice],[ClosePrice],[PropertyTax],[SquareFootage],[IDX],[Acreage],[ListDate],[VirtualTour],[DOM]
,[JLsDom],[Latitude],[Longitude],[StreetNumber],[BankOwned],[City],[Garage],[ParkingSpaces],[PostalCode],[PropTypeID],[SchoolDistrict]
,[ShortSale],[StreetDirPrefix],[StreetTypeSuffix],[MLSDBListingModTime],[MLSListingID],[CloseDate],[StatusDate],[StreetName]
,[TaxYearBuilt],[UpdateDate],[Bedrooms],[BathsFull],[BathsPartial],[PendingDate],[StreetNumberModifier],[YearBuilt],[BathsTotal]
,BuilderName,CancelDate,ListAgentID,CoListAgentID,CoListingMemberID,ListingOfficeID,CoListingOfficeID,SellingAgentID,CoSellingAgentID
,SellingOffice,CoSellingOffice,FallThroughDate,NewConstruction,PictureCount,WithdrawDate,Zoning,Area)
 Select 130,floor(s.ListPrice),floor(s.OriginalListPrice),floor(s.SoldPrice),floor(s.Taxes),floor(s.TotalApxSF)
 ,IIF(s.DisplayOnPublicWebsites='N',0,1),s.[Acreage],s.[BeginDate],s.[BrandedVirtualTour],s.[DaysOnMarket],s.[DaysOnMarket],s.[GeoLat]
 ,s.[GeoLon] ,s.[HouseNumber],s.[JLSBankOwned],s.[JLSCity],s.[JLSGarage],s.[JLSParkingSpaces],s.[JLSPostalCode],s.[JLSPropTypeID]
 ,s.[JLSSchoolDistrict],s.[JLSShortSale],s.[JLSStreetDirPrefix],s.[JLSStreetTypeSuffix],s.[ListingModTime],s.[MLSNumber],s.[SoldDate]
 ,s.[StatusChangeDate],s.[StreetName],s.[TaxYear],s.[Timestamp],s.[TotalBedrooms],s.[TotalFullBaths],s.[TotalHalfBaths]
 ,s.[UnderContractDate],s.[UnitOrLotNumber],s.[YearBuilt],TRY_CONVERT(DECIMAL(5,2),s.[TotalBaths]),s.BuilderName,s.CancelDate,s.AgentID
 ,s.CoListAgentID,s.CoListingMemberShortId,s.ListingOfficeShortId,s.CoListingOfficeShortId,s.SellingAgentID,s.CoSellingAgentID
 ,s.SellingOfficeShortId,s.CoSellingOfficeShortId,s.FallthroughDate,NewConstruction=l.LongValue,s.PictureCount,s.WithdrawalDate
 ,Zoning=l2.LongValue,s.Area_SubArea
From WaYMLS.dbo.Residential s
  left join WaYMLS.dbo.Lookups l
    on s.NewConstruction = l.[Value] and l.[Lookup] = '20060126192425742772000000'
  left join WaYMLS.dbo.Lookups l2
    on s.Zoning = l2.[Value] and l2.[Lookup] = 'GFLU20060206165622267748000000'

Insert Into mls.Listing
([MLSID],[ListPrice],[OriginalListPrice],[ClosePrice],[PropertyTax],[IDX],[Acreage],[ListDate],[VirtualTour],[DOM],[JLsDom],[Latitude]
,[Longitude],[StreetNumber],[BankOwned],[City],[PostalCode],[PropTypeID],[SchoolDistrict],[ShortSale],[StreetDirPrefix]
,[StreetTypeSuffix],[MLSDBListingModTime],[MLSListingID],[CloseDate],[StatusDate],[StreetName],[TaxYearBuilt],[UpdateDate],[PendingDate]
,[StreetNumberModifier],[YearBuilt],CancelDate,ListAgentID,CoListAgentID,CoListingMemberID,ListingOfficeID,CoListingOfficeID
,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice,FallThroughDate,PictureCount,WithdrawDate,Zoning,Area)
 Select 130,floor(s.ListPrice),floor(s.OriginalListPrice),floor(s.SoldPrice),floor(s.Taxes),IIF(s.DisplayOnPublicWebsites='N',0,1)
 ,s.[Acreage],s.[BeginDate],s.[BrandedVirtualTour],s.[DaysOnMarket],s.[DaysOnMarket],s.[GeoLat],s.[GeoLon],s.[HouseNumber]
 ,s.[JLSBankOwned] ,s.[JLSCity],s.[JLSPostalCode],s.[JLSPropTypeID],s.[JLSSchoolDistrict],s.[JLSShortSale],s.[JLSStreetDirPrefix]
 ,s.[JLSStreetTypeSuffix],s.[ListingModTime],s.[MLSNumber],s.[SoldDate],s.[StatusChangeDate],s.[StreetName],s.[TaxYear],s.[Timestamp]
 ,s.[UnderContractDate],s.[UnitOrLotNumber],s.[YearBuilt],s.CancelDate,s.AgentID,s.CoListAgentID,s.CoListingMemberShortId
 ,s.ListingOfficeShortId,s.CoListingOfficeShortId,s.SellingAgentID,s.CoSellingAgentID,s.SellingOfficeShortId,s.CoSellingOfficeShortId
 ,s.FallthroughDate,s.PictureCount,s.WithdrawalDate,Zoning=l.LongValue,s.Area_SubArea
From WaYMLS.dbo.MultiFamily s
  left join WaYMLS.dbo.Lookups l
    on s.Zoning = l.[Value] and l.[Lookup] = 'GFLU20060207025456080592000000'

Insert Into mls.Listing 
([MLSID],[ListPrice],[OriginalListPrice],[ClosePrice],[PropertyTax],[IDX],[Acreage],[ListDate],[VirtualTour],[DOM],[JLsDom],[Latitude]
,[Longitude],[StreetNumber],[BankOwned],[City],[PostalCode],[PropTypeID],[SchoolDistrict],[ShortSale],[StreetDirPrefix]
,[StreetTypeSuffix],[MLSDBListingModTime],[MLSListingID],[CloseDate],[StatusDate],[StreetName],[TaxYearBuilt],[UpdateDate],[PendingDate]
,[StreetNumberModifier],CancelDate,ListAgentID,CoListAgentID,CoListingMemberID,ListingOfficeID,CoListingOfficeID,SellingAgentID
,CoSellingAgentID,SellingOffice,CoSellingOffice,FallThroughDate,PictureCount,WithdrawDate,Zoning,Area)
 Select 130,floor(s.ListPrice),floor(s.OriginalListPrice),floor(s.SoldPrice),floor(s.Taxes)
 ,IIF(s.DisplayOnPublicWebsites='N',0,1),s.[Acreage],s.[BeginDate],s.[BrandedVirtualTour],s.[DaysOnMarket],s.[DaysOnMarket],s.[GeoLat]
 ,s.[GeoLon],s.[HouseNumber],s.[JLSBankOwned],s.[JLSCity],s.[JLSPostalCode],s.[JLSPropTypeID],s.[JLSSchoolDistrict],s.[JLSShortSale]
 ,s.[JLSStreetDirPrefix],s.[JLSStreetTypeSuffix],s.[ListingModTime],s.[MLSNumber],s.[SoldDate],s.[StatusChangeDate],s.[StreetName]
 ,s.[TaxYear],s.[Timestamp],s.[UnderContractDate],s.[UnitOrLotNumber],s.CancelDate,s.AgentID,s.CoListAgentID,s.CoListingMemberShortId
 ,s.ListingOfficeShortId,s.CoListingOfficeShortId,s.SellingAgentID,s.CoSellingAgentID,s.SellingOfficeShortId,s.CoSellingOfficeShortId
 ,s.FallthroughDate,s.PictureCount,s.WithdrawalDate,Zoning=l.LongValue,s.Area_SubArea
From WaYMLS.dbo.Land s
  left join WaYMLS.dbo.Lookups l
    on s.Zoning = l.[Value] and l.[Lookup] = 'GFLU20060207162213345098000000'

Insert Into mls.Listing 
([MLSID],[ListPrice],[OriginalListPrice],[ClosePrice],[PropertyTax],[IDX],[Acreage],[ListDate],[VirtualTour],[DOM],[JLsDom],[Latitude]
,[Longitude],[StreetNumber],[BankOwned],[City],[PostalCode],[PropTypeID],[SchoolDistrict],[ShortSale],[StreetDirPrefix]
,[StreetTypeSuffix],[MLSDBListingModTime],[MLSListingID],[CloseDate],[StatusDate],[StreetName],[TaxYearBuilt],[UpdateDate],[PendingDate]
,[StreetNumberModifier],[YearBuilt],CancelDate,ListAgentID,CoListAgentID,CoListingMemberID,ListingOfficeID,CoListingOfficeID
,SellingAgentID,CoSellingAgentID,SellingOffice,CoSellingOffice,FallThroughDate,PictureCount,WithdrawDate,Zoning,Area)
 Select 130,floor(s.ListPrice),floor(s.OriginalListPrice),floor(s.SoldPrice),floor(s.Taxes),IIF(s.DisplayOnPublicWebsites='N',0,1)
 ,s.[Acreage],s.[BeginDate],s.[BrandedVirtualTour],s.[DaysOnMarket],s.[DaysOnMarket],s.[GeoLat],s.[GeoLon],s.[HouseNumber]
 ,s.[JLSBankOwned] ,s.[JLSCity],s.[JLSPostalCode],s.[JLSPropTypeID],s.[JLSSchoolDistrict],s.[JLSShortSale],s.[JLSStreetDirPrefix]
 ,s.[JLSStreetTypeSuffix],s.[ListingModTime],s.[MLSNumber],s.[SoldDate],s.[StatusChangeDate],s.[StreetName],s.[TaxYear],s.[Timestamp]
 ,s.[UnderContractDate],s.[UnitOrLotNumber],s.[YearBuilt],s.CancelDate,s.AgentID,s.CoListAgentID,s.CoListingMemberShortId
 ,s.ListingOfficeShortId,s.CoListingOfficeShortId,s.SellingAgentID,s.CoSellingAgentID,s.SellingOfficeShortId,s.CoSellingOfficeShortId
 ,s.FallthroughDate,s.PictureCount,s.WithdrawalDate,Zoning=l.LongValue,s.Area_SubArea
From WaYMLS.dbo.Commercial s
  left join WaYMLS.dbo.Lookups l
    on s.Zoning = l.[Value] and l.[Lookup] = 'GFLU20060207202827390936000000'

END