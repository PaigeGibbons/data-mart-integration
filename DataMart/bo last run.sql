/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [Object_Name]
--      ,Event_Type_ID
--	  ,Status_ID      
      ,max(Start_Time)

  FROM [BOBIP_ADS].[dbo].[ADS_EVENT]
  where Event_Type_ID in (1002, 1011)
    and Start_Time >= '2019-07-01' 
	and [Object_Name] not like '%Scope%'
group by [Object_Name]
--      ,Event_Type_ID
--	  ,Status_ID  
order by 2 desc