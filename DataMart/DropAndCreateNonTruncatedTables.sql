USE [BI_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[CreateNonTruncatedTables]    Script Date: 1/24/2020 2:11:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER Procedure [dbo].[CreateNonTruncatedTables] AS
BEGIN

/* DO NOT RUN THIS PROCEDURE UNLESS YOU NEED TO REBUILD THE TABLES FROM SCRATCH */

CREATE TABLE [dim].[Agent](
	[AgentKey] [int] IDENTITY(1,1) NOT NULL,
	[MLSID] [int] NULL,
	[AgentMLSID] [varchar](50) NULL,
	[AgentName] [varchar](100) NULL,
	[isActive] [smallint] NULL,
	[AuxAgentID] [varchar](50) NULL,
	[MLSInternalAgentID] [varchar](50) NULL,
	InsertedDate datetime,
	UpdatedDate datetime,
    CONSTRAINT [pk_Agent] PRIMARY KEY CLUSTERED 
(
	[AgentKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


CREATE TABLE [dim].[Office](
	[OfficeKey] [int] IDENTITY(1,1) NOT NULL,
	[MLSID] [int] NULL,
	[OfficeID] [varchar](50) NULL,
	[OfficeName] [varchar](100) NULL,
    [OfficeAddress1] [varchar](50) NULL,
    [OfficeAddress2] [varchar](50) NULL,
    [OfficeCity] [varchar](25) NULL,
    [OfficeState] [varchar](5) NULL,
    [OfficeZip] [varchar](10) NULL,
	[OfficeStatus] [smallint] NULL,
	[OfficeIDAux] [varchar](50) NULL,
	[InsertedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	 CONSTRAINT [pk_Office] PRIMARY KEY CLUSTERED 
(
	[OfficeKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


CREATE TABLE dim.JLSAgent(
    JLSAgentKey int identity(1,1) not null,
	AgentMLSId char(15) NULL,
	CompanyAbbrev char(5) NULL,
	AgentNo int NULL,
	BranchNo smallint NULL,
	AgentStatus smallint NULL,
	AgentLegalFirstName char(19) NULL,
	AgentLegalLastName char(23) NULL,
	AgentPreferredFirstName char(19) NULL,
	AgentPreferredLastName char(23) NULL,
	EffectiveDate datetime NULL,
	HireDate datetime NULL,
	AnniversaryDate datetime NULL,
	TransferDate datetime NULL,
	FromBranchNo smallint NULL,
	ReleaseDate datetime NULL,
	ReleaseDescr char(31) NULL,
	ReleasedTo char(31) NULL,
	IndustryStartDate datetime NULL,
	DualAgent tinyint NULL,
	Relocation tinyint NULL,
	ReloDesignation char(31) NULL,
	ebusiness tinyint NULL,
	BrokerAgreement tinyint NULL,
	VendorTypeNo char(3) NULL,
	SpaceTypeNo char(3) NULL,
	EmailAddress char(41) NULL,
	Incorporated tinyint NULL,
	Languages tinyint NULL,
	CareerLaunch tinyint NULL,
	Candidate tinyint NULL,
	LegacyBroker tinyint NULL,
	NotEngaged tinyint NULL,
	LicenseDesignation char(31) NULL,
	FromSource char(61) NULL,
	FromCompany char(31) NULL,
	RecordStartDate datetime NOT NULL,
	RecordEndDate datetime NOT NULL default '9999-12-31',
	CONSTRAINT [pk_JLSAgent] PRIMARY KEY CLUSTERED (JLSAgentKey ASC)
) 


CREATE TABLE [dim].[JLSOffice](
    JLSOfficeKey int identity(1,1) NOT NULL,
	BranchNo smallint NULL,
	CompanyAbbrev char(5) NULL,
	BranchName char(31) NULL,
	BranchStatus smallint NULL,
	BeginDate smalldatetime NULL,
	EndDate smalldatetime NULL,
	AddressLine1 char(41) NULL,
	AddressLine2 char(41) NULL,
	AddressCity char(21) NULL,
	AddressState char(3) NULL,
	AddressZip char(9) NULL,
	AddressCounty char(31) NULL,
	BranchPhone char(11) NULL,
	BranchFax char(11) NULL,
	BranchEmailAddress char(41) NULL,
	BranchArea int NULL,
	BranchReportRegionNo char(5) NULL,
	CONSTRAINT pk_JLSOffice PRIMARY KEY CLUSTERED (JLSOfficeKey ASC)

) ON [PRIMARY]

END
