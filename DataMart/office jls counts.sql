/****** Script for SelectTopNRows command from SSMS  ******/
SELECT COUNT(*)
      ,SUM(CASE WHEN [JLSOfficeKey] IS NOT NULL THEN 1 ELSE 0 END) JLSFLAG
	  ,SUM(CASE WHEN OfficeName LIKE '%JLS%' or OfficeName like '%John L%Scott%' then 1 else 0 end) NameJLS
      ,SUM(CASE WHEN [JLSOfficeKey] IS NOT NULL and (OfficeName LIKE '%JLS%' or OfficeName like '%John L%Scott%') then 1 else 0 end) NameJLS

FROM [BI_Reporting].[dim].[Office]


SELECT *
FROM [BI_Reporting].[dim].[Office]
WHERE OfficeName LIKE '%JLS%' or OfficeName like '%John L%Scott%'